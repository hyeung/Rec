2024-03-05 Rec v36r2
===

This version uses
Lbcom [v35r2](../../../../Lbcom/-/tags/v35r2),
LHCb [v55r2](../../../../LHCb/-/tags/v55r2),
Detector [v1r27](../../../../Detector/-/tags/v1r27),
Gaudi [v38r0](../../../../Gaudi/-/tags/v38r0) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) .

This version is released on the `master` branch.
Built relative to Rec [v36r1](/../../tags/v36r1), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- Fix NDOF of tracks with no momentum information in PrKalmanFilter, !3759 (@ausachov)
- Hotfix: Check particle and protoparticle existance in Projection, !3773 (@tfulghes)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Functors | HasT/Velo/UT functors prefer central definition, !3777 (@gunther)
- ~"Flavour tagging" | Remove obsolete sspion decision functor, !3778 (@cprouve)
- Make PrChecker code easier to understand, !3779 (@gunther)
- Adapt to changes in LHCb!4192, !3606 (@rmatev)


### Documentation ~Documentation

- [Docs] Update ElectronShowerDLL docs, !3767 (@alopezhu)

### Other

- ~Decoding ~Monitoring | Updated PLUME decoding in HLT2, !3748 (@fferrari)
- ~Muon | Update for muon alignment, !3484 (@wouter) [#529]
- ~Calo ~Functors | Add HasBremAdded functor and change name of Brem functor, !3775 (@alopezhu)
- ~RICH | RICH Add detector regions to pixel objects, !3753 (@jonrob)
- ~Functors ~"Flavour tagging" | Add new functors for getting flavour tagging decision and mistag rate, !3453 (@yihou)
- ~Monitoring | RecoMon: more correlationhists recomon, configurable for pp vs PbPb, !3615 (@tmombach)
- Update References for: LHCb!4435, Rec!3748, MooreOnline!332 based on lhcb-master-mr/10859, !3790 (@lhcbsoft)
- Fix RICH detdesc-mc-dst refs, !3766 (@lhcbsoft)
- Relation table maker and options for heavy flavour tracks, !3742 (@mveghel)
- UThit position methods, !3559 (@hawu)
- Update References for: Rec!3751, Moore!2925 based on lhcb-master-mr/10585, !3764 (@lhcbsoft)
- Remove duplicates saved in TES location of extra particle before data-taking period, !3751 (@tfulghes)
- Monitor algorithm for event variables, !3555 (@sstahl)

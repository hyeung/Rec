2017-07-04 RecSys v21r5
---
This version uses Gaudi v28r2, LHCb v42r5 and Lbcom v20r5 (and LCG_88 with ROOT 6.08.06)

This version is released on `2017-patches` branch. 


## Change to release notes format
**As from this release, the file `LbcomSys/doc/release.notes` is frozen**  
Instead, there will be a file per release in the new `ReleaseNotes` directory. e.g. this file is called `ReleaseNotes/v21r5.md`

## New features
**[MR !629, !602, !634] Add Yandex PID classifiers to ANNPID**  
See LBPID-27

## Bug fixes
**[MR !642, !641] Fix for use of `DecayTreeFitter` on 2015/2016 turbo**  
On turbo 2015/2016 there are only two states on the track, namely at the beamline and at rich2. For the trajectory approximation used in DTF for Long-Long Ks, this is not enough. Add a state at the end of the Velo.

**[MR !623] ProtoParticle Moni : Fix the conversion of the track type enum to string**

**[MR !615, !614] Calo2MC tool : fix bug that affect the merged-pi0 matching based on `CaloHypo` Linker table**

## Monitoring changes
**[MR !622] Fix Tracking timing plots by adding missing sequences to timing list**

## Changes to tests
**[MR !620] Increase slightly memory threshold of `mergesmallfiles` test failure**

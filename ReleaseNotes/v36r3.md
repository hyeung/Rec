2024-03-18 Rec v36r3
===

This version uses
Lbcom [v35r3](../../../../Lbcom/-/tags/v35r3),
LHCb [v55r3](../../../../LHCb/-/tags/v55r3),
Detector [v1r28](../../../../Detector/-/tags/v1r28),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to Rec [v36r2](/../../tags/v36r2), with the following changes:

### New features ~"new feature"

- Add velo lumi counters to HLT2, !3757 (@dcraik)
- Development of BeamSpotMonitor algorithm, !3736 (@spradlin) [#5]


### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement

- ~Tracking | New ghost prob inference and training infrastructure, !3729 (@mveghel)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Remove not-used non-thread-safe GaudiHistos, !3803 (@peilian)
- Updated VPHitEfficiencyMonitor, !3556 (@mwaterla)


### Documentation ~Documentation


### Other

- ~"PV finding" | Updated VertexCompare, !3631 (@mgiza)
- ~"PV finding" | Extend PatPVFuture to 3D seeding (beamSpot independent), !3612 (@mgiza)
- Remove redundant ConditionAccessorHolder from HitExpectation, !3802 (@freiss) [GAUDI-1023]

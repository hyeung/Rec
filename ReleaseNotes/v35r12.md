2023-07-16 Rec v35r12
===

This version uses
Lbcom [v34r12](../../../../Lbcom/-/tags/v34r12),
LHCb [v54r12](../../../../LHCb/-/tags/v54r12),
Gaudi [v36r14](../../../../Gaudi/-/tags/v36r14),
Detector [v1r16](../../../../Detector/-/tags/v1r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r11](/../../tags/v35r11), with the following changes:

### New features ~"new feature"

- ~VP ~"MC checking" ~Monitoring | Algorithms to estimate hit efficiency of sensors in VELO, !3326 (@peilian)
- ~Functors | Implementation of functors for signal removal from cone, !3441 (@tfulghes)


### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement

- ~Tracking | Allow for low-momentum track reconstruction in PrVeloUT, !3485 (@decianm)
- ~FT ~Monitoring | FT hit efficiency to use DOCA, monitor unbiased residuals, !3371 (@zexu)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | Cleanup of TrackUtils, !3480 (@sponce)
- ~Monitoring | Switch all the PLUME histograms to new-style, !3420 (@vyeroshe)
- ~Monitoring | Dropped unused FastPVMonitor, !3477 (@sponce)
- Remove HighPtJets, ClearDaughters and RelatedInfoTools, !3431 (@pkoppenb)
- Cleanup track tools, dropping unused code that is not functional, !3481 (@sponce)


### Documentation ~Documentation


### Other

- ~Tracking | Thread-safe error counters in TrackMasterFitter, !3494 (@ausachov) [#492]
- ~"PV finding" | Reverting the FittedState initialization in VeloKalmanHelpers, !3464 (@mgiza)
- ~FT | Adding new monitor plots for SciFi alignment, !3469 (@miruizdi)
- ~RICH | RichCKResolutionFitter - Save fit attempt count in return result struct, !3483 (@jonrob)
- Fixed path of FT in derived conditions, !3492 (@sponce)
- Move from ipchi2 to ip for pv association, !3460 (@mveghel)
- Dropped unused code in Tr, !3482 (@sponce)
- Fixed header compilation warnings, !3478 (@sponce)
- Dropped unused code in PrFitParams, !3466 (@sponce)
- Add KS combiner for long-velo tag and probe method, !3442 (@pherrero)

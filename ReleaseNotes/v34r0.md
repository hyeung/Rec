2022-01-27 Rec v34r0
===

This version uses
Lbcom [v33r6](../../../../Lbcom/-/tags/v33r6),
LHCb [v53r6](../../../../LHCb/-/tags/v53r6),
Gaudi [v36r4](../../../../Gaudi/-/tags/v36r4) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Rec [v33r4](/../../tags/v33r4), with the following changes:

### New features ~"new feature"

- ~Tracking | Add SeedMuonBuilder to create SeedMuon probe tracks, !2669 (@rowina)
- ~FT ~Conditions | DD4Hep implementation of FT, !2562 (@lohenry)
- ~Calo ~PID | Track-based electron shower algorithm for improved electron PID, !2319 (@mveghel)
- ~Functors | New track functors, !2645 (@decianm)
- ~Functors | Implementation of a monotone lipschitz network for the MVA functor, !2638 (@lecarus)
- ~Functors ~Tuples | MC association and background category algorithm (MCTruthAndBkgCat) and few related functors, !2677 (@amathad)
- ~"Event model" | Hierarchical SOACollections, !2646 (@ahennequ)
- ~"Event model" ~Utilities | Adds PrConverters to convert between vectors of v1 and v2 RecVertex, !2629 (@spradlin)
- New Event Data Algorithms, !2537 (@anpappas) :star:


### Fixes ~"bug fix" ~workaround

- ~Tracking | Fix bug in computation of residual error for PrFitNode, !2667 (@jkubat)
- ~Tracking | PrForward Maintenance and Monitoring, !2657 (@gunther)
- ~Composites ~Functors | Add v1::Particle compatibility layer to MASSWITHHYPOTHESES Functor, !2583 (@agilman) [#220]
- ~Functors | Bug fix and add functors FOURMOMENTUM, PX, PY, PZ and ENERGY, !2619 (@amathad)
- ~Monitoring | Add scoped_lock to VPTrackMonitor to ensure thread safety when filling nTuple., !2703 (@jcobbled)


### Enhancements ~enhancement

- ~Calo | Change double to float in calo event model, !2636 (@cmarinbe)
- ~Composites | Add vertex constraints to DecayTreeFitterAlg, !2676 (@pkoppenb)
- ~Functors | Refactor(Functors): remove __doc__ hack, no longer needed in py >= 3.9, !2674 (@chasse) [#201]
- ~Functors | Compatibility with lhcb/LHCb!3367 and streamline DistanceCalculator, !2668 (@graven) [#223]
- ~"Event model" | Address #223: prefer ADL over if constexpr for customization, !2631 (@graven) [#223]
- ~"Event model" | SOACollection's proxy features, !2628 (@ahennequ)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | Drop SOA_ACCESSOR from PrForward, !2675 (@gunther)
- ~Tracking ~Monitoring | Unify fit node and fit result code in order to template TrackFitMatchMonitor, !2632 (@jkubat)
- ~Tracking ~Conditions | KalmanFilterTool and PrAddUTHitsTool featuring conditions, !2660 (@gunther)
- ~UT | Modernized HitExpectation classes, !2670 (@sponce)
- ~FT | FTHitExpectation: Fix uninitialised variable warning, !2659 (@jonrob)
- ~Muon | Make Muon code ready for DD4hep integration, !2607 (@sponce)
- ~RICH | RICH Preparations for DD4Hep support, !2648 (@jonrob)
- ~Functors | Small fix in assertion made in CHILD functor, !2664 (@mstahl)
- ~Functors | Prefer template member function of non-template struct, !2658 (@graven)
- ~"Event model" | Rec#223: More unification using ADL, !2653 (@graven) [#223]
- ~"MC checking" | Fix and clean-up TrackResChecker, !2644 (@ausachov)
- ~"MC checking" | Renamed Hits Monitors to Checkers, !2642 (@sklaver)
- ~Monitoring | Use new histograms in TrackMonitor and TrackVertexMonitor, !2565 (@rskuza)
- ~Utilities | Move TMVA_utils to LHCb, !2641 (@gunther) [#206]
- ~Utilities | Template floating-point type in TMV_utils, !2640 (@graven) [#206]
- ~Build | Remove compiler check in RichRecTests cmake, !2687 (@rmatev)
- ~Build | Fix RecDependencies.cmake, !2678 (@clemenci)
- Move away from using CERN-SWTEST, !2685 (@rmatev)
- Make usage of bound tools forward compatible with gaudi/Gaudi!1292, !2663 (@graven)
- Fixed Phoenix code compilation with DD4hep, !2654 (@sponce)
- Remove multiple ambiguous overloaded helper functions from ParticleAccessors.h, !2650 (@amathad)
- Fixed compilation in DD4hep mode (follow up !2630), !2649 (@sponce)
- Prefer Property<vector<Track::Types>> over Property<vector<int> and static_cast, !2630 (@graven)
- Remove v2::Track -> v3::Tracks converter, !2585 (@decianm)


### Move of packages

- Move DaVinciTools to Rec, !2684 (@pkoppenb)
- Move Phys packages, !2637 (@rmatev)


### Changes in Phys since v34r3

- ~"Event model" | Hierarchical SOACollections, Phys!1027 (@ahennequ)
- ~Persistency | Add a check of  PVs to CopyParticlesWithPVRelations, Phys!1025 (@sesen) [#24]
- Fix mass constraint in DecayTreeFitterAlg, Phys!1030 (@pkoppenb)
- ~Tracking ~PID | Update ProbeAndLongMatcher to check for overlap in the Muon detector if it is not stored with the lhcbIDs, Phys!1031 (@rowina)
- ~"Event model" | SOACollection's proxy features, Phys!1018 (@ahennequ)
- ~Jets | Remove Phys/LoKiJets, Phys!1033 (@erodrigu)
- ~Jets | Remove Phys/JetTagging, Phys!1032 (@erodrigu)
- Follow changes in Rec!2653, Phys!1028 (@graven)

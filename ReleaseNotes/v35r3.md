2022-12-19 Rec v35r3
===

This version uses
Lbcom [v34r3](../../../../Lbcom/-/tags/v34r3),
LHCb [v54r3](../../../../LHCb/-/tags/v54r3),
Detector [v1r7](../../../../Detector/-/tags/v1r7),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9) and
LCG [101a_LHCB_7](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Built relative to Rec [v35r2](/../../tags/v35r2), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~Tracking | PrForwardTracking fix: weaken assert now allowing almost sorted values, !3212 (@gunther) [#431]


### Enhancements ~enhancement

- ~Tracking | New parameterisations for HLT2 Long Tracks Pattern recognition, !3150 (@gunther)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Calo | Fix calo index bookkeeping, !3206 (@mveghel)
- ~RICH | Fix rich-dst-reco-pmt-v3.ref.x86_64_v3-dd4hep-opt for recent changes to master, !3215 (@jonrob)
- Fix gcc 12 warnings, !3230 (@clemenci)
- Rename MIN_E and MAX_E functors, remove duplicate functor, !3217 (@tfulghes)
- Remove now redundant `deref_if_ptr`, !3204 (@graven)
- Add missing include, !3213 (@rmatev)


### Documentation ~Documentation


### Other

- ~Tracking | Add FT hit efficiency monitor and hit masking, !3194 (@ldufour)
- ~FT ~Monitoring | FT: Set TAE from filling scheme, !3164 (@jheuel)
- ~Calo | CALO GraphClustering - Add local maxima check on insertion, !3161 (@nuvallsc)
- ~Calo ~Monitoring | Calo-track monitoring, !3203 (@mveghel)
- ~RICH | Correctly fix asymmetry parameter to zero when disabled in RICH CK theta fitter, !3243 (@jonrob)
- ~RICH | Improve fit for asymmetric normal in RICH CK theta resolution fitter, also make it the default, !3239 (@jonrob)
- ~RICH | Rich reconstruction : Update selection cuts and fitter for refractive index calibration task, !3235 (@jonrob)
- ~RICH | RICH mirror alignment: optimize subset of mirror combinations and HLT1 selection criteria (RICH-62, RICH-16), !2572 (@asolomin) [RICH-16,RICH-62]
- ~RICH ~"MC checking" ~Monitoring | Reduce RICH CK theta resolution plot limits for Run III, !3228 (@jonrob)
- ~RICH ~"MC checking" ~Monitoring | RichSIMDPhotonCherenkovAngles - Add for RICH2 R and H type panel resolution plots, !3202 (@jonrob)
- ~Functors | Decay Tree related functors, !3224 (@tfulghes)
- ~Functors | FIND_DECAY and FIND_MCDECAY functor, !3124 (@tfulghes)
- ~Functors | Add corrected mass error functor, !3111 (@masmith) [Moore#476]
- ~Functors | Add track position and momentum functors and change CLOSESTTOBEAM functor to be used in composition, !3214 (@amathad)
- ~Functors ~"MC checking" | Functor to replace MCTupleToolPrompt, !3219 (@aburke)
- ~Tuples | In DecayFinder, factor out parsing of descriptor from finding of particles, !3223 (@amathad)
- Remove printout from ParametrisedScatters, !3245 (@mstahl)
- Introduced bunch crossing type in the json files created by Phoenix Sink, !3237 (@sponce)
- Migrate {Proto,}ParticleMakers, TrackListRefiner, TrackContainerSplitter from LoKi to ThOr, !3231 (@graven)
- Add missing dependency between ConfigurableUsers, !3226 (@clemenci)
- Add operator in PrKFTool to fit Long and LongMuon tracks without UT hits, !3209 (@pherrero)
- Update References for: Rec!3228, Moore!1924 based on lhcb-master-mr/6426, !3232 (@lhcbsoft)
- Add 1D and 2D plots for the position and slope of the tracks, !3220 (@lohenry)
- Fix internal dependencies of FunctorDatahandleTest, !3218 (@clemenci)
- Update References for: LHCb!3881, Rec!3202 based on lhcb-master-mr/6350, !3210 (@lhcbsoft)
- Change input type of TrackBestTrackCreator to Track::Range, !3207 (@wouter)

2023-09-11 Rec v35r18
===

This version uses Lbcom [v34r16](../../../../Lbcom/-/tags/v34r16),
LHCb [v54r16](../../../../LHCb/-/tags/v54r17),
Detector [v1r19](../../../../Detector/-/tags/v1r19),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `` branch.
Built relative to Rec [v35r17](/../../tags/v35r17), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~Calo ~Composites | Fix of no hcal energy for neutral protos, !3566 (@mveghel)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~RICH | Add RICH QMT test detdesc-mc-dst-fake-mchits, !3571 (@jonrob)


### Documentation ~Documentation


### Other

- ~Tracking ~FT | PrForwardTracking: Downgrade 'no FT hits' error to counter, !3570 (@jonrob)
- ~RICH | RICH 4D Reco: Add new algorithm to add Tk origin 4D vertex using MC to existing reco segments, !3567 (@jonrob)
- PrDownTrack: Default initialise members + small cleanup, !3569 (@jonrob)

2023-07-04 Rec v35r11
===

This version uses
Lbcom [v34r11](../../../../Lbcom/-/tags/v34r11),
LHCb [v54r11](../../../../LHCb/-/tags/v54r11),
Detector [v1r15](../../../../Detector/-/tags/v1r15),
Gaudi [v36r14](../../../../Gaudi/-/tags/v36r14) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r10](/../../tags/v35r10), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~UT | Fixes for UT DD4hep, !3458 (@sponce)
- ~Build | Generalize check for sanitizer binary tag, !3461 (@rmatev)
- Dropped unused code around FitParams, !3479 (@sponce)
- DistanceCalculator: avoid use of _Warning and _Error as those are NOT thread safe, !3474 (@graven)
- Remove non-thread-safe unused code, !3473 (@cmarinbe)


### Documentation ~Documentation


### Other

- ~RICH | RichCKResolutionFitter: Extend exception information with fit function details, !3446 (@jonrob)

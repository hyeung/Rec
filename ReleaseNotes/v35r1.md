2022-10-26 Rec v35r1
===

This version uses
Lbcom [v34r1](../../../../Lbcom/-/tags/v34r1),
LHCb [v54r1](../../../../LHCb/-/tags/v54r1),
Detector [v1r5](../../../../Detector/-/tags/v1r5),
Gaudi [v36r8](../../../../Gaudi/-/tags/v36r8) and
This version uses LCG [101a](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Built relative to Rec [v35r0](/../../tags/v35r0), with the following changes:


### Fixes ~"bug fix" ~workaround

- ~Decoding ~Tracking | Fix Retina decoding in VeloClusterTrackingSIMD, !3156 (@gbassi) [#413]
- ~Tracking | PrForwardTracking correct check for empty hits container, !3147 (@gunther)
- ~"PV finding" ~Monitoring | Fix left/right tracks defintion in PV half monitoring, !3149 (@freiss)


### Enhancements ~enhancement

- ~Monitoring | Plume monitoring improvements, !3135 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Muon | MuonIDHltAlg - Standardise log implementation in all places to fast version, !3145 (@jonrob)
- ~RICH | Add RICH QM test for 2022 data pixel reco, !3151 (@jonrob)
- ~Functors | Add more includes to JIT_include.h, !3045 (@sstahl)
- ~Luminosity | Remove Rec/Phys/Luminosity and Rec/Rec/LumiAlgs from master, !3108 (@balagura) [#395]
- ~Core | Adapted code to drop of DetDesc compilation in DD4hep mode, !3126 (@sponce)


### Other

- ~Calo ~Functors ~Monitoring | Add functor for NeutralE19, !3146 (@cmarinbe)
- Update References for: LHCb!3798, Rec!3135 based on lhcb-master-mr/6027, !3162 (@lhcbsoft)
- Fix a bug in functor grammar, !3139 (@amathad)
- Add example RICH options for 2022 real data, !3148 (@jonrob)

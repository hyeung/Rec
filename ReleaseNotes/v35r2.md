2022-11-23 Rec v35r2
===

This version uses
Lbcom [v34r2](../../../../Lbcom/-/tags/v34r2),
LHCb [v54r2](../../../../LHCb/-/tags/v54r2),
Detector [v1r6](../../../../Detector/-/tags/v1r6),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9) and
LCG [101a_LHCB_7](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Built relative to Rec [v35r1](/../../tags/v35r1), with the following changes:

### New features ~"new feature"

- ~Tracking | Add Muon hits to PrKM, !3036 (@pherrero) :star:


### Fixes ~"bug fix" ~workaround

- ~Tracking | Fix TrBACKWARD predicate and RecSummaryAlg counter, !3197 (@ausachov)
- ~Tracking | PrForwardTracking fix worst hit finding when hits perfectly align, !3172 (@gunther)
- ~Monitoring | Fix histogram names and adapt default ranges for commissioning in TrackPV2HalfMonitor, !3183 (@sklaver)
- Clear the references to tagging particles in cloned FlavourTags, !3195 (@graven) [#428]
- Resolve ppSvc crash in DecayTreeFit, !3157 (@pkoppenb)


### Enhancements ~enhancement

- ~Tracking | PrParameterisationData fit extrapolated positions for coefficients, !3132 (@gunther)
- ~FT ~Monitoring | FT: increase binning range for nClusters histogram, !3158 (@jheuel)
- ~Monitoring | Add a coarse version of the ADC vs BCID Plume histogram, !3171 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~FT | PrSciFiHitsMonitor - Add missing includes, array range check, !3165 (@jonrob)
- ~FT | Centralise FT constants in one file in Detector and name most constants, !3154 (@lohenry) [LHCBSCIFI-186]
- ~RICH | Fix RICH QM test refs for dd4hep, !3187 (@jonrob)
- ~Functors | Change DecReportsFunctor to not throw, !3170 (@sstahl)
- Remove LoKiAlgoMC, MCParticleMaker, !3200 (@pkoppenb)
- Dropped getDet in Muon and Phys directories, !3179 (@sponce)


### Documentation ~Documentation

- FlavourTagging: do not use template specialization when overloading is intended, !3190 (@graven)

### Other

- ~Tracking | PrKF make minimum chi2 for outlier nodes configurable, !3176 (@gunther) [Moore#489]
- ~Tracking | General tool for PrTracking debugging, !3169 (@gunther)
- ~Tracking ~VP ~Calo | Introduce VeloBackward and remove Calo track type, !3122 (@ausachov) [LHCb#256]
- ~Tracking ~FT | PrForwardTracking explicitly assert presence of sentinels in SciFi hit container, !3168 (@gunther)
- ~Tracking ~FT ~Monitoring | Add globalSiPM index to FTTrackMonitor, !3175 (@gunther) [#418]
- ~Tracking ~"MC checking" | Use the general PrDebugTool for PrMatchNN, !3182 (@sesen) [#419]
- ~Tracking ~Monitoring | Monitoring for PrSciFiHits, !3159 (@gunther)
- ~Tracking ~Monitoring | Update ranges of histograms in TrackMonitor, !3140 (@decianm)
- ~Calo | SelectiveElectronMatchAlg - Use counter buffer in loop, !3178 (@jonrob)
- ~Calo | Change path of conditions in SelectiveElectronAlg.cpp for DD4HEP, !3129 (@chenjia)
- ~Calo ~Conditions | Fix CALO reconstruction condition paths for DD4HEP, !3181 (@chenjia)
- ~RICH ~Monitoring | RichTrackRadiatorMaterial - Disable TS completely in dd4hep builds, !3189 (@jonrob)
- ~Monitoring | Tweaks for Track Monitoring, !3193 (@decianm)
- ~Tuples | New PID Substitution DaVinci Tool, !3128 (@jzhuo) [#305,#309]
- VeloClusterTrackingSIMD - Make velo parameters configurable, !3205 (@ahennequ)
- Add one more state as LastMeasurement in Standalone muon tracks, !3201 (@peilian)
- Suppress, but count warning messages, !3198 (@mstahl)
- Add algorithms for 1D relation table to particle ranges, !2773 (@tnanut)
- Remove whitespace at end of histogram title, !3191 (@clemenci)
- Add monitors of correlations of velo, scifi and muon hits, !3177 (@sstahl)
- Getting rid of getDet calls, !3173 (@sponce)
- Dropped direct usage of Gaudi::Functional algorithms, !3167 (@sponce)
- Fix algorithm for selection of Flavour Tagging particles, !3186 (@cprouve)
- Update References for: LHCb!3822, Rec!3152, Moore!1828, Alignment!321, MooreOnline!169, Analysis!926, DaVinci!776, Boole!427 based on lhcb-master-mr/6222, !3185 (@lhcbsoft)
- Prepare Rec v35r1p1, !3184 (@rmatev)
- Follow LHCb!3822, !3152 (@sesen)
- Another update of monitors and selectors for alignment, !3117 (@wouter)
- Dropped unused class PrMuonStubAssociator, !3153 (@sponce)

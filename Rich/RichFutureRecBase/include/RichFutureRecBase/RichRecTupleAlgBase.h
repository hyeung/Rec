/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecTupleAlgBase.h
 *
 *  Header file for RICH reconstruction monitor algorithm base class :
 *  RichRecTupleAlgBase
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2005/01/13
 */
//-----------------------------------------------------------------------------

#pragma once

// base classes
#include "RichFutureKernel/RichTupleAlgBase.h"
#include "RichFutureRecBase/RichRecBase.h"

namespace Rich::Future::Rec {

  //-----------------------------------------------------------------------------
  /** @class TupleAlgBase RichRecTupleAlgBase.h RichRecBase/RichRecTupleAlgBase.h
   *
   *  Abstract base class for RICH reconstruction algorithms providing
   *  some basic functionality (identical to RichRecAlgBase) but with additional
   *  histogram and ntuple functionality provided by GaudiTupleAlg base class.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2005/01/13
   */
  //-----------------------------------------------------------------------------

  class TupleAlgBase : public Rich::Future::TupleAlgBase,
                       public Rich::Future::Rec::CommonBase<Rich::Future::TupleAlgBase> {

  public:
    /// Standard constructor
    TupleAlgBase( const std::string& name, ISvcLocator* pSvcLocator );

    /** Initialization of the algorithm after creation
     *
     * @return The status of the initialization
     * @retval StatusCode::SUCCESS Initialization was successful
     * @retval StatusCode::FAILURE Initialization failed
     */
    StatusCode initialize() override;

    /** Finalization of the algorithm before deletion
     *
     * @return The status of the finalization
     * @retval StatusCode::SUCCESS Finalization was successful
     * @retval StatusCode::FAILURE Finalization failed
     */
    StatusCode finalize() override;
  };

} // namespace Rich::Future::Rec

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecAlgBase.h
 *
 *  Header file for reconstruction algorithm base class : Rich::Rec::AlgBase
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   05/04/2002
 */
//-----------------------------------------------------------------------------

#pragma once

// base classes
#include "RichFutureKernel/RichAlgBase.h"
#include "RichFutureRecBase/RichRecBase.h"

namespace Rich::Future::Rec {

  //-----------------------------------------------------------------------------
  /** @class AlgBase RichRecAlgBase.h RichRecBase/RichRecAlgBase.h
   *
   *  Abstract base class for RICH reconstruction algorithms providing
   *  some basic functionality.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   05/04/2002
   */
  //-----------------------------------------------------------------------------

  template <typename PBASE = FixTESPath<Gaudi::Algorithm>>
  class AlgBase : public Rich::Future::AlgBase<PBASE>,
                  public Rich::Future::Rec::CommonBase<Rich::Future::AlgBase<PBASE>> {

  public:
    /// Standard constructor
    AlgBase( const std::string& name, ISvcLocator* pSvcLocator )
        : Rich::Future::AlgBase<PBASE>( name, pSvcLocator ) //
        , Rich::Future::Rec::CommonBase<Rich::Future::AlgBase<PBASE>>( this ) {}

  public:
    /** Initialization of the algorithm after creation
     *
     * @return The status of the initialization
     * @retval StatusCode::SUCCESS Initialization was successful
     * @retval StatusCode::FAILURE Initialization failed
     */
    StatusCode initialize() override {
      // Initialise base class
      const auto sc = Rich::Future::AlgBase<PBASE>::initialize();
      // Common initialisation
      return ( sc.isSuccess() ? this->initialiseRichReco() : sc );
    }

    /** Finalization of the algorithm before deletion
     *
     * @return The status of the finalization
     * @retval StatusCode::SUCCESS Finalization was successful
     * @retval StatusCode::FAILURE Finalization failed
     */
    StatusCode finalize() override {
      // Common finalisation
      const auto sc = this->finaliseRichReco();
      // Finalize base class
      return ( sc.isSuccess() ? Rich::Future::AlgBase<PBASE>::finalize() : sc );
    }
  };

} // namespace Rich::Future::Rec

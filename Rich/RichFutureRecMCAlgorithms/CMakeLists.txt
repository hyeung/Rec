###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureRecMCAlgorithms
---------------------------------
#]=======================================================================]

gaudi_add_module(RichFutureRecMCAlgorithms
    SOURCES
        src/RichTrSegMakerFromMCRichTracks.cpp
        src/RichSegmentAddTimeFromMC.cpp
        src/RichPixelUseMCInfo.cpp
        src/RichPhotonUseMCInfo.cpp
        src/RichTrackCKAnglesUseMCInfo.cpp
        src/RichMCTrackFilter.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::LHCbKernel
        LHCb::RichDetectorsLib
        LHCb::RichDetLib
        LHCb::RichFutureKernel
        LHCb::RichFutureUtils
        LHCb::RichUtils
        LHCb::RichFutureMCUtilsLib
        LHCb::TrackEvent
        LHCb::LinkerEvent
        LHCb::MCEvent
        LHCb::MCInterfaces
        Rec::RichFutureRecBase
        Rec::RichFutureRecEvent
        Rec::PrKernel
)

# Fixes for GCC7.
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "7.0")
    target_compile_options(RichFutureRecTrackAlgorithms PRIVATE -faligned-new)
endif()

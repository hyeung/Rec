/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecRelations.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"

// STL
#include <mutex>

namespace Rich::Future::Rec::Moni {

  /** @class PhotonSIMDEfficiency RichPhotonSIMDEfficiency.h
   *
   *  Monitors the per SIMD photon efficiency factor.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class PhotonSIMDEfficiency final
      : public LHCb::Algorithm::Consumer<void( const SIMDCherenkovPhoton::Vector&,        //
                                               const Relations::PhotonToParents::Vector&, //
                                               const LHCb::RichTrackSegment::Vector& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PhotonSIMDEfficiency( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, //
                    {KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default}} ) {}

  public:
    /// Functional operator
    void operator()( const SIMDCherenkovPhoton::Vector&        photons,      //
                     const Relations::PhotonToParents::Vector& photToSegPix, //
                     const LHCb::RichTrackSegment::Vector&     segments ) const override {

      // the lock
      std::lock_guard lock( m_updateLock );

      // loop over photon relations
      for ( const auto& iphot : photToSegPix ) {

        // get the track segment
        assert( iphot.segmentIndex() < segments.size() );
        const auto& seg = segments[iphot.segmentIndex()];

        // which radiator
        const auto rad = seg.radiator();
        if ( !radiatorIsActive( rad ) ) continue;

        // get the photon object
        assert( iphot.photonIndex() < photons.size() );
        const auto& phot = photons[iphot.photonIndex()];

        // count the valid scalar photons the SIMD photon represents
        fillHisto( h_simdEff[rad], phot.validityMask().count() );
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool       ok    = true;
      const auto NSIMD = SIMDCherenkovPhoton::SIMDFP::Size;
      // Loop over radiators
      for ( const auto rad : activeRadiators() ) {
        ok &= saveAndCheck( h_simdEff[rad], richHisto1D( HID( "simdEff", rad ),                  //
                                                         "# valid scalar photons / SIMD photon", //
                                                         -0.5, NSIMD + 0.5, NSIMD + 1 ) );
      }
      return StatusCode{ok};
    }

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;
    /// CK theta histograms
    RadiatorArray<AIDA::IHistogram1D*> h_simdEff = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( PhotonSIMDEfficiency )

} // namespace Rich::Future::Rec::Moni

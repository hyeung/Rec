/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"

// STD
#include <mutex>

namespace Rich::Future::Rec::Moni {

  /** @class PixelClusters RichPixelClusters.h
   *
   *  Monitors the RICH pixel clusters.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class PixelClusters final : public LHCb::Algorithm::Consumer<void( const Rich::PDPixelCluster::Vector& ), //
                                                               Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PixelClusters( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, //
                    KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

  public:
    /// Functional operator
    void operator()( const Rich::PDPixelCluster::Vector& clusters ) const override {
      // the lock
      std::lock_guard lock( m_updateLock );
      // loop over clusters
      for ( const auto& cluster : clusters ) {
        if ( cluster.empty() ) {
          ++m_emptyClusWarn;
        } else {
          const auto rich = cluster.rich();
          if ( richIsActive( rich ) ) { fillHisto( h_clusterSize[rich], cluster.size() ); }
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      for ( const auto rich : activeDetectors() ) {
        ok &= saveAndCheck( h_clusterSize[rich], richHisto1D( Rich::HistogramID( "clusterSize", rich ),
                                                              "Pixel Cluster Sizes", -0.5, 64.5, 65 ) );
      }
      return StatusCode{ok};
    }

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;

    /// empty cluster warning
    mutable WarningCounter m_emptyClusWarn{this, "Empty relation table!"};

    /// Cluster size histogram
    DetectorArray<AIDA::IHistogram1D*> h_clusterSize = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( PixelClusters )

} // namespace Rich::Future::Rec::Moni

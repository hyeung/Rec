/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Kernel
#include "Kernel/RichRadiatorType.h"

// RichUtils
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec::Moni {

  /** @class SIMDRecoStats RichSIMDRecoStats.h
   *
   *  Basic monitoring of the RICH reconstruction.
   *
   *  @author Chris Jones
   *  @date   2016-11-07
   */

  class SIMDRecoStats final : public LHCb::Algorithm::Consumer<void( const SIMDPixelSummaries&,                 //
                                                                     const LHCb::RichTrackSegment::Vector&,     //
                                                                     const Relations::TrackToSegments::Vector&, //
                                                                     const SIMDCherenkovPhoton::Vector& ),
                                                               Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    SIMDRecoStats( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default}} ) {
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const SIMDPixelSummaries&                 pixels,   //
                     const LHCb::RichTrackSegment::Vector&     segments, //
                     const Relations::TrackToSegments::Vector& tkToSegs, //
                     const SIMDCherenkovPhoton::Vector&        photons ) const override {

      // local counts
      DetectorArray<unsigned long long> photCount{{}};
      DetectorArray<unsigned long long> segCount{{}};
      unsigned long long                tkCount{0};
      auto                              richEff = m_richEff.buffer();

      // Loop over the track data
      for ( const auto& rels : tkToSegs ) {
        // Was this track selected. i.e. does it have at least one segment
        const bool selected = !rels.segmentIndices.empty();
        // track selection efficiency
        richEff += selected;
        // is selected ?
        if ( selected ) {
          // count selected tracks
          ++tkCount;
          // loop over segments for this track
          for ( const auto& iSeg : rels.segmentIndices ) {
            // get the segment
            const auto& seg = segments[iSeg];
            // count segments
            ++segCount[seg.rich()];
          }
        }
      }

      // Loop over photons
      for ( const auto& phot : photons ) {
        // count scalar photons per rich
        photCount[phot.rich()] += phot.validityMask().count();
      }

      // update counts

      m_nTracks += tkCount;
      for ( auto rich : {Rich::Rich1, Rich::Rich2} ) {
        // Only count pixels when 4D reco is enabled, as in 3D-only all hits are anyway
        // used. Work around to avoid unnecessary ref diffs to run3 (3D) tests.
        if ( m_enable4D[rich] ) { m_nPixs[rich] += pixels.nHitsScalar( rich ); }
        m_nSegs[rich] += segCount[rich];
        m_nPhots[rich] += photCount[rich];
      }

      // _ri_debug << "Photons " << photCount << endmsg;
    }

  private:
    // properties

    /// Enabled 4D reconstruction
    Gaudi::Property<DetectorArray<bool>> m_enable4D{this, "Enable4D", {false, false}};

  private:
    // counters

    /// # tracks per event
    mutable Gaudi::Accumulators::StatCounter<> m_nTracks{this, "# Selected Tracks"};

    /// RICH selection efficiency
    mutable Gaudi::Accumulators::BinomialCounter<> m_richEff{this, "RICH selection efficiency"};

    /// # Pixels per event
    mutable DetectorArray<Gaudi::Accumulators::StatCounter<>> m_nPixs{
        {{this, "# Rich1Gas Pixels"}, {this, "# Rich2Gas Pixels"}}};

    /// # Segments per event
    mutable DetectorArray<Gaudi::Accumulators::StatCounter<>> m_nSegs{
        {{this, "# Rich1Gas Segments"}, {this, "# Rich2Gas Segments"}}};

    /// # Photons per event
    mutable DetectorArray<Gaudi::Accumulators::StatCounter<>> m_nPhots{
        {{this, "# Rich1Gas Photons"}, {this, "# Rich2Gas Photons"}}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( SIMDRecoStats )

} // namespace Rich::Future::Rec::Moni

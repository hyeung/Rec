/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// boost
#include "boost/format.hpp"

// STD
#include <algorithm>
#include <cassert>
#include <mutex>
#include <sstream>
#include <utility>
#include <vector>

namespace Rich::Future::Rec::MC::Moni {

  /** @class CKResParameterisation RichMCCKResParameterisation.h
   *
   *  Computes parameterisation of CK resolution as function of P and expected CK Theta.
   *
   *  @author Chris Jones
   *  @date   2020-10-25
   */

  class CKResParameterisation final
      : public LHCb::Algorithm::Consumer<void( const Summary::Track::Vector&,                   //
                                               const LHCb::Track::Range&,                       //
                                               const SIMDPixelSummaries&,                       //
                                               const Rich::PDPixelCluster::Vector&,             //
                                               const Relations::PhotonToParents::Vector&,       //
                                               const LHCb::RichTrackSegment::Vector&,           //
                                               const CherenkovAngles::Vector&,                  //
                                               const SIMDCherenkovPhoton::Vector&,              //
                                               const SIMDPhotonSignals::Vector&,                //
                                               const Rich::Future::MC::Relations::TkToMCPRels&, //
                                               const LHCb::MCRichDigitSummarys& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    CKResParameterisation( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"PhotonSignalsLocation", SIMDPhotonSignalsLocation::Default},
                     KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                     KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {
      setProperty( "NBins1DHistos", 150 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Range&                       tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const SIMDPhotonSignals::Vector&                photSignals,   ///< photon signals
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

    /// Finalize
    StatusCode finalize() override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Get CK theta bin
    template <typename TYPE>
    inline std::size_t ckTBin( const TYPE               ckTheta, //
                               const Rich::RadiatorType rad ) const {
      const auto ckRange = m_ckThetaMax[rad] - m_ckThetaMin[rad];
      const auto inc     = ckRange / m_nCKTBins[rad];
      const auto bin     = ( ckTheta <= m_ckThetaMin[rad] ? 0 :                       //
                             ckTheta >= m_ckThetaMax[rad] ? m_nCKTBins[rad] - 1 : //
                                 std::size_t( ( ckTheta - m_ckThetaMin[rad] ) / inc ) );
      assert( bin < m_nCKTBins[rad] );
      return std::min( bin, m_nCKTBins[rad] - 1 );
    }

    /// Get CK theta bin limits
    inline std::pair<float, float> ckBinLimits( const std::size_t        bin, //
                                                const Rich::RadiatorType rad ) const {
      assert( bin < m_nCKTBins[rad] );
      const auto ckRange = m_ckThetaMax[rad] - m_ckThetaMin[rad];
      const auto inc     = ckRange / m_nCKTBins[rad];
      return {m_ckThetaMin[rad] + ( inc * bin ), //
              m_ckThetaMin[rad] + ( inc * ( 1 + bin ) )};
    }

    /// Is CK theta value inside the bin range
    template <typename TYPE>
    inline bool inCKTRange( const TYPE               ckTheta, //
                            const Rich::RadiatorType rad ) const {
      return ( ckTheta >= m_ckThetaMin[rad] && ckTheta <= m_ckThetaMax[rad] );
    }

  private:
    /// Get momentum bin
    template <typename TYPE>
    inline std::size_t pBin( const TYPE               p, //
                             const Rich::RadiatorType rad ) const {
      const auto pRange = m_maxP[rad] - m_minP[rad];
      const auto inc    = pRange / m_nCKTBins[rad];
      const auto bin    = ( p <= m_minP[rad] ? 0 :                       //
                             p >= m_maxP[rad] ? m_nCKTBins[rad] - 1 : //
                                 std::size_t( ( p - m_minP[rad] ) / inc ) );
      assert( bin < m_nCKTBins[rad] );
      return std::min( bin, m_nCKTBins[rad] - 1 );
    }

    /// Get momentum bin limits
    inline std::pair<float, float> pBinLimits( const std::size_t        bin, //
                                               const Rich::RadiatorType rad ) const {
      assert( bin < m_nCKTBins[rad] );
      const auto pRange = m_maxP[rad] - m_minP[rad];
      const auto inc    = pRange / m_nCKTBins[rad];
      return {m_minP[rad] + ( inc * bin ), //
              m_minP[rad] + ( inc * ( 1 + bin ) )};
    }

    /// Is momentum value inside the bin range
    template <typename TYPE>
    inline bool inPRange( const TYPE               p, //
                          const Rich::RadiatorType rad ) const {
      return ( p >= m_minP[rad] && p <= m_maxP[rad] );
    }

  private:
    // properties

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.010f, 0.010f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.056f, 0.033f}};

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<RadiatorArray<float>> m_ckResRange{this, "CKResHistoRange", {0.025f, 0.008f, 0.0050f}};

    /// Min momentum by radiator (MeV/c)
    Gaudi::Property<RadiatorArray<double>> m_minP{
        this, "MinP", {0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV}};

    /// Max momentum by radiator (MeV/c)
    Gaudi::Property<RadiatorArray<double>> m_maxP{
        this, "MaxP", {120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV}};

    /// Number of bins for binned CK theta plots
    Gaudi::Property<RadiatorArray<std::size_t>> m_nCKTBins{this, "NCKThetaBins", {200, 200, 200}};

    /// Maximum number of MCParticles associated to a given track
    Gaudi::Property<RadiatorArray<std::size_t>> m_nMaxMCPsAssoc{this, "MaxMCPsAssoc", {1, 1, 1}};

    /// The minimum cut value for photon probability
    Gaudi::Property<RadiatorArray<float>> m_minPhotonProb{
        this, "MinPhotonProbability", {1e-8f, 1e-9f, 1e-8f}, "The minimum allowed photon probability values"};

  private:
    // cached data

    /// mutex lock
    mutable std::mutex m_updateLock;

    // plots for parameterised resolutions
    using ParamCKRes = RadiatorArray<ParticleArray<std::vector<AIDA::IHistogram1D*>>>;
    // true
    ParamCKRes h_ckResTrueCKBins   = {{}};
    ParamCKRes h_ckThetaTrueCKBins = {{}};
    ParamCKRes h_ckResTruePBins    = {{}};
    ParamCKRes h_ckThetaTruePBins  = {{}};
    // fake
    ParamCKRes h_ckResFakeCKBins   = {{}};
    ParamCKRes h_ckThetaFakeCKBins = {{}};
    ParamCKRes h_ckResFakePBins    = {{}};
    ParamCKRes h_ckThetaFakePBins  = {{}};
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode CKResParameterisation::prebookHistograms() {

  bool ok = true;

  // Loop over radiators
  for ( const auto rad : activeRadiators() ) {

    assert( 0 < m_minP[rad] );
    assert( 0 < m_maxP[rad] );
    assert( m_minP[rad] < m_maxP[rad] );

    // loop over particles
    for ( const auto pid : Rich::particles() ) {
      if ( pid == Rich::BelowThreshold ) { continue; }

      // loop over CK bins
      assert( m_nCKTBins[rad] > 0 );
      h_ckResTrueCKBins[rad][pid].resize( m_nCKTBins[rad], nullptr );
      h_ckResFakeCKBins[rad][pid].resize( m_nCKTBins[rad], nullptr );
      h_ckThetaTrueCKBins[rad][pid].resize( m_nCKTBins[rad], nullptr );
      h_ckThetaFakeCKBins[rad][pid].resize( m_nCKTBins[rad], nullptr );
      h_ckResTruePBins[rad][pid].resize( m_nCKTBins[rad], nullptr );
      h_ckResFakePBins[rad][pid].resize( m_nCKTBins[rad], nullptr );
      h_ckThetaTruePBins[rad][pid].resize( m_nCKTBins[rad], nullptr );
      h_ckThetaFakePBins[rad][pid].resize( m_nCKTBins[rad], nullptr );
      for ( std::size_t bin = 0; bin < m_nCKTBins[rad]; ++bin ) {
        const auto binS = "bin" + std::to_string( bin + 1 ); // number from 1 for human readable bin labels
        // CK bins
        {
          const auto [ckmin, ckmax] = ckBinLimits( bin, rad );
          std::ostringstream ckS;
          boost::format      fS( "%10.6f" );
          ckS << " - " << fS % ckmin << "<CKThetaExp(rad)<" << fS % ckmax;
          ok &= saveAndCheck( h_ckResTrueCKBins[rad][pid][bin],
                              richHisto1D( HID( "CKBins/ckResTrue/" + binS, rad, pid ),                 //
                                           "Rec-Exp CKTheta - MC true photons - True Type" + ckS.str(), //
                                           -m_ckResRange[rad], m_ckResRange[rad], 2 * nBins1D(),        //
                                           "delta(Cherenkov Theta) / rad" ) );
          ok &= saveAndCheck( h_ckResFakeCKBins[rad][pid][bin],
                              richHisto1D( HID( "CKBins/ckResFake/" + binS, rad, pid ),                 //
                                           "Rec-Exp CKTheta - MC fake photons - True Type" + ckS.str(), //
                                           -m_ckResRange[rad], m_ckResRange[rad], 2 * nBins1D(),        //
                                           "delta(Cherenkov Theta) / rad" ) );
          ok &= saveAndCheck( h_ckThetaTrueCKBins[rad][pid][bin],                                 //
                              richHisto1D( HID( "CKBins/ckThetaTrue/" + binS, rad, pid ),         //
                                           "Reconstructed CKTheta - MC true photons" + ckS.str(), //
                                           m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),       //
                                           "Cherenkov Theta / rad" ) );
          ok &= saveAndCheck( h_ckThetaFakeCKBins[rad][pid][bin],                                 //
                              richHisto1D( HID( "CKBins/ckThetaFake/" + binS, rad, pid ),         //
                                           "Reconstructed CKTheta - MC fake photons" + ckS.str(), //
                                           m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),       //
                                           "Cherenkov Theta / rad" ) );
        }
        // momentum bins
        {
          const auto [pmin, pmax] = pBinLimits( bin, rad );
          std::ostringstream pS;
          boost::format      fS( "%8.4f" );
          pS << " - " << fS % ( pmin / Gaudi::Units::GeV ) << "<P(GeV)<" << fS % ( pmax / Gaudi::Units::GeV );
          ok &= saveAndCheck( h_ckResTruePBins[rad][pid][bin],
                              richHisto1D( HID( "PBins/ckResTrue/" + binS, rad, pid ),                 //
                                           "Rec-Exp CKTheta - MC true photons - True Type" + pS.str(), //
                                           -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),           //
                                           "delta(Cherenkov Theta) / rad" ) );
          ok &= saveAndCheck( h_ckResFakePBins[rad][pid][bin],
                              richHisto1D( HID( "PBins/ckResFake/" + binS, rad, pid ),                 //
                                           "Rec-Exp CKTheta - MC fake photons - True Type" + pS.str(), //
                                           -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),           //
                                           "delta(Cherenkov Theta) / rad" ) );
          ok &= saveAndCheck( h_ckThetaTruePBins[rad][pid][bin],                                 //
                              richHisto1D( HID( "PBins/ckThetaTrue/" + binS, rad, pid ),         //
                                           "Reconstructed CKTheta - MC true photons" + pS.str(), //
                                           m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),      //
                                           "Cherenkov Theta / rad" ) );
          ok &= saveAndCheck( h_ckThetaFakePBins[rad][pid][bin],                                 //
                              richHisto1D( HID( "PBins/ckThetaFake/" + binS, rad, pid ),         //
                                           "Reconstructed CKTheta - MC fake photons" + pS.str(), //
                                           m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),      //
                                           "Cherenkov Theta / rad" ) );
        }
      }
    }
  }

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void CKResParameterisation::operator()( const Summary::Track::Vector&                   sumTracks,     //
                                        const LHCb::Track::Range&                       tracks,        //
                                        const SIMDPixelSummaries&                       pixels,        //
                                        const Rich::PDPixelCluster::Vector&             clusters,      //
                                        const Relations::PhotonToParents::Vector&       photToSegPix,  //
                                        const LHCb::RichTrackSegment::Vector&           segments,      //
                                        const CherenkovAngles::Vector&                  expTkCKThetas, //
                                        const SIMDCherenkovPhoton::Vector&              photons,       //
                                        const SIMDPhotonSignals::Vector&                photSignals,   //
                                        const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                                        const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  Helper mcHelper( tkrels, digitSums );

  // the lock
  std::lock_guard lock( m_updateLock );

  // loop over the track info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      const auto& sigs = photSignals[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !radiatorIsActive( rad ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();
      const auto inPR = inPRange( pTot, rad );

      // Get the MCParticles for this track
      const auto mcPs = mcHelper.mcParticles( *tk, false, 0.7 );
      // require at least one MCP
      if ( mcPs.empty() ) { continue; }
      // max number of associations
      if ( mcPs.size() > m_nMaxMCPsAssoc[rad] ) { continue; }

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) { continue; }

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];
        const auto inCKR    = inCKTRange( thetaRec, rad );

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType( mcP );

          // If MC type not known, skip
          if ( Rich::Unknown == pid ) { continue; }

          // expected CK theta ( for true type )
          const auto thetaExp = expCKangles[pid];
          const auto inCKExpR = inCKTRange( thetaExp, rad );

          // in range ?
          if ( inCKExpR && inCKR && inPR ) {

            // Check photon signal prob is above minimum value for this radiator
            if ( sigs[pid][i] < m_minPhotonProb[rad] ) { continue; }

            // do we have an true MC Cherenkov photon
            const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );

            // true Cherenkov signal ?
            const auto trueCKSig = std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end();

            // delta theta
            const auto deltaTheta = thetaRec - thetaExp;

            // bins
            const auto ckbin = ckTBin( thetaExp, rad );
            const auto pbin  = pBin( pTot, rad );

            // fill some plots
            if ( !trueCKSig ) {
              fillHisto( h_ckResFakeCKBins[rad][pid][ckbin], deltaTheta, mcPW );
              fillHisto( h_ckThetaFakeCKBins[rad][pid][ckbin], thetaRec, mcPW );
              fillHisto( h_ckResFakePBins[rad][pid][pbin], deltaTheta, mcPW );
              fillHisto( h_ckThetaFakePBins[rad][pid][pbin], thetaRec, mcPW );
            } else {
              fillHisto( h_ckResTrueCKBins[rad][pid][ckbin], deltaTheta, mcPW );
              fillHisto( h_ckThetaTrueCKBins[rad][pid][ckbin], thetaRec, mcPW );
              fillHisto( h_ckResTruePBins[rad][pid][pbin], deltaTheta, mcPW );
              fillHisto( h_ckThetaTruePBins[rad][pid][pbin], thetaRec, mcPW );
            }

          } // in range

        } // loop over associated MCPs

      } // SIMD scalar loop
    }   // photon loop
  }     // track loop
}

//-----------------------------------------------------------------------------

StatusCode CKResParameterisation::finalize() {

  if ( msgLevel( MSG::DEBUG ) ) {
    // Loop over radiators
    for ( const auto rad : activeRadiators() ) {
      // loop over particles
      for ( const auto pid : Rich::particles() ) {
        if ( pid == Rich::BelowThreshold ) { continue; }

        // temporary storage for parameterised CK res
        using Params = std::vector<std::pair<float, float>>;
        Params paramResP, paramResCK;
        paramResP.reserve( m_nCKTBins[rad] );
        paramResCK.reserve( m_nCKTBins[rad] );

        // loop over bins
        for ( std::size_t bin = 0; bin < m_nCKTBins[rad]; ++bin ) {
          // bin limits
          const auto [ckmin, ckmax] = ckBinLimits( bin, rad );
          const auto ckmean         = ( ckmin + ckmax ) * 0.5f;
          const auto [pmin, pmax]   = pBinLimits( bin, rad );
          const auto pmean          = ( pmin + pmax ) * 0.5f;
          // get the CK resolution plots and save RMS values
          const auto resHCK = h_ckResTrueCKBins[rad][pid][bin];
          if ( resHCK ) { paramResCK.emplace_back( ckmean, resHCK->rms() ); }
          const auto resHP = h_ckResTruePBins[rad][pid][bin];
          if ( resHP ) { paramResP.emplace_back( pmean, resHP->rms() ); }
        }

        // print results
        _ri_debug << rad << " " << pid << " | CK Res V CK " << paramResCK << endmsg;
        _ri_debug << rad << " " << pid << " | CK Res V P  " << paramResP << endmsg;
      }
    }
  }

  return Consumer::finalize();
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CKResParameterisation )

//-----------------------------------------------------------------------------

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <array>
#include <numeric>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/RichPID.h"
#include "Event/Track.h"

// Relations
#include "RichFutureMCUtils/RichMCRelations.h"
#include "RichFutureMCUtils/TrackToMCParticle.h"

// tools
#include "RichFutureRecInterfaces/IRichPIDPlots.h"
#include "TrackInterfaces/ITrackSelector.h"

namespace Rich::Future::Rec::MC::Moni {

  /** @class PIDQC RichPIDQC.h
   *
   *  MC checking of the RICH PID performance
   *
   *  @author Chris Jones
   *  @date   2016-12-08
   */

  class PIDQC final : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&, //
                                                             const LHCb::RichPIDs&,     //
                                                             const Rich::Future::MC::Relations::TkToMCPRels& ),
                                                       Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PIDQC( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichPIDsLocation", LHCb::RichPIDLocation::Default},
                     KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles}} ) {
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

    /// Initialise
    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        if ( !m_fillPIDplots ) { m_pidPlots.disable(); }
      } );
    }

  protected:
    /// Book histograms
    StatusCode prebookHistograms() override {

      bool ok = true;

      const BinLabels pidTypes = {"unknown", "electron", "muon", "pion", "kaon", "proton", "deuteron", "below_thres"};
      ok &= saveAndCheck( h_pidTable,                                                              //
                          richHisto2D( HID( "pidTable" ),                                          //
                                       "PID Performance Table",                                    //
                                       -1.5, Rich::NParticleTypes - 0.5, Rich::NParticleTypes + 1, //
                                       -1.5, Rich::NParticleTypes - 0.5, Rich::NParticleTypes + 1, //
                                       "Reco. PID Type", "MC PID Type", "Entries",                 //
                                       pidTypes, pidTypes ) );

      ok &= saveAndCheck( h_npids,                                         //
                          richHisto1D( HID( "nPIDs" ), "# PIDs per event", //
                                       -0.5, 200.5, 201,                   //
                                       "# PIDs per event", "Entries" ) );

      ok &= saveAndCheck( h_evOK,                                                 //
                          richHisto1D( HID( "evOK" ), "Event Success V Failures", //
                                       -0.5, 1.5, 2,                              //
                                       "Event has PIDs", "Entries", {"false", "true"} ) );

      ok &= saveAndCheck( h_tkFrac,                                                      //
                          richHisto1D( HID( "pidFrac" ), "Fraction of Tracks with PIDs", //
                                       0, 1, nBins1D(),                                  //
                                       "Track Fraction with PID information", "Entries" ) );

      return StatusCode{ok};
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Range&                       tracks, //
                     const LHCb::RichPIDs&                           pids,   //
                     const Rich::Future::MC::Relations::TkToMCPRels& rels ) const override;

  private:
    /// Fill the table with the given entries
    inline void fillTable( const Rich::ParticleIDType recopid, //
                           const Rich::ParticleIDType mcpid,   //
                           const double               weight = 1.0 ) const {
      fillHisto( h_pidTable, (int)recopid, (int)mcpid, weight );
    }

  private:
    // tools

    /** Track selector.
     *  Longer term should get rid of this and pre-filter the input data instead.
     */
    ToolHandle<const ITrackSelector> m_tkSel{this, "TrackSelector", "TrackSelector"};

    /// PID plots tool
    ToolHandle<const IPIDPlots> m_pidPlots{this, "PlotsTool", "Rich::Future::Rec::PIDPlots/PIDPlots"};

  private:
    // properties

    /// Enable detailed histograms
    Gaudi::Property<bool> m_fillPIDplots{this, "FillPIDPlots", false, "Enable the filling of detailed PID plots"};

    /// Allow reassign PID to below threshold
    Gaudi::Property<bool> m_allowBTreassign{this, "AllowBTReassign", true,
                                            "Allow PIDs to be reassigned as Below Threshold"};

  private:
    // Gaudi counters and histos

    /// PID table
    AIDA::IHistogram2D* h_pidTable{nullptr};

    /// PIDs per event
    AIDA::IHistogram1D* h_npids{nullptr};

    /// Event success / fail
    AIDA::IHistogram1D* h_evOK{nullptr};

    /// Track fraction with PID
    AIDA::IHistogram1D* h_tkFrac{nullptr};

    /// Proton heavy ID efficiency
    mutable Gaudi::Accumulators::BinomialCounter<> m_protonEff{this, "Pr DLL(Pr)>0 ID Efficiency"};

    /// Kaon heavy ID efficiency
    mutable Gaudi::Accumulators::BinomialCounter<> m_kaonEff{this, "K->K,Pr,D ID Efficiency"};

    /// Pion light ID efficiency
    mutable Gaudi::Accumulators::BinomialCounter<> m_pionEff{this, "Pi->El,Mu,Pi ID Efficiency"};

    /// RICH Event ID rate
    mutable Gaudi::Accumulators::BinomialCounter<> m_eventsWithID{this, "Event ID rate"};

    /// RICH Track ID rate
    mutable Gaudi::Accumulators::BinomialCounter<> m_tracksWithID{this, "Track ID rate"};

    /// Only using RICH1
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR1{this, "Used RICH1 only"};

    /// Only using RICH2
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR2{this, "Used RICH2 only"};

    /// Using both RICH1 and RICH2
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR1R2{this, "Used RICH1 and RICH2"};

    /// Reco PID reassigned as BT
    mutable Gaudi::Accumulators::BinomialCounter<> m_recoPIDBT{this, "Reassigned Reco PID BT"};

    /// MC PID reassigned as BT
    mutable Gaudi::Accumulators::BinomialCounter<> m_mcPIDBT{this, "Reassigned MC PID BT"};
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

void PIDQC::operator()( const LHCb::Track::Range&                       tracks, //
                        const LHCb::RichPIDs&                           pids,   //
                        const Rich::Future::MC::Relations::TkToMCPRels& rels ) const {

  // counter buffers
  auto         protonEff = m_protonEff.buffer();
  auto         kaonEff   = m_kaonEff.buffer();
  auto         pionEff   = m_pionEff.buffer();
  auto         tksWithID = m_tracksWithID.buffer();
  auto         withR1    = m_withR1.buffer();
  auto         withR2    = m_withR2.buffer();
  auto         withR1R2  = m_withR1R2.buffer();
  unsigned int pidCount  = 0;

  // count events with at least 1 PID object
  m_eventsWithID += !pids.empty();

  // track selector shortcut
  const auto tkSel = m_tkSel.get();

  // count total selected tracks, PIDs etc.
  const auto nTks = std::count_if( tracks.begin(), tracks.end(), [&tkSel, &pids, &tksWithID]( const auto* tk ) {
    const bool sel = tk && tkSel->accept( *tk );
    if ( sel ) {
      tksWithID += std::any_of( pids.begin(), pids.end(), [&tk]( const auto id ) { return id && tk == id->track(); } );
    }
    return sel;
  } );
  _ri_verbo << "Counted " << nTks << " tracks" << endmsg;

  // helper for the track -> MCP relations
  const Rich::Future::MC::Relations::TrackToMCParticle tkToMPCs( rels );

  // Loop over all PID results
  for ( const auto pid : pids ) {

    // Track for this PID
    const auto tk = pid->track();

    _ri_debug << *pid << endmsg;

    // is track selected
    if ( !tkSel->accept( *tk ) ) { continue; }
    // Is the track in the input list
    // (if not skip, means we are running on a reduced track list with selection cuts)
    if ( std::none_of( tracks.begin(), tracks.end(), [&tk]( const auto* t ) { return t == tk; } ) ) { continue; }

    // Count PIDs and tracks
    ++pidCount;
    withR1 += ( pid->usedRich1Gas() && !pid->usedRich2Gas() );
    withR2 += ( pid->usedRich2Gas() && !pid->usedRich1Gas() );
    withR1R2 += ( pid->usedRich2Gas() && pid->usedRich1Gas() );

    // Get best reco PID
    auto bpid = pid->bestParticleID();
    // if below threshold, set as such
    const bool reassignRecoBT = ( m_allowBTreassign && !pid->isAboveThreshold( bpid ) );
    if ( reassignRecoBT ) {
      _ri_verbo << " -> Reassigned RecoPID to BT" << endmsg;
      bpid = Rich::BelowThreshold;
    }
    m_recoPIDBT += reassignRecoBT;
    _ri_verbo << " -> Best Reco PID = " << bpid << endmsg;

    // Get the MCParticle range for this track
    const auto mcPs = tkToMPCs.mcParticleRange( *tk );
    _ri_verbo << " -> Matched to " << mcPs.size() << " MCParticles" << endmsg;

    // Did we get any MCPs ?
    if ( mcPs.empty() ) {
      // Fill once with MC type unknown (Below threshold)
      fillTable( bpid, Rich::BelowThreshold, 1.0 );
    } else {
      // Get the sum off all MCP weights
      const auto wSum = std::accumulate( mcPs.begin(), mcPs.end(), 0.0,
                                         []( const auto sum, const auto& w ) { return sum + w.weight(); } );
      // fill once per associated MCP
      for ( const auto MC : mcPs ) {
        // get the MCP
        const auto mcP = MC.to();
        // normalised weight for this MCP
        const auto mcPW = ( wSum > 0 ? MC.weight() / wSum : 1.0 );
        // MC Truth
        auto mcpid = tkToMPCs.mcParticleType( mcP );
        // If unknown, set to below threshold (i.e. ghost).
        if ( Rich::Unknown == mcpid ) { mcpid = Rich::BelowThreshold; }
        // If a real type, but below threshold, set below threshold
        const bool reassignMCBT = ( m_allowBTreassign && !pid->isAboveThreshold( mcpid ) );
        if ( reassignMCBT ) {
          _ri_verbo << " -> Reassigned MCPID to BT" << endmsg;
          mcpid = Rich::BelowThreshold;
        }
        m_mcPIDBT += reassignMCBT;
        _ri_verbo << " -> MC PID = " << mcpid << endmsg;
        // fill table
        fillTable( bpid, mcpid, mcPW );
        // DLL plots using true MC type
        if ( m_fillPIDplots ) { m_pidPlots->plots( pid, mcpid ); }
        // fill ID counters
        if ( Rich::BelowThreshold != bpid ) {
          // For protons use DLL cut
          if ( Rich::Proton == mcpid ) { protonEff += ( pid->particleDeltaLL( Rich::Proton ) > 0 ); }
          // For kaons/pions use heavy/light seperation
          const bool heavyID = ( (int)Rich::Kaon <= (int)bpid );
          const bool lightID = ( (int)Rich::Pion >= (int)bpid );
          if ( Rich::Kaon == mcpid ) { kaonEff += heavyID; }
          if ( Rich::Pion == mcpid ) { pionEff += lightID; }
        }
      }
    }

  } // pid loop

  fillHisto( h_npids, pidCount );
  fillHisto( h_evOK, ( pids.empty() ? 0 : 1 ) );
  if ( nTks > 0 ) { fillHisto( h_tkFrac, static_cast<double>( pidCount ) / static_cast<double>( nTks ) ); }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PIDQC )

//-----------------------------------------------------------------------------

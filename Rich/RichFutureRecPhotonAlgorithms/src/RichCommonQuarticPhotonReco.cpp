/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichCommonQuarticPhotonReco.h"

using namespace Rich::Future::Rec;

//=============================================================================

StatusCode CommonQuarticPhotonReco::initialize() {

  // Sets up various tools and services
  auto sc = BasePhotonReco::initialize();
  if ( !sc ) return sc;

  // loop over radiators
  for ( const auto rad : activeRadiators() ) {

    // If rejection of ambiguous photons is turned on make sure test is turned on
    if ( m_rejectAmbigPhots[rad] && !m_testForUnambigPhots[rad] ) {
      info() << "Unambigous photon check will be enabled in order to reject ambiguous photons" << endmsg;
      m_testForUnambigPhots[rad] = true;
    }

    // information printout about configuration
    if ( m_testForUnambigPhots[rad] ) {
      _ri_debug << "Will test for unambiguous     " << rad << " photons" << endmsg;
    } else {
      _ri_debug << "Will not test for unambiguous " << rad << " photons" << endmsg;
    }

    if ( m_rejectAmbigPhots[rad] ) {
      _ri_debug << "Will reject ambiguous " << rad << " photons" << endmsg;
    } else {
      _ri_debug << "Will accept ambiguous " << rad << " photons" << endmsg;
    }

    if ( m_useAlignedMirrSegs[rad] ) {
      _ri_debug << "Will use fully alligned mirror segments for " << rad << " reconstruction" << endmsg;
    } else {
      _ri_debug << "Will use nominal mirrors for " << rad << " reconstruction" << endmsg;
    }

    if ( m_checkPrimMirrSegs[rad] ) {
      _ri_debug << "Will check for full intersecton with mirror segments for " << rad << endmsg;
    }

    _ri_debug << "Minimum active " << rad << " segment fraction = " << m_minActiveFrac[rad] << endmsg;
  }

  _ri_debug << "# iterations (Aero/R1Gas/R2Gas) = " << m_nMaxQits << endmsg;

  // return
  return sc;
}

//=============================================================================

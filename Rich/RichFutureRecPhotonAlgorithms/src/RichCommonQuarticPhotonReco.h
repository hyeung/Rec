/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/SystemOfUnits.h"

// Base class
#include "RichBasePhotonReco.h"

// RichDet
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"

// Quartic Solver
#include "RichRecUtils/QuarticSolverNewton.h"

// Utils
#include "RichUtils/RichRayTracingUtils.h"

// VDT
#include "vdt/asin.h"

// Math
#include "LHCbMath/Truncate.h"

// STL
#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <ios>
#include <type_traits>

namespace Rich::Future::Rec {

  // pull in methods from Rich::RayTracingUtils
  using namespace Rich::RayTracingUtils;

  /** @class CommonQuarticPhotonReco RichCommonQuarticPhotonReco.h
   *
   *  Common base for quartic algorithms (scalar and vector).
   *
   *  @author Chris Jones
   *  @date   2017-10-13
   */

  class CommonQuarticPhotonReco : public BasePhotonReco {

  public:
    /// Standard constructor
    CommonQuarticPhotonReco( const std::string& name, ISvcLocator* pSvcL ) : BasePhotonReco( name, pSvcL ) {
      // Corrections for the intrinsic biases
      //            Aerogel Rich1Gas Rich2Gas
#ifdef USE_DD4HEP
      m_ckBiasCorrs = {0.0, 2.15e-4, 1.8e-5};
#else
      m_ckBiasCorrs = {0.0, 1.2e-4, 5.0e-5};
#endif
    }

    /// Initialization after creation
    virtual StatusCode initialize() override;

  protected:
    // types

    /// Array of DeRich detector elements
    using DeRiches = DetectorArray<const Rich::Detector::RichBase*>;

  protected:
    /// acos for double
    inline double myacos( const double x ) const noexcept { return vdt::fast_acos( x ); }

    /// acos for float
    inline float myacos( const float x ) const noexcept { return vdt::fast_acosf( x ); }

  protected:
    // data

    /// Newton Quartic Solver
    Rich::Rec::QuarticSolverNewton m_quarticSolver;

  protected:
    // properties

    /** @brief Flag to indicate if the unambiguous photon test should be performed
     *  for each radiator
     *
     *  If set to true the reconstruction is first performed twice, using the
     *  track segment start and end points as the assumed emission points and the
     *  nominal mirror geometry. If both calculations give the same mirror segments
     *  then this photon is flagged as unambiguous and the reconstruction is then
     *  re-performed using these mirror segments and the best-guess emission point
     *  (usually the track segment centre point)
     *
     *  If false, then the photon is first reconstructed using the segment centre
     *  point as the emission point and using the nominal mirror geometry. The mirror
     *  segments found for this calculation are then used to re-do the calculation.
     */
    Gaudi::Property<RadiatorArray<bool>> m_testForUnambigPhots{
        this,
        "FindUnambiguousPhotons",
        {false, false, false},
        "Perform additional reconstruction checks to determine if each photon is anbiguous or not"};

    /** Flag to turn on rejection of ambiguous photons
     *
     *  If set true photons which are not unambiguous will be rejected
     *  Note, setting this true automatically means m_testForUnambigPhots
     *  will be set true
     */
    Gaudi::Property<RadiatorArray<bool>> m_rejectAmbigPhots{
        this, "RejectAmbiguousPhotons", {false, false, false}, "Reject ambiguous photon candidates"};

    /** @brief Flag to indicate if Cherenkov angles should be computed using the
     *  absolute mirror segment alignment.
     *
     *  If set to true, then the reconstruction will be performed twice,
     *  once with the nominal mirror allignment in order to find the appropriate
     *  mirror segments, and then again with thos segments.
     *
     *  If false, only the nominal mirror allignment will be used
     */
    Gaudi::Property<RadiatorArray<bool>> m_useAlignedMirrSegs{
        this,
        "UseMirrorSegmentAlignment",
        {true, true, true},
        "Use the fully aligned mirror segments during the photon reconstruction"};

    /** Maximum number of iterations of the quartic solution, for each radiator,
     *  in order to account for the non-flat secondary mirrors.
     */
    Gaudi::Property<RadiatorArray<int>> m_nMaxQits{
        this,
        "NQuarticIterationsForSecMirrors",
        {1, 1, 3},
        "Number of reconstruction iterations used to account for non-flat secondary mirrors"};

    /** Minimum number of iterations of the quartic solution, for each radiator,
     *  in order to account for the non-flat secondary mirrors.
     */
    Gaudi::Property<RadiatorArray<int>> m_nMinQits{this, "MinIterationsForTolCheck", {0, 0, 0}};

    /** Turn on/off the checking of photon trajectories against the mirror
     *  segments to verify if the photon hit the real active area (and not,
     *  for instance, the gaps).
     */
    Gaudi::Property<RadiatorArray<bool>> m_checkPrimMirrSegs{
        this,
        "CheckPrimaryMirrorSegments",
        {false, false, false},
        "Perform precise intersection checks with the individual mirror segments."};

    /// Minimum active segment fraction in each radiator
    Gaudi::Property<RadiatorArray<float>> m_minActiveFrac{
        this,
        "MinActiveFraction",
        {0.2f, 0.2f, 0.2f},
        "Minimum active fraction of segment path length for photon reconstruction"};

    /// Minimum tolerance for mirror reflection point during iterations
    Gaudi::Property<RadiatorArray<float>> m_minSphMirrTolIt{
        this,
        "MinSphMirrTolIt",
        {static_cast<float>( std::pow( 0.10 * Gaudi::Units::mm, 2 ) ),
         static_cast<float>( std::pow( 0.08 * Gaudi::Units::mm, 2 ) ),
         static_cast<float>( std::pow( 0.05 * Gaudi::Units::mm, 2 ) )}};

    /// Flag to control if the secondary mirrors are treated as if they are completely flat
    Gaudi::Property<DetectorArray<bool>> m_treatSecMirrsFlat{
        this, "AssumeFlatSecondaryMirrors", {true, false}, "Assume the secondary mirrors are perfectly flat"};

    /// Flag to enable the truncation of the output Cherenkov theta and phi values, for each radiator
    Gaudi::Property<RadiatorArray<bool>> m_ckValueTruncate{this, "TruncateCKAngles", {true, true, true}};

    /// Enabled 4D reconstruction
    Gaudi::Property<DetectorArray<bool>> m_enable4D{this, "Enable4D", {false, false}, "Enable 4D reconstruction"};
  };

} // namespace Rich::Future::Rec

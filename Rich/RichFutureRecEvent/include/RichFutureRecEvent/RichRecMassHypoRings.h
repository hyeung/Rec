/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <utility>

// Kernel
#include "Kernel/FastAllocVector.h"

// Utils
#include "RichRecUtils/RichMassAliasArray.h"

// Event
#include "RichFutureRecEvent/RichRecRayTracedCKRingPoint.h"

namespace Rich::Future::Rec {

  /// Type for mass hypothesis rings for each hypothesis
  using MassHypoRings = MassAliasArray<RayTracedCKRingPoint::Vector>;

  /// Container of MassHypoRings
  using MassHypoRingsVector = LHCb::STL::Vector<MassHypoRings>;

  /// photon yield TES locations
  namespace MassHypoRingsLocation {
    /** Location in TES for the mass hypothesis rings using the emitted photon
     *  spectra Cherenkov angles */
    inline const std::string Emitted = "Rec/RichFuture/MassHypoRings/Emitted";
  } // namespace MassHypoRingsLocation

  /// Type for two closest ring points to a given position on the ring
  using ClosestPoints = std::pair<const RayTracedCKRingPoint*, const RayTracedCKRingPoint*>;

  /// Return the points on the ring closest to the given azimuth
  ClosestPoints getPointsClosestInAzimuth( const RayTracedCKRingPoint::Vector& ring, const float angle );

} // namespace Rich::Future::Rec

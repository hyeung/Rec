#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

JNAME="Scope1"

SubmitRoot=$HOME"/LHCb/stack/Feature/Rec/Rich/RichFutureRecSys/examples/jobs"

u2lumis="1.5e34,1.2e34,1.0e34"

# Baseline 3D jobs
#${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits -N $JNAME"/Baseline"

# 4D jobs, basic scenarios, scanning time windows
#for twindow in "0.300,0.300" "0.150,0.150" "0.075,0.075" "0.050,0.050"; do
#    ${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits -N $JNAME"/TimeScan" -4d --lumis ${u2lumis} --photwinsR1 ${twindow} --photwinsR2 ${twindow}
#done

# 4D lumi=1.2e34 time and space resolution scans
#for twindow in "0.300,0.300" "0.150,0.150" "0.075,0.075" "0.050,0.050"; do
#    name=$JNAME"/Tardis"
#    ${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits --usePixelMCInfo --usePhotonMCInfo --overrideDetRegions -N $name -4d --lumis "1.2e34" --photwinsR1 $twindow --photwinsR2 $twindow --ckResScaleR1 "1.00,1.00" --ckResScaleR2 "1.00,1.00"
#    ${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits --usePixelMCInfo --usePhotonMCInfo --overrideDetRegions -N $name -4d --lumis "1.2e34" --photwinsR1 $twindow --photwinsR2 $twindow --ckResScaleR1 "0.75,0.75" --ckResScaleR2 "0.75,0.75"
#    ${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits --usePixelMCInfo --usePhotonMCInfo --overrideDetRegions -N $name -4d --lumis "1.2e34" --photwinsR1 $twindow --photwinsR2 $twindow --ckResScaleR1 "0.50,0.50" --ckResScaleR2 "0.50,0.50" --detEffR1 "0.7#9,0.79" --detEffR2 "0.90,0.90" --pixQR1 "1.0,1.0" --pixQR2 "1.0,1.0" 
#    ${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits --usePixelMCInfo --usePhotonMCInfo --overrideDetRegions -N $name -4d --lumis "1.2e34" --photwinsR1 $twindow --photwinsR2 $twindow --ckResScaleR1 "0.25,0.25" --ckResScaleR2 "0.25,0.25" --detEffR1 "0.5#8,0.58" --detEffR2 "0.79,0.79" --pixQR1 "1.0,1.0" --pixQR2 "1.0,1.0" 
#done

# Specific scoping doc scenarios

siPMtw="0.075"

# Option 1.0 - Value
${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits --usePixelMCInfo --usePhotonMCInfo --overrideDetRegions -N $JNAME"/Value"    -4d --lumis ${u2lumis} --photwinsR1 $siPMtw",0.3"     --photwinsR2 $siPMtw",0.3"     --ckResScaleR1 "0.426,0.913" --ckResScaleR2 "0.703,1.000" --detEffR1 "0.539,0.730" --detEffR2 "0.735,1.000" --pixQR1 "2.0,2.8" --pixQR2 "2.0,2.8"

# Option 2.0 - Mid Range
${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits --usePixelMCInfo --usePhotonMCInfo --overrideDetRegions -N $JNAME"/MidRange" -4d --lumis ${u2lumis} --photwinsR1 $siPMtw","$siPMtw --photwinsR2 $siPMtw","$siPMtw --ckResScaleR1 "0.276,0.426" --ckResScaleR2 "0.253,0.253" --detEffR1 "0.539,0.539" --detEffR2 "0.735,0.735" --pixQR1 "1.0,2.0" --pixQR2 "1.0,2.0"

# Option 3.0 - Finest
${SubmitRoot}"/RunJobs.py" --useMCTracks --useMCHits --usePixelMCInfo --usePhotonMCInfo --overrideDetRegions -N $JNAME"/Finest"   -4d --lumis ${u2lumis} --photwinsR1 $siPMtw","$siPMtw --photwinsR2 $siPMtw","$siPMtw --ckResScaleR1 "0.276,0.276" --ckResScaleR2 "0.253,0.253" --detEffR1 "0.539,0.539" --detEffR2 "0.735,0.735" --pixQR1 "1.0,1.0" --pixQR2 "1.0,1.0"

exit 0

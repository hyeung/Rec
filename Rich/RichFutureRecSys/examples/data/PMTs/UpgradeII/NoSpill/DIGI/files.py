###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import print_function
from Gaudi.Configuration import *
import glob, os
from GaudiConf import IOHelper

# name tag
tag = "U2-NoSpill-NoSIN-NoRichRandHits"

# Choose sample luminosity
lumi = os.getenv("LUMI", "2.0e33")

# DetDesc DB tags
dbTag = "dddb-20221004"
cdTag = "sim-20221220-vc-md100"

# Event type
evType = os.getenv("LHCB_EVENT_TYPE", "30000000")

relPath = "data/MC/UpgradeII/" + evType + "/" + dbTag + "/" + cdTag + "/" + tag + "-lumi_" + lumi + "/DIGI/"

# Check what is available
searchPaths = [
    # Cambridge
    "/usera/jonesc/NFS/" + relPath,
    # CERN EOS
    "/eos/lhcb/user/j/jonrob/" + relPath,
]

print("Data Files :-")
data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.digi"))
    if len(files) > 0:
        data += ["'PFN:" + file for file in files]
        for f in files:
            print(f)
    else:
        print("No data at", path)

# Batch options ?
if "CONDOR_FILE_BATCH" in os.environ:
    # Use env vars to define reduced file range for this job
    batch = int(os.environ["CONDOR_FILE_BATCH"])
    nfiles = int(os.environ["CONDOR_FILES_PER_BATCH"])
    print("CONDOR_FILE_BATCH", batch)
    print("CONDOR_FILES_PER_BATCH", nfiles)
    firstFile = batch * nfiles
    lastFile = (batch + 1) * nfiles
    if firstFile <= len(data):
        if lastFile > len(data): lastFile = len(data)
        print("Using restricted file range", firstFile, lastFile)
        data = data[firstFile:lastFile]
        for f in data:
            print(" ", f)
    else:
        print("WARNING: File range outside input data list")
        data = []

IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

from Configurables import LHCbApp, DDDBConf
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().DDDBtag = dbTag
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    # To Be Seen what is correct here...
    # https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/26
    LHCbApp().CondDBtag = "jonrob/all-pmts-active"
    # Use a geometry from before changes to RICH1
    # https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/205
    DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'
else:
    LHCbApp().CondDBtag = cdTag

from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent
UnpackRawEvent('UnpackODIN').RawEventLocation = 'DAQ/RawEvent'
UnpackRawEvent('UnpackRich').RawEventLocation = 'DAQ/RawEvent'

from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper
from DDDB.CheckDD4Hep import UseDD4Hep

# Use DD4HEP or DetDesc generated samples
isDD4HEPSIM = False

if isDD4HEPSIM and not UseDD4Hep:
    raise RuntimeError(
        "DD4HEP XSIM files can only be processed with a DD4HEP build")

if isDD4HEPSIM:
    # samples made with DD4HEP simulation
    # Still under development/testing and will evolve....
    searchPaths = [
        # Cambridge
        "/usera/jonesc/NFS/data/MC/Run3/DD4HEP_TESTS/July14-2023/",
        # CERN EOS
        "/eos/lhcb/user/s/seaso/GaussOutput/Rich-RUN3/NewFramework/RICH-SimFiles/July14-2023/"
    ]
else:
    # samples made with DetDesc simulation
    searchPaths = [
        # Cambridge
        "/usera/jonesc/NFS/data/MC/Run3/SE/August2023/",
        # CERN EOS
        "/eos/lhcb/user/s/seaso/GaussOutput/Rich-RUN3/OldFramework/RICH-SimFiles/August2023/"
    ]

print("Data Files :-")
data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.sim"))
    data += ["PFN:" + file for file in files]
    for f in files:
        print(f)

IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

from Configurables import LHCbApp, DDDBConf
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"

# DB versions
if isDD4HEPSIM:
    # todo
    pass
else:
    if UseDD4Hep:
        # To be seen if these are correct for data above...
        #LHCbApp().CondDBtag = "sim10/2023" # Seems different to master for RICH1 ??
        LHCbApp().CondDBtag = "master"
        DDDBConf().GeometryVersion = 'run3/trunk'
    else:
        LHCbApp().CondDBtag = "upgrade/sim-20230310-vc-md100"
        LHCbApp().DDDBtag = "upgrade/dddb-20230310"

# Some tweaks to the reco configuration when running on XSIM

if isDD4HEPSIM:
    # If XSIM input was created by a DD4HEP simulation disable some DetDesc workarounds
    from Configurables import Rich__Future__MC__DecodedDataFromMCRichHits as RichMCDecoder
    RichMCDecoder("RichDecodeFromMC").IsDetDescMC = False

# Create a fake ODIN as these do not exist in XSIM input
from Configurables import GaudiSequencer
from Configurables import LHCb__Tests__FakeEventTimeProducer as DummyEventTime
# Override members of GaudiSequencer 'ODINSeq' to create a fake ODIN object
# instead of attempting to build from RawEvent
odinSeq = GaudiSequencer("ODINSeq")
odinSeq.Members = [DummyEventTime("DummyEventTime", Start=123456789, Step=0)]

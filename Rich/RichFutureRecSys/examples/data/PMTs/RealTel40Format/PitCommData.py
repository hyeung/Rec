###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
import glob, os
from GaudiConf import IOHelper

# RICH2 pattern
data = [
    # "DATAFILE='/usera/jonesc/NFS/data/RunIII/Tel40Pit/AsymPattern/Run_0000227015_20220321-124055-146_R2EB01.mdf'"
    "DATAFILE='/usera/jonesc/NFS/data/RunIII/Tel40Pit/Run_0000227384_20220324-160419-900_R1EB01.mdf'"
]

IOHelper('MDF').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

from Configurables import LHCbApp

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().DDDBtag = 'upgrade/dddb-20210617'
#LHCbApp().CondDBtag = 'upgrade/jonrob/rich1-disable-u00-d00'
LHCbApp().CondDBtag = 'upgrade/jonrob/cable-mapping-2022-final'

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Calo
// #include "CaloDet/DeCalorimeter.h"

// Event
#include "Event/PlumeAdc.h"
// #include "Event/CaloDigits_v2.h"
#include "Event/ODIN.h"

// Gaudi
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuples.h"
#include "GaudiUtils/HistoLabels.h"
#include "LHCbAlgs/Consumer.h"

// AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

// ROOT
#include <TF1.h>
#include <TFitResult.h>
#include <TGraph.h>
#include <TGraphErrors.h>

#include <TProfile.h>
#include <sstream>

using LHCb::Detector::Plume::ChannelID;
using Input   = LHCb::PlumeAdcs;
using BXTypes = LHCb::ODIN::BXTypes;

namespace {
  template <typename T>
  using axis1D = Gaudi::Accumulators::Axis<T>;

  template <typename T, typename K, typename OWNER>
  void map_emplace( T& t, K&& key, OWNER* owner, std::string const& name, std::string const& title,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1 ) {
    t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
               std::forward_as_tuple( owner, name, title, axis1 ) );
  }
  template <typename T, typename K, typename OWNER>
  void map_emplace2D( T& t, K&& key, OWNER* owner, std::string const& name, std::string const& title,
                      Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1,
                      Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis2 ) {
    t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
               std::forward_as_tuple( owner, name, title, axis1, axis2 ) );
  }

  template <typename T>
  std::string to_string( const T& streamable ) {
    std::ostringstream oss;
    oss << streamable;
    return oss.str();
  }

  const auto AxisBCID    = axis1D<double>( 3565, -0.5, 3564.5 );
  const auto AxisADC     = axis1D<double>( 4096 + 256, -256, 4096 );
  const auto AxisTIME    = axis1D<double>( 100, 0, 8 );
  const auto AxisTIMEERR = axis1D<double>( 100, 0, 0.4 );
  const auto AxisSAMP    = axis1D<double>( 1000, 0, 1000 );
  const auto AxisSAMPERR = axis1D<double>( 100, 0, 10 );
} // namespace

class PlumeDigitMonitor final
    : public LHCb::Algorithm::Consumer<void( const Input&, const LHCb::ODIN& odin ),
                                       Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  // Standart constructor
  PlumeDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"Input", ""}, KeyValue{"ODIN", ""}} ){};

  virtual ~PlumeDigitMonitor() = default;
  StatusCode initialize() override;
  void       operator()( const Input&, const LHCb::ODIN& odin ) const override;

private:
  Gaudi::Property<float> m_timing_threshold{"TimingThreshold", 9.5};
  // DeCalorimeter* m_calo = nullptr;

  bool overThreshold( LHCb::PlumeAdc const& adc ) const {
    return ( adc.channelID().channelType() != ChannelID::ChannelType::TIME ) ? adc.overThreshold()
                                                                             : ( adc.adc() > m_timing_threshold );
  }

  void monitor( const Input& digits, const LHCb::ODIN& odin ) const;

  // timing s-shape
  void fit_sshapes( const Input& adcs, std::map<unsigned int, std::vector<std::pair<float, float>>>& result,
                    std::map<unsigned int, int>& status ) const;
  void fit_single_sshape( std::vector<int> sshape, std::vector<std::pair<float, float>>& result, int& status ) const;
  void initialize_function();
  std::unique_ptr<TF1>   m_fit_function_tf1;
  Gaudi::Property<float> m_threshold_sshape{this, "ThresholdSShape", 150.5};

  mutable std::mutex m_lazy_lock;

  mutable Gaudi::Accumulators::Histogram<1> m_over_thld_time{
      this, "over_threshold_time", "#events OT per channel: TIME", axis1D<double>{64, -0.5, 63.5}};

  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_lumi{this, "bcid_adc", "BCID vs ADC (LUMI)", AxisBCID, AxisADC};
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_time{this, "bcid_adc_time", "BCID vs ADC time", AxisBCID,
                                                            axis1D<double>{256, -256, 4096 - 256}};
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_pin{this, "bcid_adc_pin", "BCID vs ADC pin", AxisBCID,
                                                           axis1D<double>{256, -256, 4096 - 256}};
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_mon{this, "bcid_adc_mon", "BCID vs ADC mon", AxisBCID,
                                                           axis1D<double>{256, -256, 4096 - 256}};
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_lumi_coarse{this, "bcid_adc_coarse", "BCID vs ADC (LUMI)",
                                                                   AxisBCID, axis1D<double>{410 + 26, -260, 4100}};

  mutable Gaudi::Accumulators::Histogram<2> m_xy_hits_front{
      this, "hits_front", "Hits in the front layer",
      axis1D<double>{
          11, -5.5, 5.5, "hits_front_xaxis", {"16", "15", "14", "13", "12", "", "00", "01", "02", "03", "04"}},
      axis1D<double>{
          11, -5.5, 5.5, "hits_front_yaxis", {"22", "21", "20", "19", "18", "", "06", "07", "08", "09", "10"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_xy_hits_back{
      this, "hits_back", "Hits in the back layer",
      axis1D<double>{
          11, -5.5, 5.5, "hits_back_xaxis", {"40", "39", "38", "37", "36", "", "24", "25", "26", "27", "28"}},
      axis1D<double>{
          11, -5.5, 5.5, "hits_back_yaxis", {"46", "45", "44", "43", "42", "", "30", "31", "32", "33", "34"}}};

  mutable Gaudi::Accumulators::Histogram<1> m_occup_front_right{
      this, "hits_LUMI_00_04", "Number of hits in channels 00-04",
      axis1D<double>{5, 0, 5, "hits_LUMI_00_04_axis", {"00", "01", "02", "03", "04"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_front_left{
      this, "hits_LUMI_12_16", "Number of hits in channels 12-16",
      axis1D<double>{5, 0, 5, "hits_LUMI_12_16_axis", {"12", "13", "14", "15", "16"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_front_up{
      this, "hits_LUMI_06_10", "Number of hits in channels 06-10",
      axis1D<double>{5, 0, 5, "hits_LUMI_06_10_axis", {"06", "07", "08", "09", "10"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_front_down{
      this, "hits_LUMI_18_22", "Number of hits in channels 18-22",
      axis1D<double>{5, 0, 5, "hits_LUMI_18_22_axis", {"18", "19", "20", "21", "22"}}};

  mutable Gaudi::Accumulators::Histogram<1> m_occup_back_right{
      this, "hits_LUMI_24_28", "Number of hits in channels 24-28",
      axis1D<double>{5, 0, 5, "hits_LUMI_24_28_axis", {"24", "25", "26", "27", "28"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_back_left{
      this, "hits_LUMI_36_40", "Number of hits in channels 36-40",
      axis1D<double>{5, 0, 5, "hits_LUMI_36_40_axis", {"36", "37", "38", "39", "40"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_back_up{
      this, "hits_LUMI_30_34", "Number of hits in channels 30-34",
      axis1D<double>{5, 0, 5, "hits_LUMI_30_34_axis", {"30", "31", "32", "33", "34"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_back_down{
      this, "hits_LUMI_42_46", "Number of hits in channels 42-46",
      axis1D<double>{5, 0, 5, "hits_LUMI_42_46_axis", {"42", "43", "44", "45", "46"}}};

  mutable Gaudi::Accumulators::Histogram<1> m_assym_front_vert{
      this, "hits_front_v", "Number of hits in front layer vertical PMTs",
      axis1D<double>{10, -5., 5., "hits_front_v_axis", {"16", "15", "14", "13", "12", "00", "01", "02", "03", "04"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_assym_front_horiz{
      this, "hits_front_h", "Number of hits in front layer horizontal PMTs",
      axis1D<double>{10, -5., 5., "hits_front_h_axis", {"22", "21", "20", "19", "18", "06", "07", "08", "09", "10"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_assym_back_vert{
      this, "hits_back_v", "Number of hits in back layer vertical PMTs",
      axis1D<double>{10, -5., 5., "hits_back_v_axis", {"40", "39", "38", "37", "36", "24", "25", "26", "27", "28"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_assym_back_horiz{
      this, "hits_back_h", "Number of hits in back layer horizontal PMTs",
      axis1D<double>{10, -5., 5., "hits_back_h_axis", {"46", "45", "44", "43", "42", "30", "31", "32", "33", "34"}}};

  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<1>> m_time_channel_tot;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<1>> m_time_err_channel_tot;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<1>> m_sshape_amp_tot;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<1>> m_sshape_amp_err_tot;

  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<2>> m_time_channel;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<2>> m_time_err_channel;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<2>> m_sshape_amp;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<2>> m_sshape_amp_err;

  mutable Gaudi::Accumulators::StatCounter<> m_numAdcOTLumi = {this, "Number of ADC over threshold for LUMI"};

  // Calibration
  // ADC histogram for every channel (LUMI/PIN/...) only for calibration trigger (light)
  // (in MONET, "freeze" references for all of the above and compare with current histos)
  mutable Gaudi::Accumulators::Histogram<1> m_calibType{
      this, "odin_calib_type", "ODIN CalibrationType", {16, -0.5, 15.5}};
  // ADC histogram per channel type and BX type (only for calib triggers)
  mutable std::map<BXTypes, Gaudi::Accumulators::Histogram<1>> m_mon_bx_adc;
  mutable std::map<BXTypes, Gaudi::Accumulators::Histogram<1>> m_pin_bx_adc;

  // For physics monitoring
  // ADC histogram per channel type and BX type (excluding calib triggers)
  mutable std::map<BXTypes, Gaudi::Accumulators::Histogram<1>> m_lumi_bx_adc;
  mutable std::map<BXTypes, Gaudi::Accumulators::Histogram<1>> m_time_bx_adc;

  // ADC histogram for every channel per BB/EB/BE/EE (excluding calib triggers)
  mutable std::map<std::pair<ChannelID, BXTypes>, Gaudi::Accumulators::Histogram<1>> m_channel_bx_adc;
  // ADC histogram for every channel per BB/EB/BE/EE (only calib triggers)
  mutable std::map<std::pair<ChannelID, BXTypes>, Gaudi::Accumulators::Histogram<1>> m_channel_bx_adc_calib;

  // ADC vs BCID (TProfile1) for every channel
  mutable std::map<ChannelID, Gaudi::Accumulators::ProfileHistogram<1>> m_channel_bcid_adc;

  // Timing data
  mutable Gaudi::Accumulators::Histogram<1> m_time_adc{this, "adc_TIME", "ADC for all timing channels", AxisADC};
  mutable std::map<ChannelID, Gaudi::Accumulators::Histogram<1>> m_channel_time;

  mutable unsigned long int m_runStartGps = 0;
  mutable unsigned int      m_lastRun     = 0;
};

// =============================================================================

DECLARE_COMPONENT( PlumeDigitMonitor )

// =============================================================================
// standard initialize method
// =============================================================================

StatusCode PlumeDigitMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    // FIXME we should loop over the fiber-channel maps (once they're in the DB) in order to
    // generate the full list of possible ChannelIDs. This would avoid hardcoding ranges here.

    std::vector<ChannelID> channels;
    for ( int i = 0; i < LHCb::Plume::nLumiPMTsPerBank * 2 + LHCb::Plume::nTimingPMTs; ++i ) {
      channels.emplace_back( ChannelID::ChannelType::LUMI, i );
    } // this needs to be up to 48, because the indexing afterwards is done with the PMT index, that gets up to 47
      // (because the timing PMTs have a numbering mixed with the lumi PMTs and there are four of them)
    for ( int i = 1; i <= LHCb::Plume::nPINChannels + 1; ++i ) {
      channels.emplace_back( ChannelID::ChannelType::PIN, i );
    } // loops over PIN numbers, that are defined between 1 and 9
    for ( int i = 1; i <= LHCb::Plume::nMonitoringPMTs; ++i ) {
      channels.emplace_back( ChannelID::ChannelType::MON, i );
    } // loops over Monitoring PMT numbers, that are defined between 1 and 4
    for ( int i : {5, 29, 11, 35} ) {
      for ( int j = 0; j <= 15; ++j ) channels.emplace_back( ChannelID::ChannelType::TIME, i, j );
    }

    for ( const auto& ch : channels ) {
      if ( ch.channelType() != ChannelID::ChannelType::TIME_T ) {
        map_emplace( m_channel_bcid_adc, ch, this, "bcid_adc_" + ch.toString(), "ADC vs BCID for " + ch.toString(),
                     AxisBCID );
      }

      for ( auto bx : {BXTypes::NoBeam, BXTypes::Beam1, BXTypes::Beam2, BXTypes::BeamCrossing} ) {
        map_emplace( m_channel_bx_adc, std::make_pair( ch, bx ), this, "adc_" + ch.toString() + "_" + to_string( bx ),
                     "ADC for " + ch.toString() + "/" + to_string( bx ), AxisADC );
        if ( ch.channelType() == ChannelID::ChannelType::LUMI && bx == BXTypes::NoBeam ) {
          map_emplace( m_channel_bx_adc_calib, std::make_pair( ch, bx ), this,
                       "adc_" + ch.toString() + "_" + to_string( bx ) + "_calib_signals",
                       "ADC for " + ch.toString() + "/" + to_string( bx ) + "/calib signals", AxisADC );
        }
      }
      if ( ch.channelType() == ChannelID::ChannelType::TIME_T ) {
        map_emplace( m_channel_time, ch, this, "time_" + ch.toString(), "Time for " + ch.toString(), {4096, 0, 4096} );
      }
    }

    for ( auto bx : {BXTypes::NoBeam, BXTypes::Beam1, BXTypes::Beam2, BXTypes::BeamCrossing} ) {
      map_emplace( m_lumi_bx_adc, bx, this, "adc_LUMI_" + to_string( bx ), "ADC for LUMI/" + to_string( bx ), AxisADC );
      map_emplace( m_time_bx_adc, bx, this, "adc_TIME_" + to_string( bx ), "ADC for TIME/" + to_string( bx ), AxisADC );
      map_emplace( m_mon_bx_adc, bx, this, "adc_MON_" + to_string( bx ), "ADC for MON/" + to_string( bx ), AxisADC );
      map_emplace( m_pin_bx_adc, bx, this, "adc_PIN_" + to_string( bx ), "ADC for PIN/" + to_string( bx ), AxisADC );
    }

    for ( auto tchannel : {5, 11, 29, 35} ) {
      map_emplace( m_time_channel_tot, tchannel, this, "time_sshape_tot_" + to_string( tchannel ),
                   "Inflection point for " + to_string( tchannel ), AxisTIME );
      map_emplace( m_time_err_channel_tot, tchannel, this, "time_sshape_err_tot_" + to_string( tchannel ),
                   "Uncertainty per track for " + to_string( tchannel ), AxisTIMEERR );
      map_emplace( m_sshape_amp_tot, tchannel, this, "amp_sshape_tot_" + to_string( tchannel ),
                   "S-shape amplitude for " + to_string( tchannel ), AxisSAMP );
      map_emplace( m_sshape_amp_err_tot, tchannel, this, "amp_sshape_err_tot_" + to_string( tchannel ),
                   "S-shape amplitude error for " + to_string( tchannel ), AxisSAMPERR );

      map_emplace2D( m_time_channel, tchannel, this, "time_sshape_" + to_string( tchannel ),
                     "Inflection point for " + to_string( tchannel ), AxisBCID, AxisTIME );
      map_emplace2D( m_time_err_channel, tchannel, this, "time_sshape_err_" + to_string( tchannel ),
                     "Uncertainty per track for " + to_string( tchannel ), AxisBCID, AxisTIMEERR );
      map_emplace2D( m_sshape_amp, tchannel, this, "amp_sshape_" + to_string( tchannel ),
                     "S-shape amplitude for " + to_string( tchannel ), AxisBCID, AxisSAMP );
      map_emplace2D( m_sshape_amp_err, tchannel, this, "amp_sshape_err_" + to_string( tchannel ),
                     "S-shape amplitude error for " + to_string( tchannel ), AxisBCID, AxisSAMPERR );
    }

    initialize_function();
  } );
}

// ============================================================================
// standard execution method
// ============================================================================

void PlumeDigitMonitor::operator()( const Input& adcs, const LHCb::ODIN& odin ) const {
  // TODO: remove lock after migrating to new histograms
  std::lock_guard<std::mutex> guard_lock( m_lazy_lock );

  if ( odin.runNumber() > m_lastRun ) {
    m_lastRun     = odin.runNumber();
    m_runStartGps = odin.gpsTime();
  }

  monitor( adcs, odin );
}

// ============================================================================
// Fill histograms
// ============================================================================
void PlumeDigitMonitor::monitor( const Input& adcs, const LHCb::ODIN& odin ) const {
  unsigned int nOTLumi = 0;

  auto       calibType = odin.calibrationType();
  auto       bx        = odin.bunchCrossingType();
  const auto bcid      = odin.bunchId();

  using ChannelType = ChannelID::ChannelType;

  ++m_calibType[calibType];
  std::map<unsigned int, std::vector<std::pair<float, float>>> sshape_result{};
  std::map<unsigned int, int>                                  sshape_status{};
  fit_sshapes( adcs, sshape_result, sshape_status );
  for ( const auto& digit : adcs ) {
    const auto adc            = digit->adc();
    const auto type           = digit->channelID().channelType();
    const auto channel_id     = digit->channelID().channelID();
    const auto channel_sub_id = digit->channelID().channelSubID();
    const auto over_thld      = overThreshold( *digit );
    const auto key            = std::make_pair( digit->channelID(), odin.bunchCrossingType() );

    m_channel_bcid_adc.at( digit->channelID() )[bcid] += adc; // Profile ADC vs BXID for each channel

    if ( calibType == 0 && type == ChannelType::LUMI ) { // lumi PMT, without requiring coincidence
      ++m_channel_bx_adc.at( key )[adc];
    } else if ( calibType != 0 && type == ChannelType::LUMI &&
                bx == BXTypes::NoBeam ) { // lumi PMT, without requiring coincidence,  but with the LED signals
      ++m_channel_bx_adc_calib.at( key )[adc];
    } else if ( calibType == 0 && type == ChannelType::TIME ) { // timing PMT requires coincidence
      ++m_channel_bx_adc.at( key )[adc];
    } else if ( type == ChannelType::TIME && channel_sub_id == 0 ) {
      if ( sshape_status.at( channel_id ) == true ) {
        ++m_time_channel.at( channel_id )[{bcid, sshape_result.at( channel_id )[3].first}];
        ++m_time_err_channel.at( channel_id )[{bcid, sshape_result.at( channel_id )[3].second}];
        ++m_sshape_amp.at( channel_id )[{bcid, sshape_result.at( channel_id )[1].first}];
        ++m_sshape_amp_err.at( channel_id )[{bcid, sshape_result.at( channel_id )[1].second}];

        ++m_time_channel_tot.at( channel_id )[sshape_result.at( channel_id )[3].first];
        ++m_time_err_channel_tot.at( channel_id )[sshape_result.at( channel_id )[3].second];
        ++m_sshape_amp_tot.at( channel_id )[sshape_result.at( channel_id )[1].first];
        ++m_sshape_amp_err_tot.at( channel_id )[sshape_result.at( channel_id )[1].second];
      }
    } else if ( type == ChannelType::PIN || type == ChannelType::MON ) {
      ++m_channel_bx_adc.at( key )[adc];
    }

    switch ( type ) {
    case ChannelType::LUMI:
      if ( calibType == 0 ) ++m_lumi_bx_adc.at( bx )[adc];
      break;
    case ChannelType::TIME:
      if ( calibType == 0 ) ++m_time_bx_adc.at( bx )[adc];
      break;
    case ChannelType::PIN:
      ++m_pin_bx_adc.at( bx )[adc];
      break;
    case ChannelType::MON:
      ++m_mon_bx_adc.at( bx )[adc];
      break;
    default:
      break;
    }

    switch ( type ) {
    case ChannelID::ChannelType::LUMI: {
      ++m_bcid_adc_lumi[{bcid, adc}];
      ++m_bcid_adc_lumi_coarse[{bcid, adc}];

      // avoid calib signals, that are in empty-empty, and fill only if over threshold
      if ( bx != BXTypes::NoBeam && over_thld ) {
        // fill histograms for front layer
        int x = channel_id % 6 + 1;
        int y = channel_id % 6 + 1;
        ++nOTLumi;

        if ( channel_id > 11 && channel_id < 24 ) {
          x *= -1;
          y *= -1;
        }
        if ( channel_id > 35 && channel_id < 48 ) {
          x *= -1;
          y *= -1;
        }
        ( channel_id % 12 ) < 5 ? y = 0 : x = 0;

        // fill outliers first
        if ( channel_id == 17 ) { ++m_xy_hits_front[{-2, -2}]; }
        if ( channel_id == 23 ) { ++m_xy_hits_front[{2, -2}]; }
        if ( channel_id == 41 ) { ++m_xy_hits_back[{-2, -2}]; }
        if ( channel_id == 47 ) { ++m_xy_hits_back[{2, -2}]; }

        if ( channel_id < 24 ) {
          ++m_xy_hits_front[{x, y}];
          if ( x > 0 && y == 0 ) {
            ++m_occup_front_right[channel_id % 6];
            ++m_assym_front_horiz[x - 0.5];
          }
          if ( x == 0 && y > 0 ) {
            ++m_occup_front_up[channel_id % 6];
            ++m_assym_front_vert[y - 0.5];
          }
          if ( x < 0 && y == 0 ) {
            ++m_occup_front_left[channel_id % 6];
            ++m_assym_front_horiz[x + 0.5];
          }
          if ( x == 0 && y < 0 ) {
            ++m_occup_front_down[channel_id % 6];
            ++m_assym_front_vert[y + 0.5];
          }
        }

        if ( channel_id >= 24 ) {
          ++m_xy_hits_back[{x, y}];
          if ( x > 0 && y == 0 ) {
            ++m_occup_back_right[channel_id % 6];
            ++m_assym_back_horiz[x - 0.5];
          }
          if ( x == 0 && y > 0 ) {
            ++m_occup_back_up[channel_id % 6];
            ++m_assym_back_vert[y - 0.5];
          }
          if ( x < 0 && y == 0 ) {
            ++m_occup_back_left[channel_id % 6];
            ++m_assym_back_horiz[x + 0.5];
          }
          if ( x == 0 && y < 0 ) {
            ++m_occup_back_down[channel_id % 6];
            ++m_assym_back_vert[y + 0.5];
          }
        }
      }
      break;
    }

    case ChannelID::ChannelType::TIME:
      ++m_time_adc[adc];
      if ( over_thld ) ++m_over_thld_time[channel_id];
      ++m_bcid_adc_time[{bcid, adc}];
      break;
    case ChannelID::ChannelType::PIN:
      ++m_bcid_adc_pin[{bcid, adc}];
      break;
    case ChannelID::ChannelType::MON:
      ++m_bcid_adc_mon[{bcid, adc}];
      break;
    case ChannelID::ChannelType::TIME_T:
      ++m_channel_time.at( digit->channelID() )[digit->adc()];
    default:
      break;
    }
  }
  m_numAdcOTLumi += nOTLumi;
}

void PlumeDigitMonitor::initialize_function() {
  m_fit_function_tf1 = std::make_unique<TF1>( "sshape_tf1", "[0] + [1]*TMath::Erf( [2]*(x - [3]) )", 0., 8. );
  m_fit_function_tf1->SetParNames( "A", "B", "a", "t" );
  m_fit_function_tf1->SetParLimits( 0, -1000, 1000. );
  m_fit_function_tf1->SetParLimits( 1, 0., 1000. );
  m_fit_function_tf1->SetParLimits( 2, 0., 10. );
  m_fit_function_tf1->SetParLimits( 3, 0., 10. );
  m_fit_function_tf1->SetParameters( 30., 300., 0.4, 4. );
}

void PlumeDigitMonitor::fit_single_sshape( std::vector<int> sshape, std::vector<std::pair<float, float>>& result,
                                           int& status ) const {
  TGraphErrors sshape_graph{};
  for ( const auto point : sshape ) {
    sshape_graph.SetPoint( sshape_graph.GetN(), sshape_graph.GetN(), point );
    sshape_graph.SetPointError( sshape_graph.GetN() - 1, 0, std::sqrt( std::abs( point ) ) );
  }
  sshape_graph.Fit( m_fit_function_tf1.get(), "NQ" );
  auto fitResult = sshape_graph.Fit( m_fit_function_tf1.get(), "SNQ" );

  status = fitResult->IsValid();

  result.emplace_back( m_fit_function_tf1->GetParameter( 0 ), m_fit_function_tf1->GetParError( 0 ) );
  result.emplace_back( m_fit_function_tf1->GetParameter( 1 ), m_fit_function_tf1->GetParError( 1 ) );
  result.emplace_back( m_fit_function_tf1->GetParameter( 2 ), m_fit_function_tf1->GetParError( 2 ) );
  result.emplace_back( m_fit_function_tf1->GetParameter( 3 ), m_fit_function_tf1->GetParError( 3 ) );
}

void PlumeDigitMonitor::fit_sshapes( const Input&                                                  adcs,
                                     std::map<unsigned int, std::vector<std::pair<float, float>>>& result,
                                     std::map<unsigned int, int>&                                  status ) const {
  // map<<channelID, map<subchannelID, ADC>>
  auto timing_map = std::map<unsigned int, std::map<unsigned int, int>>{};
  // map<<channelID, vector<ADC>>
  // where vector is ordered by s-shape order
  auto sshapes = std::map<unsigned int, std::vector<int>>{};
  // fill the timing_map with the ADCs
  for ( const auto& digit : adcs ) {
    if ( digit->channelID().channelType() != ChannelID::ChannelType::TIME ) continue;
    const auto channelID    = digit->channelID().channelID();
    const auto channelSubID = digit->channelID().channelSubID();

    if ( timing_map.find( channelID ) == timing_map.end() ) timing_map[channelID] = std::map<unsigned int, int>{};
    timing_map[channelID][channelSubID] = digit->adc();
  }
  std::map<unsigned int, bool> coincidence{};

  // use the last s-shape point for checking for coincidence
  coincidence[5] = coincidence[29] =
      ( timing_map.at( 5 ).at( 15 ) - timing_map.at( 5 ).at( 7 ) ) > m_threshold_sshape &&
      ( timing_map.at( 29 ).at( 15 ) - timing_map.at( 29 ).at( 7 ) ) > m_threshold_sshape;
  coincidence[11] = coincidence[35] =
      ( timing_map.at( 11 ).at( 15 ) - timing_map.at( 11 ).at( 7 ) ) > m_threshold_sshape &&
      ( timing_map.at( 35 ).at( 15 ) - timing_map.at( 35 ).at( 7 ) ) > m_threshold_sshape;
  // fill the s-shape map with differences
  for ( const auto& tchannel : timing_map ) {
    sshapes[tchannel.first] = std::vector<int>{};
    for ( unsigned int tsubchannel = 0; tsubchannel < 8; ++tsubchannel ) {
      sshapes.at( tchannel.first )
          .push_back( tchannel.second.at( tsubchannel + 8 ) - tchannel.second.at( tsubchannel ) );
    }
  }
  // fit the s-shapes
  for ( const auto& sshape : sshapes ) {
    const auto                           channelID = sshape.first;
    std::vector<std::pair<float, float>> fit_result;
    int                                  status_sshape = -1;
    if ( coincidence[channelID] ) {
      fit_single_sshape( sshape.second, fit_result, status_sshape );
      result[channelID] = fit_result;
    } else
      result[channelID] = std::vector<std::pair<float, float>>( 4, std::pair<float, float>{-9999, -9999} );
    status[channelID] = status_sshape;
  }
}

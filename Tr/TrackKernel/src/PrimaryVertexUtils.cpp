/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TrackKernel/PrimaryVertexUtils.h"

namespace {
  template <typename IndexContainer>
  inline void collectVeloIndices( const LHCb::Particle& p, IndexContainer& veloindices ) {
    if ( p.isBasicParticle() ) {
      const LHCb::ProtoParticle* proto = p.proto();
      if ( proto ) {
        const LHCb::Track* track = proto->track();
        if ( track && track->lhcbIDs().size() > 0 ) {
          veloindices.emplace_back( LHCb::Event::PV::uniqueVeloSegmentID( track->lhcbIDs() ) );
        }
      }
    } else {
      for ( const auto& dau : p.daughters() ) collectVeloIndices( *dau, veloindices );
    }
  }
} // namespace

boost::container::small_vector<uint32_t, 16> LHCb::collectVeloSegmentIDs( const LHCb::Particle& particle ) {
  boost::container::small_vector<uint32_t, 16> veloindices;
  collectVeloIndices( particle, veloindices );
  return veloindices;
}

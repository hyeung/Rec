/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"
#include "Detector/FT/FTUtils.h"
#include "Event/FitNode.h"
#include "Event/Measurement.h"
#include "Event/PrFitNode.h"
#include "Event/PrKalmanFitResult.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "Event/TrackParameters.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/LHCbID.h"
#include "LHCbAlgs/Consumer.h"
#include "Map.h"
#include "TrackKernel/TrackFunctors.h"
#include "TrackMonitorTupleBase.h"
#include <Gaudi/Accumulators/Histogram.h>

class FTTrackMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range& tracks, const DetectorElement& ),
                                       LHCb::DetDesc::usesBaseAndConditions<TrackMonitorTupleBase, DetectorElement>> {

public:
  FTTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {KeyValue{"TracksInContainer", LHCb::TrackLocation::Default},
                   KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}} ){};

  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      if ( m_verboseMode.value() ) {
        for ( unsigned int i = 0; i < FTConstants::nModulesTotal; i++ ) {
          m_histograms_modules.emplace( std::piecewise_construct, std::forward_as_tuple( i ),
                                        std::forward_as_tuple( this, i ) );
        }
      }
    } );
  }

  void operator()( const LHCb::Track::Range& tracks, const DetectorElement& lhcb ) const override;

private:
  void fillContainers( const LHCb::Track& track, IGeometryInfo const& geometry ) const;
  template <typename NodeType, typename FitResultType>
  void fillNodeInfo( const LHCb::Track&, const FitResultType& ) const;

  std::array<Gaudi::Property<double>, 3> m_refVec{
      {{this, "ReferenceZT1", 7931.0 * Gaudi::Units::mm, "midpoint of FTStation 1"},
       {this, "ReferenceZT2", 8613.0 * Gaudi::Units::mm, "midpoint of FTStation 2"},
       {this, "ReferenceZT3", 9298.0 * Gaudi::Units::mm, "midpoint of FTStation 3"}}};

  Gaudi::Property<bool> m_expertMode{this, "ExpertMode", false};
  Gaudi::Property<bool> m_verboseMode{this, "VerboseMode", false};

private:
  // Plots for the whole SciFi
  mutable Gaudi::Accumulators::Histogram<1> m_unbiasedFTResidual{
      this, "unbiasedFTResidual", "unbiasedFTResidual", {200, -2. * Gaudi::Units::mm, 2. * Gaudi::Units::mm, "X"}};
  mutable Gaudi::Accumulators::Histogram<1> m_biasedFTResidual{
      this, "biasedFTResidual", "biasedFTResidual", {200, -2. * Gaudi::Units::mm, 2. * Gaudi::Units::mm, "X"}};
  mutable Gaudi::Accumulators::Histogram<2> m_unbiasedFTResidualLayer{
      this,
      "unbiasedResidualLayer",
      "unbiasedResidual per FTLayer",
      {{FTConstants::nLayersTotal, -0.5, FTConstants::nLayersTotal - 0.5, "X"},
       {200, -2. * Gaudi::Units::mm, 2. * Gaudi::Units::mm, "Y"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_biasedFTResidualLayer{
      this,
      "biasedResidualLayer",
      "biasedResidual per FTLayer",
      {{FTConstants::nLayersTotal, -0.5, FTConstants::nLayersTotal - 0.5, "X"},
       {200, -2. * Gaudi::Units::mm, 2. * Gaudi::Units::mm, "Y"}}};
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_RMSResidualModules{
      this,
      "RMSResidualModules",
      "Mean Residual (rms-unbiased) in each module",
      {FTConstants::nModulesTotal, -0.5, FTConstants::nModulesTotal - 0.5}};
  mutable Gaudi::Accumulators::Histogram<2> m_residualPerModule{
      this,
      "residualPerModule",
      "residual per module",
      {{FTConstants::nModulesTotal, -0.5, FTConstants::nModulesTotal - 0.5, "Global Module ID"}, {50, -1., 1., "Y"}}};

  mutable Gaudi::Accumulators::Histogram<2> m_unbiasedFTResidualPerModule{
      this,
      "unbiasedresidualPerModule",
      "unbiased residual per module",
      {{FTConstants::nModulesTotal, -0.5, FTConstants::nModulesTotal - 0.5, "Global Module ID"}, {100, -1., 1., "Y"}}};

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_RMSResidualQuarters{
      this,
      "RMSResidualQuarters",
      "Mean Residual (rms-unbiased) in each quarter",
      {FTConstants::nQuartersTotal, -0.5, FTConstants::nQuartersTotal - 0.5}};
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_UnbiasedResidualModules{
      this,
      "UnbiasedResidualModules",
      "Unbiased residual in each module",
      {FTConstants::nModulesTotal, -0.5, FTConstants::nModulesTotal - 0.5}};
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_UnbiasedResidualQuarters{
      this,
      "UnbiasedResidualQuarters",
      "Unbiased residual in each quarter",
      {FTConstants::nQuartersTotal, -0.5, FTConstants::nQuartersTotal - 0.5}};
  mutable Gaudi::Accumulators::Histogram<1> m_globalSiPMIdx{
      this,
      "globalSiPMIdx",
      "globalSiPMIdx of nodes",
      {FTConstants::nSiPMsTotal, 0, FTConstants::nSiPMsTotal, "globalSiPMIdx"}};
  mutable Gaudi::Accumulators::Histogram<2> m_residualPerSiPM{
      this,
      "residualPerSiPM",
      "(biased) residual per SiPM",
      {{FTConstants::nSiPMsTotal, -0.5, FTConstants::nSiPMsTotal - 0.5, "X"}, {50, -1., 1., "Y"}}};

  // Plots split by SciFi module
  struct HistoModules {
    mutable Gaudi::Accumulators::Histogram<1> UnbiasedResidualModules_permodule;
    mutable Gaudi::Accumulators::Histogram<1> RMSResidualModules_permodule;

    HistoModules( const FTTrackMonitor* owner, unsigned int module_id )
        : UnbiasedResidualModules_permodule{owner,
                                            LHCb::Detector::FTUtils::globalModuleToLocalString( module_id ),
                                            LHCb::Detector::FTUtils::globalModuleToLocalString( module_id ),
                                            {200, -2. * Gaudi::Units::mm, 2. * Gaudi::Units::mm, "X"}}
        , RMSResidualModules_permodule{owner,
                                       LHCb::Detector::FTUtils::globalModuleToLocalString( module_id ),
                                       LHCb::Detector::FTUtils::globalModuleToLocalString( module_id ),
                                       {200, -2. * Gaudi::Units::mm, 2. * Gaudi::Units::mm, "X"}} {}
  };

  // Plots split by SciFi station
  struct StationTrackHistogram {
    mutable Gaudi::Accumulators::ProfileHistogram<1> RMSResidualModules;
    mutable Gaudi::Accumulators::ProfileHistogram<1> UnbiasedResidualModules;
    mutable Gaudi::Accumulators::Histogram<1>        x;
    mutable Gaudi::Accumulators::Histogram<1>        y;
    mutable Gaudi::Accumulators::Histogram<1>        Tx;
    mutable Gaudi::Accumulators::Histogram<1>        Ty;
    mutable Gaudi::Accumulators::Histogram<2>        pos;
    mutable Gaudi::Accumulators::Histogram<2>        slopes;

    StationTrackHistogram( const FTTrackMonitor* owner, std::string const& station )
        : RMSResidualModules{owner,
                             "RMSResidualModules" + station,
                             "Residual (rms-unbiased) in FTStation " + station,
                             {FTConstants::nModulesMax * FTConstants::nQuarters * FTConstants::nLayers, -0.5,
                              ( FTConstants::nModulesMax * FTConstants::nQuarters * FTConstants::nLayers ) - 0.5}}
        , UnbiasedResidualModules{owner,
                                  "UnbiasedResidualModules" + station,
                                  "Unbiased Residual in FTStation " + station,
                                  {FTConstants::nModulesMax * FTConstants::nQuarters * FTConstants::nLayers, -0.5,
                                   ( FTConstants::nModulesMax * FTConstants::nQuarters * FTConstants::nLayers ) - 0.5}}
        , x{owner, "x" + station, "Track x in centre FTStation " + station, {200, -3000, 3000}}
        , y{owner, "y" + station, "Track y in centre FTStation " + station, {200, -3000, 3000}}
        , Tx{owner, "tx" + station, "Track tx in centre FTStation " + station, {200, -0.2, 0.2}}
        , Ty{owner, "ty" + station, "Track ty in FTStation " + station, {200, -0.2, 0.2}}
        , pos{owner,
              "pos" + station,
              "extrapolated track position in FTStation " + station + ";x [mm]; y [mm]",
              {200, -3000., 3000.},
              {200, -3000., 3000}}
        , slopes{owner,
                 "slopes" + station,
                 "slopes in FTStation " + station + ";t_{x}; t_{y}",
                 {200, -0.2, 0.2},
                 {200, -0.2, 0.2}} {}
  };

  std::map<LHCb::Detector::FTChannelID::StationID, StationTrackHistogram> m_histograms =
      []( const FTTrackMonitor* parent ) {
        std::map<LHCb::Detector::FTChannelID::StationID, StationTrackHistogram> map;
        for ( int i : {1, 2, 3} ) {
          map.emplace( std::piecewise_construct, std::forward_as_tuple( LHCb::Detector::FTChannelID::StationID( i ) ),
                       std::forward_as_tuple( parent, "T" + std::to_string( i ) ) );
        }
        return map;
      }( this );

  std::map<unsigned int, HistoModules> m_histograms_modules;

  // Plots split by SciFi layer // for mat alignment
  struct LayerTrackHistogram {
    mutable Gaudi::Accumulators::Histogram<3>        UnbiasedresidualPerXY;
    mutable Gaudi::Accumulators::ProfileHistogram<2> UnbiasedresidualPerXY_Profile;

    LayerTrackHistogram( const FTTrackMonitor* owner, std::string const& layer )
        : UnbiasedresidualPerXY{owner,
                                "unbiasedresidualVsXY" + layer,
                                "unbiased residual per XY, layer under study" + layer,
                                {660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm},
                                {124, -2500 * Gaudi::Units::mm, 2500 * Gaudi::Units::mm},
                                {100, -8.0 * Gaudi::Units::mm, 8.0 * Gaudi::Units::mm}}
        , UnbiasedresidualPerXY_Profile{owner,
                                        "unbiasedresidualVsXY_profile" + layer,
                                        "unbiased residual profile per XY, layer under study" + layer,
                                        {660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm},
                                        {124, -2500. * Gaudi::Units::mm, 2500. * Gaudi::Units::mm}} {}
  };

  std::map<LHCb::Detector::FTChannelID::LayerID, LayerTrackHistogram> m_histograms_layer =
      []( const FTTrackMonitor* parent ) {
        std::map<LHCb::Detector::FTChannelID::LayerID, LayerTrackHistogram> map;
        for ( int i : {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11} ) {
          map.emplace( std::piecewise_construct, std::forward_as_tuple( LHCb::Detector::FTChannelID::LayerID( i ) ),
                       std::forward_as_tuple( parent, "L" + std::to_string( i ) ) );
        }
        return map;
      }( this );
};

DECLARE_COMPONENT_WITH_ID( FTTrackMonitor, "FTTrackMonitor" )

void FTTrackMonitor::operator()( const LHCb::Track::Range& tracks, const DetectorElement& lhcb ) const {
  auto& geometry = *lhcb.geometry();

  for ( const LHCb::Track* track : tracks ) {
    const auto type = track->type();
    if ( type != LHCb::Track::Types::Downstream && type != LHCb::Track::Types::Long &&
         type != LHCb::Track::Types::Ttrack ) {
      continue;
    }
    if ( track->checkFitStatus( LHCb::Track::FitStatus::Fitted ) ) { fillContainers( *track, geometry ); }
  }
}

void FTTrackMonitor::fillContainers( const LHCb::Track& track, IGeometryInfo const& geometry ) const {

  // space to put info about whole tracks
  const double     chi2NDOF    = track.chi2PerDoF();
  const double     probChi2    = track.probChi2();
  const double     ghostProb   = track.ghostProbability();
  const double     phi         = track.phi();
  const double     eta         = track.pseudoRapidity();
  const double     p           = ( track.p() ) / Gaudi::Units::GeV;  // p in GeV
  const double     pt          = ( track.pt() ) / Gaudi::Units::GeV; // p in GeV
  const auto       momentumVec = track.momentum();
  const double     px          = momentumVec.X();
  const double     py          = momentumVec.Y();
  const double     pz          = momentumVec.Z();
  Gaudi::XYZVector slopes      = track.slopes();
  const double     tx          = slopes.X();
  const double     ty          = slopes.Y();

  const std::vector<LHCb::LHCbID> ids      = track.lhcbIDs();
  const auto                      nAllHits = ids.size();
  const auto nFTHits   = std::count_if( ids.begin(), ids.end(), []( const LHCb::LHCbID& id ) { return id.isFT(); } );
  const int  tracktype = int( track.type() );

  LHCb::StateVector aState;
  extrapolator()->propagate( track, m_refVec[0], aState, geometry ).ignore();
  const double xT1 = aState.x();
  const double yT1 = aState.y();

  for ( int station : {1, 2, 3} ) {
    auto& histos = m_histograms.at( LHCb::Detector::FTChannelID::StationID( station ) );
    extrapolator()->propagate( track, m_refVec[station - 1], aState, geometry ).ignore();

    ++histos.x[aState.x()];
    ++histos.y[aState.y()];
    ++histos.Tx[aState.tx()];
    ++histos.Ty[aState.ty()];
    ++histos.pos[{aState.x(), aState.y()}];
    ++histos.slopes[{aState.tx(), aState.ty()}];
  }

  if ( m_expertMode.value() ) {
    Tuple trackTuple = nTuple( "FTTrackTuple_tracks", "" );

    trackTuple->column( "nAllHits", nAllHits ).ignore();
    trackTuple->column( "nFTHits", nFTHits ).ignore();
    trackTuple->column( "trackType", tracktype ).ignore();

    trackTuple->column( "chi2PerDoF", chi2NDOF ).ignore();
    trackTuple->column( "probChi2", probChi2 ).ignore();
    trackTuple->column( "ghostProb", ghostProb ).ignore();

    trackTuple->column( "phi", phi ).ignore();
    trackTuple->column( "eta", eta ).ignore();
    trackTuple->column( "xT1", xT1 ).ignore();
    trackTuple->column( "yT1", yT1 ).ignore();

    trackTuple->column( "p", p ).ignore();
    trackTuple->column( "pt", pt ).ignore();
    trackTuple->column( "px", px ).ignore();
    trackTuple->column( "py", py ).ignore();
    trackTuple->column( "pz", pz ).ignore();
    trackTuple->column( "tx", tx ).ignore();
    trackTuple->column( "ty", ty ).ignore();
    trackTuple->write().ignore();
  }

  if ( track.fitResult() ) {
    auto prFitResult = dynamic_cast<const LHCb::PrKalmanFitResult*>( track.fitResult() );
    if ( prFitResult ) {
      fillNodeInfo<LHCb::Pr::Tracks::Fit::Node>( track, *prFitResult );
    } else {
      auto masterFitResult = dynamic_cast<const LHCb::TrackFitResult*>( track.fitResult() );
      if ( masterFitResult )
        fillNodeInfo<LHCb::FitNode>( track, *masterFitResult );
      else {
        throw GaudiException( "Fit result is NULL - wrong type of fit result", name(), StatusCode::FAILURE );
      }
    }
  }
}

template <typename NodeType, typename FitResultType>
void FTTrackMonitor::fillNodeInfo( LHCb::Track const& track, FitResultType const& result ) const {

  for ( const auto& node : nodes( result ) ) {
    if ( !node.hasMeasurement() ) continue;
    if ( node.isHitOnTrack() && node.isFT() ) {
      LHCb::LHCbID lhcbID = id( node );
      assert( lhcbID.isFT() );

      LHCb::Detector::FTChannelID chan                  = lhcbID.ftID();
      unsigned int                station               = to_unsigned( chan.station() );
      unsigned int                uniqueLayer           = chan.globalLayerIdx();
      unsigned int                uniqueQuarter         = chan.globalQuarterIdx();
      unsigned int                uniqueModule          = chan.globalModuleIdx();
      unsigned int                uniqueModuleInStation = chan.localModuleIdx_station();
      const auto                  globalMatIdx  = chan.localMatIdx() + FTConstants::nMats * chan.globalModuleIdx();
      const auto                  globalSiPMIdx = chan.localSiPMIdx() + FTConstants::nSiPM * globalMatIdx;
      double                      residualRms   = 999;

      if ( node.errResidual2() > TrackParameters::lowTolerance ) {
        residualRms = node.residual() * std::sqrt( node.errMeasure2() / node.errResidual2() );
      }

      ++m_unbiasedFTResidual[node.unbiasedResidual()];
      ++m_biasedFTResidual[node.residual()];
      ++m_globalSiPMIdx[globalSiPMIdx];

      if ( node.errResidual2() > TrackParameters::lowTolerance ) {
        m_RMSResidualModules[uniqueModule] += residualRms;
        m_RMSResidualQuarters[uniqueQuarter] += residualRms;
      }

      m_UnbiasedResidualModules[uniqueModule] += node.unbiasedResidual();
      m_UnbiasedResidualQuarters[uniqueQuarter] += node.unbiasedResidual();

      ++m_unbiasedFTResidualLayer[{uniqueLayer, node.unbiasedResidual()}];
      ++m_biasedFTResidualLayer[{uniqueLayer, node.residual()}];
      ++m_residualPerModule[{uniqueModule, node.residual()}];
      ++m_unbiasedFTResidualPerModule[{uniqueModule, node.unbiasedResidual()}];

      // plots per module on request
      if ( m_verboseMode.value() ) {
        auto& histos_modules = m_histograms_modules.at( chan.globalModuleIdx() );
        ++histos_modules.UnbiasedResidualModules_permodule[node.unbiasedResidual()];
        if ( node.errResidual2() > TrackParameters::lowTolerance ) {
          ++histos_modules.RMSResidualModules_permodule[residualRms];
        }
      }

      // plots per layer
      auto& histos_layers = m_histograms_layer.at( LHCb::Detector::FTChannelID::LayerID( chan.globalLayerIdx() ) );
      ++histos_layers.UnbiasedresidualPerXY[{state( node ).position().x(), state( node ).position().y(),
                                             node.unbiasedResidual()}];
      histos_layers.UnbiasedresidualPerXY_Profile[{state( node ).position().x(), state( node ).position().y()}] +=
          node.unbiasedResidual();

      // plots per station
      auto& histos = m_histograms.at( chan.station() );

      if ( node.errResidual2() > TrackParameters::lowTolerance ) {
        histos.RMSResidualModules[uniqueModuleInStation] += residualRms;
      }
      histos.UnbiasedResidualModules[uniqueModuleInStation] += node.residual();

      if ( m_expertMode.value() ) {
        // Fill ntuples in expert mode only

        Tuple nodeTuple = nTuple( "FTTrackTuple_nodes", "" );

        // Node properties
        const double     phi         = track.phi();
        const double     eta         = track.pseudoRapidity();
        const double     p           = ( track.p() ) / Gaudi::Units::GeV;  // p in GeV
        const double     pt          = ( track.pt() ) / Gaudi::Units::GeV; // p in GeV
        const auto       momentumVec = track.momentum();
        const double     px          = momentumVec.X();
        const double     py          = momentumVec.Y();
        const double     pz          = momentumVec.Z();
        Gaudi::XYZVector slopes      = track.slopes();
        const double     tx          = slopes.X();
        const double     ty          = slopes.Y();

        nodeTuple->column( "p", p ).ignore();
        nodeTuple->column( "pt", pt ).ignore();
        nodeTuple->column( "px", px ).ignore();
        nodeTuple->column( "py", py ).ignore();
        nodeTuple->column( "pz", pz ).ignore();
        nodeTuple->column( "tx", tx ).ignore();
        nodeTuple->column( "ty", ty ).ignore();
        nodeTuple->column( "phi", phi ).ignore();
        nodeTuple->column( "eta", eta ).ignore();

        // Node errors
        if constexpr ( std::is_same<NodeType, LHCb::FitNode>::value ) {
          node.template visit( [&]( auto& n ) {
            for ( int i = 0; i < n.typedim; i++ ) {
              nodeTuple->column( "Error" + std::to_string( i ), n.errMeasure()[i] ).ignore();
            }
          } );
        }
        if constexpr ( std::is_same<NodeType, LHCb::Pr::Tracks::Fit::Node>::value ) {
          nodeTuple->column( "Error", node.measurement_error ).ignore();
        }

        // Residuals
        nodeTuple->column( "residual", node.residual() ).ignore();
        nodeTuple->column( "unbiasedResidual", node.unbiasedResidual() ).ignore();
        nodeTuple->column( "errResidual", node.errResidual() ).ignore();
        nodeTuple->column( "errMeasure2", node.errMeasure2() ).ignore();
        nodeTuple->column( "errResidual2", node.errResidual2() ).ignore();

        // SciFi geometry objects
        nodeTuple->column( "station", station ).ignore();
        nodeTuple->column( "globalLayerID", uniqueLayer ).ignore();
        nodeTuple->column( "globalQuarterIndex", uniqueQuarter ).ignore();
        nodeTuple->column( "globalModuleIndex", uniqueModule ).ignore();
        nodeTuple->column( "globalMatID", chan.globalMatID() ).ignore();
        nodeTuple->column( "sipmInMatRaw", chan.sipm() ).ignore();
        nodeTuple->column( "channelInSiPMraw", chan.channel() ).ignore();

        nodeTuple->column( "uniqueModuleInStation", uniqueModuleInStation ).ignore();
        nodeTuple->column( "localMatIdxInQuarter", chan.localMatIdx_quarter() ).ignore();
        nodeTuple->column( "localSiPMIdxInQuarter", chan.localSiPMIdx_quarter() ).ignore();
        nodeTuple->column( "localChannelIdxInModule", chan.localChannelIdx_quarter() ).ignore();

        // Global coordinates for node
        const Gaudi::XYZPoint nodeGlobal( state( node ).position().x(), state( node ).position().y(),
                                          state( node ).position().z() );
        LHCb::State           unbiasedState;
        if constexpr ( std::is_same<NodeType, LHCb::FitNode>::value ) {
          unbiasedState = node.template visit( [&]( auto& n ) { return n.unbiasedState( node ); } );
        }
        if constexpr ( std::is_same<NodeType, LHCb::Pr::Tracks::Fit::Node>::value ) {
          unbiasedState = node.unbiasedState();
        }
        const Gaudi::XYZPoint node_unbiasedGlobal( unbiasedState.x(), unbiasedState.y(), unbiasedState.z() );

        nodeTuple->column( "node_", nodeGlobal ).ignore();
        nodeTuple->column( "unbiasedNode_", node_unbiasedGlobal ).ignore();

        nodeTuple->write().ignore();
      }
    }
  }
}

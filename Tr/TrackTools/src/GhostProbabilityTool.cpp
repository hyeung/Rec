/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/ServiceHandle.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "TrackTools/GhostProbabilityModels.h"

template <typename Model>
class GhostProbabilityTool : public extends<GaudiTool, IGhostProbability> {
public:
  using extends::extends;

  StatusCode initialize() override {
    StatusCode scc = extends::initialize();
    m_model        = std::make_unique<Model>();
    auto buffer    = m_filesvc->read( m_weightsfilename.value() );
    scc &= m_model->load( buffer );
    return scc;
  };

  StatusCode execute( LHCb::Track& ) const override {
    // SHOULDN'T use this, otherwise you'll not use the SIMD advantages
    throw GaudiException( "This tool is meant to evaluated in batched mode, use Track container.", "LHCb::Track",
                          StatusCode::FAILURE );
    return StatusCode::FAILURE;
  };

  StatusCode execute( LHCb::Track::Container& tracks ) const override {
    // save output in track (assumes it classifies tracks (vs ghosts) not the other way around)
    auto save_output = [&]( LHCb::Track* track, LHCb::span<float> output ) -> void {
      track->setGhostProbability( 1 - output[0] );
    };

    // runs on one type of track
    auto select = [&]( LHCb::Track const* track ) -> bool { return track->type() == m_tracktype; };

    // evaulate in SIMD batches
    m_model->evaluate( tracks, select, save_output );

    return StatusCode::SUCCESS;
  };

  std::vector<std::string_view> variableNames( LHCb::Track::Types ) const override {
    std::vector<std::string_view> names;
    LHCb::Utils::unwind<0, Model::ModelType::InputVec::size>( [&]( auto k ) {
      auto const feature = m_model->features()->template get<k>();
      names.push_back( feature.name() );
    } );
    return std::vector<std::string_view>();
  };

  std::vector<float> netInputs( LHCb::Track& track ) const override {
    std::vector<float> inputs;
    LHCb::Utils::unwind<0, Model::ModelType::InputVec::size>( [&]( auto k ) {
      auto const feature = m_model->features()->template get<k>();
      inputs.push_back( feature( &track ) );
    } );
    return inputs;
  };

private:
  // properties
  Gaudi::Property<std::string> m_weightsfilename{
      this, "WeightsFileName", {}, "locations of weights files, to be read with ParamFileSvc"};

  LHCb::Track::Types           m_tracktype;
  Gaudi::Property<std::string> m_trktypesprop{this, "TrackType", "", [&]( auto& ) {
                                                if ( parse( m_tracktype, m_trktypesprop ).isFailure() )
                                                  m_tracktype = LHCb::Track::Types::Unknown;
                                              }};

private:
  std::unique_ptr<Model> m_model;

  // services
  ServiceHandle<IFileAccess> m_filesvc{this, "FileAccessor", "ParamFileSvc", "Service used to retrieve file contents"};
};

// declare specific model versions of the tool
DECLARE_COMPONENT_WITH_ID( GhostProbabilityTool<LHCb::GhostProbability::Long_noUT>, "GhostProbabilityTool_Long_noUT" )

DECLARE_COMPONENT_WITH_ID( GhostProbabilityTool<LHCb::GhostProbability::Long>, "GhostProbabilityTool_Long" )
DECLARE_COMPONENT_WITH_ID( GhostProbabilityTool<LHCb::GhostProbability::Downstream>, "GhostProbabilityTool_Downstream" )
DECLARE_COMPONENT_WITH_ID( GhostProbabilityTool<LHCb::GhostProbability::Upstream>, "GhostProbabilityTool_Upstream" )
DECLARE_COMPONENT_WITH_ID( GhostProbabilityTool<LHCb::GhostProbability::Ttrack>, "GhostProbabilityTool_Ttrack" )
DECLARE_COMPONENT_WITH_ID( GhostProbabilityTool<LHCb::GhostProbability::Velo>, "GhostProbabilityTool_Velo" )

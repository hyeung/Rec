/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TrackInterfaces/ITrackChi2Calculator.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "Event/FitNode.h"
#include "Event/State.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Event/TrackParameters.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IDetectorElement.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"

#include "TrackInterfaces/ITrackFitter.h" //Stefania

#include <functional>
#include <string>
#include <vector>

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

/**
 *  (based on OTMuonMatching by Jan Amoraal)
 *  @author Stefania Vecchi
 *  @date   2010-06-04
 */

using InputTracks = LHCb::Track::Range;

class TrackMuonMatching
    : public LHCb::Algorithm::Transformer<
          LHCb::Tracks( const InputTracks&, const InputTracks&, const DetectorElement&, const DeMuonDetector& ),
          LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DetectorElement, DeMuonDetector>> {

public:
  TrackMuonMatching( const std::string& name, ISvcLocator* pSvcLocator );

  LHCb::Tracks operator()( const InputTracks& tracks, const InputTracks& muonTracks, const DetectorElement& geo,
                           const DeMuonDetector& ) const override;

private:
  StatusCode matchChi2( LHCb::State& Track, LHCb::State& mTrack, const double& atZ, double& chi2,
                        DetectorElement const& detelem ) const;
  StatusCode longTmuonExtrap( LHCb::State* lState, const double& atZ, DetectorElement const& detelem ) const;

  auto createMatchedTrack( const LHCb::Track& longt, const LHCb::Track& muont ) const;

  Gaudi::Property<double> m_matchAtZ{this, "MatchAtZ", 12500 * Gaudi::Units::mm};
  Gaudi::Property<bool>   m_matchAtFirstMuonHit{this, "MatchAtFirstMuonHit", false};
  Gaudi::Property<double> m_matchChi2Cut{this, "MatchChi2Cut", 100.0};
  Gaudi::Property<bool>   m_allCombinations{this, "AllCombinations", true};
  Gaudi::Property<bool>   m_fitTracks{this, "FitTracks", true}; // Stefania

  using Type = LHCb::Event::Enum::Track::Type;
  Gaudi::Property<Type> m_tracktype{this, "trackType", Type::Long};

  ToolHandle<ITrackFitter>         m_trackFitter{this, "Fitter", "TrackMasterFitter/Fitter"}; // Stefania
  ToolHandle<ITrackExtrapolator>   m_extrapolator{this, "Extrapolator", "TrackLinearExtrapolator"};
  ToolHandle<ITrackChi2Calculator> m_chi2Calculator{this, "Chi2Calculator", "TrackChi2Calculator"};

  mutable Gaudi::Accumulators::Counter<> m_countMatchedTracks{this, "nMatchedTracks"};
};

using namespace LHCb;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackMuonMatching )

auto TrackMuonMatching::createMatchedTrack( const LHCb::Track& input_track, const LHCb::Track& muonTrack ) const {
  auto matchedTrack = std::make_unique<LHCb::Track>();
  matchedTrack->copy( input_track );
  if ( msgLevel( MSG::DEBUG ) )
    debug() << " =============== TrackMuonMatching - CreateMatchedTrack BEGIN" << *matchedTrack << endmsg; // Stefania
  // Remove long track's LastMeasurement state
  // or convert it in an LastFTHit if there isn't one yet
  if ( matchedTrack->hasStateAt( LHCb::State::Location::LastFTHit ) ) {
    matchedTrack->removeFromStates( matchedTrack->stateAt( State::Location::LastMeasurement ) );
  } else {
    assert( matchedTrack->stateAt( State::Location::LastMeasurement )->z() >= StateParameters::ZBegT &&
            matchedTrack->stateAt( State::Location::LastMeasurement )->z() <= StateParameters::ZEndT );
    matchedTrack->stateAt( State::Location::LastMeasurement )->setLocation( LHCb::State::Location::LastFTHit );
  }
  // Add LastMeasurement from muon track
  // and set the momentum of state LastFTHit from long track
  if ( muonTrack.hasStateAt( LHCb::State::Location::LastMeasurement ) ) {
    matchedTrack->addToStates( *( muonTrack.stateAt( State::Location::LastMeasurement ) ) );
    matchedTrack->stateAt( State::Location::LastMeasurement )
        ->setQOverP( matchedTrack->stateAt( State::Location::LastFTHit )->qOverP() );
  }
  /// Add muon ids to copied T track
  for ( LHCbID id : muonTrack.lhcbIDs() ) matchedTrack->addToLhcbIDs( id );

  if ( input_track.checkType( LHCb::Track::Types::Long ) )
    matchedTrack->setType( LHCb::Track::Types::LongMuon );
  else if ( input_track.checkType( LHCb::Track::Types::Ttrack ) )
    matchedTrack->setType( LHCb::Track::Types::SeedMuon );
  else
    throw GaudiException( "TrackMuonMatching: Track type not recognized", name(), StatusCode::FAILURE );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << " =============== TrackMuonMatching - CreateMatchedTrack END" << matchedTrack->type()
            << endmsg; // Stefania
  return matchedTrack;
}

TrackMuonMatching::TrackMuonMatching( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                    KeyValue{"MuonTracksLocation", LHCb::TrackLocation::Muon},
                    KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top},
                    KeyValue{"DeMuonDetector", DeMuonLocation::Default}},
                   KeyValue{"TracksOutputLocation", "Rec/Track/Best/TMuon"} ) {}

LHCb::Tracks TrackMuonMatching::operator()( const InputTracks& tracks, const InputTracks& muonTracks,
                                            const DetectorElement& geo, const DeMuonDetector& ) const {
  LHCb::Tracks matchedTracks;
  bool         flaglongT = false;
  if ( tracks.size() != 0 ) flaglongT = true;

  /// Now match this T track to muon tracks

  for ( InputTracks::const_iterator m = muonTracks.begin(), mEnd = muonTracks.end(); m != mEnd; ++m ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << " MuonTrack chi2 " << ( *m )->chi2PerDoF() << endmsg;
    if ( ( *m )->chi2PerDoF() > 5. ) continue;

    if ( flaglongT ) {

      double minchi2 = 10000;
      double z       = m_matchAtZ;
      if ( m_matchAtFirstMuonHit.value() ) {
        if ( ( *m )->hasStateAt( State::Location::Muon ) )
          z = ( *m )->stateAt( State::Location::Muon )->z();
        else if ( ( *m )->hasStateAt( State::Location::FirstMeasurement ) )
          z = ( *m )->stateAt( State::Location::FirstMeasurement )->z();

        verbose() << "Found muon state. Going to extrapolate to this state with z = " << z << endmsg;
      }
      int goodmatching = 0;
      /// Matched Muon-T track
      auto best_matchedTrack = std::make_unique<LHCb::Track>();
      for ( InputTracks::const_iterator t = tracks.begin(), tEnd = tracks.end(); t != tEnd; ++t ) {
        if ( !( *t )->checkType( m_tracktype ) ) continue;
        if ( !( *t )->hasT() ) continue;
        if ( ( *t )->chi2PerDoF() > 5. ) continue;
        if ( !( *t )->checkType( LHCb::Track::Types::Long ) && !( *t )->checkType( LHCb::Track::Types::Ttrack ) )
          continue;

        double chi2 = -9999.0;
        /// Get the Track state closest to this z. Make a copy such that it can be changed.
        auto lState = ( *t )->closestState( z );
        /// Get the Muon state closest to this z
        auto mState = ( *m )->closestState( z );
        //  Calculate match chi2
        StatusCode sc = matchChi2( lState, mState, z, chi2, geo );

        if ( sc.isSuccess() && chi2 > -1.0 && chi2 < m_matchChi2Cut.value() ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "chi2 Matching is " << chi2 << endmsg;

          // create the matched track object
          auto matchtrack = createMatchedTrack( *( *t ), *( *m ) );
          // make sure to add a new state to the track at the match location
          lState.setLocation( State::Location::Muon );
          matchtrack->addToStates( lState );

          if ( m_allCombinations.value() ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " Insert matchedTrack" << endmsg;
            matchedTracks.insert( matchtrack.release() );
          } else {
            if ( chi2 < minchi2 ) {
              minchi2 = chi2;
              best_matchedTrack.reset( matchtrack.release() );
            }
          }
          ++goodmatching;
        } else {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "matching failed " << chi2 << endmsg;
          sc = StatusCode::SUCCESS;
        }
      }
      if ( msgLevel( MSG::DEBUG ) ) debug() << " Good Matching " << goodmatching << "/ " << tracks.size() << endmsg;
      if ( !m_allCombinations.value() && minchi2 < m_matchChi2Cut.value() ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << " Insert BEST matchedTrack" << endmsg;
        matchedTracks.insert( best_matchedTrack.release() );
        ++m_countMatchedTracks;
      }
    }
  }
  if ( msgLevel( MSG::DEBUG ) )
    debug() << " =============== TrackMuonMatching found " << matchedTracks.size() << endmsg;
  // Stefania
  if ( m_fitTracks ) {
    for ( auto& track : matchedTracks ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Fit the LongMuon track " << endmsg;
      auto sc = ( *m_trackFitter )( *track, *geo.geometry(), LHCb::Tr::PID::Muon() ); // Fit the track
      if ( !sc.isSuccess() ) { Warning( "LongMuon track fit FAILED ", StatusCode::FAILURE, 5 ).ignore(); }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Fit status " << track->fitStatus() << " track type " << track->type() << endmsg;
    }
  }
  return matchedTracks;
}

StatusCode TrackMuonMatching::matchChi2( LHCb::State& lState, LHCb::State& mState, const double& atZ, double& chi2,
                                         DetectorElement const& detelem ) const {
  StatusCode sc = m_extrapolator->propagate( lState, atZ, *detelem.geometry() );
  if ( !sc.isSuccess() ) { return Warning( "Could not propagate Track state", StatusCode::FAILURE, 5 ); }

  sc = m_extrapolator->propagate( mState, atZ, *detelem.geometry() );
  if ( !sc.isSuccess() ) { return Warning( "Could not propagate Muon state", StatusCode::FAILURE, 5 ); }

  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "Extrapolated Track state to z = " << atZ << " is " << lState << endmsg
              << "Extrapolated Muon state to z = " << atZ << " is " << mState << endmsg;
  }

  /// Now calculate the match chi2
  sc = m_chi2Calculator->calculateChi2( lState.stateVector(), lState.covariance(), mState.stateVector(),
                                        mState.covariance(), chi2 );
  if ( !sc.isSuccess() ) Error( "Could not invert matrices", StatusCode::FAILURE ).ignore();

  return sc;
}

StatusCode TrackMuonMatching::longTmuonExtrap( LHCb::State* lState, const double& atZ,
                                               DetectorElement const& detelem ) const {
  /// Extrapolate states
  StatusCode sc = m_extrapolator->propagate( ( *lState ), atZ, *detelem.geometry() );
  if ( !sc.isSuccess() ) Warning( "Could not propagate Track state", StatusCode::FAILURE, 5 ).ignore();
  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "Extrapolated Track state to z = " << atZ << " is " << ( *lState ) << endmsg;
  }
  return sc;
}

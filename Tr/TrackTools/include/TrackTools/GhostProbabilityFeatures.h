/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Track.h"
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/VectorizedML/Feature.h"
#include "TrackKernel/TrackFunctors.h"

namespace LHCb::GhostProbability {

  namespace Features {

    struct FitVeloChi2 : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const {
        return track->info( LHCb::Track::AdditionalInfo::FitVeloChi2, -999 );
      }

    private:
      static constexpr auto m_name = "FitVeloChi2";
    };

    struct FitTChi2 : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const {
        return track->info( LHCb::Track::AdditionalInfo::FitTChi2, -999 );
      }

    private:
      static constexpr auto m_name = "FitTChi2";
    };

    struct FitTNDOF : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const {
        return track->info( LHCb::Track::AdditionalInfo::FitTNDoF, -999 );
      }

    private:
      static constexpr auto m_name = "FitTNDOF";
    };

    struct FitTChi2PerNDOF : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const {
        auto chi2 = track->info( LHCb::Track::AdditionalInfo::FitTChi2, -999 );
        auto ndof = track->info( LHCb::Track::AdditionalInfo::FitTNDoF, -999 );
        return ( chi2 > 0 && ndof > 0 ) ? chi2 / ndof : 1000.f;
      }

    private:
      static constexpr auto m_name = "FitTChi2PerNDOF";
    };

    struct FitMatchChi2 : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const {
        return track->info( LHCb::Track::AdditionalInfo::FitMatchChi2, -999 );
      }

    private:
      static constexpr auto m_name = "FitMatchChi2";
    };

    struct Chi2 : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return track->chi2(); }

    private:
      static constexpr auto m_name = "Chi2";
    };

    struct NDOF : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return track->nDoF(); }

    private:
      static constexpr auto m_name = "NDOF";
    };

    struct Chi2PerNDOF : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return track->chi2PerDoF(); }

    private:
      static constexpr auto m_name = "Chi2PerNDOF";
    };

    struct Pt : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return track->pt() * 1e-3; }

    private:
      static constexpr auto m_name = "Pt";
    };

    struct LogPt : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return LHCb::Math::fast_log( track->pt() ); }

    private:
      static constexpr auto m_name = "LogPt";
    };

    struct Eta : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return track->pseudoRapidity(); }

    private:
      static constexpr auto m_name = "Eta";
    };

    struct nUTOutliers : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const {
        auto const* fit = LHCb::Event::v1::fitResult( *track );
        return fit ? fit->nMeasurements<LHCb::Measurement::UT>() - fit->nActiveMeasurements<LHCb::Measurement::UT>()
                   : 0.;
      }

    private:
      static constexpr auto m_name = "nUTOutliers";
    };

    struct nVPHits : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return track->nVPHits(); }

    private:
      static constexpr auto m_name = "nVPHits";
    };

    struct nUTHits : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return track->nUTHits(); }

    private:
      static constexpr auto m_name = "nUTHits";
    };

    struct nFTHits : LHCb::VectorizedML::Feature {
      const char* name() const { return m_name; }
      float       operator()( LHCb::Track const* track ) const { return track->nFTHits(); }

    private:
      static constexpr auto m_name = "nFTHits";
    };

  } // namespace Features

} // namespace LHCb::GhostProbability

/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PrVeloTracks.h"
#include "Event/PrimaryVertices.h"
#include "Event/RecVertex.h"
#include <boost/container/static_vector.hpp>

/*
A collection of structures and functions useful for the SOA PV finders.
*/

namespace LHCb::TrackPVFinder {
  using namespace LHCb::Event::PV;
  using simd    = SIMDWrapper::best::types;
  using float_v = simd::float_v;
  using int_v   = simd::int_v;

  /// Some default values of parameters used by different PV algorithms
  constexpr float    defaultMinZ                     = -600 * Gaudi::Units::mm;
  constexpr float    defaultMaxZ                     = +600 * Gaudi::Units::mm;
  constexpr float    defaultMaxDeltaChi2             = 12;
  constexpr float    defaultMaxDeltaZConverged       = 0.001 * Gaudi::Units::mm;
  constexpr float    defaultMaxDeltaChi2Converged    = 0.01;
  constexpr uint16_t defaultMaxFitIter               = 10;
  constexpr float    defaultMinVertexZSeparation     = 0.5f * Gaudi::Units::mm;
  constexpr float    defaultMinVertexZSeparationChi2 = 16;
  constexpr uint16_t defaultMinNumTracks             = 4;

  /// square
  namespace {
    template <typename FTYPE>
    inline auto sqr( FTYPE x ) {
      return x * x;
    }
  } // namespace

  /// (re)fill the PrimaryVertex data object from the velo tracks. This needs the 'pvtrackindex' column to be filled,
  /// since that is used for navigation.
  void populateFromVeloTracks( PrimaryVertexContainer& pvdata );

  /// Fill PrimaryVertex data object from RecVertices and associated tracks
  void populateFromRecVertices( PrimaryVertexContainer& pvcontainer, const RecVertex::Range& recvertices );

  /// Reassigns the labels from the PV tracks to the vertices. This is needed if a selection has been made to the
  /// vertices.
  inline void resetTrackPVIndices( PrimaryVertexContainer& pvcontainer ) {
    auto pvtracks = pvcontainer.tracks.scalar();
    for ( size_t i = 0; i < pvcontainer.vertices.size(); ++i ) {
      const auto& vertex = pvcontainer.vertices[i];
      // tricky: we do not want to reset indices -1, so we need to do this scalar or with a mask
      for ( auto offset = vertex.begin(); offset < vertex.end(); ++offset )
        if ( pvtracks[offset].field<PVTrackTag::pvindex>().get().cast() != -1 )
          pvtracks[offset].field<PVTrackTag::pvindex>().set( i );
    }
  }

  /// Refits the list of PVs using a fit configuration objects
  inline void fitAdaptive( PrimaryVertexContainer& pvcontainer, const AdaptiveFitConfig config ) {
    fitAdaptive( pvcontainer.vertices, pvcontainer.tracks, config );
  }

  /// Reassigns the tracks and refit the vertices
  inline void reassignAndRefit( PrimaryVertexContainer& pvcontainer, const AdaptiveFitConfig config ) {
    resetTrackPVIndices( pvcontainer );
    fitAdaptive( pvcontainer, config );
  }

  /// Merges vertices that are close. Returns true if the number of vertices has changed.
  inline void mergeCloseVertices( PrimaryVertexContainer& pvcontainer, const double minVertexZSeparation,
                                  const double minVertexZSeparationChi2, const AdaptiveFitConfig fitconfig ) {
    const auto&                                               vertices = pvcontainer.vertices;
    boost::container::small_vector<ExtendedPrimaryVertex, 16> mergedvertices;
    mergedvertices.reserve( vertices.size() );
    mergedvertices.push_back( vertices.front() );
    for ( size_t i = 1; i < vertices.size(); ++i ) {
      const auto& ivertex = vertices[i];
      auto&       jvertex = mergedvertices.back();
      const auto  deltaz  = ivertex.position().z() - jvertex.position().z();
      const auto  icovz   = ivertex.covMatrix()( 2, 2 );
      const auto  jcovz   = jvertex.covMatrix()( 2, 2 );
      const auto  covz    = icovz + jcovz;
      if ( ( std::abs( deltaz ) < minVertexZSeparation ) || ( deltaz * deltaz < covz * minVertexZSeparationChi2 ) ) {
        const auto begin = jvertex.begin();
        if ( jcovz > icovz ) jvertex.setPosition( ivertex.position() );
        jvertex.setRange( begin, ivertex.end() );
      } else {
        mergedvertices.push_back( ivertex );
      }
    }

    if ( vertices.size() != mergedvertices.size() ) {
      // pvcontainer.vertices.swap(mergedvertices) ; // cannot swap a vector and a small_vector
      pvcontainer.vertices.clear();
      std::copy( mergedvertices.begin(), mergedvertices.end(), std::back_inserter( pvcontainer.vertices ) );
      reassignAndRefit( pvcontainer, fitconfig );
    }
  }

  /// Applies a selection to the PVs in the PV container. Will also redirect the tracks and redo the vertex fit in order
  /// to get all ips and derivatives right.
  template <typename Selector>
  void applyPVSelection( PrimaryVertexContainer& pvcontainer, const AdaptiveFitConfig fitconfig,
                         const Selector select ) {
    // Remove any vertices that do not pass the selection. We assign their tracks to the next or previous vertex,
    // depending on which one is closer.
    auto&                                                     vertices = pvcontainer.vertices;
    boost::container::small_vector<ExtendedPrimaryVertex, 16> selectedvertices;
    selectedvertices.reserve( vertices.size() );
    for ( size_t i = 0; i < vertices.size(); ++i ) {
      const auto& vertex = vertices[i];
      if ( select( vertex ) ) {
        selectedvertices.push_back( vertex );
      } else {
        const bool prevexists = selectedvertices.size() > 0;
        const bool nextexists = i + 1 < vertices.size();
        if ( prevexists && nextexists ) {
          // add to the closest one
          const auto prevdz = selectedvertices.back().position().z() - vertex.position().z();
          const auto nextdz = vertices[i + 1].position().z() - vertex.position().z();
          if ( std::abs( prevdz ) < std::abs( nextdz ) ) {
            selectedvertices.back().setRange( selectedvertices.back().begin(), vertex.end() );
          } else {
            vertices[i + 1].setRange( vertex.begin(), vertices[i + 1].end() );
          }
        } else if ( prevexists ) {
          selectedvertices.back().setRange( selectedvertices.back().begin(), vertex.end() );
        } else if ( nextexists ) {
          vertices[i + 1].setRange( vertex.begin(), vertices[i + 1].end() );
        }
      }
    }

    if ( selectedvertices.size() < vertices.size() ) {
      vertices.clear();
      std::copy( selectedvertices.begin(), selectedvertices.end(), std::back_inserter( vertices ) );
      reassignAndRefit( pvcontainer, fitconfig );
    }
  }

  inline void applyBeamlineSelection( PrimaryVertexContainer& pvcontainer, const size_t minNumTracksPerVertex,
                                      const double minTotalTrackWeightPerVertex, const double maxVertexRho,
                                      const double beamlineX, const double beamlineY, const double beamlineTx,
                                      const double beamlineTy, const AdaptiveFitConfig fitconfig ) {
    applyPVSelection( pvcontainer, fitconfig, [=]( const auto& vertex ) {
      const auto beamlinedx    = vertex.position().x() - ( beamlineX + beamlineTx * vertex.position().z() );
      const auto beamlinedy    = vertex.position().y() - ( beamlineY + beamlineTy * vertex.position().z() );
      const auto beamlinerho2  = sqr( beamlinedx ) + sqr( beamlinedy );
      const auto maxVertexRho2 = sqr( maxVertexRho );
      return vertex.nDoF() > 0 && vertex.nTracks() >= int( minNumTracksPerVertex ) &&
             vertex.sumOfWeights() >= minTotalTrackWeightPerVertex && beamlinerho2 < maxVertexRho2;
    } );
  }

  /// Helper function to 'extend' size such that it is multiple of simd::size
  template <typename T>
  T paddedsize( T size ) {
    // https://stackoverflow.com/questions/3407012/rounding-up-to-the-nearest-multiple-of-a-number#comment45943499_4073700
    const auto factor = simd::size;
    return size + factor - 1 - ( size + factor - 1 ) % factor;
  }

  /// Resets the contents of pv tracks such that also the padded part is okay for the arithmetics (e.g. no divisions by
  /// zero): That way we don't need to worry about masks.
  template <bool resetnavigation = true>
  void reset( LHCb::Event::PV::PVTracks& pvtracks ) {
    //  fill the padding: we can save time here by only filling the padded parts and not everything
    const auto N = pvtracks.capacity(); // paddedsize(pvtracks.size()) ;
    std::fill_n( pvtracks.data<PVTrackTag::z>(), N, 0 );
    std::fill_n( pvtracks.data<PVTrackTag::x>(), N, 0 );
    std::fill_n( pvtracks.data<PVTrackTag::y>(), N, 0 );
    std::fill_n( pvtracks.data<PVTrackTag::tx>(), N, 0 );
    std::fill_n( pvtracks.data<PVTrackTag::ty>(), N, 0 );
    std::fill_n( pvtracks.data<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xx ), N, 1 );
    std::fill_n( pvtracks.data<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xtx ), N, 0 );
    std::fill_n( pvtracks.data<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::txtx ), N, 1 );
    std::fill_n( pvtracks.data<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xx ), N, 1 );
    std::fill_n( pvtracks.data<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xtx ), N, 0 );
    std::fill_n( pvtracks.data<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::txtx ), N, 1 );
    std::fill_n( pvtracks.data<PVTrackTag::veloid>(), N, -1 );
    if constexpr ( resetnavigation ) {
      std::fill_n( pvtracks.data<PVTrackTag::pvindex>(), N, -1 );
      std::fill_n( pvtracks.data<PVTrackTag::veloindex>(), N, -1 );
    }
    // These we should actually not need anymore (FIXME)
    std::fill_n( pvtracks.data<PVTrackTag::weight>(), N, 0. );
    std::fill_n( pvtracks.data<PVTrackTag::Wx>(), N, 0. );
    std::fill_n( pvtracks.data<PVTrackTag::Wy>(), N, 0. );
    for ( int i = 0; i < 3; ++i ) std::fill_n( pvtracks.data<PVTrackTag::halfDChi2DX>( i ), N, 0. );
    for ( int i = 0; i < 5; ++i ) std::fill_n( pvtracks.data<PVTrackTag::halfD2Chi2DX2>( i ), N, 0. );
  }

  /// Helper function to copy the relevant contents of a PrVeloTrack to a PV::PVTrack
  template <typename PVTrack>
  void populateFromPrVelo( PVTrack& pvtrack ) {
    const auto& velotrack = pvtrack.veloTrack();
    const auto  pos       = velotrack.StatePos( 0 );
    const auto  dir       = velotrack.StateDir( 0 );
    const auto  covX      = velotrack.StateCovX( 0 );
    const auto  covY      = velotrack.StateCovY( 0 );
    pvtrack.template field<PVTrackTag::z>().set( pos.z() );
    pvtrack.template field<PVTrackTag::x>().set( pos.x() );
    pvtrack.template field<PVTrackTag::y>().set( pos.y() );
    pvtrack.template field<PVTrackTag::tx>().set( dir.x() );
    pvtrack.template field<PVTrackTag::ty>().set( dir.y() );
    pvtrack.template field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xx ).set( covX.x() );
    pvtrack.template field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xtx ).set( covX.y() );
    pvtrack.template field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::txtx ).set( covX.z() );
    pvtrack.template field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xx ).set( covY.x() );
    pvtrack.template field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xtx ).set( covY.y() );
    pvtrack.template field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::txtx ).set( covY.z() );
    pvtrack.template field<PVTrackTag::veloid>().set( uniqueVeloSegmentIDFromPrVelo( velotrack ) );
  }

  /// Initializes a PV container from seedvertices and seedtracks. This takes a SOA container for the pvseedtracks as
  /// input. However, the code isn't vectorized, so we could as well make a version that just takes a normal stl
  /// container.
  template <typename SeedVertices, typename SeedTracks>
  void initializeFromSeeds( PrimaryVertexContainer& pvcontainer, const SeedVertices& seeds,
                            const SeedTracks& pvseedtracks, const AdaptiveFitConfig fitconfig ) {
    auto& vertices     = pvcontainer.vertices;
    auto& pvtracks     = pvcontainer.tracks;
    auto& velotrackmap = pvcontainer.velotrackmap; // indices from velo tracks to PV tracks

    // first count for every seed how many tracks are assigned. we need this to allocate space
    boost::container::small_vector<uint16_t, 16> ntracks( seeds.size(), 0 );
    for ( const auto& track : pvseedtracks.scalar() )
      ++ntracks[track.template field<PVTrackTag::pvindex>().get().cast()];

    // Initialize the vertices, setting the index for the first track
    vertices.clear();
    vertices.reserve( seeds.size() );
    uint32_t trackoffset{0};
    int      iclus{0};
    for ( const auto& seed : seeds ) {
      auto& vtx = vertices.emplace_back( Gaudi::XYZPoint{seed} );
      vtx.setRange( trackoffset, trackoffset );
      // Add padding at the end of the vertex: not stricly needed but otherwise we cannot 'refit' individual vertices.
      // see:
      // https://stackoverflow.com/questions/3407012/rounding-up-to-the-nearest-multiple-of-a-number#comment45943499_4073700
      trackoffset += paddedsize( ntracks[iclus++] );
    }
    // Initialize pv tracks with sensible values such that arithmatics doesn't fail on the padded parts
    pvtracks.reserve( trackoffset + simd::size ); // make sure we have some extra
    pvtracks.resize( trackoffset );
    reset( pvtracks );
    // Sort by PV and copy the relevant information from the PrVeloTracks
    velotrackmap.resize( pvseedtracks.size(), -1 );
    auto pvtracksscalar = pvtracks.scalar();
    for ( const auto& pvseedtrack : pvseedtracks.scalar() ) {
      // read the position from the vertex
      const auto pvindex = pvseedtrack.template get<PVTrackTag::pvindex>().cast();
      auto&      vertex  = vertices[pvindex];
      auto       offset  = vertex.end();
      // point the pv track to the right velo segment (this we can probably remove)
      auto pvtrack = pvtracksscalar[offset];
      pvtrack.template field<PVTrackTag::veloindex>().set(
          std::get<0>( pvseedtrack.template field<PVTrackTag::veloindex>().index() ),
          std::get<1>( pvseedtrack.template field<PVTrackTag::veloindex>().index() ) );
      pvtrack.template field<PVTrackTag::pvindex>().set( pvindex );
      // copy relevant info from the track: note that this only works
      // if the pvcontainer.tracks.prvelocontainers have been set. we
      // can also pass them as function arguments to simplify this.
      populateFromPrVelo( pvtrack );
      // pvtracksscalar[pvseedtrack.offset()].field<PVTrackTag::pvtrackindex>().set( offset ) ;
      velotrackmap[pvseedtrack.offset()] = offset;
      // update the vertex
      vertex.setSize( vertex.size() + 1 );
    }

    // refit the vertices
    fitAdaptive( pvcontainer, fitconfig );
  }

} // namespace LHCb::TrackPVFinder

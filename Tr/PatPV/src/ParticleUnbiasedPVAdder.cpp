/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "GaudiKernel/KeyedContainer.h"
#include "LHCbAlgs/Transformer.h"
#include "TrackKernel/PrimaryVertexUtils.h"

#include "TrackPVFinderUtils.h"
using namespace LHCb::TrackPVFinder;

using Particles         = LHCb::Particles;
using PrimaryVertex     = LHCb::PrimaryVertex;
using OutputPVContainer = LHCb::PrimaryVertex::Container;
using Output_t          = std::tuple<LHCb::Particles, OutputPVContainer>;

struct ParticleUnbiasedPVAdder : public LHCb::Algorithm::MultiTransformer<Output_t(
                                     LHCb::Particle::Range const&, LHCb::Event::PV::PrimaryVertexContainer const& )> {
  Gaudi::Property<uint16_t> m_minNumTracksPerVertex{this, "MinNumTracksPerVertex", defaultMinNumTracks,
                                                    "Min number of tracks per PV"};
  Gaudi::Property<uint16_t> m_maxFitIter{this, "MaxFitIter", defaultMaxFitIter,
                                         "Maximum number of iterations for vertex fit"};
  Gaudi::Property<float> m_maxDeltaChi2{this, "MaxDeltaChi2", -1, "Maximum chi2 contribution of track to vertex fit"};
  Gaudi::Property<float> m_maxDeltaZConverged{this, "MaxDeltaZConverged", defaultMaxDeltaZConverged,
                                              "Limit on change in z to determine if vertex fit has converged"};
  Gaudi::Property<float> m_maxDeltaChi2Converged{this, "MaxDeltaChi2Converged", defaultMaxDeltaChi2Converged,
                                                 "Limit on change in chi2 to determine if vertex fit has converged"};
  Gaudi::Property<int>   m_mode{this, "Mode", 1, "The unbias mode (0:unbias,1:refit,2:unimplemented)"};

  mutable Gaudi::Accumulators::Counter<> m_unbiased{this, "PV unbiasing successful"};

  ParticleUnbiasedPVAdder( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer(
            name, pSvcLocator,
            {KeyValue{"InputParticles", ""}, KeyValue{"PrimaryVertices", LHCb::Event::PV::DefaultLocation}},
            {KeyValue{"OutputParticles", ""}, KeyValue{"UnbiasedPrimaryVertices", ""}} ) {}

  Output_t operator()( LHCb::Particle::Range const&                   inputparticles,
                       LHCb::Event::PV::PrimaryVertexContainer const& pvs ) const override {
    // FIXME: This code is not yet finished: If we unbias a PV with too low multiplicity, then we should find the next
    // best PV.
    Output_t output;
    auto&    outputparticles     = std::get<0>( output );
    auto&    unbiasedpcvontainer = std::get<1>( output );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "In ParticleUnbiasedPVAdder" << endmsg;

    LHCb::Event::PV::AdaptiveFitConfig fitconfig;
    fitconfig.maxDeltaChi2          = m_maxDeltaChi2.value() > 0 ? m_maxDeltaChi2.value() : pvs.m_maxDeltaChi2;
    fitconfig.maxDeltaZConverged    = m_maxDeltaZConverged;
    fitconfig.maxDeltaChi2Converged = m_maxDeltaChi2Converged;
    fitconfig.maxFitIter            = m_maxFitIter;

    // Throw an error of the pvs do not have tracks: That points to a configuration problem
    if ( !pvs.empty() && pvs.tracks.empty() )
      throw GaudiException( "Input PV container has no tracks. Cannot unbias.", "ParticleUNbiasedPVAdder::operator()",
                            StatusCode::FAILURE );

    auto n_unbiased = m_unbiased.buffer();
    for ( const auto& inputparticle : inputparticles ) {
      // copy the particle
      auto* outputparticle = new LHCb::Particle{*inputparticle};
      outputparticles.insert( outputparticle, inputparticle->key() );

      if ( pvs.size() > 0 ) {
        // get the velo segment IDs
        const auto veloids = LHCb::collectVeloSegmentIDs( *inputparticle );

        // 1. copy the PVs
        boost::container::small_vector<const ExtendedPrimaryVertex*, 16> selectedpvs{pvs.size()};
        std::transform( pvs.begin(), pvs.end(), selectedpvs.begin(), []( const auto& i ) { return &i; } );

        while ( selectedpvs.size() > 0 ) {
          // 2. get the best PV
          auto bestpv = LHCb::bestPV( selectedpvs, *inputparticle );
          // 3. unbias it
          const int pvindex = bestpv->key();
          auto      unbiasedPV =
              m_mode == 0
                  ? LHCb::Event::PV::unbiasedVertex( pvs, pvindex, LHCb::span<const uint32_t>{veloids} )
                  : LHCb::Event::PV::refittedVertex( pvs, pvindex, LHCb::span<const uint32_t>{veloids}, fitconfig );
          // 4. check that it has sufficient tracks left
          if ( unbiasedPV.nTracks() >= m_minNumTracksPerVertex || selectedpvs.size() == 1 ) {

            if ( msgLevel( MSG::DEBUG ) ) {
              // check the number of velo ids in this PV
              int         numInPV{0}, numNonzeroWeight{0};
              const auto& themap = bestpv->veloSegmentIDMap();
              for ( const auto& id : veloids ) {
                const auto it = themap.find( id );
                if ( it != themap.end() ) {
                  ++numInPV;
                  if ( pvs.tracks.scalar()[it->second].field<PVTrackTag::weight>().get() > 0 ) ++numNonzeroWeight;
                }
              }
              debug() << "Adding unbiased PV: "
                      << " pvindex: " << pvindex
                      << " ; number of velo ids (tot/in pv/non-zero weight): " << veloids.size() << "/" << numInPV
                      << "/" << numNonzeroWeight << " ; number of dofs (before/after): " << pvs[pvindex].nDoF() << "/"
                      << unbiasedPV.nDoF() << " ; position (before/after): " << pvs[pvindex].position() << "/"
                      << unbiasedPV.position() << endmsg;
            }
            // Inspiration for the future
            // auto refittedPVs = refittedVertices( pvs,
            // veloids,m_maxDeltaChi2,m_maxDeltaZConverged,m_maxDeltaChi2Converged,m_maxFitIter) ; const auto pvindex =
            // LHCb::bestPVIndex(refittedPVs,*inputparticle) ;
            const auto unbiasedPVp = new PrimaryVertex{std::move( unbiasedPV )};
            unbiasedpcvontainer.add( unbiasedPVp );
            outputparticle->setPV( unbiasedPVp );
            n_unbiased++;
            break;
          } else {
            // remove the PV from the selected PVs list
            selectedpvs.erase( std::remove( selectedpvs.begin(), selectedpvs.end(), bestpv ), selectedpvs.end() );
          }
        }
      }
    }
    return output;
  }
};

DECLARE_COMPONENT( ParticleUnbiasedPVAdder )

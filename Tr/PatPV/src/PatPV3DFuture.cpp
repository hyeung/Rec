/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/Condition.h"
#include "Event/RecVertex.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbDet/InteractionRegion.h"

#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include <assert.h>
#include <string>
#include <vector>

namespace {

  // auxiliary class for searching of clusters of tracks in 2D seeding
  struct vtxCluster1D final {
    double z          = 0; // z of the cluster
    double sigsq      = 0; // sigma**2 of the cluster
    double sigsqmin   = 0; // minimum sigma**2 of the tracks forming cluster
    int    ntracks    = 0; // number of tracks in the cluster
    bool   not_merged = 0; // flag for iterative merging
  };

  // auxiliary class for searching of clusters of tracks in 3D seeding
  struct vtxCluster3D {
    Gaudi::XYZPoint position;           // position
    Gaudi::XYZPoint error;              // uncertainty
    int             ntracks    = 0;     // number of tracks in the cluster
    bool            not_merged = false; // flag for iterative merging
  };

  bool seedcompZ( const vtxCluster3D& first, const vtxCluster3D& second ) {
    return ( first.position.Z() > second.position.Z() );
  }

  bool seedcomp( const vtxCluster3D& first, const vtxCluster3D& second ) {
    if ( first.ntracks > second.ntracks ) return true;
    if ( first.ntracks < second.ntracks ) return false;
    return seedcompZ( first, second );
  }

  struct seedTrack {
    const LHCb::Track* lbtrack = nullptr;
    bool               used    = false;

    seedTrack( const LHCb::Track* track ) : lbtrack( track ), used( false ) {}
  };

  struct closeNode {
    Gaudi::XYZPoint closest_point;
    seedTrack*      seed_track = nullptr;
    bool            take       = false;
  };

  class PVTrack final {
  public:
    using Track           = LHCb::Event::v1::Track;
    const Track* refTrack = nullptr;
    // Current state of the track at the current point
    LHCb::State stateG;
    // Normalized vector of slope
    Gaudi::XYZVector unitVect;
    // Flag if the track has been used in a previous vertex
    bool isUsed = false;

    // Result for impact parameter
    Gaudi::XYZVector vd0;        // Impact parameter vector
    double           d0sq   = 0; // Impact parameter squared
    double           err2d0 = 0; // IP error squared
    double           chi2   = 0; // chi2 = d02 / d0err**2
    double           weight = 0; // Weight assigned to track
    Track::Types     type   = Track::Types::Velo;

    double zClose() const {
      return ( stateG.z() - ( unitVect.z() * ( unitVect.x() * stateG.x() + unitVect.y() * stateG.y() ) /
                              ( 1.0 - std::pow( unitVect.z(), 2 ) ) ) );
    }
  };

  using PVTracks    = std::vector<PVTrack>;
  using PVTrackPtrs = std::vector<PVTrack*>;

  struct PVVertex final {
    PVTrackPtrs     pvTracks;
    LHCb::RecVertex primVtx;
  };

} // namespace

/**
 *  Algorithm to find the primary vertices at the HLT.
 *
 *  This version is temporary and should be dropped and replaced
 *  with TrackBeamLineVertexFinder when it is ready. Its only purpose
 *  is to provide a PatPV using Tracks v2 and RecVertex v2 in the
 *  mean time.
 *  Note that this class integrates directly the code of the tools
 *  previously used by PatPV3D
 */
using TrackContainer = LHCb::Event::v1::Track::Range;
class PatPV3DFuture
    : public LHCb::Algorithm::Transformer<LHCb::RecVertices( const TrackContainer&,
                                                             LHCb::Conditions::InteractionRegion const& ),
                                          LHCb::DetDesc::usesConditions<LHCb::Conditions::InteractionRegion>> {

public:
  using RecVertex   = LHCb::RecVertex;
  using RecVertices = LHCb::RecVertices;
  using Track       = LHCb::Event::v1::Track;

  /// Initialization
  StatusCode initialize() override;
  PatPV3DFuture( const std::string& name, ISvcLocator* pSvcLocator );

  RecVertices operator()( const TrackContainer&, LHCb::Conditions::InteractionRegion const& ) const override;

private:
  ////////  From PVOfflineTool
  PatPV3DFuture::RecVertices reconstructMultiPV( const TrackContainer&  inputTracks,
                                                 Gaudi::XYZPoint const& beamSpot ) const;

  PatPV3DFuture::RecVertices reconstructMultiPVFromTracks( std::vector<const PatPV3DFuture::Track*>& tracks2use,
                                                           Gaudi::XYZPoint const&                    beamSpot ) const;

  void removeTracks( std::vector<const PatPV3DFuture::Track*>&       tracks,
                     const std::vector<const PatPV3DFuture::Track*>& tracks2remove ) const;

  ////////  From AdaptivePV3DFitter
  StatusCode fitVertex( const Gaudi::XYZPoint& seedPoint, const std::vector<const Track*>& tracks,
                        PatPV3DFuture::RecVertices& outputVtxVec, std::vector<const Track*>& tracks2remove ) const;

  // Get Tukey's weight
  double getTukeyWeight( double trchi2, int iter ) const;

  ////////  Seeding 2D
  std::vector<Gaudi::XYZPoint> getSeeds2D( const std::vector<const PatPV3DFuture::Track*>& inputTracks,
                                           const Gaudi::XYZPoint&                          beamspot ) const;
  // Seeding 3D
  std::vector<Gaudi::XYZPoint> getSeeds3D( const std::vector<const PatPV3DFuture::Track*>& inputTracks ) const;

  std::vector<double> findClusters( std::vector<vtxCluster1D>& vclus ) const;
  bool                mergeClusters( std::vector<vtxCluster3D>& vclus ) const;
  double              errorForPVSeedFinding( double tx, double ty ) const;

private:
  ////////  From PVOfflineTool
  Gaudi::Property<bool>   m_refitpv{this, "RefitPV", false, "Flag to refit PVs when converting to type PrimaryVertex"};
  Gaudi::Property<bool>   m_requireVelo{this, "RequireVelo", true, "Option to use tracks with VELO segment only"};
  Gaudi::Property<double> m_pvsChi2Separation{this, "PVsChi2Separation", 25.};
  Gaudi::Property<double> m_pvsChi2SeparationLowMult{this, "PVsChi2SeparationLowMult", 91.};
  Gaudi::Property<bool>   m_useBeamSpotRCut{this, "UseBeamSpotRCut", false};
  Gaudi::Property<double> m_beamSpotRCut{this, "BeamSpotRCut", 0.2};
  Gaudi::Property<double> m_beamSpotRCutHMC{this, "BeamSpotRHighMultiplicityCut", 0.4};
  Gaudi::Property<unsigned int> m_beamSpotRMT{this, "BeamSpotRMultiplicityTreshold", 10};
  Gaudi::Property<double>       m_resolverBound{this, "ResolverBound", 5 * Gaudi::Units::mm};
  Gaudi::Property<float> m_beamLineOffsetX{this, "BeamLineOffsetX", 0.0f, "X correction applied to beamline position"};
  Gaudi::Property<float> m_beamLineOffsetY{this, "BeamLineOffsetY", 0.0f, "Y correction applied to beamline position"};

  ////////  From AdaptivePV3DFitter
  Gaudi::Property<size_t> m_minTr{this, "MinTracks", 4, "Minimum number of tracks to make a vertex"};
  Gaudi::Property<int>    m_Iterations{this, "Iterations", 20, "Number of iterations for minimisation"};
  Gaudi::Property<int>    m_minIter{this, "minIter", 5, "Min number of iterations"};
  Gaudi::Property<double> m_maxDeltaZ{this, "maxDeltaZ", 0.0005 * Gaudi::Units::mm,
                                      "Fit convergence condition"}; // 0.001 * Gaudi::Units::mm)
  Gaudi::Property<double> m_minTrackWeight{this, "minTrackWeight", 0.00000001,
                                           "Minimum Tukey's weight to accept a track"}; // 0.00001)
  Gaudi::Property<double> m_TrackErrorScaleFactor{this, "TrackErrorScaleFactor", 1.0};
  Gaudi::Property<double> m_maxChi2{this, "maxChi2", 400.0, "max chi2 of track-to-vtx to be considered for fit"};
  Gaudi::Property<double> m_trackMaxChi2{this, "trackMaxChi2", 12.};
  double                  m_trackChi; // sqrt of trackMaxChi2
  Gaudi::Property<double> m_trackMaxChi2Remove{
      this,
      "trackMaxChi2Remove",
      25.,
      [this]( auto& ) { this->m_trackChi = std::sqrt( this->m_trackMaxChi2 ); },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}, // insure m_trackChi is in sync with m_trackMaxChi2
      "Max chi2 tracks to be removed from next PV search"};
  Gaudi::Property<double> m_maxDeltaZCache{this, "maxDeltaZCache", 1 * Gaudi::Units::mm,
                                           "Update derivatives if distance of reference is more than this"};

  ////////  From PVSeedTool
  // no beamline constraint is used in the 3D seeding case
  Gaudi::Property<bool> m_seeding{this, "Seeding3D", false, "Flag to switch into 3D seeting without a beamline"};
  // steering parameters for merging procedure
  Gaudi::Property<double> m_maxChi2Merge{this, "maxChi2Merge", 25.};
  Gaudi::Property<double> m_factorToIncreaseErrors{this, "factorToIncreaseErrors", 15.};

  // steering parameters for final cluster selection
  Gaudi::Property<int>    m_minClusterMult{this, "minClusterMult", 3};
  Gaudi::Property<double> m_dzCloseTracksInCluster{this, "dzCloseTracksInCluster", 5. * Gaudi::Units::mm};
  Gaudi::Property<int>    m_minCloseTracksInCluster{this, "minCloseTracksInCluster", 3};
  double                  m_radialDistanceSeedingSq = 0;
  Gaudi::Property<double> m_radialDistanceSeeding{
      this, "radialDistanceSeeding", 0.3,
      [this]( auto& ) { this->m_radialDistanceSeedingSq = std::pow( this->m_radialDistanceSeeding, 2 ); },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};

  // steering parameters for 2D seedinhg
  Gaudi::Property<int>    m_highMult{this, "highMult", 10};
  Gaudi::Property<double> m_ratioSig2HighMult{this, "ratioSig2HighMult", 1.0};
  Gaudi::Property<double> m_ratioSig2LowMult{this, "ratioSig2LowMult", 0.9};
  double                  m_scatCons = 0; // calculated from m_x0MS
  Gaudi::Property<double> m_x0MS{this, "x0MS", 0.01,
                                 [this]( auto& ) {
                                   double X0        = this->m_x0MS;
                                   this->m_scatCons = ( 13.6 * sqrt( X0 ) * ( 1. + 0.038 * log( X0 ) ) );
                                 },
                                 Gaudi::Details::Property::ImmediatelyInvokeHandler{true}}; // X0 (tunable) of MS to add
                                                                                            // for extrapolation of
                                                                                            // track parameters to PV

  // steering parameters for 3D seedinhg
  double                  m_TrackPairMaxDistanceSq = 0.;
  double                  m_zMaxSpreadSq           = 0.;
  Gaudi::Property<double> m_TrackPairMaxCos2Theta{this, "TrackPairMaxCos2Theta", 0.998}; // 0.99
  Gaudi::Property<double> m_TrackPairMaxDistance{
      this, "TrackPairMaxDistance", 0.2 * Gaudi::Units::mm,
      [this]( auto& ) { this->m_TrackPairMaxDistanceSq = std::pow( this->m_TrackPairMaxDistance, 2 ); },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}}; // 0.2
  Gaudi::Property<double> m_zMaxSpread{this, "zMaxSpread", 0.9 * Gaudi::Units::mm,
                                       [this]( auto& ) { this->m_zMaxSpreadSq = std::pow( this->m_zMaxSpread, 2 ); },
                                       Gaudi::Details::Property::ImmediatelyInvokeHandler{true}}; // 0.9

  // trivial accessor to minimum allowed chi2
  double minAllowedChi2( const RecVertex& rvtx ) const {
    return ( rvtx.tracks().size() < 7 ? std::max( m_pvsChi2Separation, m_pvsChi2SeparationLowMult )
                                      : m_pvsChi2Separation );
  }

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nbPVsCounter{this, "Nb PVs"};
};

DECLARE_COMPONENT( PatPV3DFuture )

PatPV3DFuture::PatPV3DFuture( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"InputTracks", LHCb::TrackLocation::Default},
                    KeyValue{"InteractionRegionCache", "AlgorithmSpecific-" + name + "-InteractionRegion"}},
                   KeyValue( "OutputVerticesName", LHCb::RecVertexLocation::Velo3D ) ) {}

StatusCode PatPV3DFuture::initialize() {
  return Transformer::initialize().andThen( [&]() {
    LHCb::Conditions::InteractionRegion::addConditionDerivation( this,
                                                                 inputLocation<LHCb::Conditions::InteractionRegion>() );
  } );
}

namespace {

  using RecVertexSpan = LHCb::span<const PatPV3DFuture::RecVertex>;
  bool isChi2Separated( const PatPV3DFuture::RecVertex& rvtx, const LHCb::RecVertices& outvtxvec,
                        double minAllowedChi2 ) {
    return std::none_of( outvtxvec.begin(), outvtxvec.end() - 1,
                         [rz = rvtx.position().z(), sigma2z = rvtx.covMatrix()( 2, 2 ),
                          minAllowedChi2]( const PatPV3DFuture::RecVertex* v ) {
                           return std::pow( rz - v->position().z(), 2 ) / ( sigma2z + v->covMatrix()( 2, 2 ) ) <
                                  minAllowedChi2;
                         } );
  }

  bool vtxcomp( vtxCluster1D* first, vtxCluster1D* second ) { return first->z < second->z; }
  bool multcomp( vtxCluster1D* first, vtxCluster1D* second ) { return first->ntracks > second->ntracks; }

  // auxiliary class for merging procedure of tracks/clusters
  struct pair_to_merge final {
    vtxCluster1D* first    = nullptr;                            // pointer to first cluster to be merged
    vtxCluster1D* second   = nullptr;                            // pointer to second cluster to be merged
    double        chi2dist = std::numeric_limits<double>::max(); // a chi2dist = zdistance**2/(sigma1**2+sigma2**2)
    pair_to_merge( vtxCluster1D* f, vtxCluster1D* s, double chi2 ) : first( f ), second( s ), chi2dist( chi2 ) {}
  };

  bool compLessThanChi2( const pair_to_merge& first, const pair_to_merge& second ) {
    return first.chi2dist < second.chi2dist;
  }

  struct pair_to_merge_seed final {
    vtxCluster3D* first    = nullptr;                            // pointer to first cluster to be merged
    vtxCluster3D* second   = nullptr;                            // pointer to second cluster to be merged
    double        chi2dist = std::numeric_limits<double>::max(); // a chi2dist = zdistance**2/(sigma1**2+sigma2**2)
    pair_to_merge_seed( vtxCluster3D& f, vtxCluster3D& s, double chi2 ) : first( &f ), second( &s ), chi2dist( chi2 ) {}
  };

  bool compLessThanChi2Seed( const pair_to_merge_seed& first, const pair_to_merge_seed& second ) {
    return first.chi2dist < second.chi2dist;
  }

  /*
  Threshold to avoid copying the vector of pairs (vclus) with size <= 5000, i.e. 5 (nb of consequtive clusters) times
  1000 (threshold nb of initial clusters). Copying starts for nb of initial clusters (vecp2m size) > 1000. For vecp2m
  size <= 1000 and >= 5, 5 * vecp2m.size() is reserved. For vecp2m size < 5, ( vecp2m.size() )**2 is reserved.
  */
  constexpr static const int s_p2mstatic = 5000;

  double zCloseBeamOfflineTool( const PatPV3DFuture::Track* track, const Gaudi::XYZPoint& beamspot,
                                const double maxr2 ) {

    Gaudi::XYZPoint  tpoint   = track->position();
    Gaudi::XYZVector tdir     = track->slopes();
    double           wx       = ( 1. + tdir.x() * tdir.x() ) / track->firstState().errX2();
    double           wy       = ( 1. + tdir.y() * tdir.y() ) / track->firstState().errY2();
    double           x0       = tpoint.x() - tpoint.z() * tdir.x() - beamspot.X();
    double           y0       = tpoint.y() - tpoint.z() * tdir.y() - beamspot.Y();
    double           den      = wx * tdir.x() * tdir.x() + wy * tdir.y() * tdir.y();
    double           zAtBeam  = -( wx * x0 * tdir.x() + wy * y0 * tdir.y() ) / den;
    double           xb       = tpoint.x() + tdir.x() * ( zAtBeam - tpoint.z() ) - beamspot.X();
    double           yb       = tpoint.y() + tdir.y() * ( zAtBeam - tpoint.z() ) - beamspot.Y();
    double           r2AtBeam = xb * xb + yb * yb;
    return r2AtBeam < maxr2 ? zAtBeam : 10e8;
  }

  std::optional<Gaudi::XYZPoint> xPointParameters( const LHCb::Track* track1, const LHCb::Track* track2,
                                                   const double max_distanceSq, const double max_cos2Theta ) {

    Gaudi::XYZPoint closestpoint;
    auto            o1 = track1->position();
    auto            o2 = track2->position();
    auto            v1 = track1->slopes();
    auto            v2 = track2->slopes();

    auto v0 = o1 - o2;

    double d12   = v1.Dot( v2 );
    double d22   = v2.Dot( v2 );
    double d1122 = d22 * ( v1.Dot( v1 ) );
    double d10   = v1.Dot( v0 );
    double d20   = v2.Dot( v0 );

    double det = d1122 - d12 * d12;
    if ( fabs( det ) < 1.0e-6 ) {
      return std::nullopt; // reject parallel track pairs
    }
    double l1     = ( d12 * d20 - d22 * d10 ) / det;
    double l2     = ( d20 + d12 * l1 ) / d22;
    auto   close1 = o1 + l1 * v1;
    auto   close2 = o2 + l2 * v2;

    closestpoint.SetCoordinates( 0.5 * ( close1.x() + close2.x() ), 0.5 * ( close1.y() + close2.y() ),
                                 0.5 * ( close1.z() + close2.z() ) );

    if ( ( d12 * d12 < d1122 * max_cos2Theta ) && ( ( close1 - close2 ).Mag2() < max_distanceSq ) ) {
      return closestpoint;
    } else {
      return std::nullopt;
    }
  }

  double cos2Theta( const Gaudi::XYZVector& v1, const Gaudi::XYZVector& v2 ) {
    const auto v1v2 = v1.Dot( v2 );
    return ( v1v2 * v1v2 ) / ( v1.Mag2() * v2.Mag2() );
  }

  std::optional<vtxCluster3D> seedSelection( std::vector<closeNode>& close_nodes, const seedTrack& base_track,
                                             double spread2_max, int minCloseTracks ) {
    vtxCluster3D pseed;
    pseed.position.SetXYZ( 0., 0., 0. );
    pseed.error.SetXYZ( 0., 0., 0. );
    pseed.ntracks = close_nodes.size();

    if ( close_nodes.size() < 2 ) return std::nullopt;

    double x = 0.;
    double y = 0.;
    double z = 0.;

    double ex = 0.;
    double ey = 0.;
    double ez = 0.;

    Gaudi::XYZPoint pmean;
    Gaudi::XYZPoint perror;
    closeNode*      itmax = nullptr;

    double dist2_max = 1000. * 1000.;
    int    ngood     = 1000;
    while ( dist2_max > spread2_max && ngood > 1 ) {
      int ng = 0;
      x      = 0.;
      y      = 0.;
      z      = 0.;

      double sum_wx  = 0.;
      double sum_wy  = 0.;
      double sum_wz  = 0.;
      double sum_wxx = 0.;
      double sum_wyy = 0.;
      double sum_wzz = 0.;

      for ( auto it = close_nodes.begin(); it != close_nodes.end(); it++ ) {
        if ( !it->take ) continue;
        ng++;

        // 0.1 based on raw Run12 estimation, to be followed up
        double errxy2 = 0.1 * 0.1;

        double c2 =
            cos2Theta( base_track.lbtrack->firstState().slopes(), it->seed_track->lbtrack->firstState().slopes() );
        double ctanth2 = c2 / ( 1. - c2 );
        double errz2   = 2. * ctanth2 * errxy2;

        double wx = 1. / errxy2;
        double wy = 1. / errxy2;
        double wz = 1. / errz2;
        sum_wx += wx;
        sum_wy += wy;
        sum_wz += wz;
        sum_wxx += wx * it->closest_point.X();
        sum_wyy += wy * it->closest_point.Y();
        sum_wzz += wz * it->closest_point.Z();
      }

      if ( ng < minCloseTracks ) return std::nullopt;

      x = sum_wxx / sum_wx;
      y = sum_wyy / sum_wy;
      z = sum_wzz / sum_wz;
      pmean.SetXYZ( x, y, z );

      ex = sqrt( 1. / sum_wx );
      ey = sqrt( 1. / sum_wy );
      ez = sqrt( 1. / sum_wz );

      double d2max = 0.;
      for ( auto it = close_nodes.begin(); it != close_nodes.end(); it++ ) {
        if ( !it->take ) continue;
        double dist2 = ( pmean - it->closest_point ).Mag2();
        if ( dist2 > d2max ) {
          d2max = dist2;
          itmax = &( *it );
        }
      }
      ngood = ng;
      if ( d2max > spread2_max ) {
        itmax->take = false;
        ngood--;
      }
      dist2_max = d2max;
    } // end while

    if ( ngood < minCloseTracks ) return std::nullopt;
    perror.SetXYZ( ex, ey, ez );
    pseed.position = pmean;
    pseed.error    = perror;
    pseed.ntracks  = ngood;

    return pseed;
  }

} // namespace

PatPV3DFuture::RecVertices PatPV3DFuture::operator()( TrackContainer const&                      inputTracks,
                                                      LHCb::Conditions::InteractionRegion const& region ) const {
  auto rvts = reconstructMultiPV( inputTracks, region.avgPosition );
  m_nbPVsCounter += rvts.size();
  return rvts;
}

PatPV3DFuture::RecVertices PatPV3DFuture::reconstructMultiPV( const TrackContainer&  inputTracks,
                                                              Gaudi::XYZPoint const& beamSpot ) const {
  std::vector<const PatPV3DFuture::Track*> rtracks;
  std::for_each( inputTracks.begin(), inputTracks.end(), [&]( const PatPV3DFuture::Track* trk ) {
    if ( !m_requireVelo || trk->hasVelo() ) { rtracks.push_back( trk ); }
  } );

  return reconstructMultiPVFromTracks( rtracks, beamSpot );
}

void PatPV3DFuture::removeTracks( std::vector<const PatPV3DFuture::Track*>&       tracks,
                                  const std::vector<const PatPV3DFuture::Track*>& tracks2remove ) const {
  auto firstToErase = std::remove_if( begin( tracks ), end( tracks ), [&tracks2remove]( auto trk ) {
    return std::find( begin( tracks2remove ), end( tracks2remove ), trk ) != tracks2remove.end();
  } );
  tracks.erase( firstToErase, std::end( tracks ) );
}

namespace {
  class AdaptivePVTrack final {
  public:
    using Track = LHCb::Event::v1::Track;
    AdaptivePVTrack( const Track& track, const Gaudi::XYZPoint& vtx );
    void                       updateCache( const Gaudi::XYZPoint& vtx );
    double                     weight() const { return m_weight; }
    void                       setWeight( double w ) { m_weight = w; }
    const Gaudi::SymMatrix3x3& halfD2Chi2DX2() const { return m_halfD2Chi2DX2; }
    const Gaudi::Vector3&      halfDChi2DX() const { return m_halfDChi2DX; }
    double                     chi2() const { return m_chi2; }
    inline double              chi2( const Gaudi::XYZPoint& vtx ) const;
    const Track*               track() const { return m_track; }

  private:
    void                              invertCov();
    double                            m_weight;
    const Track*                      m_track;
    LHCb::State                       m_state;
    Gaudi::SymMatrix2x2               m_invcov;
    Gaudi::SymMatrix3x3               m_halfD2Chi2DX2;
    Gaudi::Vector3                    m_halfDChi2DX;
    double                            m_chi2;
    ROOT::Math::SMatrix<double, 3, 2> m_H;
  };

  AdaptivePVTrack::AdaptivePVTrack( const LHCb::Event::v1::Track& track, const Gaudi::XYZPoint& vtx )
      : m_track( &track ) {
    // get the state
    m_state = track.firstState();
    if ( m_state.location() != LHCb::State::Location::ClosestToBeam ) {
      const LHCb::State* closestToBeam = track.stateAt( LHCb::State::Location::ClosestToBeam );
      if ( closestToBeam ) m_state = *closestToBeam;
    }
    // do here things we could evaluate at z_seed. may add cov matrix here, which'd save a lot of time.
    m_H( 0, 0 ) = m_H( 1, 1 ) = 1;
    m_H( 2, 0 )               = -m_state.tx();
    m_H( 2, 1 )               = -m_state.ty();
    // update the cache
    updateCache( vtx );
  }

  void AdaptivePVTrack::invertCov() {
    auto invdet      = 1 / ( m_invcov( 0, 0 ) * m_invcov( 1, 1 ) - m_invcov( 0, 1 ) * m_invcov( 1, 0 ) );
    auto new00       = m_invcov( 1, 1 ) * invdet;
    m_invcov( 1, 1 ) = m_invcov( 0, 0 ) * invdet;
    m_invcov( 0, 0 ) = new00;
    m_invcov( 0, 1 ) = m_invcov( 1, 0 ) = -m_invcov( 0, 1 ) * invdet;
  }

  void AdaptivePVTrack::updateCache( const Gaudi::XYZPoint& vtx ) {
    // transport to vtx z
    m_state.linearTransportTo( vtx.z() );

    // invert cov matrix
    m_invcov = m_state.covariance().Sub<Gaudi::SymMatrix2x2>( 0, 0 );
    invertCov();

    // The following can all be written out, omitting the zeros, once
    // we know that it works.
    Gaudi::Vector2                    res{vtx.x() - m_state.x(), vtx.y() - m_state.y()};
    ROOT::Math::SMatrix<double, 3, 2> HW = m_H * m_invcov;
    ROOT::Math::AssignSym::Evaluate( m_halfD2Chi2DX2, HW * ROOT::Math::Transpose( m_H ) );
    // m_halfD2Chi2DX2 = ROOT::Math::Similarity(H, invcov ) ;
    m_halfDChi2DX = HW * res;
    m_chi2        = ROOT::Math::Similarity( res, m_invcov );
  }

  inline double AdaptivePVTrack::chi2( const Gaudi::XYZPoint& vtx ) const {
    double         dz = vtx.z() - m_state.z();
    Gaudi::Vector2 res{vtx.x() - ( m_state.x() + dz * m_state.tx() ), vtx.y() - ( m_state.y() + dz * m_state.ty() )};
    return ROOT::Math::Similarity( res, m_invcov );
  }
} // namespace

StatusCode PatPV3DFuture::fitVertex( const Gaudi::XYZPoint& seedPoint, const std::vector<const Track*>& rTracks,
                                     PatPV3DFuture::RecVertices& outputVtxVec,
                                     std::vector<const Track*>&  tracks2remove ) const {
  tracks2remove.clear();
  // position at which derivatives are evaluated
  Gaudi::XYZPoint refpos = seedPoint;
  // prepare tracks
  std::vector<AdaptivePVTrack> pvTracks;
  pvTracks.reserve( rTracks.size() );
  for ( const auto& track : rTracks )
    if ( track->hasVelo() ) {
      pvTracks.emplace_back( *track, refpos );
      if ( pvTracks.back().chi2() >= m_maxChi2 ) pvTracks.pop_back();
    }
  if ( pvTracks.size() < m_minTr ) { return StatusCode::FAILURE; }

  // current vertex position
  Gaudi::XYZPoint vtxpos = refpos;
  // vertex covariance matrix
  Gaudi::SymMatrix3x3 vtxcov;
  bool                converged = false;
  double              maxdz     = m_maxDeltaZ;
  int                 nbIter    = 0;
  double              chi2      = 0;
  size_t              ntrin     = 0;

  while ( ( nbIter < m_minIter ) || ( !converged && nbIter < m_Iterations ) ) {
    ++nbIter;
    Gaudi::SymMatrix3x3 halfD2Chi2DX2;
    Gaudi::Vector3      halfDChi2DX;
    // update cache if too far from reference position. this is the slow part.
    if ( std::abs( refpos.z() - vtxpos.z() ) > m_maxDeltaZCache ) {
      refpos = vtxpos;
      for ( auto& trk : pvTracks ) trk.updateCache( refpos );
    }

    // add contribution from all tracks
    chi2  = 0;
    ntrin = 0;
    for ( auto& trk : pvTracks ) {
      // compute weight
      double trkchi2 = trk.chi2( vtxpos );
      double weight  = getTukeyWeight( trkchi2, nbIter );
      trk.setWeight( weight );
      // add the track
      if ( weight > m_minTrackWeight ) {
        ++ntrin;
        halfD2Chi2DX2 += weight * trk.halfD2Chi2DX2();
        halfDChi2DX += weight * trk.halfDChi2DX();
        chi2 += weight * trk.chi2();
      }
    }

    // check nr of tracks that entered the fit
    if ( ntrin < m_minTr ) { return StatusCode::FAILURE; }

    // compute the new vertex covariance
    vtxcov = halfD2Chi2DX2;
    if ( !vtxcov.InvertChol() ) { return StatusCode::FAILURE; }
    // compute the delta w.r.t. the reference
    Gaudi::Vector3 delta = -1.0 * vtxcov * halfDChi2DX;

    // note: this is only correct if chi2 was chi2 of reference!
    chi2 += ROOT::Math::Dot( delta, halfDChi2DX );

    // deltaz needed for convergence
    const double deltaz = refpos.z() + delta( 2 ) - vtxpos.z();

    // update the position
    vtxpos.SetX( refpos.x() + delta( 0 ) );
    vtxpos.SetY( refpos.y() + delta( 1 ) );
    vtxpos.SetZ( refpos.z() + delta( 2 ) );

    // loose convergence criteria if close to end of iterations
    if ( 1. * nbIter > 0.8 * m_Iterations ) maxdz = 10. * m_maxDeltaZ;
    converged = std::abs( deltaz ) < maxdz;

  } // end iteration loop
  if ( !converged ) return StatusCode::FAILURE;

  LHCb::RecVertex* vtx = new LHCb::RecVertex();
  vtx->setPosition( vtxpos );
  vtx->setCovMatrix( vtxcov );
  vtx->setChi2AndDoF( chi2, 2 * ntrin - 3 );

  for ( const auto& trk : pvTracks ) {
    if ( trk.weight() > m_minTrackWeight ) vtx->addToTracks( trk.track(), trk.weight() );
    // remove track for next PV search
    if ( trk.chi2( vtxpos ) < m_trackMaxChi2Remove ) tracks2remove.push_back( trk.track() );
  }
  vtx->setTechnique( RecVertex::RecVertexType::Primary );
  outputVtxVec.insert( vtx );
  return StatusCode::SUCCESS;
}

double PatPV3DFuture::getTukeyWeight( double trchi2, int iter ) const {
  if ( iter < 1 ) return 1.;
  double ctrv = m_trackChi * std::max( m_minIter - iter, 1 );
  auto   sq   = std::pow( ctrv * m_TrackErrorScaleFactor, 2 );
  return sq > trchi2 ? std::pow( 1. - ( trchi2 / sq ), 2 ) : 0.;
}

double PatPV3DFuture::errorForPVSeedFinding( double tx, double ty ) const {
  // the seeding results depend weakly on this eror parametrization
  double invPMean2    = 1. / ( 3000. * Gaudi::Units::MeV * 3000. * Gaudi::Units::MeV );
  double tanTheta2    = tx * tx + ty * ty;
  double invSinTheta2 = 1. / tanTheta2 + 1.;
  // assume that first hit in VD at 8 mm
  double distr        = 8. * Gaudi::Units::mm;
  double dist2        = distr * distr * invSinTheta2;
  double sigma_ms2    = m_scatCons * m_scatCons * dist2 * invPMean2;
  double fslope2      = 0.0005 * 0.0005;
  double sigma_slope2 = fslope2 * dist2;
  return ( sigma_ms2 + sigma_slope2 ) * invSinTheta2;
}

std::vector<double> PatPV3DFuture::findClusters( std::vector<vtxCluster1D>& vclus ) const {
  std::vector<double> zclusters;
  if ( vclus.empty() ) return zclusters;
  std::vector<vtxCluster1D*> pvclus;
  pvclus.reserve( vclus.size() );
  for ( auto& itvtx : vclus ) {
    itvtx.sigsq *= m_factorToIncreaseErrors * m_factorToIncreaseErrors; // blow up errors
    itvtx.sigsqmin = itvtx.sigsq;
    pvclus.push_back( &itvtx );
  }
  std::sort( pvclus.begin(), pvclus.end(), vtxcomp );
  bool more_merging = true;
  while ( more_merging ) {
    // find pair of clusters for merging
    // refresh flag for this iteration
    for ( auto ivc = pvclus.begin(); ivc != pvclus.end(); ivc++ ) { ( *ivc )->not_merged = true; }
    // for a given cluster look only up to a few consequtive ones to merge
    // "a few" might become a property?
    auto                       n_consequtive = std::min( 5, static_cast<int>( pvclus.size() ) );
    auto                       ivc2up        = std::next( pvclus.begin(), n_consequtive );
    std::vector<pair_to_merge> vecp2m;
    vecp2m.reserve( std::min( static_cast<int>( pvclus.size() ) * n_consequtive, s_p2mstatic ) );
    for ( auto ivc1 = pvclus.begin(); ivc1 != pvclus.end() - 1; ivc1++ ) {
      if ( ivc2up != pvclus.end() ) ++ivc2up;
      for ( auto ivc2 = std::next( ivc1 ); ivc2 != ivc2up; ivc2++ ) {
        double zdist    = ( *ivc1 )->z - ( *ivc2 )->z;
        double chi2dist = zdist * zdist / ( ( *ivc1 )->sigsq + ( *ivc2 )->sigsq );
        if ( chi2dist < m_maxChi2Merge && vecp2m.size() < s_p2mstatic ) {
          vecp2m.emplace_back( *ivc1, *ivc2, chi2dist );
        }
      }
    }
    // done if no more pairs to merge
    if ( vecp2m.empty() ) {
      more_merging = false;
    } else {
      // sort if number of pairs reasonable. Sorting increases efficency.
      if ( vecp2m.size() < 100 ) std::sort( vecp2m.begin(), vecp2m.end(), compLessThanChi2 );
      // merge pairs
      for ( auto [pvtx1, pvtx2, _] : vecp2m ) {
        if ( pvtx1->not_merged && pvtx2->not_merged ) {
          double z1       = pvtx1->z;
          double z2       = pvtx2->z;
          double s1       = pvtx1->sigsq;
          double s2       = pvtx2->sigsq;
          double s1min    = pvtx1->sigsqmin;
          double s2min    = pvtx2->sigsqmin;
          double sigsqmin = s1min;
          if ( s2min < s1min ) sigsqmin = s2min;
          double w_inv    = ( s1 * s2 / ( s1 + s2 ) );
          double zmerge   = w_inv * ( z1 / s1 + z2 / s2 );
          pvtx1->z        = zmerge;
          pvtx1->sigsq    = w_inv;
          pvtx1->sigsqmin = sigsqmin;
          pvtx1->ntracks += pvtx2->ntracks;
          pvtx2->ntracks    = 0; // mark second cluster as used
          pvtx1->not_merged = false;
          pvtx2->not_merged = false;
        }
      }
      // remove clusters which where used
      pvclus.erase(
          std::remove_if( pvclus.begin(), pvclus.end(), []( const vtxCluster1D* cl ) { return cl->ntracks < 1; } ),
          pvclus.end() );
    }
  }
  // End of clustering.
  // Sort according to multiplicity
  std::sort( pvclus.begin(), pvclus.end(), multcomp );
  // Select good clusters.
  for ( auto ivc = pvclus.begin(); ivc != pvclus.end(); ivc++ ) {
    const int n_tracks_close  = std::count_if( vclus.begin(), vclus.end(), [&]( const auto& itvtx ) {
      return std::abs( itvtx.z - ( *ivc )->z ) < m_dzCloseTracksInCluster;
    } );
    auto      dist_to_closest = std::numeric_limits<double>::max();
    if ( pvclus.size() > 1 ) {
      for ( auto ivc1 = pvclus.begin(); ivc1 != pvclus.end(); ivc1++ ) {
        if ( ivc != ivc1 && ( fabs( ( *ivc1 )->z - ( *ivc )->z ) < dist_to_closest ) ) {
          dist_to_closest = fabs( ( *ivc1 )->z - ( *ivc )->z );
        }
      }
    }
    // ratio to remove clusters made of one low error track and many large error ones
    double           rat                 = ( *ivc )->sigsq / ( *ivc )->sigsqmin;
    int              ntracks             = ( *ivc )->ntracks;
    constexpr double min_dist_to_closest = 10.;
    constexpr double max_ratio           = 0.95;

    bool igood = ntracks >= m_minClusterMult && ( ( dist_to_closest > min_dist_to_closest && rat < max_ratio ) ||
                                                  ( ntracks >= m_highMult && rat < m_ratioSig2HighMult ) ||
                                                  ( ntracks < m_highMult && rat < m_ratioSig2LowMult ) );

    // veto
    if ( n_tracks_close < m_minCloseTracksInCluster ) igood = false;
    if ( igood ) zclusters.push_back( ( *ivc )->z );
  }
  //  print_clusters(pvclus);
  return zclusters;
}

// merging 3D clusters
// We start by sorting the input clusters based on their z-position
// Then we iterate over the clusters calculating the chi2dist based on the z-positions and errors of each pair of
// clusters. Pairs of clusters that satisfy the condition to have chi2dist smaller than m_maxChi2Merge threshold are
// stored in vecp2m for potential merging. If pairs of merging exist in vec2pm, we either sort them or directly proceed
// to merge them. Merging involves calculating new positions (xmerge, ymerge, zmerge) and errors for the merged cluster.
// After merging, we remove clusters that have been marked as "used" from the vclus vector.
// 5 is a maximum number of consecutive clusters that are evaluated for potential merging
bool PatPV3DFuture::mergeClusters( std::vector<vtxCluster3D>& vclus ) const {
  if ( vclus.size() < 2 ) { return false; }

  std::sort( vclus.begin(), vclus.end(), seedcompZ );

  bool more_merging = true;
  while ( more_merging ) {
    // find pair of clusters for merging
    // refresh flag for this iteration
    for ( auto& ivc : vclus ) ivc.not_merged = true;
    auto                            n_consequtive = std::min( 5, static_cast<int>( vclus.size() ) );
    auto                            ivc2up        = std::next( vclus.begin(), n_consequtive );
    std::vector<pair_to_merge_seed> vecp2m;
    vecp2m.reserve( std::min( static_cast<int>( vclus.size() ) * n_consequtive, s_p2mstatic ) );
    for ( auto ivc1 = vclus.begin(); ivc1 != vclus.end() - 1; ivc1++ ) {
      if ( ivc2up != vclus.end() ) ++ivc2up;
      for ( auto ivc2 = std::next( ivc1 ); ivc2 != ivc2up; ivc2++ ) {
        double zdist = ivc1->position.Z() - ivc2->position.Z();
        double chi2dist =
            zdist * zdist /
            ( ( m_factorToIncreaseErrors * m_factorToIncreaseErrors * ivc1->error.Z() * ivc1->error.Z() ) +
              ( m_factorToIncreaseErrors * m_factorToIncreaseErrors * ivc2->error.Z() * ivc2->error.Z() ) );
        if ( chi2dist < m_maxChi2Merge && vecp2m.size() < s_p2mstatic ) {
          vecp2m.emplace_back( *ivc1, *ivc2, chi2dist );
        }
      }
    }

    if ( vecp2m.empty() ) {
      more_merging = false;
    } else {
      // sort if number of pairs reasonable. Sorting increases efficency.
      if ( vecp2m.size() < 100 ) std::sort( vecp2m.begin(), vecp2m.end(), compLessThanChi2Seed );
      // merge pairs
      for ( auto [pvtx1, pvtx2, _] : vecp2m ) {
        if ( pvtx1->not_merged && pvtx2->not_merged ) {
          double zs1 = pvtx1->error.Z() * pvtx1->error.Z();
          double zs2 = pvtx2->error.Z() * pvtx2->error.Z();
          double ys1 = pvtx1->error.Y() * pvtx1->error.Y();
          double ys2 = pvtx2->error.Y() * pvtx2->error.Y();
          double xs1 = pvtx1->error.X() * pvtx1->error.X();
          double xs2 = pvtx2->error.X() * pvtx2->error.X();

          double wz_inv = ( zs1 * zs2 / ( zs1 + zs2 ) );
          double zmerge = wz_inv * ( pvtx1->position.Z() / zs1 + pvtx2->position.Z() / zs2 );

          double wy_inv = ( ys1 * ys2 / ( ys1 + ys2 ) );
          double ymerge = wy_inv * ( pvtx1->position.Y() / ys1 + pvtx2->position.Y() / ys2 );

          double wx_inv = ( xs1 * xs2 / ( xs1 + xs2 ) );
          double xmerge = wx_inv * ( pvtx1->position.X() / xs1 + pvtx2->position.X() / xs2 );

          pvtx1->position.SetXYZ( xmerge, ymerge, zmerge );
          pvtx1->error.SetXYZ( sqrt( wx_inv ), sqrt( wy_inv ), sqrt( wz_inv ) );
          pvtx1->ntracks += pvtx2->ntracks;
          pvtx2->ntracks    = 0; // mark second cluster as used
          pvtx1->not_merged = false;
          pvtx2->not_merged = false;
        }
      }
      // remove clusters which where used
      vclus.erase( std::remove_if( vclus.begin(), vclus.end(), []( const vtxCluster3D cl ) { return cl.ntracks < 1; } ),
                   vclus.end() );
    }
  }
  return true;
}

PatPV3DFuture::RecVertices
PatPV3DFuture::reconstructMultiPVFromTracks( std::vector<const PatPV3DFuture::Track*>& rtracks,
                                             Gaudi::XYZPoint const&                    beamSpot ) const {
  PatPV3DFuture::RecVertices outvtxvec;
  outvtxvec.reserve( 20 );
  const auto newBeamspot = Gaudi::XYZPoint( beamSpot.X() + m_beamLineOffsetX, beamSpot.Y() + m_beamLineOffsetY, 0 );
  bool       goOn        = true;

  while ( goOn ) {
    goOn       = false;
    auto seeds = [&]() {
      // seeding
      return m_seeding ? getSeeds3D( rtracks )               // seeding 3D without the beamline
                       : getSeeds2D( rtracks, newBeamspot ); // seeding 2D with the beam line
    }();
    for ( const auto& seed : seeds ) {
      std::vector<const Track*> tracks2remove;
      {
        // fitting
        StatusCode scvfit = fitVertex( seed, rtracks, outvtxvec, tracks2remove );
        if ( !scvfit.isSuccess() ) { continue; }
      }

      assert( !outvtxvec.empty() ); // code below implicitly assumes not empty !!
      if ( !isChi2Separated( **( outvtxvec.end() - 1 ), outvtxvec, minAllowedChi2( **( outvtxvec.end() - 1 ) ) ) ) {
        outvtxvec.erase( *( outvtxvec.end() - 1 ) );
        continue;
      }
      if ( m_useBeamSpotRCut.value() ) {
        const RecVertex& recvtx = **( outvtxvec.end() - 1 );
        const auto&      pos    = recvtx.position();
        auto             r2     = std::pow( pos.x() - newBeamspot.x(), 2 ) + std::pow( pos.y() - newBeamspot.y(), 2 );
        auto             r =
            ( recvtx.tracks().size() < m_beamSpotRMT.value() ? m_beamSpotRCut.value() : m_beamSpotRCutHMC.value() );
        if ( r2 > r * r ) {
          outvtxvec.erase( *( outvtxvec.end() - 1 ) );
          continue;
        }
      }
      goOn = true;
      removeTracks( rtracks, tracks2remove );
    } // iterate on seeds
  }   // iterate on vtx
  return outvtxvec;
}

std::vector<Gaudi::XYZPoint> PatPV3DFuture::getSeeds2D( const std::vector<const PatPV3DFuture::Track*>& inputTracks,
                                                        const Gaudi::XYZPoint& beamspot ) const {
  std::vector<Gaudi::XYZPoint> seeds;
  if ( inputTracks.size() < 3 ) return seeds;
  std::vector<vtxCluster1D> vclusters;

  for ( const auto& trk : inputTracks ) {
    constexpr double min_zclu = 2000.;

    const double maxr2 = m_useBeamSpotRCut ? 0.5 * 0.5 : 1e6;
    double       zclu  = zCloseBeamOfflineTool( trk, beamspot, maxr2 );
    double       sigsq = errorForPVSeedFinding( trk->firstState().tx(), trk->firstState().ty() );

    if ( fabs( zclu ) > min_zclu ) continue;
    vtxCluster1D clu;
    clu.z        = zclu;
    clu.sigsq    = sigsq;
    clu.sigsqmin = clu.sigsq;
    clu.ntracks  = 1;
    vclusters.push_back( clu );
  }
  auto zseeds = findClusters( vclusters );
  seeds.reserve( zseeds.size() );
  std::transform( zseeds.begin(), zseeds.end(), std::back_inserter( seeds ), [&]( double z ) {
    return Gaudi::XYZPoint{beamspot.X(), beamspot.Y(), z};
  } );
  return seeds;
}

std::vector<Gaudi::XYZPoint>
PatPV3DFuture::getSeeds3D( const std::vector<const PatPV3DFuture::Track*>& inputTracks ) const {

  std::vector<Gaudi::XYZPoint> seeds;
  if ( inputTracks.size() < 3 ) return seeds;

  if ( msgLevel( MSG::DEBUG ) ) { debug() << " This is 3D PV seeding. Beam spot is ignored." << endmsg; }

  std::vector<vtxCluster3D> seed_points;
  std::vector<seedTrack>    seed_tracks;
  seed_tracks.reserve( inputTracks.size() );

  for ( const LHCb::Track* ptr : inputTracks ) { seed_tracks.emplace_back( ptr ); }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << " seed_tracks.size  " << seed_tracks.size() << endmsg;
    debug() << " inputTracks.size  " << inputTracks.size() << endmsg;
    ;
  }

  std::vector<closeNode> close_nodes;
  for ( auto its1 = seed_tracks.begin(); its1 != seed_tracks.end(); its1++ ) {
    if ( !its1->used ) {
      int closeTracksNumber = 0;
      close_nodes.clear();

      for ( auto its2 = seed_tracks.begin(); its2 != seed_tracks.end(); its2++ ) {
        if ( !its2->used && its1 != its2 ) {
          std::optional<Gaudi::XYZPoint> closest_point;
          const LHCb::Track*             lbtr1 = its1->lbtrack;
          const LHCb::Track*             lbtr2 = its2->lbtrack;
          double           value = zCloseBeamOfflineTool( lbtr2, lbtr1->position(), m_radialDistanceSeedingSq );
          constexpr double max_zCloseBeamOfflineTool = 500.0;
          if ( std::fabs( value ) < max_zCloseBeamOfflineTool ) {
            closest_point = xPointParameters( lbtr1, lbtr2, m_TrackPairMaxDistanceSq, m_TrackPairMaxCos2Theta );
            if ( closest_point.has_value() ) {
              closeTracksNumber++;
              closeNode closetr;
              closetr.take          = true;
              closetr.seed_track    = &( *its2 );
              closetr.closest_point = closest_point.value();
              close_nodes.push_back( closetr );
            }
          }
        }
      } // its2

      if ( closeTracksNumber < m_minCloseTracksInCluster ) continue;

      // debug
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " close nodes (pairs of tracks wrt one track): " << endmsg;
        for ( auto const& cn : close_nodes ) {
          debug() << format( " xyz %7.3f %7.3f %7.3f ", cn.closest_point.X(), cn.closest_point.Y(),
                             cn.closest_point.Z() )
                  << endmsg;
        }
      }

      std::optional<vtxCluster3D> seed = seedSelection( close_nodes, *its1, m_zMaxSpreadSq, m_minCloseTracksInCluster );
      close_nodes.erase(
          std::remove_if( close_nodes.begin(), close_nodes.end(), []( const closeNode cl ) { return cl.take == 0; } ),
          close_nodes.end() );

      if ( seed.has_value() ) {
        seed_points.push_back( seed.value() );
        for ( auto& cn : close_nodes ) { cn.seed_track->used = true; }
      }
    }
  }

  mergeClusters( seed_points );
  std::sort( seed_points.begin(), seed_points.end(), seedcomp );

  for ( const auto& sp : seed_points ) {
    int ntracks = sp.ntracks;
    if ( ntracks >= m_minClusterMult ) {
      seeds.push_back( sp.position );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " xyz seed multi  " << sp.position.X() << " " << sp.position.Y() << " " << sp.position.Z() << " | "
                << sp.error.X() << " " << sp.error.Y() << " " << sp.error.Z() << " | " << sp.ntracks << " " << endmsg;
      }
    }
  }

  return seeds;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONLOGHIT_H
#define MUONLOGHIT_H 1
#include "Detector/Muon/TileID.h"
#include <sstream>

class MuonLogHit final {
public:
  /// Standard constructor
  MuonLogHit() = default;
  /// constructor from tile
  explicit MuonLogHit( LHCb::Detector::Muon::TileID tile ) : m_tile( tile ) {}
  LHCb::Detector::Muon::TileID tile() const { return m_tile; }
  int                          odeNumber() const { return m_ODEnumber; }
  void                         setOdeNumber( unsigned int& OdeNumber ) { m_ODEnumber = (int)OdeNumber; }
  int                          odeChannel() const { return m_ODEchannel; }
  void                         setOdeChannel( unsigned int& OdeChannel ) { m_ODEchannel = (int)OdeChannel; }
  int                          odeIndex() const { return m_ODEindex; }
  void                         setOdeIndex( short int& OdeIndex ) { m_ODEindex = (int)OdeIndex; }
  float                        time() const { // avoid bias due to bins 0 and 1 being added in bin 1
    return (float)( ( m_time + 50 * 16 ) % 16 == 1 ? m_time - 0.499 : m_time );
  }
  int  rawtime() const { return m_time; }
  bool intime() const { return ( m_time > -1 && m_time < 16 ); }
  int  bx() { return ( ( m_time + 160 ) / 16 - 10 ); }

  void        setTime( int TDCtime ) { m_time = TDCtime; }
  std::string odeName() const {
    std::stringstream sname;
    sname << "Q" << ( m_tile.quarter() + 1 ) << "M" << ( m_tile.station() + 1 ) << "R" << ( m_tile.region() + 1 ) << "_"
          << m_ODEindex;
    return sname.str();
  }
  int odeHwID() const {
    return ( m_tile.quarter() + 1 ) * 1000 + ( m_tile.station() + 1 ) * 100 + ( m_tile.region() + 1 ) * 10 + m_ODEindex;
  }

private:
  LHCb::Detector::Muon::TileID m_tile;
  int                          m_ODEnumber;
  int                          m_ODEchannel;
  int                          m_ODEindex;
  int                          m_time;
};
#endif // MUONLOGHIT_H

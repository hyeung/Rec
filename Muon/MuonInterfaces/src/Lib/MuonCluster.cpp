/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "MuonInterfaces/MuonCluster.h"
#include "MuonInterfaces/MuonPad.h"
#include <cmath>

namespace {
  double recomputeClusterPos( const std::vector<double>& data, double& dpos, int& clsize, double step ) {
    int    np  = 0;
    double sum = 0., sum2 = 0.;
    for ( auto ip = data.begin(); ip != data.end(); ++ip ) {
      // check that this position is not already the same of a previous pad
      bool prendila = std::none_of( data.begin(), ip, [&]( double p ) { return std::abs( *ip - p ) < 0.5 * step; } );
      if ( prendila ) {
        ++np;
        sum += *ip;
        sum2 += std::pow( *ip, 2 );
      }
    }
    if ( np > 1 ) {
      const float tmp = ( sum2 - sum * sum / np ) / ( np * np - np );
      dpos            = ( tmp > 0 ? std::sqrt( tmp ) : 0.0f );
    }
    clsize = np;
    return ( np > 0 ? sum / np : 0.0 );
  }
} // namespace

void MuonCluster::createFromPad( const MuonPad* pad, const DeMuonDetector::TilePosition pos ) {
  m_pads.clear();
  if ( pad->type() == MuonPad::LogPadType::UNPAIRED ) return;
  if ( pos.z() > 0 ) {
    m_pads.push_back( pad );
    m_padx.push_back( pos.x() );
    m_pady.push_back( pos.y() );
    m_padz.push_back( pos.z() );
    static const auto isqrt_12 = 1.0 / std::sqrt( 12. );
    m_dx                       = isqrt_12 * pos.dX();
    m_dy                       = isqrt_12 * pos.dY();
    m_dz                       = isqrt_12 * pos.dZ();
    m_hit_minx                 = pos.x() - pos.dX();
    m_hit_maxx                 = pos.x() + pos.dX();
    m_hit_miny                 = pos.y() - pos.dY();
    m_hit_maxy                 = pos.y() + pos.dY();
    m_hit_minz                 = pos.z() - pos.dZ();
    m_hit_maxz                 = pos.z() + pos.dZ();
    SetXYZ( pos.x(), pos.y(), pos.z() );
    m_xsize = m_ysize = 1;

    recomputeTime();
  }
}

void MuonCluster::recomputeTime() {
  int   np  = 0;
  float sum = 0., sum2 = 0.;
  m_mintime = 999.;
  m_maxtime = -999.;

  auto accumulate = [&]( float time ) {
    sum += time;
    sum2 += time * time;
    np++;
    if ( time < m_mintime ) m_mintime = time;
    if ( time > m_maxtime ) m_maxtime = time;
  };

  for ( const auto& pad : m_pads ) {
    if ( pad->type() == MuonPad::LogPadType::XTWOFE ) { // consider the two measurements as independent
      accumulate( pad->timeX() );
      accumulate( pad->timeY() );
    } else {
      accumulate( pad->time() );
    }
  }

  m_time = ( np > 0 ? sum / np : 0.0f );
  if ( np > 1 ) {
    const float tmp = ( sum2 - sum * sum / np ) / ( np * np - np );
    m_dtime         = ( tmp > 0 ? std::sqrt( tmp ) : 0.0f );
  } else {
    m_dtime = m_pads[0]->dtime();
  }
}

void MuonCluster::addPad( const MuonPad* mp, const DeMuonDetector::TilePosition pos ) {
  if ( mp->type() == MuonPad::LogPadType::UNPAIRED ) return;
  if ( m_pads.empty() ) {
    createFromPad( mp, pos );
  } else {

    if ( pos.z() > 0 ) {
      m_pads.push_back( mp );
      m_padx.push_back( pos.x() );
      m_pady.push_back( pos.y() );
      m_padz.push_back( pos.z() );
      if ( ( pos.x() - pos.dX() ) < m_hit_minx ) m_hit_minx = pos.x() - pos.dX();
      if ( ( pos.x() + pos.dX() ) > m_hit_maxx ) m_hit_maxx = pos.x() + pos.dX();
      if ( ( pos.y() - pos.dY() ) < m_hit_miny ) m_hit_miny = pos.y() - pos.dY();
      if ( ( pos.y() + pos.dY() ) > m_hit_maxy ) m_hit_maxy = pos.y() + pos.dY();
      if ( ( pos.z() - pos.dZ() ) < m_hit_minz ) m_hit_minz = pos.z() - pos.dZ();
      if ( ( pos.z() + pos.dZ() ) > m_hit_maxz ) m_hit_maxz = pos.z() + pos.dZ();
      auto x = recomputeClusterPos( m_padx, m_dx, m_xsize, pos.dX() );
      auto y = recomputeClusterPos( m_pady, m_dy, m_ysize, pos.dY() );
      auto z = recomputeClusterPos( m_padz, m_dz, m_zsize, 10 * pos.dZ() );
      SetXYZ( x, y, z );

      recomputeTime();
    }
  }
}

/// return tile sizes
std::array<double, 3> MuonCluster::hitTile_Size() const {
  return {( m_hit_maxx - m_hit_minx ) / 2., ( m_hit_maxy - m_hit_miny ) / 2., ( m_hit_maxz - m_hit_minz ) / 2.};
}

/// store a progressive hit number for debugging
void MuonCluster::setHitID( int id ) {
  if ( id != 0 ) m_hit_ID = id;
}

std::vector<const CommonMuonHit*> MuonCluster::getHits() const {
  std::vector<const CommonMuonHit*> out;
  for ( const auto& pad : m_pads ) {
    auto padhits = pad->getHit();
    out.insert( out.end(), padhits );
  }
  return out;
}

std::vector<LHCb::Detector::Muon::TileID> MuonCluster::getPadTiles() const {
  std::vector<LHCb::Detector::Muon::TileID> tiles;
  tiles.reserve( m_pads.size() );
  std::transform( m_pads.begin(), m_pads.end(), std::back_inserter( tiles ),
                  []( const MuonPad* mlp ) { return mlp->tile(); } );
  return tiles;
}

std::vector<float> MuonCluster::getTimes() const {
  auto               hits = getHits();
  std::vector<float> times;
  times.reserve( hits.size() );
  std::transform( hits.begin(), hits.end(), std::back_inserter( times ),
                  []( const CommonMuonHit* mlh ) { return mlh->time(); } );
  return times;
}

LHCb::Detector::Muon::TileID MuonCluster::centerTile() const {
  if ( m_pads.size() == 1 ) return m_pads.front()->tile();
  double                       d2min = std::numeric_limits<double>::max();
  LHCb::Detector::Muon::TileID out;
  for ( unsigned int ip = 0; ip < m_pads.size(); ip++ ) {
    auto d2 = std::pow( m_padx[ip] - X(), 2 ) + std::pow( m_pady[ip] - Y(), 2 ) + std::pow( m_padz[ip] - Z(), 2 );
    if ( d2 < d2min ) {
      d2min = d2;
      out   = m_pads[ip]->tile();
    }
  }
  return out;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonChi2MatchTool.h"
#include "Event/Track.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "GaudiKernel/SystemOfUnits.h"
#include <TMatrixF.h>
#include <algorithm>
#include <numeric>
#include <vector>

#define ISQRT12 0.2886751345948129

/** @class MuonChi2MatchTool MuonChi2MatchTool.h component/MuonChi2MatchTool.h
 *
 *
 *  @author Violetta Cogoni, Marianna Fontana, Rolf Oldeman, ported here by Giacomo Graziani
 *  @date   2015-11-11
 */

// convention for station counters to avoid confusions:
//   S from  0 to nStations (real station number obtained from condDB)
//   s from  0 to nSt       (hardcoded to 4) for vectors defined for M2-M5
//   iO from 0 to ord       (number of matched stations on which the algorithm is run)

MuonChi2MatchTool::MuonChi2MatchTool( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {}

StatusCode MuonChi2MatchTool::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return sc; }

  // initialize geometry
  const auto& mudet     = m_mudet.get();
  m_nStations           = mudet.stations();
  unsigned int nRegions = mudet.regions(); // this is 20 for Run I/II and 16 for Upgrade

  m_myFirstStation = ( m_nStations == 5 ) ? 1 : 0; // upgrade-wise

  //  const float isqrt12 = 1. / std::sqrt(12);
  for ( unsigned S = m_myFirstStation; S < m_nStations; ++S ) {
    for ( unsigned int R = 0; R != nRegions / m_nStations; ++R ) {
      m_sigmapadX[S][R] = mudet.getPadSizeX( S, R ) * ISQRT12;
      m_sigmapadY[S][R] = mudet.getPadSizeY( S, R ) * ISQRT12;
    }
  }

  // Define radiation lenghts z/X0 wrt z position
  // TODO: remove hardcoding of these numbers
  //                           z pos   z/X0
  constexpr auto mcs = std::array{std::pair<float, float>{12800. * Gaudi::Units::mm, 28.},   // ECAL + SPD + PS
                                  std::pair<float, float>{14300. * Gaudi::Units::mm, 53.},   // HCAL
                                  std::pair<float, float>{15800. * Gaudi::Units::mm, 47.5},  // M23 filter
                                  std::pair<float, float>{17100. * Gaudi::Units::mm, 47.5},  // M34 filter
                                  std::pair<float, float>{18300. * Gaudi::Units::mm, 47.5}}; // M45 filter

  for ( unsigned i = m_myFirstStation; i < m_nStations; ++i ) {
    for ( unsigned j = m_myFirstStation; j <= i; ++j ) {
      m_scattercache[i * ( i + 1 ) / 2 + j] =
          std::accumulate( mcs.begin(), mcs.begin() + ( j + ( m_myFirstStation ? 1 : 2 ) ), 0.f,
                           [i, j, &mudet]( float sum, const std::pair<float, float>& mcs ) {
                             return sum + ( mudet.getStationZ( i ) - mcs.first ) *
                                              ( mudet.getStationZ( j ) - mcs.first ) * 13.6 * Gaudi::Units::MeV * 13.6 *
                                              Gaudi::Units::MeV * mcs.second;
                           } );
    }
  }

  // vectors used for the algorithm
  m_maxcomb = pow( m_maxnhitinFOI.value(), nSt );

  return StatusCode::SUCCESS;
}

// invert matrix by hand
std::array<std::array<float, 4>, 4> MuonChi2MatchTool::invert( MuonChi2MatchTool::MuonCandCovariance cov ) const {

  unsigned int                        dim = cov.size();
  float                               deter;
  std::array<std::array<float, 4>, 4> minor, invCov;

  if ( dim == 1 ) { invCov[0][0] = 1 / cov( 0, 0 ); }

  if ( dim == 2 ) {
    minor[0][0]  = cov( 1, 1 );
    minor[0][1]  = cov( 1, 0 );
    minor[1][0]  = minor[0][1];
    minor[1][1]  = cov( 0, 0 );
    deter        = std::abs( cov( 0, 0 ) * cov( 1, 1 ) - cov( 1, 0 ) * cov( 0, 1 ) );
    invCov[0][0] = minor[0][0] / deter;
    invCov[0][1] = -minor[0][1] / deter;
    invCov[1][0] = -minor[1][0] / deter;
    invCov[1][1] = minor[1][1] / deter;
  } else if ( dim == 3 ) {
    minor[0][0]  = cov( 1, 1 ) * cov( 2, 2 ) - cov( 1, 2 ) * cov( 2, 1 );
    minor[0][1]  = cov( 1, 0 ) * cov( 2, 2 ) - cov( 1, 2 ) * cov( 2, 0 );
    minor[0][2]  = cov( 1, 0 ) * cov( 2, 1 ) - cov( 1, 1 ) * cov( 2, 0 );
    minor[1][0]  = minor[0][1];
    minor[1][1]  = cov( 0, 0 ) * cov( 2, 2 ) - cov( 0, 2 ) * cov( 2, 0 );
    minor[1][2]  = cov( 0, 0 ) * cov( 2, 1 ) - cov( 0, 1 ) * cov( 2, 0 );
    minor[2][0]  = minor[0][2];
    minor[2][1]  = minor[1][2];
    minor[2][2]  = cov( 0, 0 ) * cov( 1, 1 ) - cov( 0, 1 ) * cov( 1, 0 );
    deter        = std::abs( cov( 0, 0 ) * minor[0][0] - cov( 0, 1 ) * minor[0][1] + cov( 0, 2 ) * minor[0][2] );
    invCov[0][0] = minor[0][0] / deter;
    invCov[0][1] = -minor[0][1] / deter;
    invCov[0][2] = minor[0][2] / deter;
    invCov[1][0] = -minor[1][0] / deter;
    invCov[1][1] = minor[1][1] / deter;
    invCov[1][2] = -minor[1][2] / deter;
    invCov[2][0] = minor[2][0] / deter;
    invCov[2][1] = -minor[2][1] / deter;
    invCov[2][2] = minor[2][2] / deter;
  } else if ( dim == 4 ) {
    minor[0][0] = cov( 1, 1 ) * cov( 2, 2 ) * cov( 3, 3 ) + cov( 2, 1 ) * cov( 3, 2 ) * cov( 1, 3 ) +
                  cov( 1, 2 ) * cov( 2, 3 ) * cov( 3, 1 ) - cov( 1, 3 ) * cov( 2, 2 ) * cov( 3, 1 ) -
                  cov( 1, 2 ) * cov( 2, 1 ) * cov( 3, 3 ) - cov( 2, 3 ) * cov( 3, 2 ) * cov( 1, 1 );
    minor[0][1] = cov( 1, 0 ) * cov( 2, 2 ) * cov( 3, 3 ) + cov( 2, 0 ) * cov( 3, 2 ) * cov( 1, 3 ) +
                  cov( 1, 2 ) * cov( 2, 3 ) * cov( 3, 0 ) - cov( 1, 3 ) * cov( 2, 2 ) * cov( 3, 0 ) -
                  cov( 1, 2 ) * cov( 2, 0 ) * cov( 3, 3 ) - cov( 2, 3 ) * cov( 3, 2 ) * cov( 1, 0 );
    minor[0][2] = cov( 1, 0 ) * cov( 2, 1 ) * cov( 3, 3 ) + cov( 2, 0 ) * cov( 3, 1 ) * cov( 1, 3 ) +
                  cov( 1, 1 ) * cov( 2, 3 ) * cov( 3, 0 ) - cov( 1, 3 ) * cov( 2, 1 ) * cov( 3, 0 ) -
                  cov( 1, 1 ) * cov( 2, 0 ) * cov( 3, 3 ) - cov( 2, 3 ) * cov( 3, 1 ) * cov( 1, 0 );
    minor[0][3] = cov( 1, 0 ) * cov( 2, 1 ) * cov( 3, 2 ) + cov( 2, 0 ) * cov( 3, 1 ) * cov( 1, 2 ) +
                  cov( 1, 1 ) * cov( 2, 2 ) * cov( 3, 0 ) - cov( 1, 2 ) * cov( 2, 1 ) * cov( 3, 0 ) -
                  cov( 1, 1 ) * cov( 2, 0 ) * cov( 3, 2 ) - cov( 2, 2 ) * cov( 3, 1 ) * cov( 1, 0 );
    minor[1][0] = minor[0][1];
    minor[1][1] = cov( 0, 0 ) * cov( 2, 2 ) * cov( 3, 3 ) + cov( 2, 0 ) * cov( 3, 2 ) * cov( 0, 3 ) +
                  cov( 0, 2 ) * cov( 2, 3 ) * cov( 3, 0 ) - cov( 0, 3 ) * cov( 2, 2 ) * cov( 3, 0 ) -
                  cov( 0, 2 ) * cov( 2, 0 ) * cov( 3, 3 ) - cov( 2, 3 ) * cov( 3, 2 ) * cov( 0, 0 );
    minor[1][2] = cov( 0, 0 ) * cov( 2, 1 ) * cov( 3, 3 ) + cov( 2, 0 ) * cov( 3, 1 ) * cov( 0, 3 ) +
                  cov( 0, 1 ) * cov( 2, 3 ) * cov( 3, 0 ) - cov( 0, 3 ) * cov( 2, 1 ) * cov( 3, 0 ) -
                  cov( 0, 1 ) * cov( 2, 0 ) * cov( 3, 3 ) - cov( 2, 3 ) * cov( 3, 1 ) * cov( 0, 0 );
    minor[1][3] = cov( 0, 0 ) * cov( 2, 1 ) * cov( 3, 2 ) + cov( 2, 0 ) * cov( 3, 1 ) * cov( 0, 2 ) +
                  cov( 0, 1 ) * cov( 2, 2 ) * cov( 3, 0 ) - cov( 0, 2 ) * cov( 2, 1 ) * cov( 3, 0 ) -
                  cov( 0, 1 ) * cov( 2, 0 ) * cov( 3, 2 ) - cov( 2, 2 ) * cov( 3, 1 ) * cov( 0, 0 );
    minor[2][0] = minor[0][2];
    minor[2][1] = minor[1][2];
    minor[2][2] = cov( 0, 0 ) * cov( 1, 1 ) * cov( 3, 3 ) + cov( 1, 0 ) * cov( 3, 1 ) * cov( 0, 3 ) +
                  cov( 0, 1 ) * cov( 1, 3 ) * cov( 3, 0 ) - cov( 0, 3 ) * cov( 1, 1 ) * cov( 3, 0 ) -
                  cov( 0, 1 ) * cov( 1, 0 ) * cov( 3, 3 ) - cov( 1, 3 ) * cov( 3, 1 ) * cov( 0, 0 );
    minor[2][3] = cov( 0, 0 ) * cov( 1, 1 ) * cov( 3, 2 ) + cov( 1, 0 ) * cov( 3, 1 ) * cov( 0, 2 ) +
                  cov( 0, 1 ) * cov( 1, 2 ) * cov( 3, 0 ) - cov( 0, 2 ) * cov( 1, 1 ) * cov( 3, 0 ) -
                  cov( 0, 1 ) * cov( 1, 0 ) * cov( 3, 2 ) - cov( 1, 2 ) * cov( 3, 1 ) * cov( 0, 0 );
    minor[3][0] = minor[0][3];
    minor[3][1] = minor[1][3];
    minor[3][2] = minor[2][3];
    minor[3][3] = cov( 0, 0 ) * cov( 1, 1 ) * cov( 2, 2 ) + cov( 1, 0 ) * cov( 2, 1 ) * cov( 0, 2 ) +
                  cov( 0, 1 ) * cov( 1, 2 ) * cov( 2, 0 ) - cov( 0, 2 ) * cov( 1, 1 ) * cov( 2, 0 ) -
                  cov( 0, 1 ) * cov( 1, 0 ) * cov( 2, 2 ) - cov( 1, 2 ) * cov( 2, 1 ) * cov( 0, 0 );
    deter        = std::abs( cov( 0, 0 ) * minor[0][0] - cov( 0, 1 ) * minor[0][1] + cov( 0, 2 ) * minor[0][2] -
                      cov( 0, 3 ) * minor[0][3] );
    invCov[0][0] = minor[0][0] / deter;
    invCov[0][1] = -minor[0][1] / deter;
    invCov[0][2] = minor[0][2] / deter;
    invCov[0][3] = -minor[0][3] / deter;
    invCov[1][0] = -minor[1][0] / deter;
    invCov[1][1] = minor[1][1] / deter;
    invCov[1][2] = -minor[1][2] / deter;
    invCov[1][3] = minor[1][3] / deter;
    invCov[2][0] = minor[2][0] / deter;
    invCov[2][1] = -minor[2][1] / deter;
    invCov[2][2] = minor[2][2] / deter;
    invCov[2][3] = -minor[2][3] / deter;
    invCov[3][0] = -minor[3][0] / deter;
    invCov[3][1] = minor[3][1] / deter;
    invCov[3][2] = -minor[3][2] / deter;
    invCov[3][3] = minor[3][3] / deter;
  }
  return invCov;
}

// Calculate Chi2
float MuonChi2MatchTool::calcChi2( const MuonCandidate& cand, float p, const SigmaPadCache& sigmapadX,
                                   const SigmaPadCache& sigmapadY ) const {
  // build covariance matrices
  MuonCandCovariance covX( cand, p, m_scattercache, sigmapadX );
  MuonCandCovariance covY( cand, p, m_scattercache, sigmapadY );

  // try to get rid of Cholesky decomposition from ROOT libraries
  std::array<std::array<float, 4>, 4> invCovX = invert( covX );
  std::array<std::array<float, 4>, 4> invCovY = invert( covY );

  // take the inverse of covariances
  //  ROOT::Math::CholeskyDecompGenDim<double> decompX(covX.size(), &covX(0, 0));
  //  if (!decompX) return std::numeric_limits<double>::max();
  //  ROOT::Math::CholeskyDecompGenDim<double> decompY(covY.size(), &covY(0, 0));
  //  if (!decompY) return std::numeric_limits<double>::max();
  //  decompX.Invert(&covX(0, 0));
  //  decompY.Invert(&covY(0, 0));
  // compute chi2 from brute force
  float chisqbf = 0.;
  for ( unsigned i = 0; i < cand.size(); ++i ) {
    for ( unsigned j = 0; j < i; ++j ) {
      chisqbf += 2 * ( cand.dx( i ) * invCovX[i][j] * cand.dx( j ) + cand.dy( i ) * invCovY[i][j] * cand.dy( j ) );
    }
    chisqbf += cand.dx( i ) * invCovX[i][i] * cand.dx( i ) + cand.dy( i ) * invCovY[i][i] * cand.dy( i );
  }
  return chisqbf;
}

MuonChi2MatchTool::MuonCandidates
MuonChi2MatchTool::buildCandidates( std::vector<std::pair<float, float>>                             trkExtrap,
                                    std::vector<std::vector<std::pair<const CommonMuonHit*, float>>> dhit,
                                    std::vector<int> matchedStationIndex, unsigned ord ) const {
  std::vector<MuonCandidate> cands;
  cands.reserve( m_maxcomb );

  std::array<unsigned, 5> max    = {0};
  std::array<unsigned, 5> sta    = {0};
  unsigned                dimmax = 0;
  for ( unsigned i = 0; i < ord; ++i ) {
    const auto station = matchedStationIndex[i];
    const auto n       = std::min( unsigned( dhit[station].size() ), unsigned( m_maxnhitinFOI ) );
    if ( !n ) continue;
    sta[dimmax]   = station;
    max[dimmax++] = n;
  }
  unsigned comb = 0;
  for ( MultiIndex<5> idx( dimmax, max ); idx && comb < m_maxcomb; ++idx ) {
    std::array<const CommonMuonHit*, 5> hits{};
    std::array<float, 5>                xextrap, yextrap;
    for ( unsigned dim = 0; dim < idx.size(); ++dim ) {
      hits[dim]    = dhit[sta[dim]][idx[dim]].first;
      xextrap[dim] = trkExtrap[sta[dim]].first;
      yextrap[dim] = trkExtrap[sta[dim]].second;
    }
    cands.emplace_back( idx.size(), hits, xextrap, yextrap );
    ++comb;
  }

  return cands;
}

// =============== MAIN FUNCTION ===============
StatusCode MuonChi2MatchTool::run( const LHCb::Track* track, std::vector<TrackMuMatch>* bestMatches, float& chi2,
                                   unsigned& ndof, std::vector<TrackMuMatch>* spareMatches ) const {
  std::vector<bool> hitOnStation;
  hitOnStation.resize( m_nStations );
  for ( unsigned int S = 0; S != m_nStations; ++S ) { hitOnStation[S] = false; }

  if ( !bestMatches ) { return StatusCode::SUCCESS; }
  if ( bestMatches->size() < 2 ) { return StatusCode::SUCCESS; } // require at least 2 reasonable matches

  std::vector<std::vector<std::pair<const CommonMuonHit*, float>>> dhit;
  dhit.resize( nSt );

  std::vector<int> nhit;
  nhit.resize( nSt );
  for ( unsigned int s = 0; s != nSt; ++s ) nhit[s] = 0;
  int missedStations = nSt;

  std::vector<std::pair<float, float>> trkExtrap;
  trkExtrap.resize( nSt );

  auto getResidual = [&]( const TrackMuMatch& match ) {
    const auto& [muhit, matchSigma, extraX, extraY] = match;
    if ( matchSigma > m_InputMaxSigmaMatch ) { return; }
    int s = muhit->station() - m_myFirstStation; // index for our M2-M5 vectors
    if ( s < 0 || s > ( nSt - 1 ) ) { return; }  // do not use M1
    if ( m_OnlyCrossedHits && muhit->uncrossed() ) { return; }
    if ( hitOnStation[muhit->station()] == false ) { missedStations--; }
    hitOnStation[muhit->station()] = true;
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "MuonChi2MatchTool found match in M" << muhit->station() + 1 << " uncrossed? " << muhit->uncrossed()
              << endmsg;
    }
    nhit[s]++;

    trkExtrap[s] = std::make_pair( extraX, extraY );
    dhit[s].push_back( std::make_pair( muhit, pow( extraX - muhit->x(), 2 ) + pow( extraY - muhit->y(), 2 ) ) );
  };

  std::vector<TrackMuMatch>::iterator ih;
  if ( bestMatches ) {
    for ( ih = bestMatches->begin(); ih != bestMatches->end(); ++ih ) { getResidual( *ih ); }
  }

  if ( missedStations > 0 && spareMatches ) { // also look among low quality muon hits if nothing better was found
    for ( ih = spareMatches->begin(); ih != spareMatches->end(); ++ih ) {
      if ( false == hitOnStation[( std::get<0>( *ih ) )->station()] ) { getResidual( *ih ); }
    }
  }

  if ( missedStations > 2 ) return StatusCode::SUCCESS; // require at least 2 matched stations

  // Sort distsq from closest to farthest hits
  for ( unsigned int s = 0; s != nSt; ++s ) {
    if ( nhit[s] > 0 ) {
      std::sort( dhit[s].begin(), dhit[s].end(),
                 []( const std::pair<const CommonMuonHit*, float>& left,
                     const std::pair<const CommonMuonHit*, float>& right ) { return left.second < right.second; } );
    }
  }

  unsigned         ord = nSt - missedStations; // ord is the number of matched station (order of our covariance matrix)
  std::vector<int> matchedStationIndex;
  matchedStationIndex.resize( ord ); // map dimension to station

  std::vector<unsigned int> c( ord ); // vector with running indexes for the combinations
  for ( unsigned int s = 0, iO = 0; s != nSt; ++s ) {
    if ( nhit[s] > 0 ) { // use this station
      nhit[s]                 = std::min( m_maxnhitinFOI.value(), nhit[s] );
      matchedStationIndex[iO] = s;
      c[iO]                   = 0;
      iO++;
    }
  }

  // build all candidates, and calculate the chi^2 for each
  auto cands = buildCandidates( trkExtrap, dhit, matchedStationIndex, ord );
  if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Found " << cands.size() << " combinations" << endmsg; }
  std::vector<float> chi2s;
  chi2s.reserve( cands.size() );
  const auto p = track->p();
  std::transform( cands.begin(), cands.end(), std::back_inserter( chi2s ),
                  [this, p]( const MuonCandidate& c ) { return calcChi2( c, p, m_sigmapadX, m_sigmapadY ); } );

  // find smallest chi^2 combination
  unsigned    minidx   = std::min_element( chi2s.begin(), chi2s.end() ) - chi2s.begin();
  const auto& bestcomb = cands[minidx];
  const auto  bestchi2 = chi2s[minidx];
  // store the best CommonMuonHits
  if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "MuonChi2MatchTool best comb is " << minidx << endmsg; }
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "MuonChi2MatchTool output: chi2=" << bestchi2 << " with ndof=" << bestcomb.size() * 2 << endmsg;
  }
  chi2 = bestchi2;
  ord  = bestcomb.size();
  ndof = 2 * ord;

  // compute chi2 contribution (requires at least 2 matched stations)
  if ( ord >= 2 ) {
    std::vector<float> bestcand_chi2contrib;
    bestcand_chi2contrib.reserve( ord );
    for ( unsigned skip = 0; skip < ord; skip++ ) {
      float delta2 = std::pow( bestcomb.dx( skip ), 2 ) + std::pow( bestcomb.dy( skip ), 2 );
      std::array<const CommonMuonHit*, 5> pbestcand_hits;
      std::array<float, 5>                pbestcand_xextrap, pbestcand_yextrap;
      unsigned                            iOr = 0;
      for ( unsigned iO = 0; iO < ord; iO++ ) {
        if ( iO != skip ) {
          unsigned s             = bestcomb.hit( iO )->station() - m_myFirstStation;
          pbestcand_hits[iOr]    = bestcomb.hit( iO );
          pbestcand_xextrap[iOr] = trkExtrap[s].first;
          pbestcand_yextrap[iOr] = trkExtrap[s].second;
          iOr++;
        }
      }
      MuonCandidate pbestcand( ord - 1, pbestcand_hits, pbestcand_xextrap, pbestcand_yextrap );
      float         delta_chi2 = bestchi2 - calcChi2( pbestcand, p, m_sigmapadX, m_sigmapadY );
      bestcand_chi2contrib.push_back( delta_chi2 >= 0 ? std::sqrt( delta2 / delta_chi2 ) : -1. );
    }
  }
  return StatusCode::SUCCESS;
}

DECLARE_COMPONENT( MuonChi2MatchTool )

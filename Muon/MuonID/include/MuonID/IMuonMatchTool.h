/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Muon/TileID.h"
#include "Event/Track.h"
#include "MuonDAQ/CommonMuonHit.h"

#include "GaudiKernel/IAlgTool.h"

#include <tuple>
#include <vector>

/**
 *  tool to match tracks to the muon detector, starting from pre-selected matching tables
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-10
 */

/// TrackMuMatch contains MuonCommonHit, sigma_match, track extrap X, track extrap Y
typedef std::tuple<const CommonMuonHit*, float, float, float> TrackMuMatch;

struct IMuonMatchTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonMatchTool, 3, 0 );

  enum MuonMatchType { NoMatch = 0, Uncrossed = 1, Good = 2 };

  virtual StatusCode run( const LHCb::Track*, std::vector<TrackMuMatch>* bestMatches, float& chi2, unsigned& ndof,
                          std::vector<TrackMuMatch>* spareMatches = nullptr ) const = 0;
};

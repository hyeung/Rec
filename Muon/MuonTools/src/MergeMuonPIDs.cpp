/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MuonPID.h"
#include "Event/MuonPIDs_v2.h"
#include "LHCbAlgs/MergingTransformer.h"

#include <memory>
#include <numeric>

namespace LHCb {

  using MuonPIDsv2  = Event::v2::Muon::PIDs;
  namespace MuonTag = Event::v2::Muon::Tag;
  using StatusMasks = Event::v2::Muon::StatusMasks;
  using MuonFlags   = Event::flags_v<SIMDWrapper::scalar::types, StatusMasks>;

  /**
   *  Merges MuonPID objects from multiple containers into one based on MergeRichPIDs
   *
   *  @author Ricardo Vazquez Gomez
   *  @date   2021-12-16
   */
  struct MergeMuonPIDs final
      : Algorithm::MergingTransformer<MuonPIDsv2( const Gaudi::Functional::vector_of_const_<MuonPIDsv2>& )> {

    MergeMuonPIDs( const std::string& name, ISvcLocator* pSvcLocator )
        : MergingTransformer( name, pSvcLocator, {"InputMuonPIDLocations", {}},
                              {"OutputMuonPIDLocation", MuonPIDLocation::Default} ) {}
    MuonPIDsv2 operator()( const Gaudi::Functional::vector_of_const_<MuonPIDsv2>& inPIDs ) const override;
  };

  MuonPIDsv2 MergeMuonPIDs::operator()( const Gaudi::Functional::vector_of_const_<MuonPIDsv2>& inPIDs ) const {
    // the merged PID container
    MuonPIDsv2 outPIDs;
    // reserve total size
    outPIDs.reserve( std::accumulate( inPIDs.begin(), inPIDs.end(), 0u,
                                      []( const auto sum, const auto& pids ) { return sum + pids.size(); } ) );
    for ( const auto& pids : inPIDs ) {
      for ( const auto& pid : pids.scalar() ) {
        auto newPID = outPIDs.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
        newPID.field<MuonTag::Status>().set( pid.get<MuonTag::Status>().getFlags() );
        newPID.field<MuonTag::Chi2Corr>().set( pid.Chi2Corr() );
        newPID.field<MuonTag::LLMu>().set( pid.LLMu() );
        newPID.field<MuonTag::LLBg>().set( pid.LLBg() );
      }
    }
    return outPIDs;
  }

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( MergeMuonPIDs, "MergeMuonPIDs" )

} // namespace LHCb

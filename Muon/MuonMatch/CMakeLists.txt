###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Muon/MuonMatch
--------------
#]=======================================================================]


gaudi_add_module(MuonMatch
    SOURCES
        src/MuonMatchVeloUTNtuple.cpp
        src/MuonMatchVeloUTSoA.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::AssociatorsBase
        LHCb::DAQEventLib
        LHCb::DigiEvent
        LHCb::EventBase
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::LinkerEvent
        LHCb::MagnetLib
        LHCb::MCEvent
        LHCb::MCInterfaces
        LHCb::MuonDAQLib
        LHCb::MuonDetLib
        LHCb::RecEvent
        LHCb::TrackEvent
        Rangev3::rangev3
        ROOT::Matrix    
)

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

void MakeANNReweighting() {

  // load the data and MC ANN(data/MC) responses

  // const std::string rootDir = "/r03/lhcb/jonesc/ANNPID/ProtoParticlePIDtuples/Test/V1/StripFiltered/WithMVAs/V1/";
  const std::string rootDir = "/Volumes/2TB-Mobile/ANNPID/StripFiltered//WithMVAs/V2/";

  auto* data_f = TFile::Open( ( rootDir + "2015-S23r1-ANNPID-BHADRONCOMPLETEEVENT.root" ).c_str() );
  auto* data   = (TTree*)gDirectory->Get( "ANNPID/DecayTree" );
  auto* mc_f   = TFile::Open( ( rootDir + "Incb-DevMCJun2015-S23r1-ANNPID-Pythia8.root" ).c_str() );
  auto* mc     = (TTree*)gDirectory->Get( "ANNPID/DecayTree" );
  if ( !data || !mc ) return;

  // long tracks only
  const std::string tkSel = "( TrackType==3 )";

  const std::string histOpts = "(200,0.0,1.0)";

  // const std::string func = "log((2*(2015DataMCDiffLongMLPBPCE))/(2-2*(2015DataMCDiffLongMLPBPCE)))/5";
  const std::string func = "2015DataMCDiffLongMLPBPCE";

  data->Draw( ( func + ">>dataH" + histOpts ).c_str(), tkSel.c_str(), "prof" );
  auto* dataH = (TH1F*)gDirectory->Get( "dataH" );
  if ( !dataH ) return;
  dataH->SetMarkerColor( kRed );
  dataH->SetLineColor( kRed );
  const auto dataEn = dataH->GetEntries();
  dataH->Scale( 1.0 / dataEn );

  mc->Draw( ( func + ">>mcH" + histOpts ).c_str(), tkSel.c_str(), "prof" );
  auto* mcH = (TH1F*)gDirectory->Get( "mcH" );
  if ( !mcH ) return;
  mcH->SetMarkerColor( kBlue );
  mcH->SetLineColor( kBlue );
  const auto mcEn = mcH->GetEntries();
  mcH->Scale( 1.0 / mcEn );

  TCanvas* c = new TCanvas( "MVA1", "MVA1", 1400, 1000 );
  gStyle->SetOptStat( 0 );
  gStyle->SetOptFit( 0 );

  // which plot has biggest bin entry ? Need to draw first.
  auto plots = ( dataH->GetBinContent( dataH->GetMaximumBin() ) > mcH->GetBinContent( mcH->GetMaximumBin() )
                     ? std::make_pair( dataH, mcH )
                     : std::make_pair( mcH, dataH ) );

  plots.first->SetTitle( "DataMC Comparison MVA Response" );
  plots.first->Draw();
  plots.second->Draw( "SAME" );

  // Legion for plot
  TLegend* legend = new TLegend( 0.885, 0.9, 0.935, 0.85 );
  legend->SetTextSize( 0.02 );
  legend->SetMargin( 0.12 );
  legend->AddEntry( dataH, "Data", "p" );
  legend->AddEntry( mcH, "MC", "p" );
  legend->Draw();

  c->SaveAs( "DataMCANNN-Response.pdf" );

  dataH->Divide( mcH );
  dataH->SetTitle( "Data/MC Comparison MVA Response" );

  const auto norma = dataH->GetBinContent( dataH->GetMaximumBin() );
  // const auto norma = dataH->GetBinContent(dataH->GetNbinsX()-1);
  // dataH->Scale( 1.0/norma );
  dataH->Scale( mcEn / dataEn );

  dataH->Fit( "pol9" );

  c = new TCanvas( "MVA2", "MVA2", 1400, 1000 );

  // auto * spline = new TSpline3(dataH);
  // spline->SaveAs("DataMCANNN.cpp");

  c->SetLogy( true );
  dataH->Draw();
  // spline->Draw("SAME");
  c->SaveAs( "DataMCANNN-DataOverMC.pdf" );
}

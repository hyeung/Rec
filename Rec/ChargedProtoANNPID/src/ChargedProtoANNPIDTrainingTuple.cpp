/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RecInterfaces/IChargedProtoANNPIDTupleTool.h"

#include "DetDesc/DetectorElement.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "LHCbAlgs/Consumer.h"

// This must be included after everything else because it implies a `using namespace std;`
#include "ChargedProtoANNPIDAlgBase.h"

namespace ANNGlobalPID {

  /**
   *  Makes an ntuple for PID ANN training, starting from ProtoParticles.
   *
   *  @author Chris Jones
   *  @date   2010-03-09
   */
  class ChargedProtoANNPIDTrainingTuple final
      : public LHCb::Algorithm::Consumer<
            void( LHCb::ProtoParticles const&, DetectorElement const& ),
            LHCb::DetDesc::usesBaseAndConditions<ChargedProtoANNPIDAlgBase, IDetectorElement>> {

  public:
    ChargedProtoANNPIDTrainingTuple( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {{"ProtoParticleLocation", LHCb::ProtoParticleLocation::Charged},
                     {"StandardGeometryTop", LHCb::standard_geometry_top}} ) {
      // Turn off Tuple printing during finalize
      setProperty( "NTuplePrint", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

    void operator()( LHCb::ProtoParticles const& protos, DetectorElement const& lhcb ) const override {
      // Loop over all ProtoParticles and fill tuple
      for ( const auto* P : protos ) {
        // Check proto is charged
        if ( !P->track() ) continue;
        // make a tuple
        auto tuple = nTuple( "annInputs", "ProtoParticle PID Information for ANN Training" );
        // Fill variables
        m_tuple->fill( tuple, P, *lhcb.geometry() )
            .orThrow( "Failed to fill Tuple", "ChargedProtoANNPIDTrainingTuple::operator()" );
        // Finally, write the tuple for this ProtoParticle
        tuple->write().orThrow( "Failed to write Tuple", "ChargedProtoANNPIDTrainingTuple::operator()" );
      }
    }

  private:
    ToolHandle<IChargedProtoANNPIDTupleTool> m_tuple{
        this, "TupleTool", "ANNGlobalPID::ChargedProtoANNPIDTupleTool/Tuple", "Pointer to the tuple tool"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( ChargedProtoANNPIDTrainingTuple )

} // namespace ANNGlobalPID

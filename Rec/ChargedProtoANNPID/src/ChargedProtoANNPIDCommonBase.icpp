/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ChargedProtoANNPIDCommonBase.h"
#include "Kernel/STLOStreamRedirect.h"
#include "boost/algorithm/string.hpp"
#include "fmt/format.h"
#include <fstream>
#include <regex>
#include <set>

namespace {
  /// ProtoParticle Extra Info object
  auto inProtoExInfo( LHCb::ProtoParticle::additionalInfo info, double def = -999 ) {
    return [=]( const LHCb::ProtoParticle* proto ) { return proto->info( info, def ); };
  }

  /// Track Extra Info object
  auto inTrackExInfo( LHCb::Track::AdditionalInfo info, double def = -999 ) {
    return [=]( const LHCb::ProtoParticle* proto ) { return proto->track()->info( info, def ); };
  }

  /// RICH DLL accessor
  auto inRichDLL( Rich::ParticleIDType type ) {
    return [=]( const LHCb::ProtoParticle* proto ) {
      return proto->richPID() ? proto->richPID()->particleDeltaLL( type ) : -999;
    };
  }

  /// The coordinate to return
  enum class Coord : unsigned char { Undefined, X, Y, Z };
  Coord toCoord( std::string_view c ) {
    if ( c == "X" ) return Coord::X;
    if ( c == "Y" ) return Coord::X;
    if ( c == "Z" ) return Coord::X;
    return Coord::Undefined;
  }

  /// The position in LHCb for the state information
  enum class Where : unsigned char { Undefined, Vertex, RICH1Entry, RICH2Entry, RICH1Exit, RICH2Exit };
  Where toWhere( std::string_view w ) {
    if ( w == "Rich1Entry" ) return Where::RICH1Entry;
    if ( w == "Rich2Entry" ) return Where::RICH2Entry;
    if ( w == "Rich1Exit" ) return Where::RICH1Exit;
    if ( w == "Rich2Exit" ) return Where::RICH2Exit;
    if ( w == "Vertex" ) return Where::Vertex;
    return Where::Undefined;
  }

  /// Track (x,y,z) position
  auto inTrackXYZ( Coord coord, Where where, ToolHandle<ITrackStateProvider> const& stateProviderHandle,
                   IGeometryInfo const& geometry ) {
    assert( Coord::Undefined != coord );
    assert( Where::Undefined != where );
    // z positions for each RICHes entry and exit planes
    // clang-format off
    const auto z = ( where == Where::RICH1Entry ?   990
                   : where == Where::RICH1Exit  ?  2165
                   : where == Where::RICH2Entry ?  9450
                   : where == Where::RICH2Exit  ? 11900
                   : 0 );
    // clang-format on
    auto* stateProvider = stateProviderHandle.get();
    if ( !stateProvider ) {
      stateProviderHandle.retrieve().ignore();
      stateProvider = stateProviderHandle.get();
    }
    assert( stateProvider != nullptr );

    /// Compute the input value for a given protoparticle
    return [=, &geometry]( const LHCb::ProtoParticle* proto ) {
      /// returns the required coordinate from a given vector-like object
      auto coordinate = []( const auto& v, Coord coord ) -> double {
        switch ( coord ) {
        case Coord::X:
          return v.x();
        case Coord::Y:
          return v.y();
        case Coord::Z:
          return v.z();
        default:
          return -9999;
        }
      };
      if ( where == Where::Vertex ) {
        // just use the first (vertex) state
        return coordinate( proto->track()->firstState(), coord );
      } else {
        // copy the vertex state
        auto state = proto->track()->firstState();
        // move to required z position
        const auto sc = stateProvider->stateFromTrajectory( state, *proto->track(), z, geometry );
        // return the required coord
        return sc ? coordinate( state, coord ) : -9999;
      }
    };
  }

  ANNGlobalPID::Input getInput_( const std::string& name ) {

    struct CompareName {
      using is_transparent = void;
      bool operator()( const ANNGlobalPID::Input& lhs, const ANNGlobalPID::Input& rhs ) const {
        return lhs.name() < rhs.name();
      }
      bool operator()( std::string_view lhs, const ANNGlobalPID::Input& rhs ) const { return lhs < rhs.name(); }
      bool operator()( const ANNGlobalPID::Input& lhs, std::string_view rhs ) const { return lhs.name() < rhs; }
    };

    // clang-format off
    static const auto inputs = std::set<ANNGlobalPID::Input, CompareName>{ {
      /// Track Momentum
      {"TrackP", []( const LHCb::ProtoParticle* proto ) {
         constexpr auto MaxP = 5000.0 * Gaudi::Units::GeV;
         const auto var = proto->track()->p();
         return var < MaxP ? var : -999;
      }},
      /// Track Transverse Momentum
      {"TrackPt", []( const LHCb::ProtoParticle* proto ) {
         constexpr auto MaxPt = 1000.0 * Gaudi::Units::GeV;
         const auto var = proto->track()->pt();
         return var < MaxPt ? var : -999;
      }},
      {"TrackLikelihood", []( const LHCb::ProtoParticle* proto ) {
          const auto var = proto->track()->likelihood();
          return var > -120.0 ? var : -999;
      }},
      {"TrackGhostProbability", []( const LHCb::ProtoParticle* proto ) {
          const auto var = proto->track()->ghostProbability();
          return var > 0.00001 ? var : -999;
      }},
      {"TrackCloneDist", []( const LHCb::ProtoParticle* proto ) {
          const auto var = proto->track()->info( LHCb::Track::AdditionalInfo::CloneDist, -999 );
          return var >= 0 ? var : -999;
      }},
      {"TrackFitMatchChi2", inTrackExInfo( LHCb::Track::AdditionalInfo::FitMatchChi2 )},
      {"TrackFitVeloChi2", inTrackExInfo( LHCb::Track::AdditionalInfo::FitVeloChi2 )},
      {"TrackFitVeloNDoF", inTrackExInfo( LHCb::Track::AdditionalInfo::FitVeloNDoF )},
      {"TrackFitTChi2", inTrackExInfo( LHCb::Track:: AdditionalInfo::FitTChi2 )},
      {"TrackFitTNDoF", inTrackExInfo( LHCb::Track:: AdditionalInfo::FitTNDoF )},
      {"TrackMatchChi2", inTrackExInfo( LHCb:: Track::AdditionalInfo::MatchChi2 )},
      {"TrackDOCA", []( const LHCb::ProtoParticle* proto ) {
          const auto& s         = proto->track()->firstState();
          const auto z_axis     = Gaudi::Math::XYZLine{ Gaudi::XYZPoint{ 0, 0, 0 },
                                                        Gaudi::XYZVector{ 0, 0, 1 } };
          const auto track_line = Gaudi::Math::XYZLine{ Gaudi::XYZPoint{ s.x(), s.y(), s.z() },
                                                        Gaudi::XYZVector{ s.tx(), s.ty(), 1.0 } };
          return Gaudi::Math::distance( track_line, z_axis );
      }},
      // Rich Variables
      /// Used RICH Aerogel
      {"RichUsedAero", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID()->usedAerogel() : 0;
      }},
      /// Used RICH Rich1 Gas
      {"RichUsedR1Gas", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID()->usedRich1Gas() : 0;
      }},
      /// Used RICH Rich2 Gas
      {"RichUsedR2Gas", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID()->usedRich2Gas() : 0;
      }},
      /// RICH above electron threshold
      {"RichAboveElThres", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID() ->electronHypoAboveThres() : 0;
      }},
      /// RICH above muon threshold
      {"RichAboveMuThres", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID() ->muonHypoAboveThres() : 0;
      }},
      /// RICH above pion threshold
      {"RichAbovePiThres", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID() ->pionHypoAboveThres() : 0;
      }},
      /// RICH above kaon threshold
      {"RichAboveKaThres", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID() ->kaonHypoAboveThres() : 0;
      }},
      /// RICH above proton threshold
      {"RichAbovePrThres", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID() ->protonHypoAboveThres() : 0;
      }},
      /// RICH above deuteron threshold
      {"RichAboveDeThres", []( const LHCb::ProtoParticle* proto ) {
          return proto->richPID() ? proto->richPID() ->deuteronHypoAboveThres() : 0;
      }},
      {"RichDLLe", inRichDLL( Rich::Electron )},
      {"RichDLLmu", inRichDLL( Rich::Muon )},
      {"RichDLLpi", inRichDLL( Rich::Pion )},
      {"RichDLLk", inRichDLL( Rich::Kaon )},
      {"RichDLLp", inRichDLL( Rich::Proton )},
      {"RichDLLd", inRichDLL( Rich::Deuteron )},
      {"RichDLLbt", inRichDLL( Rich::BelowThreshold )},
      // Muon variables
      {"MuonIsLooseMuon", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() ? proto->muonPID()->IsMuonLoose() : 0;
      }},
      {"MuonIsMuon", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() ? proto->muonPID()->IsMuon() : 0;
      }},
      /// Muon # shared hits
      {"MuonNShared", []( const LHCb::ProtoParticle* proto ) {
          return proto->info( LHCb::ProtoParticle:: additionalInfo::MuonNShared, -1.0 ) + 1.0;
      }},
      /// Muon Muon likelihood
      {"MuonMuLL", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->MuonLLMu() : -999;
      }},
      /// Muon background likelihood
      {"MuonBkgLL", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->MuonLLBg() : -999;
      }},
      {"MuonMVA1", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->muonMVA1() : -999;
      }},
      {"MuonMVA2", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->muonMVA2() : -999;
      }},
      {"MuonMVA3", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->muonMVA3() : -999;
      }},
      {"MuonMVA4", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->muonMVA4() : -999;
      }},
      {"MuonChi2Corr", []( const LHCb::ProtoParticle* proto ) {
          return proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->chi2Corr() : -999;
      }},
      // GEC Variables
      {"NumProtoParticles", []( const LHCb::ProtoParticle* proto ) {
          const auto* protos = dynamic_cast<const LHCb::ProtoParticles*>( proto->parent() );
          return protos ? protos->size() : -999;
      }},
      /// Number of CALO Hypos
      {"NumCaloHypos", []( const LHCb::ProtoParticle* proto ) { return proto->calo().size(); }},
      // Proto Extra Info variables with specific default requirements, not -999
      {"InAccMuon", inProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccMuon, 0 )},
      {"InAccEcal", inProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccEcal, 0 )},
      {"InAccHcal", inProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccHcal, 0 )},
      {"InAccPrs", inProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccPrs, 0 )},
      {"InAccSpd", inProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccSpd, 0 )},
      {"InAccBrem", inProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccBrem, 0 )},
      /// Calo Ecal chi^2
      {"CaloEcalChi2", []( const LHCb::ProtoParticle* proto ) {
          auto var = proto->info( LHCb::ProtoParticle::additionalInfo::CaloEcalChi2, -999 );
          return ( var < -100 || var > 9999.99 ) ? -999 : var;
      }},
      /// Calo Brem chi^2
      {"CaloBremChi2", []( const LHCb::ProtoParticle* proto ) {
          auto var = proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremChi2, -999 );
          return ( var < -100 || var > 9999.99 ) ? -999 : var;
      }} ,
      /// Calo Cluster chi^2
      { "CaloClusChi2", []( const LHCb::ProtoParticle* proto ) {
          auto var = proto->info( LHCb::ProtoParticle::additionalInfo::CaloClusChi2, -999 );
          return ( var < -100 || var > 999.99 ) ? -999 : var;
      }}
    } };
    // clang-format on

    auto i = inputs.find( name );
    if ( i != inputs.end() ) return *i;

    // Generic ProtoParticle Extra Info
    return {name, inProtoExInfo( LHCb::ProtoParticle::convertExtraInfo( name ) )};
  }

} // namespace

//=============================================================================
template <class PBASE>
typename ANNGlobalPID::Input::ConstVector
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::getInputs( const StringInputs&  names,
                                                              IGeometryInfo const& geometry ) const {
  typename Input::ConstVector inputs;
  inputs.reserve( names.size() );
  std::transform( names.begin(), names.end(), std::back_inserter( inputs ),
                  [this, &geometry]( const auto& name ) { return getInput( name, geometry ); } );
  return inputs;
}
//=============================================================================

//=============================================================================
template <class PBASE>
typename ANNGlobalPID::Input
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::getInput( std::string const&   name,
                                                             IGeometryInfo const& geometry ) const {

  // first try the ones which capture the state provider
  const auto re = std::regex{"Track(Vertex|Rich(1|2)(Entry|Exit))(X|Y|Z)"};
  if ( std::smatch m; std::regex_match( name, m, re ) ) {
    return {name, inTrackXYZ( toCoord( m[4].str() ), toWhere( m[1].str() ), m_trStateP, geometry )};
  }

  // finally try the 'trivial' list
  return getInput_( name );
}
//=============================================================================

//=============================================================================
// Cut constructor
//=============================================================================
template <class PBASE>
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::Cut::Cut( IGeometryInfo const& geometry, std::string desc,
                                                             ChargedProtoANNPIDCommonBase<PBASE> const* parent )
    : m_desc( std::move( desc ) ), m_variable{"<empty>", []( const LHCb::ProtoParticle* ) { return 0.; }} {

  // Cuts must have a precise form. Either
  //    variable > value
  // or
  //    variable < value

  // Parse the cut string
  if ( std::smatch m; std::regex_match( m_desc, m, std::regex{"(\\S+)\\s*(<=|<|>=|>)\\s*(\\S+)"} ) ) {
    m_variable = parent->getInput( m[1].str(), geometry ); // Get the variable from its name
    m_OK       = setDelim( m[2].str() );                   // Delimitor
    m_cut      = std::stod( m[3].str() );                  // The cut value
  } else {
    parent->error() << "Cannot decode cut string '" << m_desc << "'" << endmsg;
    m_OK = false;
  }

  // Remove spaces from the cached description string
  boost::erase_all( m_desc, " " );
}

//=============================================================================
// Get ANN output for TMVA Reader helper
//=============================================================================
template <class PBASE>
float ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::TMVAReaderANN:: //
    GetMvaValue( const typename ANNHelper::InVars& vars ) const {
  // lock...
  auto _ = std::scoped_lock{m_varsLock};
  // copy vars to TMVA vector...
  assert( vars.size() == m_vars.size() );
  m_vars.assign( vars.begin(), vars.end() );
  // get the output and return
  return m_reader->EvaluateMVA( "PID" );
}

//=============================================================================
// NetConfig Constructor
//=============================================================================
template <class PBASE>
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::NetConfig::NetConfig(
    std::string const& tkType, std::string const& pidType, std::string const& netVersion,
    ChargedProtoANNPIDCommonBase<PBASE> const* parent, IGeometryInfo const& geometry ) {

  // Assume init will go OK until proved otherwise
  bool OK = true;

  // Check the configuration
  if ( tkType.empty() ) {
    parent->Error( "No TrackType specified" ).ignore();
    OK = false;
  }
  if ( pidType.empty() ) {
    parent->Error( "No PIDType specified" ).ignore();
    OK = false;
  }
  if ( netVersion.empty() ) {
    parent->Error( "No NetVersion specified" ).ignore();
    OK = false;
  }

  // Config file name
  const std::string filename_extension = "_ANN.txt";
  const std::string configFile         = "GlobalPID_" + pidType + "_" + tkType + filename_extension;

  // Determine where to load the data files from.
  // First try via env var.
  const std::string paramEnv = "CHARGEDPROTOANNPIDROOT";
  if ( !getenv( paramEnv.c_str() ) ) {
    OK = false;
    parent->Error( "$" + paramEnv + " not set" ).ignore();
  }
  std::string   paramRoot = ( std::string( getenv( paramEnv.c_str() ) ) + "/data/" + netVersion + "/" );
  std::ifstream config( ( paramRoot + configFile ).c_str() );
  if ( !config.is_open() ) {
    // That failed. So try as a local file (i.e. in Ganga).
    parent->Warning( "Failed to open " + paramRoot + configFile + ".. Trying local file..." ).ignore();
    paramRoot = "data/" + netVersion + "/";
    config.open( ( paramRoot + configFile ).c_str() );
  }

  // Open the config file
  if ( config.is_open() ) {

    // Read the particle type
    config >> m_particleType;
    if ( !boost::iequals( m_particleType, pidType ) ) {
      OK = false;
      parent->Error( "Mis-match in particle types " + m_particleType + " != " + pidType ).ignore();
    }

    // Read the track Type
    config >> m_trackType;
    if ( !boost::iequals( m_trackType, tkType ) ) {
      OK = false;
      parent->Error( "Mis-match in track types " + m_trackType + " != " + tkType ).ignore();
    }

    // Track selection cuts file name
    std::string cutsFile;
    config >> cutsFile;

    // Split up files seperated by comma
    std::vector<std::string> files;
    boost::split( files, cutsFile, boost::is_any_of( "," ) );
    for ( const auto& file : files ) {

      // open cuts file
      const auto    fullFile = paramRoot + file;
      std::ifstream cuts( fullFile.c_str() );

      // If OK, read
      if ( !cuts.is_open() ) {
        OK = false;
        parent->Error( "Track Selection cuts file '" + fullFile + "' cannot be opened" ).ignore();
      } else {

        // Read the cuts and create a cut object for each
        std::string cut;
        while ( std::getline( cuts, cut ) ) {
          // Skip empty lines or comments
          if ( !cut.empty() && cut.find( "#" ) == std::string::npos ) {
            // try and make a cut for this string
            m_cuts.emplace_back( geometry, cut, parent );
            if ( !m_cuts.back().isOK() ) {
              OK = false;
              parent->Error( "Failed to decode selection cut '" + cut + "'" ).ignore();
            }
          }
        }

        // close the file
        cuts.close();
      }

    } // loop over cuts file

    // Read the network type
    std::string annType;
    config >> annType;

    // read parameters file name
    std::string paramFileName;
    config >> paramFileName;
    paramFileName = paramRoot + paramFileName; // add full path

    // test opening of network parameters file
    {
      std::ifstream netparamtest( paramFileName.c_str() );
      if ( !netparamtest.is_open() ) {
        parent->Error( "Network parameters file '" + paramFileName + "' cannot be opened" ).ignore();
        OK = false;
      }
      netparamtest.close();
    }

    // Read the list of inputs
    std::string  input;
    StringInputs inputs;
    while ( config >> input ) {
      // Skip empty lines and comments
      if ( !input.empty() && input.find( "#" ) == std::string::npos ) { inputs.emplace_back( input ); }
    }
    // if all is OK so far, Load the network helper object
    if ( OK ) {
      if ( "TMVA" == annType ) {
        // First see if we have a built in C++ implementation for this case
        m_netHelper = std::make_unique<TMVAImpANN>( netVersion, particleType(), trackType(), inputs, parent, geometry );
        if ( !m_netHelper->isOK() ) {
          // No, so try again with a TMVA Reader
          parent->warning() << "Compiled TMVA implementation not available for " << netVersion << " " << particleType()
                            << " " << trackType() << " -> Reverting to Generic XML Reader" << endmsg;
          m_netHelper = std::make_unique<TMVAReaderANN>( paramFileName, inputs, parent, geometry );
        }
      } else {
        parent->Error( "Unknown ANN type '" + annType + "'" ).ignore();
        OK = false;
      }

      // is the final helper OK ?
      if ( OK && m_netHelper.get() && m_netHelper->isOK() ) {
        // print a summary of the configuration
        parent->info() << fmt::format( "ANNPID : Tune={:<13} TrackType={:<12} Particle={:<12}", netVersion, trackType(),
                                       particleType() )
                       << endmsg;
        if ( parent->msgLevel( MSG::DEBUG ) ) {
          parent->debug() << "Classifier type  = " << annType << endmsg                     //
                          << "ConfigFile       = " << paramRoot << configFile << endmsg     //
                          << "ParamFile        = " << paramFileName << endmsg               //
                          << "ANN inputs (" << inputs.size() << ")  = " << inputs << endmsg //
                          << "Preselection Cuts (" << m_cuts.size() << ") = " << m_cuts << endmsg;
        }
      } else {
        parent->Error( "Problem configuring classifier" ).ignore();
        OK = false;
      }
    }

  } else {
    OK = false;
    parent->Error( "Failed to open configuration file '" + paramRoot + configFile + "'" ).ignore();
  }

  // Close the config file
  config.close();

  // If something went wrong, clean up
  if ( !OK ) { cleanUp(); }
}

//=============================================================================

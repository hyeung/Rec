/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "../../TMVAImpFactory.h"

namespace ANNGlobalPID {

  namespace MCUpTuneV1 {
#include "GlobalPID_Muon_Downstream_TMVA.class.C"
  } // namespace MCUpTuneV1

  namespace {
    constexpr auto tune  = Tunes::MCUpTune_V1;
    constexpr auto track = LHCb::Track::Types::Downstream;
    constexpr auto part  = Rich::Muon;
  } // namespace

  template <>
  void TMVAImpFactory::add<tune, track, part>() {
    add<MCUpTuneV1::ReadMuon_Downstream_TMVA>( tune, part, track );
  }

} // namespace ANNGlobalPID

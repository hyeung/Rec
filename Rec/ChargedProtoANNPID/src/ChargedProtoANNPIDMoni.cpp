/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Consumer.h"

// This must be included after everything else because it implies a `using namespace std;`
#include "ChargedProtoANNPIDAlgBase.h"

namespace ANNGlobalPID {

  /**
   *  Monitor for the ANNPID
   *
   *  @author Chris Jones
   *  @date   2012-01-12
   */

  class ChargedProtoANNPIDMoni final
      : public LHCb::Algorithm::Consumer<void( LHCb::ProtoParticles const& ),
                                         Gaudi::Functional::Traits::BaseClass_t<ChargedProtoANNPIDAlgBase>> {

  public:
    ChargedProtoANNPIDMoni( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, {{"ProtoParticleLocation", LHCb::ProtoParticleLocation::Charged}} ) {
      // histo base dir
      setProperty( "HistoTopDir", "PROTO/" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

    void operator()( LHCb::ProtoParticles const& protos ) const override {
      // Loop over ProtoParticles
      for ( const auto P : protos ) {
        // Skip velo tracks (never have ANN PID information)
        if ( P->track()->type() == LHCb::Track::Types::Velo ) { continue; }
        // Get the track type
        const auto type = toString( P->track()->type() );
        // Fill plots
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNe, 0 ), type + "/ElectronANN",
                type + " Electron ANN PID", 0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNmu, 0 ), type + "/MuonANN", type + " Muon ANN PID",
                0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNpi, 0 ), type + "/PionANN", type + " Pion ANN PID",
                0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNk, 0 ), type + "/KaonANN", type + " Kaon ANN PID",
                0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNp, 0 ), type + "/ProtonANN",
                type + " Proton ANN PID", 0.0, 1.0, 100 );
        // CRJ Do not enable by default just yet
        // plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNd, 0 ),
        //        type+"/DeuteronANN", type+" Deuteron ANN PID",
        //        0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNghost, 0 ), type + "/GhostANN",
                type + " Ghost ANN PID", 0.0, 1.0, 100 );
      }
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( ChargedProtoANNPIDMoni )

} // namespace ANNGlobalPID

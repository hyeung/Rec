/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-------------------------------------------------------------------------------
/** @file ChargedProtoANNPIDAlg.cpp
 *
 *  Implementation file for ANN Combined PID algorithm ChargedProtoANNPIDAlg
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   10/09/2010
 */
//-------------------------------------------------------------------------------
#include "ChargedProtoANNPIDToolBase.h"
#include "Interfaces/IProtoParticleTool.h"
#include "Kernel/FPEGuard.h"
#include "fmt/format.h"
#include <memory>

namespace {
  void setProbNN( LHCb::GlobalChargedPID* pid, LHCb::ProtoParticle::additionalInfo pidtype, float value ) {
    if ( !pid ) return;
    using Info = LHCb::ProtoParticle::additionalInfo;
    switch ( pidtype ) {
    case Info::ProbNNe:
      pid->setProbNNe( value );
      break;
    case Info::ProbNNmu:
      pid->setProbNNmu( value );
      break;
    case Info::ProbNNpi:
      pid->setProbNNpi( value );
      break;
    case Info::ProbNNk:
      pid->setProbNNk( value );
      break;
    case Info::ProbNNp:
      pid->setProbNNmu( value );
      break;
    case Info::ProbNNd:
      pid->setProbNNd( value );
      break;
    case Info::ProbNNghost:
      pid->setProbNNghost( value );
      break;
    default:
      break;
    }
  }

  std::optional<float> getProbNN( LHCb::GlobalChargedPID const* pid, LHCb::ProtoParticle::additionalInfo pidtype ) {
    if ( !pid ) return std::nullopt;
    using Info                 = LHCb::ProtoParticle::additionalInfo;
    std::optional<float> value = std::nullopt;
    switch ( pidtype ) {
    case Info::ProbNNe:
      value = pid->ProbNNe();
      break;
    case Info::ProbNNmu:
      value = pid->ProbNNmu();
      break;
    case Info::ProbNNpi:
      value = pid->ProbNNpi();
      break;
    case Info::ProbNNk:
      value = pid->ProbNNk();
      break;
    case Info::ProbNNp:
      value = pid->ProbNNp();
      break;
    case Info::ProbNNd:
      value = pid->ProbNNd();
      break;
    case Info::ProbNNghost:
      value = pid->ProbNNghost();
      break;
    default:
      break;
    }
    if ( value && *value == LHCb::GlobalChargedPID::DefaultProbNN ) value = std::nullopt;
    return value;
  }
} // namespace

namespace ANNGlobalPID {

  //-----------------------------------------------------------------------------
  /** @class ChargedProtoANNPIDAlg ChargedProtoANNPIDAlg.h
   *
   *  Adds ANN PID information to ProtoParticles
   *
   *  @author Chris Jones
   *  @date   2010-03-09
   */
  //-----------------------------------------------------------------------------

  class ChargedProtoParticleAddANNPIDInfo final
      : public extends<ChargedProtoANNPIDToolBase, LHCb::Rec::Interfaces::IProtoParticles> {

  public:
    /// Standard constructor
    ChargedProtoParticleAddANNPIDInfo( const std::string& type, const std::string& name, const IInterface* parent )
        : extends( type, name, parent ) {
      // turn off histos and ntuples
      setProperty( "HistoProduce", false ).ignore();
      setProperty( "NTupleProduce", false ).ignore();
      setProperty( "EvtColsProduce", false ).ignore();
    }

    /// Execute
    StatusCode operator()( LHCb::ProtoParticles& protos, IGeometryInfo const& geometry ) const override {
      // Create a new network configation if needed
      std::call_once( m_netConfigFlag, [this, &geometry]() {
        m_netConfig = std::make_unique<NetConfig>( m_trackType, m_pidType, m_netVersion, this, geometry );
        if ( !m_netConfig->isOK() ) {
          throw GaudiException( "Failed to configure the classifier", "ChargedProtoParticleAddANNPIDInfo",
                                StatusCode::FAILURE );
        }
      } );

      // Load the charged ProtoParticles

      // shortcut to mva
      const auto mva = m_netConfig->netHelper();

      // local cache for input variable storage
      auto vars = mva->inputStorage();

      // Loop over ProtoParticles
      for ( auto proto : protos ) {

        // Select Tracks
        if ( !proto->track() ) { return Error( "Charged ProtoParticle has NULL Track pointer" ); }
        if ( !proto->track()->checkType( m_tkType ) ) continue;

        // Clear current ANN PID information
        LHCb::GlobalChargedPID* pid = proto->globalChargedPID();
        if ( proto->hasInfo( m_protoInfo ) || getProbNN( pid, m_protoInfo ) ) {
          //       std::ostringstream mess;
          //       mess << "ProtoParticle already has '" << m_protoInfo
          //            << "' information -> Replacing.";
          //       Warning( mess.str(), StatusCode::SUCCESS, 1 ).ignore();
          proto->eraseInfo( m_protoInfo );
          setProbNN( pid, m_protoInfo, LHCb::GlobalChargedPID::DefaultProbNN );
        }

        // ANN Track Selection.
        if ( !m_netConfig->passCuts( proto ) ) continue;

        // get the ANN output for this proto
        // [[CHECK ME]]
        if ( !m_filldefaults ) {
          const auto nnOut = mva->getOutput( proto, vars );

          if ( msgLevel( MSG::VERBOSE ) ) {
            verbose() << "ProtoParticle " << *proto << endmsg;
            verbose() << " -> Inputs :";
            for ( const auto& in : mva->inputs() ) { verbose() << " " << in.name() << "=" << in( proto ); }
            verbose() << endmsg;
            verbose() << " -> ANN value = " << nnOut << endmsg;
          }

          // add to protoparticle
          setProbNN( pid, m_protoInfo, nnOut );
        }

      } // loop over protos

      return StatusCode::SUCCESS;
    }

  private:
    /// Network Configuration
    mutable std::unique_ptr<NetConfig> m_netConfig;
    mutable std::once_flag             m_netConfigFlag;

    /// fills ProbNN values to defaults
    Gaudi::Property<bool> m_filldefaults{
        this, "FillDefaults", false,
        "Fills default values, e.g. used for easing transition from this algo to SIMD ones"};

    /// The track type for this instance
    LHCb::Track::Types m_tkType = LHCb::Track::Types::Unknown;

    /// The extra info to fill on the ProtoParticle
    LHCb::ProtoParticle::additionalInfo m_protoInfo = LHCb::ProtoParticle::additionalInfo::NoPID;

    /// Track type
    Gaudi::Property<std::string> m_trackType{this, "TrackType", "UNDEFINED", [&]( auto& ) {
                                               // Determine the track type to fill
                                               if ( parse( m_tkType, m_trackType ).isFailure() ) {
                                                 m_tkType = LHCb::Track::Types::Unknown;
                                               };
                                             }};

    /// PID type
    Gaudi::Property<std::string> m_pidType{
        this, "PIDType", "UNDEFINED", [&]( auto& ) {
          // Proto variable to fill
          if ( "Electron" == m_pidType ) {
            m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNe;
          } else if ( "Muon" == m_pidType ) {
            m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNmu;
          } else if ( "Pion" == m_pidType ) {
            m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNpi;
          } else if ( "Kaon" == m_pidType ) {
            m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNk;
          } else if ( "Proton" == m_pidType ) {
            m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNp;
          } else if ( "Deuteron" == m_pidType ) {
            m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNd;
          } else if ( "Ghost" == m_pidType ) {
            m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNghost;
          } else {
            throw std::invalid_argument( fmt::format( "bad value for {}::PIDType: {}", name(), m_pidType.value() ) );
          }
        }};

    /// The version of the PID networks training to use
    Gaudi::Property<std::string> m_netVersion{this, "NetworkVersion", "MCUpTuneV1"};
  };

  //=============================================================================

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( ChargedProtoParticleAddANNPIDInfo )

  //=============================================================================

} // namespace ANNGlobalPID

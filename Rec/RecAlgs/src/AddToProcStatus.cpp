/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProcStatus.h"
#include "GaudiAlg/GaudiAlgorithm.h"

//-----------------------------------------------------------------------------
// Implementation file for class : AddToProcStatus
//
// 2011-06-15 : Patrick Koppenburg
//-----------------------------------------------------------------------------
/** @class AddToProcStatus AddToProcStatus.h
 *
 *  Add an entry to ProcStatus if some sequeence failed
 *
 *  @author Patrick Koppenburg
 *  @date   2011-06-15
 */
class AddToProcStatus final : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_subsystem{this, "Subsystem", "", "Subsystem that has aborted"}; ///< subsystem
  Gaudi::Property<std::string> m_reason{this, "Reason", "", "Reason"};                           ///< Reason for error
  Gaudi::Property<int>         m_status{this, "Status", -3, "Status Code"};                      ///< return code
  Gaudi::Property<bool>        m_abort{this, "Abort", true, "Abort Event?"}; ///< should processing be aborted?
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( AddToProcStatus )

//=============================================================================
// Initialization
//=============================================================================
StatusCode AddToProcStatus::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by GaudiAlgorithm

  if ( m_reason.empty() ) { return Error( "Please provide a Reason" ); }
  if ( m_subsystem.empty() ) { return Error( "Please provide a Subsystem" ); }

  info() << "Whenever called will add the following to ProcStatus:" << endmsg;
  info() << "  Algorithm: " << name() << ", Subsystem: " << m_subsystem.value() << ", Status: " << m_status.value()
         << ", Abort: " << m_abort.value() << endmsg;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode AddToProcStatus::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  LHCb::ProcStatus* procStat = getOrCreate<LHCb::ProcStatus, LHCb::ProcStatus>( LHCb::ProcStatusLocation::Default );

  // give some indication that we had to skip this event
  // (ProcStatus returns zero status for us in cases where we don't
  // explicitly add a status code)
  procStat->addAlgorithmStatus( name(), m_subsystem, m_reason, m_status, m_abort );
  return Warning( "Processing aborted", StatusCode::SUCCESS, 0 );
}

//=============================================================================

###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

## @package GlobalReco
#  Base configurables for the Global PID
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   15/08/2008

__author__ = "Chris Jones <Christopher.Rob.Jones@cern.ch>"

from LHCbKernel.Configuration import *
from Configurables import ChargedProtoANNPIDConf

# ----------------------------------------------------------------------------------


def fixupName(path, fix):
    s = str(path).rpartition("/")
    return s[1].join([s[0], fix, s[2]])


hypoConverterMap = dict()


def converterForHypo(path):
    # TODO: make all converters (other than the 'split' clusters of the merged Pi0) use the
    #  underlying /Event/Rec/Calo/EcalClusters clusters after they in turn have been converted...
    if str(path).startswith("/Event/"): path = str(path)[len("/Event/"):]
    if str(path) not in hypoConverterMap:

        from Configurables import LHCb__Converters__Calo__Hypo__v1__fromV2 as HypoConverter
        from Configurables import LHCb__Converters__Calo__Hypo__v1__MergedPi0__fromV2 as MergedPi0HypoConverter
        from Gaudi.Configuration import allConfigurables

        if "MergedPi0" in str(path):
            converter = MergedPi0HypoConverter("HypoConverter_" +
                                               str(path).replace('/', '_'))
            converter.InputSplitClusters = allConfigurables[
                'CaloSplitClusterConverter'].OutputClusters
            converter.OutputSplitPhotons = fixupName(
                str(path).replace("Rec", "Rec/Converted"),
                "SplitPhotonsFromMergedPi0")
        else:
            converter = HypoConverter("HypoConverter_" +
                                      str(path).replace('/', '_'))

        converter.InputHypos = path
        converter.InputClusters = allConfigurables[
            'CaloClusterConverter'].OutputClusters
        converter.OutputHypos = str(path).replace("Rec", "Rec/Converted")
        converter.OutputTable = fixupName(converter.OutputHypos, "Link2v2")
        hypoConverterMap[str(path)] = converter
    return hypoConverterMap[str(path)]


hypoTableConverterMap = dict()


def converterForHypo2TrackTable(table_path, hypo_path):
    if str(table_path).startswith("/Event/"):
        table_path = str(table_path)[len("/Event/"):]
    if str(hypo_path).startswith("/Event/"):
        hypo_path = str(hypo_path)[len("/Event/"):]
    if str(table_path) not in hypoTableConverterMap:
        from Configurables import LHCb__Converters__Calo__Hypo2TrackTable__v1__fromV2 as Hypo2TrackTableConverter
        converter = Hypo2TrackTableConverter("Hypo2TrackTableConverter_" +
                                             str(table_path).replace('/', '_'))
        converter.InputTable = table_path
        converter.InputHypotheses = hypo_path
        converter.OutputTable = str(converter.InputTable).replace(
            "Rec", "Rec/Converted")
        hypoTableConverterMap[str(table_path)] = converter
    converter = hypoTableConverterMap[str(table_path)]
    assert (str(converter.InputHypotheses) == str(hypo_path))
    return converter


clusterTableConverterMap = dict()


def converterForCluster2TrackTable(table_path, cluster_path):
    if str(table_path).startswith("/Event/"):
        table_path = str(table_path)[len("/Event/"):]
    if str(cluster_path).startswith("/Event/"):
        cluster_path = str(cluster_path)[len("/Event/"):]
    if str(table_path) not in clusterTableConverterMap:
        from Configurables import LHCb__Converters__Calo__Cluster2TrackTable__v1__fromV2 as Cluster2TrackTableConverter
        converter = Cluster2TrackTableConverter(
            "Cluster2TrackTableConverter_" + str(table_path).replace('/', '_'))
        converter.InputTable = table_path
        converter.InputClusters = cluster_path
        converter.OutputTable = str(converter.InputTable).replace(
            "Rec", "Rec/Converted")
        clusterTableConverterMap[str(table_path)] = converter
    converter = clusterTableConverterMap[str(table_path)]
    assert (str(converter.InputClusters) == str(cluster_path))
    return converter


## @class GlobalRecoConf
#  Configurable for the Global PID reconstruction
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   15/08/2008
class GlobalRecoConf(LHCbConfigurableUser):

    ## Possible used Configurables
    __used_configurables__ = [ChargedProtoANNPIDConf]

    ## Options
    __slots__ = {
        "SpecialData": [],
        "Context": "Offline"  # The context within which to run
        ,
        "RecoSequencer": None  # The sequencer to use
        ,
        "OutputLevel": INFO  # The printout level to use
        ,
        "TrackTypes": ["Long", "Upstream", "Downstream"],
        "TrackCuts": {
            "Long": {
                "Chi2Cut": [0, 5]
            },
            "Upstream": {
                "Chi2Cut": [0, 5]
            },
            "Downstream": {
                "Chi2Cut": [0, 5]
            }
        },
        "AddANNPIDInfo": True,
        "DataType": ""  # Type of data, propagated from application
        ,
        "NoSpdPrs": True  # Upgrade configuration with no SPd/Prs
    }

    ## Configure a track selector with the given name
    def setupTypeTrackSelector(self, tsname, selector):
        from Configurables import TrackSelector
        selector.addTool(TrackSelector, name=tsname)
        ts = getattr(selector, tsname)
        # Set Cuts
        ts.TrackTypes = [tsname]
        cuts = self.getProp("TrackCuts")
        if tsname in cuts:
            for name, cut in cuts[tsname].items():
                ts.setProp("Min" + name, cut[0])
                ts.setProp("Max" + name, cut[1])

    ## Apply the configuration to the given sequence
    def applyConf(self):

        if not self.isPropertySet("RecoSequencer"):
            raise RuntimeError("ERROR : PROTO Sequencer not set")

        seq = self.getProp("RecoSequencer")
        seq.Context = self.getProp("Context")

        # Charged Proto particles
        from Configurables import (
            GaudiSequencer, FunctionalChargedProtoParticleMaker,
            ChargedProtoParticleAddRichInfo, ChargedProtoParticleAddMuonInfo,
            ChargedProtoParticleAddCaloInfo, ChargedProtoParticleAddBremInfo,
            ChargedProtoParticleAddCombineDLLs, DelegatingTrackSelector)
        cseq = GaudiSequencer("ChargedProtoParticles")
        seq.Members += [cseq]

        # Make Charged ProtoParticles
        charged = FunctionalChargedProtoParticleMaker(
            "ChargedProtoPMaker", AddInfo=[])
        charged.addTool(DelegatingTrackSelector, name="TrackSelector")
        tracktypes = self.getProp("TrackTypes")
        charged.TrackSelector.TrackTypes = tracktypes
        for type in tracktypes:
            self.setupTypeTrackSelector(type, charged.TrackSelector)

        # Add PID information
        def addInfo(alg, typ, name):
            t = alg.addTool(typ, name)
            alg.AddInfo += [t]
            return t

        rich = addInfo(charged, ChargedProtoParticleAddRichInfo, "AddRich")
        muon = addInfo(charged, ChargedProtoParticleAddMuonInfo, "AddMuon")
        if not self.getProp("NoSpdPrs"):
            raise RuntimeError("NoPrsSpd false not supported in Run 3")

        calo = addInfo(charged, ChargedProtoParticleAddCaloInfo, "AddCalo")

        ecal_converter1 = converterForHypo(
            "/Event/Rec/Calo/Electrons"
        )  # FIXME: use the propery of the producer... (ClassifyPhotonElectronalg)
        ecal_converter2 = converterForHypo2TrackTable(
            calo.InputElectronMatchLocation, ecal_converter1.OutputHypos)

        calo.InputElectronMatchLocation = ecal_converter2.OutputTable  # HypoTrTable2D

        from Configurables import CaloFutureHypoEstimator
        ecal_hypo = calo.addTool(CaloFutureHypoEstimator,
                                 "CaloFutureHypoEstimator")
        ecal_hypo.ElectronMatchLocation = calo.InputElectronMatchLocation
        ecal_hypo.BremMatchLocation = calo.InputElectronMatchLocation

        brem = addInfo(charged, ChargedProtoParticleAddBremInfo, "AddBrem")
        brem.InputBremMatchLocation = ecal_converter2.OutputTable  # HypoTrTable2D    --  Rec/Calo/ElectronMatch
        brem_hypo = brem.addTool(CaloFutureHypoEstimator,
                                 "CaloFutureHypoEstimator")
        brem_hypo.BremMatchLocation = brem.InputBremMatchLocation
        brem_hypo.ElectronMatchLocation = calo.InputElectronMatchLocation

        # Fill the Combined DLL information in the charged protoparticles
        combine = addInfo(charged, ChargedProtoParticleAddCombineDLLs,
                          "CombDLLs")

        # Fill the sequence
        cseq.Members += [ecal_converter1, ecal_converter2, charged]

        # Neutrals
        from Configurables import FutureNeutralProtoPAlg
        nseq = GaudiSequencer("NeutralProtoParticles")
        seq.Members += [nseq]
        neutral = FutureNeutralProtoPAlg("FutureNeutralProtoPMaker")

        ecal_converter1 = converterForHypo(neutral.Photons)
        neutral.Photons = ecal_converter1.OutputHypos

        mpi0_converter1 = converterForHypo(neutral.MergedPi0s)
        neutral.MergedPi0s = mpi0_converter1.OutputHypos
        neutral.SplitPhotons = mpi0_converter1.OutputSplitPhotons

        from Configurables import CaloFutureHypoEstimator
        neutral_hypo = neutral.addTool(CaloFutureHypoEstimator,
                                       "CaloFutureHypoEstimator")

        neutral_hypo.ElectronMatchLocation = ecal.InputElectronMatchLocation
        neutral_hypo.BremMatchLocation = ecal.InputElectronMatchLocation  # FIXME???

        nseq.Members += [mpi0_converter1, ecal_converter1, neutral]

        # Set output levels
        if self.isPropertySet("OutputLevel"):
            level = self.getProp("OutputLevel")
            charged.OutputLevel = level
            rich.OutputLevel = level
            muon.OutputLevel = level
            calo.OutputLevel = level
            brem.OutputLevel = level
            combine.OutputLevel = level
            neutral.OutputLevel = level

        # ANN PID
        if self.getProp("AddANNPIDInfo"):
            nnpidseq = GaudiSequencer("ANNGPIDSeq")
            cseq.Members += [nnpidseq]
            annconf = ChargedProtoANNPIDConf()
            self.setOtherProps(annconf, ["DataType", "OutputLevel", "Context"])
            annconf.RecoSequencer = nnpidseq


## @class GlobalRecoChecks
#  Configurable for the Global PID reconstruction MC based checking
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   26/02/2010
class GlobalRecoChecks(LHCbConfigurableUser):

    ## Options
    __slots__ = {
        "Sequencer": None,  # The sequencer to add monitors to
        "OutputLevel": INFO,  # The printout level to use
        "Context": "Offline",  # The context within which to run
        "AddANNPIDInfo": True,  # Enable ANNPID monitoring
        "ProtoTupleName": "protoparticles.tuples.root",
        "ANNTupleName": "ProtoPIDANN.tuples.root"
    }

    ## Apply the configuration to the given sequence
    def applyConf(self):

        if not self.isPropertySet("Sequencer"):
            raise RuntimeError("ERROR : Sequencer not set")

        protoSeq = self.getProp("Sequencer")
        protoSeq.Context = self.getProp("Context")

        # The ntuple maker
        from Configurables import ChargedProtoParticleTupleAlg
        protoChecker = ChargedProtoParticleTupleAlg("ChargedProtoTuple")
        protoChecker.NTupleLUN = "PROTOTUPLE"

        # Fill sequence
        protoSeq.Members += [protoChecker]

        # The output ntuple ROOT file
        from Configurables import NTupleSvc
        NTupleSvc().Output += [
            "PROTOTUPLE DATAFILE='" + self.getProp("ProtoTupleName") +
            "' TYP='ROOT' OPT='NEW'"
        ]

        # ANN training ntuple
        if self.getProp("AddANNPIDInfo"):
            from Configurables import (
                ANNGlobalPID__ChargedProtoANNPIDTrainingTuple)
            annTuple = ANNGlobalPID__ChargedProtoANNPIDTrainingTuple(
                "ChargedProtoPIDANNTuple")
            annTuple.NTupleLUN = "ANNPIDTUPLE"
            protoSeq.Members += [annTuple]
            NTupleSvc().Output += [
                "ANNPIDTUPLE DATAFILE='" + self.getProp("ANNTupleName") +
                "' TYP='ROOT' OPT='NEW'"
            ]
            if self.isPropertySet("OutputLevel"):
                annTuple.OutputLevel = self.getProp("OutputLevel")

        # Set output levels
        if self.isPropertySet("OutputLevel"):
            level = self.getProp("OutputLevel")
            protoChecker.OutputLevel = level

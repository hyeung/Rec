###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rec/RecConf
-----------
#]=======================================================================]

gaudi_install(PYTHON)
gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    CaloFuture/CaloFutureReco
    Kernel/LHCbAlgs:LHCbAlgs
    Muon/MuonID:MuonID
    Muon/MuonTrackMonitor
    Rec/ChargedProtoANNPID
    Rec/GlobalReco
)

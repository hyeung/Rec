/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CellMatrix2x2.h"
#include "SubClusterSelectorBase.h"

// ============================================================================
/** @file FutureSubClusterSelector2x2.cpp
 *
 *  Implementation file for class : FutureSubClusterSelector2x2
 *
 *  @author F. Machefert
 *  @date 06/14/2014
 */
// ============================================================================

namespace LHCb::Calo {

  class SubClusterSelector2x2 : public SubClusterSelectorBase {
  public:
    using SubClusterSelectorBase::SubClusterSelectorBase;
    StatusCode
    tag( const DeCalorimeter& calo,
         Event::Calo::Clusters::Entries<SIMDWrapper::InstructionSet::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
             entries ) const override;
    StatusCode
    untag( const DeCalorimeter& calo,
           Event::Calo::Clusters::Entries<SIMDWrapper::InstructionSet::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
               entries ) const override;

  private:
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    StatusCode choice2x2( const DeCalorimeter& calo, LHCb::Event::Calo::Clusters::Entries<simd, behaviour> entries,
                          CellMatrix2x2::SubMatrixType& type, double& energy ) const;

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_no_seed{
        this, "could not find the seed of the cluster in 'choice2x2()'."};
  };

  DECLARE_COMPONENT_WITH_ID( SubClusterSelector2x2, "SubClusterSelector2x2" )

  // ============================================================================
  /** The main processing method
   *  @param cluster pointer to CaloCluster object to be processed
   *  @return status code
   */
  // ============================================================================
  StatusCode SubClusterSelector2x2::tag(
      const DeCalorimeter& calo,
      Event::Calo::Clusters::Entries<SIMDWrapper::InstructionSet::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
          entries ) const {
    double                       energy;
    CellMatrix2x2::SubMatrixType type;
    //
    return choice2x2( calo, entries, type, energy ).andThen( [&] {
      return Functor::Cluster::tagTheSubCluster( entries, CellMatrix2x2{&calo, type}, modify(), mask(),
                                                 DigitStatus::Mask::ModifiedByMax2x2Tagger );
    } );
  }

  // ============================================================================
  /** The main processing method (untag)
   *  @param cluster pointer to CaloCluster object to be processed
   *  @return status code
   */
  // ============================================================================
  StatusCode SubClusterSelector2x2::untag(
      const DeCalorimeter& calo,
      Event::Calo::Clusters::Entries<SIMDWrapper::InstructionSet::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
          entries ) const {
    double                       energy;
    CellMatrix2x2::SubMatrixType type;
    return choice2x2( calo, entries, type, energy ).andThen( [&] {
      return Functor::Cluster::untagTheSubCluster( entries, CellMatrix2x2{&calo, type},
                                                   DigitStatus::Mask::ModifiedByMax2x2Tagger );
    } );
  }

  // ============================================================================
  /** The method that selects the 2x2 cluster out of the full cluster
   *  @param cluster pointer to CaloCluster object to be processed
   *  @param type the selected type of cluster
   *  @param energy the energy of the selected cluster
   *  @return status code
   */
  // ============================================================================
  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
  StatusCode SubClusterSelector2x2::choice2x2( const DeCalorimeter&                            calo,
                                               Event::Calo::Clusters::Entries<simd, behaviour> entries,
                                               CellMatrix2x2::SubMatrixType& type, double& energy ) const {
    // find seed digit
    auto seedEntry = Functor::clusterLocateDigit( entries.begin(), entries.end(), DigitStatus::Mask::SeedCell );
    // check the seed
    if ( entries.end() == seedEntry ) {
      ++m_no_seed;
      return StatusCode::FAILURE;
    }

    auto seedCell = ( *seedEntry ).cellID();

    // loop over all entried

    constexpr auto mode = std::array{CellMatrix2x2::UpperLeft, CellMatrix2x2::UpperRight, CellMatrix2x2::LowerLeft,
                                     CellMatrix2x2::LowerRight};

    std::array<double, 4> e;
    for ( int j = 0; j < 4; ++j ) {
      auto matrix = CellMatrix2x2{&calo, mode[j]};
      e[j]        = std::accumulate( entries.begin(), entries.end(), 0., [&]( double e, const auto& entry ) {
        double frac = matrix( seedCell, entry.cellID() );
        return e + frac * entry.energy();
      } );
    }
    // select the max energy case
    type   = mode[0];
    energy = e[0];
    for ( int i = 1; i < 4; ++i ) {
      if ( e[i] > energy ) {
        energy = e[i];
        type   = mode[i];
      }
    }
    return StatusCode::SUCCESS;
  }

} // namespace LHCb::Calo
// ============================================================================
// The End
// ============================================================================

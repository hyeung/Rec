/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "ClusterSpreadTool.h"

namespace LHCb::Calo {
  /** @file FutureClusterSpreadTool.cpp
   *
   *  Implementation file for class : FutureClusterSpreadTool
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 23/11/2001
   */
  DECLARE_COMPONENT_WITH_ID( ClusterSpreadTool, "FutureClusterSpreadTool" )

  /*  standard finalization method
   *  @return status code
   */
  StatusCode ClusterSpreadTool::finalize() {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " Corrected Clusters, Ratio : " << m_ratio << endmsg;
      debug() << " Corrected Clusters, Et    : " << m_energy << endmsg;
      debug() << " Corrected Clusters, Cells : " << m_cells << endmsg;
    }
    // finalize the base class
    return extends::finalize();
  }
} // namespace LHCb::Calo

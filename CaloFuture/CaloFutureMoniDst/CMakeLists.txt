###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
CaloFuture/CaloFutureMoniDst
----------------------------
#]=======================================================================]

gaudi_add_module(CaloFutureMoniDst
    SOURCES
        src/CaloFutureClusterMonitor.cpp
        src/CaloFutureDigitMonitor.cpp
	src/CaloFutureLEDMonitor.cpp
        src/CaloFutureMoniAlg.cpp
        src/CaloFutureTimeAlignment.cpp
        src/CaloTAEData.cpp
        src/ChargedPIDsChecker.cpp
        src/ChargedPIDsMonitor.cpp
        src/CaloFuturePedestal.cpp
        src/FutureCounterLevel.cpp
    LINK
        AIDA::aida
        Boost::headers
        fmt::fmt
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        Gaudi::GaudiUtilsLib
        LHCb::CaloDetLib
        LHCb::CaloFutureInterfaces
        LHCb::CaloFutureUtils
        LHCb::DAQEventLib
        LHCb::DetDescLib
        LHCb::DigiEvent
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::LinkerEvent
        LHCb::MCEvent
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        LHCb::TrackEvent
        Rec::TrackInterfacesLib
        ROOT::Core
)

lhcb_env(SET CALOFUTUREMONIDSTOPTS ${CMAKE_CURRENT_SOURCE_DIR}/options)

/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloFutureInterfaces/Enums.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CaloHypo.h"
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/STLExtensions.h"
#include <map>
#include <optional>

/** @class IGammaPi0SeparationTool IGammaPi0SeparationTool.h
 *
 *
 *  @author Miriam Calvo Gomez --
 *  @date   2021-06-28
 */

namespace LHCb::Calo::Interfaces {
  struct IGammaPi0Separation : extend_interfaces<IAlgTool> {

    struct Observables {
      double fracE1{0};
      double fracE2{0};
      double fracE3{0};
      double fracE4{0};
      double fracEseed{0};
      double fracE6{0};
      double fracE7{0};
      double fracE8{0};
      double fracE9{0};
      double Et{0};
      double Ecl{0};
      int    area{0};
    };
    // Return the interface ID
    DeclareInterfaceID( IGammaPi0Separation, 2, 0 );

    virtual std::optional<double>      isPhoton( const CaloHypo&                      hypo,
                                                 const LHCb::Event::Calo::v2::Digits& digits ) const    = 0;
    virtual std::optional<double>      isPhoton( Observables const& observables ) const                 = 0;
    virtual std::optional<Observables> observables( const CaloHypo&                      hypo,
                                                    const LHCb::Event::Calo::v2::Digits& digits ) const = 0;
  };
} // namespace LHCb::Calo::Interfaces

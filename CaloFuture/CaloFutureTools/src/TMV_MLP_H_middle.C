/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Class: ReadMLPHMiddle

/* configuration options =====================================================

#GEN -*-*-*-*-*-*-*-*-*-*-*- general info -*-*-*-*-*-*-*-*-*-*-*-

Method         : MLP::MLP
TMVA Release   : 4.2.1         [262657]
ROOT Release   : 6.24/00       [399360]
Creator        : calvom
Date           : Thu Jul  1 15:58:11 2021
Host           : Linux centos7-docker 4.18.0-193.19.1.el8_2.x86_64 #1 SMP Mon Sep 14 14:37:00 UTC 2020 x86_64 x86_64
x86_64 GNU/Linux Training events: 278000 Analysis type  : [Classification]

#OPT -*-*-*-*-*-*-*-*-*-*-*-*- options -*-*-*-*-*-*-*-*-*-*-*-*-

# Set by User:
NCycles: "500" [Number of training cycles]
HiddenLayers: "N" [Specification of hidden layer architecture]
NeuronType: "tanh" [Neuron activation function type]
V: "False" [Verbose output (short form of "VerbosityLevel" below - overrides the latter one)]
VarTransform: "N" [List of variable transformations performed before training, e.g.,
"D_Background,P_Signal,G,N_AllClasses" for: "Decorrelation, PCA-transformation, Gaussianisation, Normalisation, each for
the given class of events ('Al$ H: "True" [Print method-specific help message] TestRate: "5" [Test for overtraining
performed at each #th epochs] UseRegulator: "False" [Use regulator to avoid over-training] # Default: RandomSeed: "1"
[Random seed for initial synapse weights (0 means unique seed for each run; default value '1')] EstimatorType: "CE" [MSE
(Mean Square Estimator) for Gaussian Likelihood or CE(Cross-Entropy) for Bernoulli Likelihood] NeuronInputType: "sum"
[Neuron input function type] VerbosityLevel: "Default" [Verbosity level] CreateMVAPdfs: "False" [Create PDFs for
classifier outputs (signal and background)] IgnoreNegWeightsInTraining: "False" [Events with negative weights are
ignored in the training (but are included for testing and performance evaluation)] TrainingMethod: "BP" [Train with
Back-Propagation (BP), BFGS Algorithm (BFGS), or Genetic Algorithm (GA - slower and worse)] LearningRate: "2.000000e-02"
[ANN learning rate parameter] DecayRate: "1.000000e-02" [Decay rate for learning parameter] EpochMonitoring: "False"
[Provide epoch-wise monitoring plots according to TestRate (caution: causes big ROOT output file!)] Sampling:
"1.000000e+00" [Only 'Sampling' (randomly selected) events are trained each epoch] SamplingEpoch: "1.000000e+00"
[Sampling is used for the first 'SamplingEpoch' epochs, afterwards, all events are taken for training]
SamplingImportance: "1.000000e+00" [ The sampling weights of events in epochs which successful (worse estimator than
before) are multiplied with SamplingImportance, else they are divided.] SamplingTraining: "True" [The training sample is
sampled] SamplingTesting: "False" [The testing sample is sampled] ResetStep: "50" [How often BFGS should reset history]
Tau: "3.000000e+00" [LineSearch "size step"]
BPMode: "sequential" [Back-propagation learning mode: sequential or batch]
BatchSize: "-1" [Batch size: number of events/batch, only set if in Batch Mode, -1 for BatchSize=number_of_events]
ConvergenceImprove: "1.000000e-30" [Minimum improvement which counts as improvement (<0 means automatic convergence
check is turned off)] ConvergenceTests: "-1" [Number of steps (without improvement) required for convergence (<0 means
automatic convergence check is turned off)] UpdateLimit: "10000" [Maximum times of regulator update] CalculateErrors:
"False" [Calculates inverse Hessian matrix at the end of the training to be able to calculate the uncertainties of an
MVA value] WeightRange: "1.000000e+00" [Take the events for the estimator calculations from small deviations from the
desired value to large deviations only over the weight range]
##


#VAR -*-*-*-*-*-*-*-*-*-*-*-* variables *-*-*-*-*-*-*-*-*-*-*-*-

NVar 9
fracE1                  fracE1                fracE1                  fracE1 'F'
[-0.184250742197,0.80723965168] fracE2                  fracE2                fracE2 fracE2
'F'    [-0.164724186063,0.558665275574] fracE3                  fracE3                fracE3
fracE3                                                    'F'    [-0.216716110706,0.844425261021] fracE4
fracE4               fracE4                 fracE4 'F'    [-0.184997737408,0.820176422596]
fracEseed                 fracEseed               fracEseed                 fracEseed 'F'
[0.0330240167677,1.60290372372] fracE6                 fracE6               fracE6 fracE6
'F'    [-0.257074505091,0.832035958767] fracE7                 fracE7               fracE7
fracE7                                                   'F'    [-0.190231963992,0.848194479942] fracE8
fracE8               fracE8                 fracE8 'F'    [-0.194924339652,0.567717313766]
fracE9                 fracE9               fracE9                 fracE9 'F'
[-0.212707176805,0.922476112843] NSpec 0

============================================================================ */

#include "Kernel/STLExtensions.h"
#include "Kernel/TMV_utils.h"
#include <array>
#include <cmath>
#include <string_view>

namespace Data::ReadMLPHMiddle {
  namespace {
    constexpr auto ActivationFnc = []( double x ) {
      // fast hyperbolic tan approximation
      if ( x > 4.97 ) return 1.f;
      if ( x < -4.97 ) return -1.f;
      float x2 = x * x;
      float a  = x * ( 135135.0f + x2 * ( 17325.0f + x2 * ( 378.0f + x2 ) ) );
      float b  = 135135.0f + x2 * ( 62370.0f + x2 * ( 3150.0f + x2 * 28.0f ) );
      return a / b;
    };

    constexpr auto OutputActivationFnc = []( double x ) {
      // sigmoid
      return 1.0 / ( 1.0 + exp( -x ) );
    };

    // Normalization transformation
    constexpr auto fMin_1 =
        std::array{std::array{-0.180593907833, -0.151716500521, -0.216716110706, -0.163436755538, 0.0578971579671,
                              -0.191029891372, -0.184195950627, -0.192670628428, -0.212707176805},
                   std::array{-0.184250742197, -0.164724186063, -0.171345025301, -0.184997737408, 0.0330240167677,
                              -0.257074505091, -0.190231963992, -0.194924339652, -0.205805703998},
                   std::array{-0.184250742197, -0.164724186063, -0.216716110706, -0.184997737408, 0.0330240167677,
                              -0.257074505091, -0.190231963992, -0.194924339652, -0.212707176805}};

    constexpr auto fMax_1 =
        std::array{std::array{0.80723965168, 0.545081615448, 0.704621016979, 0.755800306797, 1.60290372372,
                              0.765266060829, 0.791082143784, 0.566249787807, 0.784780263901},
                   std::array{0.806771039963, 0.558665275574, 0.844425261021, 0.820176422596, 1.52706480026,
                              0.832035958767, 0.848194479942, 0.567717313766, 0.922476112843},
                   std::array{0.80723965168, 0.558665275574, 0.844425261021, 0.820176422596, 1.60290372372,
                              0.832035958767, 0.848194479942, 0.567717313766, 0.922476112843}};

    // weight matrix from layer 0 to 1
    constexpr auto fWeightMatrix0to1 = std::array{
        std::array{0.426463632595623, -1.38237545756149, -4.85003147829583, 1.27932157357076, 3.26240360130551,
                   -4.65628893942299, -4.53637419915128, 2.18704292249009, 2.0127102839142, 0.300765820730824},
        std::array{-1.30800244254481, 1.4299526099625, 2.11840101219231, -7.19261653439867, 3.83397826608208,
                   2.54282273620988, -0.258105783637862, 1.51790754116778, 2.26818859779945, 0.682959593717636},
        std::array{-2.10648270307239, -4.46130054620954, -1.81681336348268, 0.625105470132663, 1.68273178952565,
                   0.549617270996029, 1.20390938181562, 0.787987791473059, 1.44409077848589, -1.20381923705681},
        std::array{-0.764702145686324, -1.41231670761937, 0.00240709884539659, -2.19074554230603, -3.21900173861052,
                   8.64571889411421, -2.3124147378762, -1.16968438002578, 1.18903315076348, -0.0458687441037299},
        std::array{0.931724778704851, -5.13319071255125, 1.09385148295311, 1.49675396020746, 2.9637566128948,
                   1.69381206860436, 1.42591624465428, -5.66176431560232, 1.48673202683762, -0.95769537374774},
        std::array{1.14747463710574, 1.2163218782297, -3.00358432084211, 2.12649482779284, 4.46630512516987,
                   2.104051340277, -3.61802726011812, 1.89210062858265, -3.12408441647481, 0.139257938702907},
        std::array{-0.117621550012976, 1.13285328183916, 0.671910460885041, 0.909831095109371, 1.85290878612333,
                   0.958614081675939, -0.729608144005077, -5.73280730993973, -0.758884167733876, -0.830555601484293},
        std::array{2.27999343816693, 1.96204913777368, 3.35928312119271, -8.01016553360505, 5.014059543723,
                   -9.76270654506313, 2.75446545593349, 2.19259429637269, 2.58487857430284, -1.99874115937752},
        std::array{6.74504659868593, -2.49689311774881, 5.03178404975525, -3.48045522471043, -4.64942921234117,
                   -3.9441215530281, -0.553928795961498, -2.68297046244042, -1.1994436929941, 0.918096364737461}};
    // weight matrix from layer 1 to 2
    constexpr auto fWeightMatrix1to2 =
        std::array{-0.670682765187192, 1.74035718411496, 2.03609272111418,  -1.60362106235283, -1.82658677442234,
                   1.36798261716024,   1.73691813529119, -1.68901373458535, 1.1356110110776,   -4.20377765027302};

    // the training input variables
    constexpr auto validator =
        TMV::Utils::Validator{"ReadMLPHMiddle", std::tuple{"fracE1", "fracE2", "fracE3", "fracE4", "fracEseed",
                                                           "fracE6", "fracE7", "fracE8", "fracE9"}};

    constexpr auto transformer = TMV::Utils::Transformer{fMin_1, fMax_1};
    constexpr auto l0To1       = TMV::Utils::Layer{fWeightMatrix0to1, ActivationFnc};
    constexpr auto l1To2       = TMV::Utils::Layer{fWeightMatrix1to2, OutputActivationFnc};
    constexpr auto MVA         = TMV::Utils::MVA{validator, transformer, 2, l0To1, l1To2};
  } // namespace
} // namespace Data::ReadMLPHMiddle

struct ReadMLPHMiddle final {

  // constructor
  ReadMLPHMiddle( LHCb::span<const std::string_view, 9> theInputVars ) {
    Data::ReadMLPHMiddle::MVA.validate( theInputVars );
  }

  // the classifier response
  // "inputValues" is a vector of input values in the same order as the
  // variables given to the constructor
  static constexpr double GetMvaValue( LHCb::span<const double, 9> input ) {
    return Data::ReadMLPHMiddle::MVA( input );
  }
};

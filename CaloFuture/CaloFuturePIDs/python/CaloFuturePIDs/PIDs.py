#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# =============================================================================
## The major building blocks of CaloFuturerimeter PID
#  @author Vanya BELYAEV Ivan.Belyaev@nikhe.nl
#  @date 2008-07-17
# =============================================================================
"""
The major building blocks of CaloFuturerimeter PID
"""
from __future__ import print_function
# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
# =============================================================================
from Gaudi.Configuration import *

from Configurables import GaudiSequencer

from CaloKernel.ConfUtils import (setTheProperty, addAlgs)


# =============================================================================
## define "inEcalAcceptance" algorithm
def inEcalAcc(enableRecoOnDemand, trackLocation="", matchTrTypes=[]):
    """

    define 'inEcalFutureAcceptance' algorithm

    """

    from Configurables import (InEcalFutureAcceptance,
                               InEcalFutureAcceptanceAlg)

    ## check if the track is in Ecal acceptance
    inEcal = InEcalFutureAcceptanceAlg('InECALFuture')

    if trackLocation:
        inEcal.Inputs = trackLocation

    if matchTrTypes != []:
        inEcal.AcceptedType = matchTrTypes

    return inEcal


## ============================================================================
## define the minimal track match sequnce for photon recontruction
def trackMatch(enableRecoOnDemand,
               trackLocation="",
               matchTrTypes=[],
               fastReco=False,
               clusterLocation=""):
    """

    Define the minimal track match sequnce for photon reconstruction

    """

    from Configurables import FuturePhotonMatchAlg

    seq = GaudiSequencer('CaloFutureTrackMatch')

    ## perform the actual track <-> cluster match
    clmatch = FuturePhotonMatchAlg('FutureClusterMatch')

    if clusterLocation != '':
        clmatch.Calos = clusterLocation

#    log.info(" ========= trackLocation = " + trackLocation )

## check if the track is in Ecal acceptance
    inEcal = inEcalAcc(enableRecoOnDemand, trackLocation, matchTrTypes)

    seq.Members = [inEcal, clmatch]

    if matchTrTypes != []:
        clmatch.AcceptedType = matchTrTypes

    if trackLocation:
        clmatch.Tracks = trackLocation

    log.debug("Configure Cluster Track Match : '%s'" % (seq.name()))

    return seq


# =================================================================================
## define various CaloFuture PIDs evaluation
def caloPIDs(enableRecoOnDemand,
             list,
             trackLocation='',
             matchTrTypes=[],
             caloTrTypes=[],
             bremTrTypes=[],
             skipNeutrals=False,
             skipCharged=False,
             fastPID=False,
             clusterLocation='',
             name=''):
    """
    Define various CaloFuture PIDs evaluation
    """

    from Configurables import (
        InEcalFutureAcceptance, InEcalFutureAcceptanceAlg,
        InBremFutureAcceptance, FutureInBremFutureAcceptanceAlg,
        InHcalFutureAcceptance, InHcalFutureAcceptanceAlg)

    from Configurables import (FutureElectronMatchAlg, BremMatchAlgFuture)

    from Configurables import (FutureTrack2EcalEAlg, FutureTrack2HcalEAlg)

    from Configurables import (FutureEcalChi22ID, BremChi22IDFuture,
                               FutureClusChi22ID, FutureEcalPIDeAlg,
                               BremPIDeAlgFuture, FutureHcalPIDeAlg,
                               FutureEcalPIDmuAlg, FutureHcalPIDmuAlg)

    ##  SANITY CHECK FOR TRACK TYPES (caloTrTypes must be included  in matchTrTypes)
    defMatchTrTypes = ["Long", "Downstream", "Ttrack"]
    if matchTrTypes != []:
        defMatchTrTypes = matchTrTypes
    for item in caloTrTypes:
        if item not in defMatchTrTypes:
            raise AttributeError(
                'TrackTypes for ClusterMatching must include CaloFuturePID TrackTypes'
            )

    if matchTrTypes != []:
        log.info(" ! Will use track types = % s for matching" % matchTrTypes)
    if caloTrTypes != []:
        log.info(" ! Will use track types = % s for caloPID" % caloTrTypes)
    if bremTrTypes != []:
        log.info(" ! Will use track types = % s for bremPID" % bremTrTypes)

    ## global PID sequence
    _name = 'CaloFuturePIDs' + name
    seq = GaudiSequencer(_name)
    seq.Members = []

    ## add Charged
    if not skipCharged:
        _name = 'ChargedPIDs' + name

        charged = GaudiSequencer(_name)

        # inAcceptance
        inAcc = GaudiSequencer('InCaloFutureAcceptance')

        inECAL = inEcalAcc(enableRecoOnDemand, trackLocation, matchTrTypes)
        inHCAL = InHcalFutureAcceptanceAlg('InHCALFuture')
        inBREM = FutureInBremFutureAcceptanceAlg('InBREMFuture')
        inAcc.Members = [inECAL, inHCAL, inBREM]

        # matching
        match = GaudiSequencer('CaloFutureMatch')
        cluster = trackMatch(enableRecoOnDemand, trackLocation, matchTrTypes,
                             fastPID, clusterLocation)

        electron = FutureElectronMatchAlg("ElectronMatchFuture")
        brem = BremMatchAlgFuture("BremMatchFuture")

        match.Members = [cluster, electron, brem]

        # energy
        energy = GaudiSequencer('CaloFutureEnergy')
        ecalE = FutureTrack2EcalEAlg('EcalEFuture')
        hcalE = FutureTrack2HcalEAlg('HcalEFuture')
        energy.Members = [ecalE, hcalE]

        # Chi2's
        chi2 = GaudiSequencer('CaloFutureChi2')
        eChi2 = FutureEcalChi22ID('FutureEcalChi22ID')
        bChi2 = BremChi22IDFuture('BremChi22IDFuture')
        cChi2 = FutureClusChi22ID('FutureClusChi22ID')
        chi2.Members = [eChi2, bChi2, cChi2]

        # DLL
        dlle = GaudiSequencer('CaloFutureDLLeFuture')
        dllmu = GaudiSequencer('CaloFutureDLLmuFuture')
        ecale = FutureEcalPIDeAlg('EcalPIDeFuture')
        breme = BremPIDeAlgFuture('BremPIDeFuture')
        hcale = FutureHcalPIDeAlg('HcalPIDeFuture')
        ecalmu = FutureEcalPIDmuAlg('EcalPIDmuFuture')
        hcalmu = FutureHcalPIDmuAlg('HcalPIDmuFuture')
        dllmu.Members = [ecalmu, hcalmu]
        dlle.Members = [ecale, breme, hcale]

        # alternative sequence (per caloPID technique)
        ecalT = GaudiSequencer('EcalPIDFuture')
        ecalT.Members = [
            inECAL, cluster, electron, ecalE, eChi2, cChi2, ecale, ecalmu
        ]

        hcalT = GaudiSequencer('HcalPIDFuture')
        hcalT.Members = [inHCAL, hcalE, hcale, hcalmu]

        bremT = GaudiSequencer('BremPIDFuture')
        bremT.Members = [inBREM, brem, bChi2, breme]

        # === Redefine accepted track types ===
        # matchTrTypes propagated to the relevant modules (inEcalAcc & clusterMatch)
        if caloTrTypes != []:
            electron.AcceptedType = caloTrTypes
            cChi2.AcceptedType = caloTrTypes
            eChi2.AcceptedType = caloTrTypes
            ecalE.AcceptedType = caloTrTypes
            ecale.AcceptedType = caloTrTypes
            ecalmu.AcceptedType = caloTrTypes
            inHCAL.AcceptedType = caloTrTypes
            hcalE.AcceptedType = caloTrTypes
            hcale.AcceptedType = caloTrTypes
            hcalmu.AcceptedType = caloTrTypes
        if bremTrTypes != []:
            inBREM.AcceptedType = bremTrTypes
            brem.AcceptedType = bremTrTypes
            bChi2.AcceptedType = bremTrTypes
            breme.AcceptedType = bremTrTypes

        # === Override CaloFutureAlgUtils default track location ===
        if trackLocation:
            electron.Tracks = trackLocation
            brem.Tracks = trackLocation
            inBREM.Inputs = trackLocation
            inHCAL.Inputs = trackLocation
            eChi2.Tracks = trackLocation
            bChi2.Tracks = trackLocation
            cChi2.Tracks = trackLocation
            ecalE.Inputs = trackLocation
            hcalE.Inputs = trackLocation

        charged.Members = []
        # updatXe global sequence with charged
        if 'InAcceptance' in list: charged.Members += [inAcc]
        if 'Match' in list: charged.Members += [match]
        if 'Energy' in list: charged.Members += [energy]
        if 'Chi2' in list: charged.Members += [chi2]
        if 'DLL' in list or 'DLLe' in list: charged.Members += [dlle]
        if 'DLL' in list or 'DLLmu' in list: charged.Members += [dllmu]

        # alternative full sequence per technique
        if 'EcalPID' in list: charged.Members += [ecalT]
        if 'BremPID' in list: charged.Members += [bremT]
        if 'HcalPID' in list: charged.Members += [hcalT]
        if charged.Members: addAlgs(seq, charged)

    log.debug('Configure CaloFuture PIDs  Reco : %s' % (seq.name()))

    return seq


def referencePIDs(dataType=''):
    """
    Define various reference Histograms on THS
    """
    hsvc = HistogramSvc('HistogramDataSvc')
    inputs = hsvc.Input

    # photon PDF default
    pfound = False
    for line in inputs:
        if 0 == line.find('CaloFutureNeutralPIDs'): pfound = True

    if pfound:
        log.info(
            "CaloFuturePIDsConf: LUN 'CaloFutureNeutralPIDs' has been defined already"
        )
    else:
        hsvc.Input += [
            "CaloFutureNeutralPIDs DATAFILE='$PARAMFILESROOT/data/PhotonPdf.root' TYP='ROOT'"
        ]

    # charged PDF default
    found = False
    for line in inputs:
        if 0 == line.find('CaloFuturePIDs'): found = True

    if found:
        log.info(
            "CaloFuturePIDsConf: LUN 'CaloFuturePIDs' has been defined already"
        )
    elif 'DC06' == dataType:
        hsvc.Input += [
            "CaloFuturePIDs DATAFILE='$PARAMFILESROOT/data/CaloPIDs_DC06_v2.root' TYP='ROOT'"
        ]
    else:
        hsvc.Input += [
            "CaloFuturePIDs DATAFILE='$PARAMFILESROOT/data/CaloPIDs_DC09_v1.root' TYP='ROOT'"
        ]

    return


# =============================================================================
if '__main__' == __name__:
    print(__doc__)

/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "SelectiveMatchUtils.h"

/** @class SelectiveTrackMatchAlg SelectiveTrackMatchAlg.h
 *
 *  Matches tracks with local clusters in and around calo cell
 *  corresponding to track extrapolation.
 *  Sufficient since resolution is about cell size / sqrt(12).
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

// ============================================================================
/** @file
 *
 *  Implementation file for class SelectiveTrackMatchAlg
 *  It takes inspiration from TrackMatchAlg and ClassifyPhotonElectron
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

namespace LHCb::Calo {

  using namespace LHCb::Calo::TrackUtils;
  using namespace LHCb::Calo::SelectiveMatchUtils;

  using CaloObjects = CaloClusters;
  using OutputData  = Tracks2Clusters;

  using MyState = LHCb::Event::v3::detail::FittedState<
      Tracks::FittedProxy<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::ScatterGather, const Tracks>>;

  // main class
  class SelectiveTrackMatchAlg
      : public Algorithm::Transformer<OutputData( DeCalorimeter const&, CaloObjects const&, TracksInEcal const&,
                                                  cellSizeCovariances const& ),
                                      DetDesc::usesConditions<DeCalorimeter, cellSizeCovariances>> {
  public:
    // standard constructor
    SelectiveTrackMatchAlg( const std::string& name, ISvcLocator* pSvc );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    OutputData operator()( DeCalorimeter const&, CaloObjects const&, TracksInEcal const&,
                           cellSizeCovariances const& ) const override;

  private:
    // properties
    int                   m_nmaxelements;
    Gaudi::Property<int>  m_nsquares{this,
                                    "nNeighborSquares",
                                    1,
                                    [this]( auto& ) { m_nmaxelements = std::pow( 2 * m_nsquares + 1, 2 ); },
                                    Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                    "Number of squares of cells around central cell to scan for clusters"};
    Gaudi::Property<bool> m_usespread{this, "useSpread", false, "Use spread or cellsize/sqrt(12) for uncertainties"};
    Gaudi::Property<CaloPlane::Plane> m_caloplane{
        this, "CaloPlane", CaloPlane::Middle,
        "Plane at the calorimeter of where to extrapolate to (middle is for hadrons)"};

    Gaudi::Property<float> m_threshold{this, "MaxChi2Threshold", 10000., "Maximum allowed chi2 value"};

    // statistics
    mutable Gaudi::Accumulators::StatCounter<>          m_nMatchFailure{this, "#match failure"};
    mutable Gaudi::Accumulators::StatCounter<>          m_nLinks{this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<>          m_nOverflow{this, "#above threshold"};
    mutable Gaudi::Accumulators::StatCounter<float>     m_chi2{this, "average chi2"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_unique{this, "Failed to generate index"};
  };

  DECLARE_COMPONENT_WITH_ID( SelectiveTrackMatchAlg, "SelectiveTrackMatchAlg" )

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================

  SelectiveTrackMatchAlg::SelectiveTrackMatchAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     // Inputs
                     {KeyValue( "Detector", {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )} ),
                      KeyValue( "InputClusters", {CaloFutureAlgUtils::CaloFutureClusterLocation( "Ecal" )} ),
                      KeyValue( "InputTracksInCalo", "" ),
#ifdef USE_DD4HEP
                      KeyValue( "cellSizeCovariances", {"/world:AlgorithmSpecific-" + name + "-cellsizecovariances"} )},
#else
                      KeyValue( "cellSizeCovariances", {"AlgorithmSpecific-" + name + "-cellsizecovariances"} )},
#endif
                     // Outputs
                     {KeyValue( "Output", {CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )} )} ) {
  }

  // ============================================================================
  //   Initialization of algorithm / tool
  // ============================================================================
  StatusCode SelectiveTrackMatchAlg::initialize() {
    auto sc = Transformer::initialize().andThen( [&] {
      addConditionDerivation<cellSizeCovariances( const DeCalorimeter& )>(
          {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )}, inputLocation<cellSizeCovariances>() );
    } );
    return sc;
  }

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  OutputData SelectiveTrackMatchAlg::operator()( DeCalorimeter const& calo, CaloObjects const& caloobjects,
                                                 TracksInEcal const&        tracksincalo,
                                                 cellSizeCovariances const& cellsizecovs ) const {
    // declare output
    OutputData output_table( tracksincalo.from(), &caloobjects );
    output_table.reserve( 5 * tracksincalo.size() );

    // track state from where to extrapolate (linearly) to calo from
    auto const state_loc = extrapolation_stateloc( *tracksincalo.from() );
    if ( !state_loc.has_value() ) {
      throw GaudiException( "Not a valid track type for this calo energy type.", "LHCb::Event::Enum::Track::Type",
                            StatusCode::FAILURE );
    }

    // obtain cluster indeces
    auto caloindex = caloobjects.index();
    if ( !caloindex ) {
      ++m_not_unique;
      return output_table;
    }

    // some info that can be loaded before loop(s)
    const auto plane_at_calo = calo.plane( m_caloplane );
    const auto other_planes  = std::array{calo.plane( CaloPlane::Front ), calo.plane( CaloPlane::Back )};
    const auto cscovs        = cellsizecovs();

    // declare what is needed in the loop
    std::vector<Detector::Calo::CellID> cellids;
    cellids.reserve( m_nmaxelements );
    std::vector<Detector::Calo::CellID> found_caloobjs;
    found_caloobjs.reserve( m_nmaxelements );

    LHCb::State     state;
    Match2D::Vector track_pos, calo_pos;
    Match2D::Matrix track_cov, comb_cov;

    // main loop over tracks
    for ( const auto& trackincalo : tracksincalo.scalar() ) {
      // get state at calo plane
      auto track     = trackincalo.from();
      auto ref_state = track.state( state_loc.value() );
      if ( !propagateToCaloWithCov<MyState>( state, ref_state, plane_at_calo ) ) continue;

      // get closest cell to track extrapolation
      const auto closestcell = getClosestCellID( calo, state.position(), state.slopes(), other_planes );
      if ( !closestcell ) continue;

      // clear up obj/cellid vectors if needed
      found_caloobjs.clear();
      cellids.clear();

      // check if there is a calo object and otherwise look at neighboring cells
      const auto firstcaloobj = caloindex.find( closestcell );
      if ( firstcaloobj != caloindex.end() ) {
        found_caloobjs.push_back( closestcell );
      } else {
        cellids.push_back( closestcell );
        if ( !getNeighborCellIDs( cellids, calo, m_nsquares ) ) continue;
        // loop over local calo objects via cellids
        for ( const LHCb::Detector::Calo::CellID cellid : cellids ) {
          const auto caloobj = caloindex.find( cellid );
          // if cellid does not have a cluster associated to it, continue
          if ( caloobj == caloindex.end() ) continue;
          // otherwise add
          found_caloobjs.push_back( cellid );
        }
        // quit if we didnt find calo objs
        if ( found_caloobjs.empty() ) continue;
      }

      // obtain relevant info for matching from track
      track_pos = {state.x(), state.y()};
      track_cov = state.covariance().Sub<Gaudi::SymMatrix2x2>( 0, 0 );

      // loop over local calo objects and calculate chi2
      for ( const auto& cellid : found_caloobjs ) {
        const auto caloobj = *caloindex.find( cellid );
        // obtain calo obj info
        calo_pos = {caloobj.position().x(), caloobj.position().y()};
        comb_cov = ( m_usespread ? caloobj.spread() : cscovs[caloobj.cellID().area()] ) + track_cov;

        // calculate chi2
        if ( !comb_cov.Invert() ) {
          m_nMatchFailure += 1;
          continue;
        }
        auto chi2 = ROOT::Math::Similarity( calo_pos - track_pos, comb_cov );

        // check if it has proper value
        if ( m_threshold < chi2 ) {
          m_nOverflow += 1;
          continue;
        }

        // only now push proper result
        output_table.add( track, caloobj, chi2 );
      }
    }

    // monitor statistics: number of links and average chi2
    const auto nLinks = output_table.size();
    m_nLinks += nLinks;
    for ( auto const& proxy : output_table.scalar() ) m_chi2 += proxy.get<ClusterMatch>().cast() / nLinks;

    return output_table;
  }

} // namespace LHCb::Calo

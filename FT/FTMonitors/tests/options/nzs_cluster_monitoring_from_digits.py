###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from os.path import basename
from Gaudi.Configuration import ApplicationMgr, MessageSvc, INFO, ERROR
from Configurables import LHCbApp, GaudiSequencer
from GaudiConf import IOHelper
from Configurables import Gaudi__Monitoring__JSONSink as JSONSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Configurables import FTNZSRawBankDecoder, FTNZSClusterCreator, FTClusterMonitor
from Configurables import createODIN, LHCb__UnpackRawEvent
from DDDB.CheckDD4Hep import UseDD4Hep
from os.path import expandvars
import sys

sys.path.append(expandvars("$FTMONITORSROOT/tests/python"))
from glue import filling_scheme

app = LHCbApp(DataType="Upgrade")
if not UseDD4Hep:
    app.DDDBtag = "upgrade/dddb-20221004"
    app.CondDBtag = "upgrade/SFCAVERN_SF_20220922_142756_All-FixedOrder"
    app.Simulation = True
    from Configurables import CondDB

    CondDB().Upgrade = True
    CondDB().IgnoreHeartBeat = True

MessageSvc().OutputLevel = ERROR

files = [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/Run_0000250946_20221026-063447-446_SCEB13.mdf",
]

output_name = basename(files[0]).replace(".mdf", "_nzs_clusters.root")

IOHelper("MDF").inputFiles(files)

monSeq = GaudiSequencer("SciFiMonitorSequence", IgnoreFilterPassed=True)

# https://gitlab.cern.ch/lhcb/Boole/-/commit/62c15cebc68e71a5a7b71b8649495b7191b3367e
# this might change
BankTypes = ["FTCluster", 'FTNZS', "ODIN"]
monSeq.Members += [
    LHCb__UnpackRawEvent(
        "UnpackRawEvent",
        BankTypes=BankTypes,
        RawEventLocation="/Event/DAQ/RawEvent",
        RawBankLocations=[
            "/Event/DAQ/RawBanks/{}".format(i) for i in BankTypes
        ]),
]

decodeODIN = createODIN(RawBanks="DAQ/RawBanks/ODIN")
monSeq.Members += [decodeODIN]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer

    monSeq.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=decodeODIN.ODIN)]

monSeq.Members += [
    FTNZSRawBankDecoder(
        "DecodeFT", OutputLevel=INFO, RawBank='/Event/DAQ/RawBanks/FTNZS')
]
monSeq.Members += [FTNZSClusterCreator("ClusterFT", OutputLevel=INFO)]
monSeq.Members += [
    FTClusterMonitor(
        ClusterLocation="Raw/FT/LiteClustersfromNZSdigits",
        OutputLevel=INFO,
        DrawHistsPerStation=True,
        DrawHistsPerQuarter=True,
        DrawHistsPerModule=True,
        FillingScheme=filling_scheme(
            "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/fills/8314_fillingscheme.txt"
        ),
        enableTAE=True,
        Online=False,
    )
]

appMgr = ApplicationMgr(
    EvtMax=100,
    TopAlg=[monSeq],
    HistogramPersistency="ROOT",
    ExtSvc=[
        MessageSvcSink(),
        JSONSink(
            FileName="nzs_cluster_monitoring_from_digits.json",
            NamesToSave=[
                ".*MainEvent/LiteClustersPerPseudoChannel_64",
                ".*nClustersPerTAE",
            ],
        ),
    ],
)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    appMgr.ExtSvc += [DD4hepSvc(DetectorList=["/world", "FT"])]

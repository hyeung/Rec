/*****************************************************************************\
 * * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

#include "Event/Particle.h"
#include "JetUtils.h"
#include "LHCbAlgs/Transformer.h"

/*

Receives two lists of particles: listA and listB. Removes any particle from listA that belongs to listB.

*/

struct ParticleFlowFilter : LHCb::Algorithm::Transformer<LHCb::Particle::Selection( const LHCb::Particle::Range&,
                                                                                    const LHCb::Particle::Range& )> {

  /// Constructor.
  ParticleFlowFilter( const std::string& name, ISvcLocator* svc )
      : Transformer( name, svc, {KeyValue{"Inputs", ""}, KeyValue{"ParticlesToBan", ""}},
                     {"Output", "Phys/ParticleFlow/Particles"} ) {}

  // Main method
  LHCb::Particle::Selection operator()( const LHCb::Particle::Range& Inputs,
                                        const LHCb::Particle::Range& InputsToBan ) const override {

    std::set<const LHCb::ProtoParticle*> toban;
    for ( auto& itb : InputsToBan ) {
      if ( itb->isBasicParticle() ) {
        toban.insert( itb->proto() );
      } else {
        for ( auto dtr : LHCb::JetAccessories::getBasics( *itb ) ) toban.insert( dtr->proto() );
      }
    }

    LHCb::Particle::Selection prts;

    for ( auto& Input : Inputs ) {
      if ( Input->isBasicParticle() ) {
        if ( toban.insert( ( Input )->proto() ).second ) prts.insert( Input );
      } else {
        bool isdauban = false;
        for ( auto dtr : LHCb::JetAccessories::getBasics( *Input ) ) {
          if ( !toban.insert( dtr->proto() ).second ) isdauban = true;
        }
        if ( !isdauban ) prts.insert( Input );
      }
    }

    debug() << "Number of output particles: " << prts.size() << endmsg;

    return prts;
  }
};

DECLARE_COMPONENT( ParticleFlowFilter )

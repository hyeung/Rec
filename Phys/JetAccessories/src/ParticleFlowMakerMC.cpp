/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "GaudiAlg/MergingTransformer.h"
#include "Kernel/JetEnums.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/Vec3.h"
#include <Gaudi/Accumulators.h>

/*

Receives a vector of LHCb::MCParticle containers, basic  or composite, outputs a list of particles
removing duplicities(no MC particle is used more than once) and particles to be banned (with their daughters)
If use a composite particle, doesn't use its daughters. Else all basic stable particles are used.

TODO: The test performed to avoid double counting is done by using the particle's address.
We plan to include LoKi::CheckOverlap for next version.

*/

namespace LHCb {

  namespace {
    // Check if a particle is from a collision.
    bool primary( const MCParticle* prt ) {
      while ( true ) {
        if ( !prt ) return false;
        const MCVertex* vrt = prt->originVertex();
        if ( !vrt ) return false;
        if ( vrt->isPrimary() ) return true;
        if ( vrt->type() < 1 || vrt->type() > 4 ) return false;
        prt = vrt->mother();
      }
    }

    std::unique_ptr<Particle> PfromMCP( const MCParticle& mcprt ) {
      auto key = mcprt.key();

      auto prt = std::make_unique<Particle>( mcprt.particleID(), key );
      prt->setMeasuredMass( mcprt.virtualMass() );
      prt->setMomentum( mcprt.momentum() );

      auto pos = referencePoint( mcprt );
      prt->setReferencePoint( {pos.X().cast(), pos.Y().cast(), pos.Z().cast()} );

      // Use jets enums to save MC PV position into particle
      prt->addInfo( JetPFMCInfo::MCParticleKey, key );
      auto mcpv = mcprt.primaryVertex();
      if ( mcpv ) {
        prt->addInfo( JetPFMCInfo::mcpv_x, mcpv->position().X() );
        prt->addInfo( JetPFMCInfo::mcpv_y, mcpv->position().Y() );
        prt->addInfo( JetPFMCInfo::mcpv_z, mcpv->position().Z() );
        prt->addInfo( JetPFMCInfo::MCPVkey, mcpv->key() );
      }
      return prt;
    }

    /**
     * Recursively determine the status of an MCParticle decay tree.
     * For each MC particle, check if it is in the particles to use list and comes from a PV.
     * Then check if it is not in the particle to ban list.
     * If not, scan its daughters, mark them as used
     * Codes: 0: Not flagged yet, 1: OK, 2: used, 3: banned, 4: from interactions with detector material, 5: Not from PV
     */
    enum struct Code { NotFlaggetYet, OK, NotRequested, Used, Banned, NotFromPV, FromDetMatInteraction };
    std::vector<std::pair<const MCParticle*, Code>> status( std::vector<const MCParticle*> const& mcprts,
                                                            std::vector<int> const&               banPIDs,
                                                            std::vector<int> const& requestedParticlesPIDs ) {
      std::vector<std::pair<const MCParticle*, Code>> prtsStatus;
      prtsStatus.reserve( mcprts.size() );

      auto isBanned = [b = begin( banPIDs ), e = end( banPIDs )]( int pid ) {
        return std::any_of( b, e, [pid]( int PID ) { return PID == pid; } );
      };
      auto notRequested = [b = begin( requestedParticlesPIDs ), e = end( requestedParticlesPIDs )]( int pid ) {
        return std::none_of( b, e, [pid]( int PID ) { return PID == pid; } );
      };

      for ( auto iPrt : mcprts ) {
        if ( std::any_of( prtsStatus.begin(), prtsStatus.end(), [&]( const auto& p ) { return iPrt == p.first; } ) ) {
          continue; // Particles already flagged
        }
        int  pid  = std::abs( iPrt->particleID().pid() );
        Code flag = ( isBanned( pid )
                          ? Code::Banned
                          : notRequested( pid ) ? Code::NotRequested : !primary( iPrt ) ? Code::NotFromPV : Code::OK );
        if ( flag != Code::NotRequested ) {
          // If NotRequested, don't flag daughters, as they might be requested.
          // If not and it's a decay vertex, flag the daughters
          for ( const auto& vrt : iPrt->endVertices() ) {
            if ( vrt && ( vrt->type() == 2 || vrt->type() == 3 ) ) {
              auto drtFlag = ( flag == Code::OK ? Code::Used : flag ); // If use mother, don't use daughters
              for ( auto const& iDtr : vrt->products() ) {
                auto iDtrStats = std::find_if( prtsStatus.begin(), prtsStatus.end(),
                                               [&]( const auto& p ) { return iDtr.target() == p.first; } );
                if ( iDtrStats != prtsStatus.end() ) {
                  iDtrStats->second = drtFlag;
                } else {
                  prtsStatus.emplace_back( iDtr, drtFlag );
                }
              }
            }
          }
        }
        prtsStatus.emplace_back( iPrt, flag );
      }
      return prtsStatus;
    }

  } // namespace

  using MCParticlesVector = const Gaudi::Functional::vector_of_const_<MCParticles>;

  class ParticleFlowMakerMC : public Gaudi::Functional::MergingTransformer<Particles( MCParticlesVector const& )> {
    Gaudi::Property<std::vector<int>> m_requestedParticlesPIDs{
        this, "requestedParticlesPIDs", {0}, "List of MC particles PIDs to keep in the particle flow for jets"};

    Gaudi::Property<std::vector<int>> m_banPIDs{
        this, "banPIDs", {0}, "List of MC particles PIDs to be rejected, including its daughters"};

    mutable Gaudi::Accumulators::Counter<> m_count{this, "00: # Number of Events"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_emptyVec{this, "Empty Input vector. Skipping", 0};

  public:
    /// Constructor.
    ParticleFlowMakerMC( const std::string& name, ISvcLocator* svc )
        : MergingTransformer( name, svc, {"Inputs", {}}, {"Output", "Phys/ParticleFlow/Particles"} ) {}

    // Main method
    Particles operator()( MCParticlesVector const& Inputs ) const override {

      ++m_count;

      if ( msgLevel( MSG::DEBUG ) ) {
        info() << "=============== New event: " << m_count.nEntries() << "  ================" << endmsg;
        info() << "Number of input vectors: " << Inputs.size() << endmsg;
        info() << "Sizes of Input vectors: ";
        int total( 0 );
        for ( auto& Input : Inputs ) {
          info() << Input.size() << " ";
          total += Input.size();
        }
        info() << " Total: " << total << endmsg;
      }

      std::vector<const MCParticle*> mcprts;
      auto                           size = std::accumulate( Inputs.begin(), Inputs.end(), 0,
                                   []( int i, const auto& Input ) { return i + Input.size(); } );
      mcprts.reserve( size );

      //  Merge all inputs
      for ( auto& Input : Inputs ) {
        if ( Input.empty() ) ++m_msg_emptyVec;
        mcprts.insert( mcprts.end(), Input.begin(), Input.end() );
      }

      // Determine the status of each MC particle
      auto prtsStatus = status( mcprts, m_banPIDs.value(), m_requestedParticlesPIDs.value() );

      // Scan the MCParticle list again and save the ones with flag == 1 as Particles
      Particles prts;

      for ( auto iPrt : prtsStatus ) {
        if ( iPrt.second == Code::OK ) {
          auto prt       = PfromMCP( *( iPrt.first ) );
          auto newprtkey = prts.add( prt.release() );
          debug() << "--> Added particle with key " << newprtkey << endmsg;
        }
      }
      debug() << "Returning " << prts.size() << " particles" << endmsg;

      return prts;
    }
  };

  DECLARE_COMPONENT_WITH_ID( ParticleFlowMakerMC, "ParticleFlowMakerMC" )

} // namespace LHCb

/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IFileAccess.h"
#include "JetUtils.h"
#include "Kernel/IParticleCombiner.h"
#include "Kernel/IRelatedPVFinder.h"
#include "Kernel/JetEnums.h"
#include "Kernel/Particle2Vertex.h"
#include "LHCbAlgs/Transformer.h"
#include "Math/VectorUtil.h"
#include "Relations/Relation1D.h"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/PseudoJet.hh"

// Histograms.
#include "TFile.h"
#include "TH1D.h"
#include "TMemFile.h"

#include <unordered_map>

/*
Run3 Jet building (JB) algorithm class for use in the HLT and offline.
2019-12-10 : Algorithm based from Run 2 code

Goal: Use list of particles and clusters them into jets using the
FastJet package. Optionally, it can then correct the jet energy
(JEC), calculate additional information about the jet, and tag jets with Particles.

1) FastJet Tool is created and configured with the defined parameters.
2) List of particles is created to call the Jet clustering. In this step,
particles can be filtered out.
3) List of jet particles is created from the reconstructed jets.
The jet reconstruction can be performed for each primary vertex or indenpendent
of the primary vertex
In Run2, the standard was to reconstruct inclusively, since the Pile-up impact was small.
If built per PV, then particles are associated to PVs if they contain one or more
tracks. Particles without tracks are associated to all PVs. The, jets are built
using the input for each PV and the PV location is set as the reference point of the jet.
Note that when building per PV, the neutrals are double counted when considering
jets from all PVs.

TODO: Add a JetID filter , probably as a separate transformer.
*/

using P2PVTable = LHCb::Relation1D<LHCb::RecVertex*, LHCb::Particle*>;
// the actual type of input data container
using InputJ = LHCb::Particle::ConstVector;
// FastJet Jet type
using PJet = fastjet::PseudoJet;

namespace FastJetDetails {
  enum struct Ordering { by_rapidity = 3, by_pt = 2, by_E = 1 };

  template <typename In>
  auto sorted_by( Ordering order, In&& input ) {
    switch ( order ) {
    case Ordering::by_rapidity:
      return fastjet::sorted_by_rapidity( std::forward<In>( input ) );
    case Ordering::by_pt:
      return fastjet::sorted_by_pt( std::forward<In>( input ) );
    case Ordering::by_E:
      return fastjet::sorted_by_E( std::forward<In>( input ) );
    }
    throw std::out_of_range( "unknown enumeration value" );
  }
} // namespace FastJetDetails

namespace {
  constexpr int to_user_index( int index ) { return index + 10000; }
  constexpr int from_user_index( int index ) { return index - 10000; }
  PJet          createJetInput( const LHCb::Particle& p, const int index ) {
    // trivial function which "converts" LHCb particle into the fastjet particle
    const auto& v        = p.momentum();
    auto        jetinput = PJet{v.Px(), v.Py(), v.Pz(), v.E()};
    jetinput.set_user_index( index );
    return jetinput;
  }

  using namespace std::string_view_literals;

  constexpr auto StrategyMap = std::array{std::pair{fastjet::Strategy::N2MinHeapTiled, "N2MinHeapTiled"sv},
                                          std::pair{fastjet::Strategy::N2Tiled, "N2Tiled"sv},
                                          std::pair{fastjet::Strategy::N2PoorTiled, "N2PoorTiled"sv},
                                          std::pair{fastjet::Strategy::N2Plain, "N2Plain"sv},
                                          std::pair{fastjet::Strategy::N3Dumb, "N3Dumb"sv},
                                          std::pair{fastjet::Strategy::Best, "Best"sv},
                                          std::pair{fastjet::Strategy::NlnN, "NlnN"sv},
                                          std::pair{fastjet::Strategy::NlnN3pi, "NlnN3pi"sv},
                                          std::pair{fastjet::Strategy::NlnN4pi, "NlnN4pi"sv},
                                          std::pair{fastjet::Strategy::NlnNCam4pi, "NlnNCam4pi"sv},
                                          std::pair{fastjet::Strategy::NlnNCam2pi2R, "NlnNCam2pi2R"sv},
                                          std::pair{fastjet::Strategy::NlnNCam, "NlnNCam"sv},
                                          std::pair{fastjet::Strategy::plugin_strategy, "plugin_strategy"sv}};

  constexpr auto JetAlgorithmMap =
      std::array{std::pair{fastjet::JetAlgorithm::kt_algorithm, "kt_algorithm"sv},
                 std::pair{fastjet::JetAlgorithm::cambridge_algorithm, "cambridge_algorithm"sv},
                 std::pair{fastjet::JetAlgorithm::antikt_algorithm, "antikt_algorithm"sv},
                 std::pair{fastjet::JetAlgorithm::genkt_algorithm, "genkt_algorithm"sv},
                 std::pair{fastjet::JetAlgorithm::cambridge_for_passive_algorithm, "cambridge_for_passive_algorithm"sv},
                 std::pair{fastjet::JetAlgorithm::genkt_for_passive_algorithm, "genkt_for_passive_algorithm"sv},
                 std::pair{fastjet::JetAlgorithm::ee_kt_algorithm, "ee_kt_algorithm"sv},
                 std::pair{fastjet::JetAlgorithm::ee_genkt_algorithm, "ee_genkt_algorithm"sv},
                 std::pair{fastjet::JetAlgorithm::plugin_algorithm, "plugin_algorithm"sv}};

  /// the various recombination schemes
  constexpr auto RecombinationSchemeMap =
      std::array{std::pair{fastjet::RecombinationScheme::E_scheme, "E_scheme"sv},
                 std::pair{fastjet::RecombinationScheme::pt_scheme, "pt_scheme"sv},
                 std::pair{fastjet::RecombinationScheme::pt2_scheme, "pt2_scheme"sv},
                 std::pair{fastjet::RecombinationScheme::Et_scheme, "Et_scheme"sv},
                 std::pair{fastjet::RecombinationScheme::Et2_scheme, "Et2_scheme"sv},
                 std::pair{fastjet::RecombinationScheme::BIpt_scheme, "BIpt_scheme"sv},
                 std::pair{fastjet::RecombinationScheme::BIpt2_scheme, "BIpt2_scheme"sv},
                 std::pair{fastjet::RecombinationScheme::external_scheme, "external_scheme"sv}};

  constexpr auto OrderingMap = std::array{
      std::pair{FastJetDetails::Ordering::by_rapidity, "by_rapidity"sv},
      std::pair{FastJetDetails::Ordering::by_pt, "by_pt"sv},
      std::pair{FastJetDetails::Ordering::by_E, "by_E"sv},
  };

  template <typename Enum, auto N>
  std::string toString_( Enum e, std::array<std::pair<Enum, std::string_view>, N> const& tbl ) {
    auto i = std::find_if( tbl.begin(), tbl.end(), [e]( const auto& entry ) { return entry.first == e; } );
    if ( i == tbl.end() ) throw std::out_of_range( "unknown enumeration value" );
    return std::string{i->second};
  }

  template <typename Enum, auto N>
  StatusCode parse_( Enum& e, std::string_view s, std::array<std::pair<Enum, std::string_view>, N> const& tbl ) {
    if ( !s.empty() && s.front() == s.back() && ( s.front() == '\'' || s.front() == '\"' ) ) {
      s.remove_prefix( 1 );
      s.remove_suffix( 1 );
    }
    auto i = std::find_if( tbl.begin(), tbl.end(), [s]( const auto& entry ) { return entry.second == s; } );
    if ( i == tbl.end() ) return StatusCode::FAILURE;
    e = i->first;
    return StatusCode::SUCCESS;
  }
} // namespace

namespace FastJetDetails {
  std::string   toString( Ordering e ) { return toString_( e, OrderingMap ); }
  StatusCode    parse( Ordering& bt, const std::string& in ) { return parse_( bt, in, OrderingMap ); }
  std::ostream& toStream( Ordering e, std::ostream& os ) { return os << std::quoted( toString( e ), '\'' ); }
  std::ostream& operator<<( std::ostream& s, Ordering e ) { return toStream( e, s ); }
} // namespace FastJetDetails

namespace fastjet {
  std::string toString( RecombinationScheme e ) { return toString_( e, RecombinationSchemeMap ); }
  StatusCode  parse( RecombinationScheme& bt, const std::string& in ) {
    return parse_( bt, in, RecombinationSchemeMap );
  }
  std::ostream& toStream( RecombinationScheme e, std::ostream& os ) { return os << std::quoted( toString( e ), '\'' ); }
  std::ostream& operator<<( std::ostream& s, RecombinationScheme e ) { return toStream( e, s ); }

  std::string   toString( JetAlgorithm e ) { return toString_( e, JetAlgorithmMap ); }
  StatusCode    parse( JetFinder& bt, const std::string& in ) { return parse_( bt, in, JetAlgorithmMap ); }
  std::ostream& toStream( JetAlgorithm e, std::ostream& os ) { return os << std::quoted( toString( e ), '\'' ); }
  std::ostream& operator<<( std::ostream& s, JetFinder e ) { return toStream( e, s ); }

  std::string   toString( Strategy e ) { return toString_( e, StrategyMap ); }
  StatusCode    parse( Strategy& bt, const std::string& in ) { return parse_( bt, in, StrategyMap ); }
  std::ostream& toStream( Strategy e, std::ostream& os ) { return os << std::quoted( toString( e ), '\'' ); }
  std::ostream& operator<<( std::ostream& s, Strategy e ) { return toStream( e, s ); }
} // namespace fastjet

namespace {
  // Add additional information to jet
  template <typename Range>
  void addInfo( LHCb::Particle& prt, Range dtrs ) {
    double cpx( 0 ), cpy( 0 ), cptMax( 0 ), nptMax( 0 ), width( 0 ), norm( 0 ), trks( 0 ), pt( prt.momentum().Pt() );
    for ( auto dtr : dtrs ) {
      const Gaudi::LorentzVector& vec = dtr->momentum();
      if ( dtr->charge() != 0 ) {
        if ( vec.Pt() > cptMax ) cptMax = vec.Pt();
        cpx += vec.Px();
        cpy += vec.Py();
        ++trks;
      } else if ( vec.Pt() > nptMax )
        nptMax = vec.Pt();
      width += ROOT::Math::VectorUtil::DeltaR( vec, prt.momentum() ) * vec.Pt();
      norm += vec.Pt();
    }
    prt.addInfo( LHCb::JetIDInfo::Ntracks, trks );
    prt.addInfo( LHCb::JetIDInfo::MTF, cptMax / pt );
    prt.addInfo( LHCb::JetIDInfo::MNF, nptMax / pt );
    prt.addInfo( LHCb::JetIDInfo::MPT, cptMax );
    prt.addInfo( LHCb::JetIDInfo::CPF, sqrt( cpx * cpx + cpy * cpy ) / pt );
    prt.addInfo( LHCb::JetIDInfo::JetWidth, width );
    prt.addInfo( LHCb::JetIDInfo::JetWidthNorm, width / norm ); // Added to Phys/Phys/JetAccessories/Kernel/JetEnums.h.
                                                                // Remove this or the one above after tests
  }

  ///<  Relate particles from MCParticles to MC PV positions
  auto relateMCP2MCPV( LHCb::Particle::Range const& Particles ) {
    std::vector<std::pair<Gaudi::XYZPoint, std::vector<const LHCb::Particle*>>> map;
    map.reserve( Particles.size() );

    for ( auto const* prt : Particles ) {
      auto pv = Gaudi::XYZPoint{prt->info( LHCb::JetPFMCInfo::mcpv_x, -9999. ),
                                prt->info( LHCb::JetPFMCInfo::mcpv_y, -9999. ),
                                prt->info( LHCb::JetPFMCInfo::mcpv_z, -9999. )};
      if ( pv.X() < -999 ) {
        throw GaudiException{"Particle from MC particle does not contain MC PV info", "FastJetBuilder::relateMCP2MCPV",
                             StatusCode::FAILURE};
      }
      auto i =
          std::find_if( map.begin(), map.end(), [&pv]( const auto& j ) { return ( pv - j.first ).Mag2() < 1e-5; } );
      if ( i != map.end() ) {
        i->second.push_back( prt );
      } else {
        map.emplace_back( pv, std::vector{prt} );
      }
    }
    return map;
  }

} // namespace

class FastJetBuilder
    : public LHCb::Algorithm::Transformer<LHCb::Particles( const LHCb::Particle::Range&, const LHCb::RecVertices&,
                                                           const DetectorElement& ),
                                          LHCb::DetDesc::usesConditions<DetectorElement>> {

public:
  FastJetBuilder( const std::string& name, ISvcLocator* svc ); // Constructor
  StatusCode      initialize() override;                       // Initialize
  LHCb::Particles operator()( const LHCb::Particle::Range&, const LHCb::RecVertices&,
                              const DetectorElement& ) const override; // Main method

private:
  /// Tool to RelatedPVFinder
  ToolHandle<IRelatedPVFinder> m_prtVtxTool = {
      this, "RelatedPVFinder",
      "GenericParticle2PVRelator__p2PVWithIPChi2_OfflineDistanceCalculatorName_/P2PVWithIPChi2"};

  // Combiner to be used
  ToolHandle<IParticleCombiner> m_combiner{this, "ParticleCombiner", "MomentumCombiner"};

  Gaudi::Property<bool> m_jetVtx = {this, "JetsByVtx", true,
                                    "If true, build jets for each primary vertex, otherwise "
                                    "build inclusively."};

  Gaudi::Property<bool> m_MCJets = {this, "MCJets", false, "If true, input particles are from MC"};

  // FastJet configuration
  Gaudi::Property<FastJetDetails::Ordering> m_sort  = {this, "Sort", FastJetDetails::Ordering::by_pt,
                                                      "FastJet: sorting criteria"};
  Gaudi::Property<int>                      m_jetID = {this, "JetID", 98, "LHCb PID number for jets"};
  Gaudi::Property<double>                   m_r     = {this, "RParameter", 0.5, "FastJet: cone size"};
  Gaudi::Property<double>                   m_ptmin = {this, "PtMin", 5000,
                                     [this]( auto& ) {
                                       if ( m_ptmin.value() < 0 )
                                         throw std::runtime_error{"min pt for FastJet is negative for FastJetBuilder"};
                                     },
                                     "FastJet: min pT"};

  Gaudi::Property<fastjet::RecombinationScheme> m_recom = {this, "Recombination",
                                                           fastjet::RecombinationScheme::E_scheme, "FastJet: scheme "};
  Gaudi::Property<fastjet::Strategy>     m_strat = {this, "Strategy", fastjet::Strategy::Best, "FastJet: strategy"};
  Gaudi::Property<fastjet::JetAlgorithm> m_type  = {this, "Type", fastjet::JetAlgorithm::antikt_algorithm,
                                                   "FastJet: JetAlgorithm"};

  ///< Perform jet energy correction.
  void jec( LHCb::Particle* prt, int nPVs ) const;

  /// Limits of n(PV) JEC bins.
  Gaudi::Property<std::vector<int>> m_jecLimNPvs = {this, "jecLimNPvs", {0, 1}, "Limits of n(PV) JEC bins."};

  /// Limits of the JEC eta bins.
  Gaudi::Property<std::vector<double>> m_jecLimEta = {
      this, "jecLimEta", {2.0, 2.2, 2.3, 2.4, 2.6, 2.8, 3.0, 3.2, 3.6, 4.2, 4.5}, "Limits of the JEC eta bins."};

  /// Limits of the JEC (sum charged pT)/pT bins.
  Gaudi::Property<std::vector<double>> m_jecLimCpf = {
      this, "jecLimCpf", {0.06, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0001}, "Limits of the JEC (sum charged pT)/pT bins."};

  /// Limits of the JEC phi bins.
  Gaudi::Property<std::vector<double>> m_jecLimPhi = {
      this, "jecLimPhi", {0, 1.0 / 6.0 * M_PI, 1.0 / 3.0 * M_PI, 0.5 * M_PI}, "Limits of the JEC phi bins."};

  /// Limits of the JEC pT bins.
  Gaudi::Property<std::vector<double>> m_jecLimPt = {this, "jecLimPt", {5, 298}, "Limits of the JEC pT bins."};

  /// Shift the jet energy correction by this (in JEC sigma).
  Gaudi::Property<double> m_jetEcShift = {this, "jetEcShift", 0.,
                                          "Shift the jet energy correction by this (in JEC sigma)"};

  /// Path to histograms for JEC. If empty, skip JEC. Configured in Hlt2Conf/standard_jets.py
  Gaudi::Property<std::string> m_jetEcFilePath = {this, "jetEcFilePath", "",
                                                  "Location of ROOT file with jet energy correction histograms"};

  /// Path to histograms for JEC in root file. If empty, skip JEC
  Gaudi::Property<std::string> m_jetEcPath = {this, "jetEcPath", "", "Path to histograms for jet energy correction."};

  /// File accessor to ParamFile
  ServiceHandle<IFileAccess> m_file{this, "FileAccessor", "ParamFileSvc", "Service used to retrieve file contents"};

  std::map<int, TH1D> m_jecsHistos; // Assume above bin limits less then 100

  mutable Gaudi::Accumulators::SummingCounter<> m_nbInput{this, "Nb of Inputs"};
  mutable Gaudi::Accumulators::SummingCounter<> m_nbJetCounter{this, "Nb of Reconstructed Jets"};
  mutable Gaudi::Accumulators::SummingCounter<> m_nbJetCounter10{this, "Nb of Reconstructed Jets with pT>10GeV"};
  mutable Gaudi::Accumulators::SummingCounter<> m_nbJetCounter20{this, "Nb of Reconstructed Jets with pT>20GeV"};

  /// MsgSvc counters
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_noBestPV{
      this, "Failed to relate a particle with its bestPV.", 0};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_invalid_input{
      this, "Invalid input particle -> FastJet input", 0};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_nojetdaughters{this, "Jets have no daughters", 0};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_invalidjetdaughter{this, "Invalid jet daughter index", 0};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_momcomb{
      this, "Error in momentum combiner with jet daughters", 0};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_nobestPV{this, "Could not find bestPV", 0};

  ///< Relate a particles with bestPVs
  P2PVTable relateP2Vtx( LHCb::span<LHCb::Particle const* const>, const LHCb::RecVertices& ) const;

  // run fastjet
  std::vector<std::unique_ptr<LHCb::Particle>> makeFastJets( LHCb::span<LHCb::Particle const* const> particles,
                                                             const DetectorElement&                  geometry ) const;

  // prepare fastjet definition
  std::vector<fastjet::PseudoJet> prepare( LHCb::span<LHCb::Particle const* const> Particles ) const;
};

/*-----------------------------------------------------------------------------
Implementation file for class : FastJetBuilder
-----------------------------------------------------------------------------*/

// Declare the algorithm factory.
DECLARE_COMPONENT( FastJetBuilder )

//=============================================================================
// Constructor.
//=============================================================================
FastJetBuilder::FastJetBuilder( const std::string& name, ISvcLocator* svc )
    : Transformer( name, svc,
                   {KeyValue{"Input", ""}, KeyValue{"PVLocation", "Rec/Vertex/Primary"},
                    KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}},
                   KeyValue{"Output", "Phys/Jets/Particles"} ) {}

//=============================================================================
// Initialize.
//=============================================================================
StatusCode FastJetBuilder::initialize() {
  StatusCode sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;

  if ( !msgLevel( MSG::INFO ) ) { fastjet::ClusterSequence::set_fastjet_banner_stream( nullptr ); }

  // Retrieve the JEC histograms.
  if ( !m_jetEcFilePath.empty() ) {
    if ( !m_jetEcPath.empty() ) m_jetEcPath = m_jetEcPath + "/";
    m_file.retrieve().orThrow( "Could not obtain IFileAccess instance" );
    const auto buffer = m_file->read( m_jetEcFilePath );
    if ( !buffer )
      throw GaudiException( "Failed to obtain histogram file " + m_jetEcFilePath, __func__, StatusCode::FAILURE );
    auto     filedata = TMemFile::ZeroCopyView_t{buffer->data(), buffer->size()};
    TMemFile tfile{"JECParamsDummyName", filedata};
    if ( msgLevel( MSG::DEBUG ) ) {
      tfile.Print();
      tfile.ls();
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "File: " << m_jetEcFilePath << m_jetEcPath << endmsg;
      debug() << "Bins limits for Jet Energy Correction:" << endmsg;
      debug() << "NPvs: " << m_jecLimNPvs << endmsg;
      debug() << "Eta:  " << m_jecLimEta << endmsg;
      debug() << "Cpf:  " << m_jecLimCpf << endmsg;
      debug() << "Phi:  " << m_jecLimPhi << endmsg;
    }

    // Check bins limits do not corrupt histo map key variable
    std::string badList;
    if ( m_jecLimNPvs.size() > 100 ) badList += std::string( "jecLimNPvs " );
    if ( m_jecLimEta.size() > 100 ) badList += std::string( "m_jecLimEta " );
    if ( m_jecLimCpf.size() > 100 ) badList += std::string( "m_jecLimCpf " );
    if ( m_jecLimPhi.size() > 100 ) badList += std::string( "m_jecLimPhi " );
    if ( !badList.empty() )
      throw GaudiException( "Number of bins in JEC histograms in [ " + badList +
                                " ] is greater than 100. Histograms map keys will be corrupted",
                            __func__, StatusCode::FAILURE );

    for ( long unsigned int pvs = 0; pvs < m_jecLimNPvs.size() - 1; pvs++ )
      for ( long unsigned int eta = 0; eta < m_jecLimEta.size() - 1; eta++ )
        for ( long unsigned int cpf = 0; cpf < m_jecLimCpf.size() - 1; cpf++ )
          for ( long unsigned int phi = 0; phi < m_jecLimPhi.size() - 1; phi++ ) {
            auto name = fmt::format( "{}JECSYS_PV{}_ETA{}_CPF{}_PHI{}", m_jetEcPath.value(), pvs + 1, eta, cpf, phi );
            auto hist = tfile.Get<TH1D>( name.c_str() );
            if ( !hist ) throw GaudiException( "Histogram '" + name + "' not found", __func__, StatusCode::FAILURE );
            int hkey           = pvs + 100 * eta + 1000 * cpf + 10000 * phi;
            m_jecsHistos[hkey] = *hist;
            if ( msgLevel( MSG::DEBUG ) )
              debug() << "[pvs][eta][cpf][phi]: " << pvs << " " << eta << " " << cpf << " " << phi
                      << " map key: " << hkey << " hname: " << name << " mean: " << m_jecsHistos[hkey].GetMean()
                      << endmsg;
          }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Execute.
//=============================================================================
LHCb::Particles FastJetBuilder::operator()( const LHCb::Particle::Range& Particles,
                                            const LHCb::RecVertices&     PrimaryVertices,
                                            const DetectorElement&       geometry ) const {

  m_nbInput += Particles.size();

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "New event: #Particles: " << Particles.size() << " PVs: " << PrimaryVertices.size()
            << " PV key list ----> ";
    for ( auto vtx : PrimaryVertices ) debug() << vtx->key() << " ";
    debug() << "<----" << endmsg;
  }

  // Output jets
  LHCb::Particles recojets;

  if ( m_jetVtx.value() ) { // Make jets by PVs
    if ( m_MCJets.value() ) {
      // Scan MCparticles, grouping them by MCPV, saving them in a list.
      auto vtx2prtRel = relateMCP2MCPV( Particles );
      if ( vtx2prtRel.empty() ) {
        throw GaudiException{"Empty map from MC particles to MC vertices", "FastJetBuilder::operator",
                             StatusCode::FAILURE};
      }
      debug() << "Making MC jets by PVs" << endmsg;
      // For each MCPV in the list create jets with the lsit of MCParticles associated to it
      for ( const auto& [vtx, prts] : vtx2prtRel ) {
        // Make the jets
        auto tmprecojets = makeFastJets( prts, geometry );
        recojets.reserve( tmprecojets.size() );
        for ( auto& jet : tmprecojets ) {
          jet->addInfo( LHCb::JetIDInfo::vtx_x, vtx.X() );
          jet->addInfo( LHCb::JetIDInfo::vtx_y, vtx.Y() );
          jet->addInfo( LHCb::JetIDInfo::vtx_z, vtx.Z() );
          recojets.insert( jet.release() );
        }
      }
    } else {

      // Split Particles, separating photons, merged and resolved pi0s
      std::vector<const LHCb::Particle*> tmpparttracks;
      std::vector<const LHCb::Particle*> tmpneutrals;
      tmpparttracks.reserve( 0.7 * Particles.size() );
      tmpneutrals.reserve( 0.5 * Particles.size() );
      for ( auto const* prt : Particles ) {
        int pid = prt->particleID().pid();
        ( pid == 22 || pid == 111 ) ? tmpneutrals.push_back( prt ) : tmpparttracks.push_back( prt );
      }

      // Relate particles with tracks (or their composites) to best PV
      if ( PrimaryVertices.empty() ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "No PrimaryVertices in this event" << endmsg;
        return recojets;
      }
      auto vtx2prtRel = relateP2Vtx( tmpparttracks, PrimaryVertices );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Making jets by PVs" << endmsg;
      for ( auto const& vtx : PrimaryVertices ) {
        auto const&                        tmppartRange = vtx2prtRel.relations( vtx );
        std::vector<const LHCb::Particle*> tmppart;
        tmppart.reserve( tmppartRange.size() + tmpneutrals.size() );
        for ( const auto& prt : tmppartRange ) tmppart.push_back( prt.to() );
        // Extend including neutral (They go to every PV)
        tmppart.insert( tmppart.end(), tmpneutrals.begin(), tmpneutrals.end() );

        // Make the jets
        auto tmprecojets = makeFastJets( tmppart, geometry );

        if ( msgLevel( MSG::DEBUG ) )
          debug() << "Vtx key: " << vtx->key() << " #jets: " << tmprecojets.size() << endmsg;
        recojets.reserve( tmprecojets.size() );
        for ( auto& jet : tmprecojets ) {
          jet->setPV( vtx );
          jet->addInfo( LHCb::JetIDInfo::vtx_x, vtx->position().X() ); // TODO: remove: redundant information
          jet->addInfo( LHCb::JetIDInfo::vtx_y, vtx->position().Y() ); // TODO: remove: redundant information
          jet->addInfo( LHCb::JetIDInfo::vtx_z, vtx->position().Z() ); // TODO: remove: redundant information
          recojets.insert( jet.release() );
        }
      }
    }
  } else { // Make jets inclusively
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Making jets inclusively" << endmsg;

    // Make the jets
    auto tmprecojets = makeFastJets( std::vector( Particles.begin(), Particles.end() ), geometry );
    recojets.reserve( tmprecojets.size() );
    for ( auto& jet : tmprecojets ) {
      auto vtx = m_prtVtxTool->relatedPV( jet.get(), PrimaryVertices );
      jet->setPV( vtx );
      if ( vtx ) {
        jet->addInfo( LHCb::JetIDInfo::vtx_x, vtx->position().X() ); // TODO: remove: redundant information
        jet->addInfo( LHCb::JetIDInfo::vtx_y, vtx->position().Y() ); // TODO: remove: redundant information
        jet->addInfo( LHCb::JetIDInfo::vtx_z, vtx->position().Z() ); // TODO: remove: redundant information
      } else {
        jet->addInfo( LHCb::JetIDInfo::vtx_x, 0. ); // TODO: remove: redundant information
        jet->addInfo( LHCb::JetIDInfo::vtx_y, 0. ); // TODO: remove: redundant information
        jet->addInfo( LHCb::JetIDInfo::vtx_z, 0. ); // TODO: remove: redundant information
      }
      recojets.insert( jet.release() );
    }
  }
  m_nbJetCounter += recojets.size();

  auto c10 = m_nbJetCounter10.buffer();
  auto c20 = m_nbJetCounter20.buffer();
  for ( auto* jet : recojets ) {
    if ( m_MCJets.value() ) {
      addInfo( *jet, jet->daughters() );
    } else {
      addInfo( *jet, LHCb::JetAccessories::getBasics( *jet ) );
    }

    if ( !m_jetEcFilePath.empty() ) jec( jet, PrimaryVertices.size() ); // Perform jet energy correction

    const auto pt = jet->momentum().Pt();
    if ( pt > 10 * Gaudi::Units::GeV ) { c10 += 1; }
    if ( pt > 20 * Gaudi::Units::GeV ) { c20 += 1; }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    for ( auto* jet : recojets ) {
      auto jetBestPV = m_prtVtxTool->relatedPV( jet, PrimaryVertices );
      debug() << "P: " << jet->momentum().P() << " Pt: " << jet->momentum().Pt()
              << " Ntracks: " << jet->info( LHCb::JetIDInfo::Ntracks, 0 )
              << " MTF: " << jet->info( LHCb::JetIDInfo::MTF, 0 ) << " MNF: " << jet->info( LHCb::JetIDInfo::MNF, 0 )
              << " MPT: " << jet->info( LHCb::JetIDInfo::MPT, 0 ) << " CPF: " << jet->info( LHCb::JetIDInfo::CPF, 0 )
              << " JetWidth: " << jet->info( LHCb::JetIDInfo::JetWidth, 0 )
              << " JetWidthNorm: " << jet->info( LHCb::JetIDInfo::JetWidthNorm, 0 )
              << " #Jet daughters: " << jet->daughters().size() << " Jet BestPV: key " << jetBestPV->key() << endmsg;
    }
  }

  return recojets;
}

//  Relate particles with bestPVs
P2PVTable FastJetBuilder::relateP2Vtx( LHCb::span<LHCb::Particle const* const> Particles,
                                       const LHCb::RecVertices&                PrimaryVertices ) const {
  auto vtx2prtRel = P2PVTable{};
  auto vtx        = LHCb::VertexBase::ConstVector{PrimaryVertices.begin(), PrimaryVertices.end()};
  for ( auto* prt : Particles ) {
    auto bestPV = m_prtVtxTool->relatedPV( prt, vtx );
    if ( !bestPV ) {
      ++m_msg_nobestPV;
      continue;
    }
    auto sc = vtx2prtRel.relate( static_cast<const LHCb::RecVertex*>( bestPV ), prt );
    if ( sc.isFailure() ) ++m_msg_noBestPV;
  }
  return vtx2prtRel;
}

// prepare FastJet
std::vector<fastjet::PseudoJet> FastJetBuilder::prepare( LHCb::span<LHCb::Particle const* const> particles ) const {
  std::vector<fastjet::PseudoJet> jets;
  jets.reserve( particles.size() );
  for ( const auto& [i, p] : LHCb::range::enumerate( particles ) ) {
    if ( !p ) {
      ++m_msg_invalid_input;
      continue;
    }
    jets.push_back( createJetInput( *p, to_user_index( i ) ) );
  }
  return jets;
}

// Run FastJet
std::vector<std::unique_ptr<LHCb::Particle>>
FastJetBuilder::makeFastJets( LHCb::span<LHCb::Particle const* const> particles,
                              const DetectorElement&                  geometry ) const {

  // prepare the input data and define the jets
  auto jetinputs = prepare( particles );

  if ( jetinputs.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) { debug() << "No particle inputs to reconstruct jets" << endmsg; }
    return {};
  }

  // Reconstructed Jets from FastJet

  auto jetDef = fastjet::JetDefinition{m_type.value(), m_r, m_recom.value(), m_strat.value()};
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "fastjet::JetDefinition:" << endmsg;
    debug() << jetDef.description() << endmsg;
  }

  auto clusters = fastjet::ClusterSequence( jetinputs, jetDef );
  auto jets     = sorted_by( m_sort.value(), clusters.inclusive_jets( m_ptmin ) );

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "No jets from fastjet::ClusterSequence" << endmsg; }

  std::vector<std::unique_ptr<LHCb::Particle>> recojets;
  recojets.reserve( jets.size() );

  for ( const PJet& jet : jets ) {

    // jet daughter list to be used in the combiner
    LHCb::Particle::ConstVector daughters;

    for ( const PJet& c : clusters.constituents( jet ) ) {
      // find the appropriate input particle
      const int index = from_user_index( c.user_index() );
      if ( 0 > index || (int)jetinputs.size() <= index ) {
        ++m_msg_invalidjetdaughter;
        continue;
      }
      const LHCb::Particle* p = particles[index];
      daughters.push_back( p );
    }
    if ( daughters.empty() ) {
      ++m_msg_nojetdaughters;
      continue;
    }
    auto&        pJet = *recojets.emplace_back( std::make_unique<LHCb::Particle>() );
    LHCb::Vertex vJet;
    if ( daughters.size() > 1 || daughters[0]->particleID() != LHCb::ParticleID( 22 ) ) {
      auto sc = m_combiner->combine( daughters, pJet, vJet, geometry );
      if ( sc.isFailure() ) {
        recojets.pop_back();
        ++m_msg_momcomb;
        continue;
      }
    } else {
      // Jets with built with only one particle which is a photon are removed
      recojets.pop_back();
      continue;
    }

    pJet.setParticleID( LHCb::ParticleID( m_jetID ) );
    // redefine the momentum with the FastJet output
    pJet.setMomentum( {jet.px(), jet.py(), jet.pz(), jet.e()} );
  }
  return recojets;
}

// Perform jet energy correction.
void FastJetBuilder::jec( LHCb::Particle* prt, int nPVs ) const {
  Gaudi::LorentzVector vec = prt->momentum();
  double               pt( vec.Pt() / Gaudi::Units::GeV ), eta( vec.Eta() ), phi( vec.Phi() ),
      cpf( prt->info( LHCb::JetIDInfo::CPF, 0 ) );
  std::size_t iPvs( 0 ), iEta( 0 ), iCpf( 0 ), iPhi( 0 );
  phi = abs( abs( phi ) - M_PI / 2.0 );

  for ( ; iPvs < m_jecLimNPvs.size() - 2; ++iPvs )
    if ( nPVs < m_jecLimNPvs[iPvs + 1] ) break;
  for ( ; iEta < m_jecLimEta.size() - 2; ++iEta )
    if ( eta < m_jecLimEta[iEta + 1] ) break;
  for ( ; iCpf < m_jecLimCpf.size() - 2; ++iCpf )
    if ( cpf < m_jecLimCpf[iCpf + 1] ) break;
  for ( ; iPhi < m_jecLimPhi.size() - 2; ++iPhi )
    if ( phi < m_jecLimPhi[iPhi + 1] ) break;

  if ( pt < m_jecLimPt[0] ) pt = m_jecLimPt[0];
  if ( pt > m_jecLimPt[m_jecLimPt.size() - 1] ) pt = m_jecLimPt[m_jecLimPt.size() - 1];

  int hkey = iPvs + 100 * iEta + 1000 * iCpf + 10000 * iPhi;

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "JEC: eta: " << eta << " cpf: " << cpf << " phi: " << phi << " pt: " << pt << endmsg;
    debug() << "JEC: nPVs: " << nPVs << " iPvs: " << iPvs << " iEta: " << iEta << " iCpf: " << iCpf << " iPhi: " << iPhi
            << endmsg;
    auto hname = "JECSYS_PV" + std::to_string( iPvs + 1 ) + "_ETA" + std::to_string( iEta ) + "_CPF" +
                 std::to_string( iCpf ) + "_PHI" + std::to_string( iPhi );
    debug() << "Retrieving histogram with map key " << hkey << " name: " << hname << endmsg;
  }
  auto const& hst = m_jecsHistos.at( hkey );
  double      err = hst.GetBinError( hst.FindFixBin( pt ) );
  double      cor = hst.Interpolate( pt ) + m_jetEcShift * err;
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "pt : " << pt << " pt index: " << hst.FindFixBin( pt )
            << " Bin content: " << hst.GetBinContent( hst.FindFixBin( pt ) ) << " err: " << err << " cor: " << cor
            << endmsg;
  prt->addInfo( LHCb::JECInfo::JEC, cor );
  prt->addInfo( LHCb::JECInfo::NPVsForJEC, nPVs );
  prt->addInfo( LHCb::JECInfo::JECError, err );
  prt->setMomentum( cor * vec );
}

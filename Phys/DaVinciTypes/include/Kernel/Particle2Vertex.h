/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_PARTICLE2VERTEX_H
#define KERNEL_PARTICLE2VERTEX_H 1

// Include files
#include "Event/Particle.h"
#include "Event/VertexBase.h"
#include "GaudiKernel/HashMap.h"
#include "Relations/Relation.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted.h"
#include "Relations/RelationWeighted1D.h"
/** @namespace Particle2Vertex Particle2Vertex.h Kernel/Particle2Vertex.h
 *
 * Namespace containing types corresponding to the Particle -> VertexBase
 * uni-directional weighted relationship.
 *
 *  @author Juan PALACIOS
 *  @date   2006-08-17
 */
namespace Particle2Vertex {
  typedef double                                                                 WeightType;
  typedef LHCb::RelationWeighted1D<LHCb::Particle, LHCb::VertexBase, WeightType> WTable;

  typedef Relations::RelationWeighted<LHCb::Particle, LHCb::VertexBase, WeightType> LightWTable;

  typedef WTable::IBase::Entry   WRelation;
  typedef WTable::IBase::Entries WRelations;

  typedef LHCb::Relation1D<LHCb::Particle, LHCb::VertexBase> Table;

  typedef Relations::Relation<LHCb::Particle, LHCb::VertexBase> LightTable;
  typedef Table::IBase::Entry                                   Relation;
  typedef Table::IBase::Entries                                 Relations;

  typedef GaudiUtils::HashMap<const LHCb::Particle*, const LHCb::VertexBase*> Map;

} // namespace Particle2Vertex
#endif // KERNEL_PARTICLE2VERTEX_H

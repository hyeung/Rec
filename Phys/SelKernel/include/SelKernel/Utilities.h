/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/FunctorDefaults.h"
#include "Event/Particle.h"
#include "Event/Proxy.h"
#include "Event/SOAZip.h"
#include "Event/State.h"
#include "Event/TrackVertexUtils.h"
#include "Event/Track_v3.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/MatrixTransforms.h"
#include "LHCbMath/MatrixUtils.h"
#include "SelKernel/ParticleTraits.h"

#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/SmartRef.h"
#include "GaudiKernel/detected.h"

#include <cassert>

/** @file  Utilities.h
 *  @brief Helper types and functions for selections.
 */

namespace LHCb {
  class Particle;
  class VertexBase;
} // namespace LHCb

namespace Functors {
  template <typename T>
  constexpr bool is_legacy_particle =
      std::is_same_v<LHCb::Particle, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;
} // namespace Functors

namespace Sel::Utils {
  /** Helper to determine if the given type has a static mask_true() method. */
  template <typename T>
  using has_static_mask_true_ = decltype( T::mask_true() );

  template <typename T>
  inline constexpr bool has_static_mask_true_v = Gaudi::cpp17::is_detected_v<has_static_mask_true_, T>;

  template <typename T>
  using has_dType = typename T::dType;

  template <typename T>
  inline constexpr bool has_dType_v = Gaudi::cpp17::is_detected_v<has_dType, T>;

  /** Define plain bool versions that mirror the functions defined for
   *  SIMDWrapper's mask_v types. These are useful when writing generic
   *  functor code that works both for scalar and vector types.
   */
  constexpr bool all( bool x ) { return x; }
  constexpr bool any( bool x ) { return x; }
  constexpr bool none( bool x ) { return !x; }
  constexpr int  popcount( bool x ) { return x; }
  template <typename T>
  constexpr T select( bool x, T a, T b ) {
    return x ? a : b;
  }
  template <typename T, std::enable_if_t<std::is_arithmetic_v<T>, int> = 0>
  constexpr T hmin( T x, bool ) {
    return x;
  }
  template <typename T, std::enable_if_t<std::is_arithmetic_v<T>, int> = 0>
  constexpr T hmax( T x, bool ) {
    return x;
  }

  template <typename T, typename M>
  constexpr auto hmin( T const& x, M const& m ) {
    return x.hmin( m );
  }
  template <typename T, typename M>
  constexpr auto hmax( T const& x, M const& m ) {
    return x.hmax( m );
  }

  template <typename T>
  auto&& deref_if_ptr( T&& x ) {
    if constexpr ( LHCb::Event::details::is_SmartRef_v<std::remove_cv_t<std::remove_reference_t<T>>> ) {
      assert( x.target() );
      return *x;
    } else if constexpr ( std::is_pointer_v<std::remove_reference_t<T>> ) {
      if ( !x ) throw GaudiException( "trying to derefernce nullptr", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      return *x;
    } else {
      return std::forward<T>( x );
    }
  }

  /** Helper to determine if the given type has a bestPV() method. */
  template <typename T>
  using has_bestPV_ = decltype( std::declval<T>().bestPV() );

  template <typename T>
  inline constexpr bool has_bestPV_v = Gaudi::cpp17::is_detected_v<has_bestPV_, T>;

  /** Helper to determine if the given type has a size() method, to determine if one deals with a int_v or int. */
  template <typename T>
  using has_size_ = decltype( std::declval<T>().size() );

  template <typename T>
  inline constexpr bool has_size_v = Gaudi::cpp17::is_detected_v<has_size_, T>;

  template <typename T>
  constexpr bool is_lhcb_vertexbase = std::is_base_of_v<LHCb::VertexBase, T>;

  inline constexpr auto get_track_from_particle = []( auto const& p ) {
    auto const* pp = deref_if_ptr( p ).proto();
    return pp ? pp->track() : nullptr;
  };

  inline constexpr auto get_track_property_from_particle = []( auto const& p, auto&& accessor, auto&& invalid ) {
    auto const* trk = get_track_from_particle( p );
    return trk ? accessor( trk ) : invalid;
  };

  // TODO: make sure this predicate becomes obsolete by moving the 'dispatch' code
  //       into the relevant event model classes -- and the code to which it dispatches
  //       (which is written in terms of more basic quantities like positions, directions,
  //       and covariance matrices) into LHCbMath
  template <typename T>
  constexpr auto canBeExtrapolatedDownstream_v = std::remove_pointer_t<std::decay_t<T>>::canBeExtrapolatedDownstream;

  template <typename T>
  constexpr auto isBasicParticle_v = std::remove_pointer_t<std::decay_t<T>>::isBasicParticle;

  template <typename T>
  constexpr auto hasTrack_v = std::remove_pointer_t<std::decay_t<T>>::hasTrack;

  /** Helpers for dispatching to the right fdchi2 calculation. */
  template <typename Vertex1, typename Vertex2>
  auto flightDistanceChi2( Vertex1 const& v1, Vertex2 const& v2 ) {
    using LHCb::Event::endVertexPos;
    using LHCb::Event::posCovMatrix;
    auto cov                 = posCovMatrix( v1 ) + posCovMatrix( v2 );
    using mask_t             = decltype( cov.invChol().first );
    using ret_t              = decltype( similarity( endVertexPos( v1 ) - endVertexPos( v2 ), cov ) );
    auto success             = mask_t{true};
    std::tie( success, cov ) = cov.invChol();
    if constexpr ( std::is_arithmetic_v<mask_t> ) {
      return success ? similarity( endVertexPos( v1 ) - endVertexPos( v2 ), cov )
                     : std::numeric_limits<ret_t>::quiet_NaN();
    } else {
      return select( success, similarity( endVertexPos( v1 ) - endVertexPos( v2 ), cov ),
                     ret_t{std::numeric_limits<float>::quiet_NaN()} );
    }
  }

  /** @fn    impactParameterSquared
   *  @brief Helper for dispatching to the correct ip calculation (squared to speed up use of ip as discriminant).
   */

  template <typename Position_t, typename TrackChunk>
  auto impactParameterSquared( Position_t const& vertex_pos, TrackChunk const& track_chunk ) {
    using float_v = decltype( referencePoint( track_chunk ).X() );
    auto pos      = referencePoint( track_chunk );
    auto dir      = slopes( track_chunk );
    auto ip2      = ( pos - vertex_pos.template cast<float_v>() ).Cross( dir ).mag2() / dir.mag2();
    static_assert( std::is_same_v<decltype( ip2 ), float_v> );
    return ip2;
  }

  /** @fn    impactParameterChi2
   *  @brief Helper for dispatching to the correct ipchi2 calculation.
   *
   * The input be particle-like, with an (x, y, z) position and 3/4-momentum, or
   * it could be state-like, with a (x, y, tx, ty[, q/p]) vector and covariance
   * matrix.
   *
   * LHCb::TrackVertexUtils::vertexChi2() has the ipchi2 calculation for a
   * state and [primary] vertex position/covariance.
   *
   * @todo Add a [template] version of LHCb::TrackVertexUtils::vertexChi2()
   *       that takes a particle-like? It only uses the 4x4 upper corner of
   *       the state-like covariance matrix, so we might be able to save
   *       something?
   */

  template <typename TrackOrParticle, typename Vertex>
  __attribute__( ( flatten ) ) auto impactParameterChi2( TrackOrParticle const& obj, Vertex const& vertex ) {
    if constexpr ( std::is_pointer_v<TrackOrParticle> ) {
      assert( obj );
      return impactParameterChi2( *obj, vertex );
    } else if constexpr ( Sel::Utils::canBeExtrapolatedDownstream_v<TrackOrParticle> ) {
      return LHCb::TrackVertexUtils::vertexChi2( trackState( obj ), endVertexPos( vertex ), posCovMatrix( vertex ) );
    } else {
      // composite with a vertex
      auto [chi2, decaylength, decaylength_err] =
          LHCb::TrackVertexUtils::computeChiSquare( referencePoint( obj ), threeMomentum( obj ), covMatrix( obj ),
                                                    endVertexPos( vertex ), posCovMatrix( vertex ) );
      return chi2;
    }
  }

  template <typename Vertex>
  auto impactParameterChi2( LHCb::Particle const& particle, Vertex const& vertex ) {
    auto const* pp    = particle.proto();
    auto const* track = ( pp ? pp->track() : nullptr );
    if ( track ) return impactParameterChi2( *track, vertex );
    auto [chi2, decaylength, decaylength_err] = LHCb::TrackVertexUtils::computeChiSquare(
        referencePoint( particle ), threeMomentum( particle ), covMatrix( particle ), endVertexPos( vertex ),
        posCovMatrix( vertex ) );
    return chi2;
  }

} // namespace Sel::Utils

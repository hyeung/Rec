/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_PARTICLE2MCPARTICLE_H
#define KERNEL_PARTICLE2MCPARTICLE_H 1

// Include files
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Kernel/MCAssociation.h"
#include "Relations/Relation.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"

/** @namespace Particle2MCParticle Kernel/Particle2MCParticle.h
 *
 *
 *  Namespace containing types corresponding to the Particle -> MCParticle
 *  uni-directional weighted relationship.
 *
 *  @author Juan PALACIOS
 *  @date   2009-01-20
 */
namespace Particle2MCParticle {

  typedef double                                                                    WeightType;
  typedef LHCb::RelationWeighted1D<LHCb::Particle, LHCb::MCParticle, WeightType>    WTable;
  typedef Relations::RelationWeighted<LHCb::Particle, LHCb::MCParticle, WeightType> LightWTable;
  typedef LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle>                        Table;

  typedef Relations::Relation<LHCb::Particle, LHCb::MCParticle> LightTable;
  typedef Table::Range                                          Range;
  typedef Table::IBase::Entry                                   Relation;
  typedef Table::IBase::Entries                                 Relations;
  typedef Table::To                                             To;
  typedef Table::From                                           From;

  typedef std::vector<MCAssociation> ToVector;

  struct SumWeights {
    double operator()( const double& x, const MCAssociation& assoc ) const { return x + assoc.weight(); }
  };

  struct SortByWeight {
    bool operator()( const MCAssociation& assoc1, const MCAssociation& assoc2 ) const {
      return assoc1.weight() < assoc2.weight();
    }
  };

  struct NotInRange {

    NotInRange( const LHCb::MCParticle::ConstVector* range ) : m_toRange( range ) {}

    bool operator()( const MCAssociation& assoc ) const {
      bool OK = true;
      for ( LHCb::MCParticle::ConstVector::const_iterator itW = m_toRange->begin(); itW != m_toRange->end(); ++itW ) {
        if ( ( *itW ) == assoc.to() ) {
          OK = false;
          break;
        }
      }
      return OK;
    }

  private:
    const LHCb::MCParticle::ConstVector* m_toRange;
  };

  /**
   *  Filter a container of MCAssociations depending on whether the associated
   *  MCParticles come from a container of MCParticles
   *
   *  @param mcAssociations Container of MCAssociaitons to be filtered
   *  @param mcps Container of MCParticles to compare to for filtering
   *  @return container of filtered associaitons, subset of mcAssociaitons containing only
   *          associations with MCParticles in mcps
   *  @author Juan PALACIOS juan.palacios@nikhef.nl
   **/
  ToVector FilterMCAssociations( const ToVector& mcAssociations, const LHCb::MCParticle::ConstVector& mcps );

} // namespace Particle2MCParticle

#endif // KERNEL_PARTICLE2MCPARTICLE_H

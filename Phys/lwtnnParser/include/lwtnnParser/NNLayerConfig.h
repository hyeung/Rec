#ifndef NN_LAYER_CONFIG_HH
#define NN_LAYER_CONFIG_HH

// MIT License

// Copyright (c) 2017 Daniel Hay Guest and lwtnn contributors

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Layer Configiruation for Lightweight Tagger
//
// The structures below are used to initalize
// `LightweightNeuralNetwork` and the simpler `Stack`.
//
// Author: Dan Guest <dguest@cern.ch>

#include <map>
#include <vector>

namespace lwt {
  enum class Activation { NONE, LINEAR, SIGMOID, RECTIFIED, SOFTPLUS, SOFTMAX, TANH, HARD_SIGMOID, ELU };
  enum class Architecture { NONE, DENSE, NORMALIZATION, MAXOUT, HIGHWAY, LSTM, GRU, EMBEDDING };
  // components (for LSTM, etc)
  enum class Component {
    I,
    O,
    C,
    F, // LSTM
    Z,
    R,
    H, // GRU
    T,
    CARRY
  }; // Highway

  // structure for embedding layers
  struct EmbeddingConfig {
    std::vector<double> weights;
    int                 index;
    int                 n_out;
  };

  // main layer configuration
  struct LayerConfig {
    // dense layer info
    std::vector<double> weights;
    std::vector<double> bias;
    std::vector<double> U; // TODO(lwtnn): what is this thing called in LSTMs?
    Activation          activation;
    Activation          inner_activation; // for LSTMs and GRUs

    // additional info for sublayers
    std::vector<LayerConfig>         sublayers;
    std::map<Component, LayerConfig> components;
    std::vector<EmbeddingConfig>     embedding;

    // arch flag
    Architecture architecture;
  };

  // graph node configuration
  struct NodeConfig {
    enum class Type { INPUT, INPUT_SEQUENCE, FEED_FORWARD, CONCATENATE, SEQUENCE, TIME_DISTRIBUTED };
    Type                     type;
    std::vector<std::size_t> sources;
    int                      index; // input node size, or layer number
  };
} // namespace lwt

#endif // NN_LAYER_CONFIG_HH

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES29_H
#  define LOKI_PARTICLES29_H 1
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#  include "Event/Particle.h"
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/AuxDesktopBase.h"
#  include "LoKi/BasicFunctors.h"
// ============================================================================
/** @file LoKi/Particles29.h
 *  The file with functors for Jaap Panman
 *  the functors are moved to LoKi/Legacy.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2009-04-30
 *
 */
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES29_H
// ============================================================================

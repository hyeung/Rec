/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Event/Vertex.h"
#include "Event/VertexBase.h"

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/ImpParBase.h"
#include "LoKi/Keeper.h"
#include "LoKi/PhysRangeTypes.h"
#include "LoKi/PhysTypes.h"

/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-19
 */

namespace LoKi {

  namespace Particles {

    /** @class ImpPar
     *  class for evaluation of impact parameter of
     *  particle with respect to vertex
     *  The tool IDistanceCalculator is used
     *
     *  @see IDistanceCalculator
     *  @see LoKi::Vertices::ImpParBase
     *  @see LoKi::Vertices::ImpactParamTool
     *  @see LHCb::Particle
     *  @see LHCb::Vertex
     *  @see LoKi::Cuts::IP
     *
     *  Actually it is a common base class for all functors, dealing with
     *  IDistanceCalculator
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2002-07-15
     */
    struct GAUDI_API ImpPar : LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                              LoKi::Vertices::ImpParBase,
                              virtual LoKi::AuxDesktopBase {
      /// constructor
      ImpPar( IDVAlgorithm const* algo, const LHCb::VertexBase* vertex, const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor
      ImpPar( IDVAlgorithm const* algo, const LoKi::Point3D& point, const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor
      ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::VertexHolder& vertex,
              const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor
      ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool, const LHCb::VertexBase* vertex );
      /// constructor
      ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool, const LoKi::Point3D& point );
      /// constructor
      ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
              const LoKi::Vertices::VertexHolder& vertex );
      /// constructor
      ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpParBase& tool );
      /// MANDATORY: clone method ("virtual constructor")
      ImpPar* clone() const override { return new ImpPar( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return ip( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "IP"; }
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

      /// the actual evaluator of impact parameter
      double ip( argument p, IGeometryInfo const& geometry ) const;
      /// the actual evaluator of impact parameter chi2
      double ipchi2( argument p, IGeometryInfo const& geometry ) const;
      /** the actual evaluation of 'path-distance'
       *  @see IDistanceCalculator::pathDistance
       *  @param p        (INPUT) the particle
       *  @param distance (OUTPUT) the distance
       *  @param error    (OUTPUT) the error
       *  @param chi2     (OUTPUT) the chi2
       *  @return status code
       */
      StatusCode path( argument p, double& distance, double& error, double& chi2, IGeometryInfo const& geometry ) const;
      /** the actual evaluation of 'projected-distance'
       *  @see IDistanceCalculator::projectedDistance
       *  @param p        (INPUT) the particle
       *  @param distance (OUTPUT) the distance
       *  @param error    (OUTPUT) the error
       *  @return status code
       */
      StatusCode projected( argument p, double& distance, double& error, IGeometryInfo const& geometry ) const;
      /** the actual evaluation of 'projected-distance'
       *  @see IDistanceCalculator::projectedDistance
       *  @param p        (INPUT) the particle
       *  @return the distance
       */
      double projected( argument p, IGeometryInfo const& geometry ) const;
    };

    /** @class ImpParChi2
     *  class for evaluation of impact parameter chi2
     *  of particle with respect to vertex
     *  The tool IDistanceCalculator is used
     *
     *  @see IDistanceCalculator
     *  @see LoKi::Vertices::ImpParBase
     *  @see LoKi::Vertices::ImpactParamTool
     *  @see LHCb::Particle
     *  @see LHCb::Vertex
     *  @see LoKi::Cuts::IPCHI2
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2002-07-15
     */
    struct GAUDI_API ImpParChi2 : LoKi::Particles::ImpPar {
      /// constructor
      ImpParChi2( IDVAlgorithm const* algo, const LHCb::VertexBase* vertex,
                  const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::AuxDesktopBase( algo ), LoKi::Particles::ImpPar( algo, vertex, tool ) {}
      /// constructor
      ImpParChi2( IDVAlgorithm const* algo, const LoKi::Point3D& point, const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::AuxDesktopBase( algo ), LoKi::Particles::ImpPar( algo, point, tool ) {}
      /// constructor
      ImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::VertexHolder& vertex,
                  const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::AuxDesktopBase( algo ), LoKi::Particles::ImpPar( algo, vertex, tool ) {}
      /// constructor
      ImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                  const LHCb::VertexBase* vertex )
          : LoKi::AuxDesktopBase( algo ), LoKi::Particles::ImpPar( algo, vertex, tool ) {}
      /// constructor
      ImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool, const LoKi::Point3D& point )
          : LoKi::AuxDesktopBase( algo ), LoKi::Particles::ImpPar( algo, point, tool ) {}
      /// constructor
      ImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                  const LoKi::Vertices::VertexHolder& vertex )
          : LoKi::AuxDesktopBase( algo ), LoKi::Particles::ImpPar( algo, vertex, tool ) {}
      /// constructor
      ImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpParBase& tool )
          : LoKi::AuxDesktopBase( algo ), LoKi::Particles::ImpPar( algo, tool ) {}
      /// MANDATORY: clone method ("virtual constructor")
      ImpParChi2* clone() const override { return new ImpParChi2( *this ); };
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return ipchi2( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "IPCHI2"; }
    };

    /** @class MinImpPar
     *  class for evaluation of minimal valeu of
     *  impact parameter of
     *  particle with respect to seevral vertices
     *
     *  The tool IDistanceCalculator is used
     *
     *  @see IDistanceCalculator
     *  @see LoKi::Vertices::ImpParBase
     *  @see LoKi::Vertices::ImpactParamTool
     *  @see LoKi::Vertices::ImpPar
     *  @see LHCb::Particle
     *  @see LHCb::Vertex
     *  @see LoKi::Cuts::MIP
     *  @see LoKi::Cuts::MINIP
     *  @see LoKi::Cuts::IPMIN
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2002-07-15
     */
    struct GAUDI_API MinImpPar : LoKi::Particles::ImpPar, LoKi::UniqueKeeper<LHCb::VertexBase> {

      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LHCb::VertexBase::ConstVector& vertices,
                 const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LHCb::Vertex::ConstVector& vertices,
                 const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LoKi::PhysTypes::VRange& vertices,
                 const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LHCb::RecVertex::ConstVector& vertices,
                 const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LHCb::RecVertex::Container* vertices,
                 const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                 const LHCb::VertexBase::ConstVector& vertices );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                 const LHCb::Vertex::ConstVector& vertices );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                 const LoKi::PhysTypes::VRange& vertices );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                 const LHCb::RecVertex::ConstVector& vertices );
      /// constructor from the vertices and the tool
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                 const LHCb::RecVertex::Container* vertices );

      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpPar( IDVAlgorithm const* algo, VERTEX first, VERTEX last, const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::Particles::ImpPar( algo, (const LHCb::VertexBase*)0, tool )
          , LoKi::UniqueKeeper<LHCb::VertexBase>( first, last ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Keeper<VERTEX>& keeper,
                 const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::Particles::ImpPar( algo, (const LHCb::VertexBase*)0, tool )
          , LoKi::UniqueKeeper<LHCb::VertexBase>( keeper.begin(), keeper.end() ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpPar( IDVAlgorithm const* algo, const LoKi::UniqueKeeper<VERTEX>& keeper,
                 const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::Particles::ImpPar( algo, (const LHCb::VertexBase*)0, tool )
          , LoKi::UniqueKeeper<LHCb::VertexBase>( keeper.begin(), keeper.end() ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param tool  helper tool
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       */
      template <class VERTEX>
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool, VERTEX first, VERTEX last )
          : LoKi::Particles::ImpPar( algo, (const LHCb::VertexBase*)0, tool )
          , LoKi::UniqueKeeper<LHCb::VertexBase>( first, last ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                 const LoKi::Keeper<VERTEX>& keeper )
          : LoKi::Particles::ImpPar( algo, (const LHCb::VertexBase*)0, tool )
          , LoKi::UniqueKeeper<LHCb::VertexBase>( keeper.begin(), keeper.end() ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                 const LoKi::UniqueKeeper<VERTEX>& keeper )
          : LoKi::Particles::ImpPar( algo, (const LHCb::VertexBase*)0, tool )
          , LoKi::UniqueKeeper<LHCb::VertexBase>( keeper.begin(), keeper.end() ) {}
      /// MANDATORY: clone method ("virtual constructor")
      MinImpPar* clone() const override { return new MinImpPar( *this ); };
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return mip( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// the actual evaluator
      result_type mip( argument p, IGeometryInfo const& geometry ) const;
      /// the actual evaluator
      result_type mipchi2( argument p, IGeometryInfo const& geometry ) const;
    };

    /** @class MinImpParChi2
     *  class for evaluation of minimal value of
     *  chi2 of impact parameter of
     *  particle with respect to seevral vertices
     *
     *  The tool IDistanceCalculator is used
     *
     *  @see IDistanceCalculator
     *  @see LoKi::Vertices::ImpParBase
     *  @see LoKi::Vertices::ImpactParamTool
     *  @see LoKi::Vertices::ImpParChi2
     *  @see LHCb::Particle
     *  @see LHCb::Vertex
     *  @see LoKi::Cuts::MIPCHI2
     *  @see LoKi::Cuts::CHI2MIP
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2002-07-15
     */
    struct GAUDI_API MinImpParChi2 : LoKi::Particles::MinImpPar {

      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LHCb::VertexBase::ConstVector& vertices,
                     const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LHCb::Vertex::ConstVector& vertices,
                     const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::PhysTypes::VRange& vertices,
                     const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LHCb::RecVertex::ConstVector& vertices,
                     const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LHCb::RecVertex::Container* vertices,
                     const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                     const LHCb::VertexBase::ConstVector& vertices );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                     const LHCb::Vertex::ConstVector& vertices );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                     const LoKi::PhysTypes::VRange& vertices );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                     const LHCb::RecVertex::ConstVector& vertices );
      /// constructor from the vertices and the tool
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                     const LHCb::RecVertex::Container* vertices );

      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpParChi2( IDVAlgorithm const* algo, VERTEX first, VERTEX last, const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::Particles::MinImpPar( algo, first, last, tool ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Keeper<VERTEX>& keeper,
                     const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::Particles::MinImpPar( algo, keeper, tool ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::UniqueKeeper<VERTEX>& keeper,
                     const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::Particles::MinImpPar( algo, keeper, tool ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param tool  helper tool
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       */
      template <class VERTEX>
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool, VERTEX first, VERTEX last )
          : LoKi::Particles::MinImpPar( algo, tool, first, last, tool ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                     const LoKi::Keeper<VERTEX>& keeper )
          : LoKi::Particles::MinImpPar( algo, tool, keeper ) {}
      /** templated constructor from arbitrary sequence
       *  of objects, convertible to "const LHCb::Vertex*"
       *  @param first 'begin'-iterator of the sequence
       *  @param last  'end'-iterator of the sequence
       *  @param tool  helper tool
       */
      template <class VERTEX>
      MinImpParChi2( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                     const LoKi::UniqueKeeper<VERTEX>& keeper )
          : LoKi::Particles::MinImpPar( algo, tool, keeper ) {}

      /// MANDATORY: clone method ("virtual constructor")
      MinImpParChi2* clone() const override { return new MinImpParChi2( *this ); };
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return mipchi2( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

  } // namespace Particles

} //                                                      end of namespace LoKi

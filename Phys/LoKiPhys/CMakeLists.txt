###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/LoKiPhys
-------------
#]=======================================================================]

gaudi_add_library(LoKiPhysLib
    SOURCES
        src/AuxDTFBase.cpp
        src/AuxDesktopBase.cpp
        src/Child.cpp
        src/ChildSelector.cpp
        src/DecayChain.cpp
        src/Decays.cpp
        src/GetPhysDesktop.cpp
        src/IDecay.cpp
        src/ImpParBase.cpp
        src/ImpactParamTool.cpp
        src/LoKiPhys.cpp
        src/LoKiPhys_dct.cpp
        src/PIDOperators.cpp
        src/Particles.cpp
        src/Particles0.cpp
        src/Particles1.cpp
        src/Particles10.cpp
        src/Particles11.cpp
        src/Particles12.cpp
        src/Particles13.cpp
        src/Particles14.cpp
        src/Particles15.cpp
        src/Particles16.cpp
        src/Particles17.cpp
        src/Particles18.cpp
        src/Particles19.cpp
        src/Particles2.cpp
        src/Particles20.cpp
        src/Particles21.cpp
        src/Particles22.cpp
        src/Particles23.cpp
        src/Particles24.cpp
        src/Particles25.cpp
        src/Particles26.cpp
        src/Particles27.cpp
        src/Particles28.cpp
        src/Particles29.cpp
        src/Particles3.cpp
        src/Particles31.cpp
        src/Particles32.cpp
        src/Particles33.cpp
        src/Particles34.cpp
        src/Particles35.cpp
        src/Particles36.cpp
        src/Particles37.cpp
        src/Particles38.cpp
        src/Particles39.cpp
        src/Particles4.cpp
        src/Particles40.cpp
        src/Particles41.cpp
        src/Particles42.cpp
        src/Particles43.cpp
        src/Particles44.cpp
        src/Particles45.cpp
        src/Particles46.cpp
        src/Particles47.cpp
        src/Particles48.cpp
        src/Particles5.cpp
        src/Particles6.cpp
        src/Particles7.cpp
        src/Particles8.cpp
        src/Particles9.cpp
        src/Photons.cpp
        src/PhysAlgsDicts.cpp
        src/PhysCnv.cpp
        src/PhysDump.cpp
        src/PhysExtract.cpp
        src/PhysExtractDicts.cpp
        src/PhysKinematics.cpp
        src/PhysMoniDicts.cpp
        src/PhysSinks.cpp
        src/PhysSources.cpp
        src/PhysStreamers.cpp
        src/PointingMass.cpp
        src/PrintDecay.cpp
        src/Sections.cpp
        src/SelectVertex.cpp
        src/TreeFactory.cpp
        src/VertexHolder.cpp
        src/Vertices.cpp
        src/Vertices0.cpp
        src/Vertices1.cpp
        src/Vertices2.cpp
        src/Vertices3.cpp
        src/Vertices5.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            LHCb::DigiEvent
            LHCb::LoKiCoreLib
            LHCb::PartPropLib
            LHCb::PhysEvent
            LHCb::PhysInterfacesLib
            LHCb::RecEvent
            LHCb::RelationsLib
            LHCb::TrackEvent
            Rec::DaVinciInterfacesLib
            Rec::DaVinciTypesLib
            Rec::LoKiUtils
            Rec::RecInterfacesLib
        PRIVATE
            Gaudi::GaudiAlgLib
            GSL::gsl
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            VDT::vdt
)

gaudi_add_module(LoKiPhys
    SOURCES
        src/Components/Decay.cpp
    LINK
        LHCb::LoKiCoreLib
        LHCb::PhysEvent
        LoKiPhysLib
)

gaudi_add_dictionary(LoKiPhysDict
    HEADERFILES dict/LoKiPhysDict.h
    SELECTION dict/LoKiPhys.xml
    LINK LoKiPhysLib
    OPTIONS ${PHYS_DICT_GEN_DEFAULT_OPTS}
)

gaudi_add_executable(DecayGrammarTest
    SOURCES
        src/tests/DecayGrammarTest.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::LoKiCoreLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LoKiPhysLib
)

gaudi_install(PYTHON)

gaudi_add_tests(pytest ${CMAKE_CURRENT_SOURCE_DIR}/python/LoKiPhys/tests.py)

###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/Luminosity
---------------
#]=======================================================================]

gaudi_add_module(Luminosity
    SOURCES
        src/LumiCounterFromContainer.cpp
        src/LumiCountersFromCalo.cpp
        src/LumiCountersFromPVs.cpp
        src/LumiCountersFromRich.cpp
        src/LumiCountersFromVeloHits.cpp
        src/LumiCountersFromVeloTracks.cpp
        src/LumiCounterMerger.cpp
    LINK
        Gaudi::GaudiAlgLib
        LHCb::LumiEventLib
        LHCb::RecEvent
        LHCb::RichFutureUtils
        LHCb::TrackEvent
        Rec::PrKernel
)

gaudi_add_tests(QMTest)

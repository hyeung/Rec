#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Small script to test functors instantiation
#  @author Saverio Mariani
##
# =============================================================================
from PyConf import Algorithms
from Configurables import (ApplicationMgr, LHCbApp)
from Functors import *
from Functors.tests.categories import DUMMY_DATA_DEP, dummy_data_pv_container, dummy_data_fwdtracks
import Functors.math as fmath
from GaudiKernel.SystemOfUnits import GeV
from Gaudi.Configuration import VERBOSE

generic_functors = [
    ALL,
    NONE,
]

all_new_eventmodel_track_functors = [
    NHITS,
]

# these rely on forward->velo ancestor links. vectorised following of these is
# not currently implemented, so we exclude them from the test (compilation
# would fail with a static_assert)
all_tracks_except_unfitted_prforward_avx = [
    ETA,
    PHI,
    MINIP(dummy_data_pv_container),
    POSITION_X @ TRACKSTATE,
    POSITION_Y @ TRACKSTATE,
    POSITION_Z @ TRACKSTATE,
    TX @ TRACKSTATE,
    TY @ TRACKSTATE,
    MINIPCUT(IPCut=0.0, Vertices=dummy_data_pv_container),
]

# TODO this is no longer true
# The MatrixNet implementation is currently not generalised to support running
# with vector types (OL doesn't see a major barrier to this being done though!)
scalar_track_functors = [
    MVA(MVAType='MatrixNet',
        Config={'MatrixnetFile': "paramfile://data/Hlt1TwoTrackMVA.mx"},
        Inputs={
            'chi2': ETA,
            'fdchi2': PHI,
            'sumpt': ETA,
            'nlt16': PHI,
        }),
]

only_velo_track_functors = []

only_long_track_functors = [
    P,
]

only_long_track_functors_except_track_v2 = [
    QOVERP,
]

# see all_tracks_except_unfitted_prforward_avx above -- PT requires ETA and P,
# ETA requires navigating back to the state in the VELO
long_tracks_except_unfitted_prforward_avx = [
    PT,
]

fitted_track_functors = [
    QOVERP @ TRACKSTATE,
]
for row in range(5):
    for col in range(row, 5):
        fitted_track_functors.append(COV(Row=row, Col=col) @ TRACKSTATE)

fitted_track_or_composite_functors = [
    NDOF,
    CHI2DOF,
    MINIPCHI2(dummy_data_pv_container),
    BPVIP(dummy_data_pv_container),
    BPVIPCHI2(dummy_data_pv_container),
    MINIPCHI2CUT(IPChi2Cut=0.0, Vertices=dummy_data_pv_container),
    # FIXME the following cannot be here
    # PROBNN_MU,
]

# this was removed from v2::Track, hopefully there will be some new ghostprob
# event model object soon...
only_v1_track_functors = [
    NDOF,  # TODO can this move?
    GHOSTPROB,
]

only_combination_functors = [
    DOCA(1, 2),
    SDOCA(1, 2),
    DOCACHI2(1, 2),
    SDOCACHI2(1, 2),
    MAXDOCA,
    MAXSDOCA,
    MAXDOCACHI2,
    MAXSDOCACHI2,
    MAXDOCACUT(10.),
    MAXSDOCACUT(10.),
    MAXDOCACHI2CUT(10.),
    MAXSDOCACHI2CUT(10.),
    SUM(PT),
    MIN(PT),
    MAX(PT),
    CHARGE,
    ALV(1, 2),
]

only_composite_functors = [
    MASSWITHHYPOTHESES(
        Masses=(497., 497.)),  # assume we'll test with a 2-body vertex...
    MASSWITHHYPOTHESES(Masses=(137., 'pi+')),
    MASSWITHHYPOTHESES(Masses=("mu+", "mu-")),
    BPVETA(dummy_data_pv_container),
    # FIXME fix BPVCORRM functor or the test?
    # ../Phys/FunctorCore/include/Functors/Composite.h:196:31: error: 'const struct LHCb::TrackKernel::TrackCompactVertex<2, double, long unsigned int>' has no member named 'momentum'
    #   196 |           auto p4 = composite.momentum();
    # BPVCORRM(Vertices=DUMMY_DATA_DEP),
    BPVDIRA(dummy_data_pv_container),
    BPVFDCHI2(dummy_data_pv_container),
    BPVFDVEC(dummy_data_pv_container),
    BPVFDIR(dummy_data_pv_container),
    BPVVDX(dummy_data_pv_container),
    BPVVDY(dummy_data_pv_container),
    BPVVDZ(dummy_data_pv_container),
    BPVVDRHO(dummy_data_pv_container),
    BPVDLS(dummy_data_pv_container),
    END_VX,
    END_VY,
    END_VZ,
    END_VRHO,
    COMB(
        Functor=SUM(PT),
        # C++ version of this functor isn't well-formed with zero containers...
        ChildContainers=[dummy_data_fwdtracks]),
    MVA(
        MVAType='SigmaNet',
        Config={
            'File':
            "file://${FUNCTORCOREROOT}/tests/options/SigmaNet_weights.json",
            'Name':
            "SigmaNet",
            'Lambda':
            "2.0",
            'NLayers':
            "5",
            'InputSize':
            "6",
            'Monotone_Constraints':
            '[1,0,1,0,0,1]',
            'Variables':
            'TwoBody_LoKi_DIRA_OWNPV,log(TwoBody_LoKi_ENDVERTEX_CHI2),log(TwoBody_LoKi_FDCHI2_OWNPV),log(TwoBody_LoKi_MINIPCHI2),TwoBody_LoKi_Mcorr,TwoBody_LoKi_PT',
        },
        Inputs={
            "TwoBody_LoKi_DIRA_OWNPV":
            BPVDIRA(dummy_data_pv_container),
            "log(TwoBody_LoKi_ENDVERTEX_CHI2)":
            fmath.log(CHI2DOF),
            "log(TwoBody_LoKi_FDCHI2_OWNPV)":
            fmath.log(BPVFDCHI2(dummy_data_pv_container)),
            "log(TwoBody_LoKi_MINIPCHI2)":
            fmath.log(BPVIPCHI2(dummy_data_pv_container)),
            "TwoBody_LoKi_Mcorr":
            PT /
            GeV,  #BPVCORRM(DUMMY_DATA_DEP)/GeV, # As mentioned above using BPVCORRM causes this test to fail. Therefore PT is used as a replacement as this is not related to the MVA functor.
            "TwoBody_LoKi_PT":
            PT / GeV,
        }),
    MVA(MVAType='TMVA',
        Config={
            'XMLFile': 'paramfile://data/Hlt2_Radiative_HHgamma.xml',
            'Name': 'BDT',
        },
        Inputs={
            "ipchi2": ETA,
            'ipchi2_min': ETA,
            'gamma_pt': ETA,
            'm_corrected': ETA,
            'vm_corrected': ETA,
            'fdchi2': ETA,
            'vtx_chi2': ETA,
            'doca': ETA
        }),
]

particle_functors = [
    # Functors from Particle.h valid for both track-like and composites
    IS_ID('pi+'),
    IS_ABS_ID('pi+'),
]


def test_functors(alg_name_suffix, functors_to_test, SkipCut=False):
    algo = getattr(Algorithms, 'InstantiateFunctors__' + alg_name_suffix)
    test = algo(
        name='Test' + alg_name_suffix,
        Functions={
            functor.code_repr(): functor
            for functor in functors_to_test
        },
        Cut=FILTER(ALL) if not SkipCut else None)
    algs, _ = test.configuration().apply()
    ApplicationMgr(OutputLevel=VERBOSE).TopAlg.append(algs[-1])


def test_pr(prname, functors, only_unwrapped_functors=[]):
    test_functors(prname, functors)


app = LHCbApp(DataType="Upgrade", Simulation=True)
app.EvtMax = 0

# Simple instantiation test: are the templates working?
#
# See InstantiateFunctors.cpp for the explicit type names that are being used
# here, and if you want to add new instantiations.
test_functors(
    'vector__TrackCompactVertex__2_double', generic_functors +
    fitted_track_or_composite_functors + only_composite_functors)
test_pr(
    'PrVeloTracks',
    generic_functors + only_velo_track_functors +
    all_tracks_except_unfitted_prforward_avx +
    all_new_eventmodel_track_functors,
    only_unwrapped_functors=scalar_track_functors)
forward_functors = generic_functors + only_long_track_functors
test_pr(
    'PrLongTracks',
    forward_functors + all_new_eventmodel_track_functors +
    only_long_track_functors_except_track_v2,
    only_unwrapped_functors=scalar_track_functors +
    all_tracks_except_unfitted_prforward_avx +
    long_tracks_except_unfitted_prforward_avx)
test_functors('Track_v1', only_v1_track_functors)
test_functors('vector__ParticleCombination__FittedWithMuonID__2',
              generic_functors + only_combination_functors)
test_functors('Composites', particle_functors)

/*****************************************************************************\
* (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Core/FloatComparison.h"
#include "Event/Bremsstrahlung.h"
#include "Event/ProtoParticle.h"
#include "Event/TrackEnums.h"
#include "Event/Track_v1.h"
#include "Event/Track_v3.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "GaudiKernel/detected.h"
#include "MCInterfaces/IMCReconstructed.h"
#include "SelKernel/ParticleAccessors.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"

#include <string>
#include <type_traits>

/** @file  TrackLike.h
 *  @brief Definitions of functors for track-like objects.
 */

/** @namespace Functors::Track
 *
 *  This defines some basic functors that operate on track-like, or maybe more
 *  accurately charged-particle-like, objects. Both predicates and functions
 *  are defined in the same file for the moment.
 */

namespace Functors::detail {
  template <typename T>
  constexpr bool is_proto_particle =
      std::is_same_v<LHCb::ProtoParticle, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

  template <typename T>
  constexpr bool is_legacy_track = std::is_same_v<LHCb::Track, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

  /*
   * Helper for ProbNN
   */
  enum struct Pid { electron, muon, pion, kaon, proton, deuteron, ghost };

  template <Pid pid, typename T>
  constexpr auto combDLL( const T& d ) {
    if constexpr ( is_legacy_particle<T> ) {
      auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
      return ( pp && pp->globalChargedPID() ) ? combDLL<pid>( *( pp->globalChargedPID() ) ) : std::nullopt;
    } else if constexpr ( detail::is_proto_particle<T> ) {
      auto gpid = Sel::Utils::deref_if_ptr( d ).globalChargedPID();
      return gpid ? detail::combDLL<pid>( *gpid ) : std::nullopt;
    } else {
      Functors::Optional<decltype( d.CombDLLe() )> dll = std::nullopt;
      switch ( pid ) {
      case Pid::electron:
        dll = d.CombDLLe();
        break;
      case Pid::muon:
        dll = d.CombDLLmu();
        break;
      case Pid::pion:
        dll = d.CombDLLpi();
        break;
      case Pid::kaon:
        dll = d.CombDLLk();
        break;
      case Pid::proton:
        dll = d.CombDLLp();
        break;
      case Pid::deuteron:
        dll = d.CombDLLd();
        break;
      default:
        throw std::domain_error{"impossible PID type"};
      }
      return dll;
    }
  }

  /** @brief helpers for probNN
   */
  template <Pid id>
  struct has_probNN {
    template <typename T>
    using check_for_probNN_id = decltype( std::declval<T const&>().template probNN<id>() );
    template <typename T>
    static constexpr bool value = Gaudi::cpp17::is_detected_v<check_for_probNN_id, T>;
  };

  template <Pid id, typename T>
  constexpr bool has_probNN_v = has_probNN<id>::template value<T>;

  template <Pid pid, typename T>
  constexpr auto probNN( const T& d ) {
    if constexpr ( is_legacy_particle<T> ) {
      auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
      return ( !pp || !pp->globalChargedPID() ) ? probNN<pid>( *( pp->globalChargedPID() ) ) : std::nullopt;
    } else {
      Functors::Optional<float> output = std::nullopt;
      switch ( pid ) {
      case Pid::electron:
        output = d.ProbNNe();
        break;
      case Pid::muon:
        output = d.ProbNNmu();
        break;
      case Pid::pion:
        output = d.ProbNNpi();
        break;
      case Pid::kaon:
        output = d.ProbNNk();
        break;
      case Pid::proton:
        output = d.ProbNNp();
        break;
      case Pid::deuteron:
        output = d.ProbNNd();
        break;
      case Pid::ghost:
        output = d.ProbNNghost();
        break;
      default:
        throw std::domain_error{"impossible PID type"};
      }
      return output.value() != LHCb::GlobalChargedPID::DefaultProbNN ? output : std::nullopt;
    }
  }

  template <typename StatePos_t, typename StateDir_t, typename VertexPos_t>
  [[gnu::always_inline]] inline auto impactParameterSquared( StatePos_t const& state_pos, StateDir_t const& state_dir,
                                                             VertexPos_t const& vertex ) {
    // TODO: handle one track, multiple vertices case..
    return ( state_pos - StatePos_t{vertex} ).Cross( state_dir ).mag2() / state_dir.mag2();
  }

} // namespace Functors::detail

namespace Functors::Track {

  /**
   * @brief Retrieve const Container with pointers to all the states
   */
  struct States : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).states();
    }
  };

  /**
   * @brief Access state on v1 or v3 track by location.
   *
   */
  struct StateAt : public Function {

    static constexpr auto name() { return "StateAt"; }

    StateAt( LHCb::Event::Enum::State::Location location ) : m_loc{location} {}
    StateAt( std::string location ) { parse( m_loc, location ).ignore(); }

    auto operator()( const LHCb::Event::v1::Track& track ) const {
      const auto* state = track.stateAt( m_loc );
      if ( !state ) {
        throw GaudiException( toString( m_loc ) + " not found on track.", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
      return *state;
    }

    template <typename Track>
    auto operator()( const Track& track ) const {
      if ( !track.has_state( m_loc ) ) {
        throw GaudiException( toString( m_loc ) + " not found on track.", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
      return track.state( m_loc );
    }

    auto operator()( const LHCb::Event::v1::Track* track ) const { return ( *this )( *track ); }

  private:
    LHCb::Event::Enum::State::Location m_loc{};
  };

  /**
   * @brief referencePoint, as defined by the referencePoint() accessor.
   * @note referencePoint is the position at which the object has its threeMomentum
   * defined
   */
  struct ReferencePoint : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::referencePoint;
      return referencePoint( d );
    }
  };

  /**
   * @brief The TrackState is the first state on the track.
   *
   */
  struct TrackState : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      using LHCb::Event::trackState;
      return trackState( track );
    }
  };

  /** @brief slopes vector, as defined by the slopes() accessor.
   */
  struct Slopes : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::slopes;
      return slopes( d );
    }
  };

  /** @brief FourMomentum vector e.g. (px, py, pz, E)
   */
  struct FourMomentum : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::fourMomentum;
      return fourMomentum( d );
    }
  };

  /** @brief ThreeMomentum vector e.g. (px, py, pz)
   */
  struct ThreeMomentum : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::threeMomentum;
      return threeMomentum( d );
    }
  };

  /** @brief Return covariance matrix of input
   */
  struct Covariance : public Function {
    template <typename Data>
    decltype( auto ) operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).covariance();
    }
  };

  /** @brief Charge, as defined by the charge() accessor.
   */
  struct Charge : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).charge();
    }
  };

  /** @brief HasBremAdded, as defined by the HasBremAdded accessor.
   */
  struct HasBremAdded : public Predicate {
    auto operator()( LHCb::Particle const& p ) const {
      return !LHCb::essentiallyZero( p.info( LHCb::Particle::additionalInfo::HasBremAdded, 0 ) );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
  };

  /** @brief Number of degrees of freedom, as defined by the nDoF accessor.
   */
  struct nDoF : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nDoF();
    }
  };

  /** @brief chi^2/d.o.f., as defined by the chi2PerDoF accessor.
   *
   * If the input is a legacy LHCb::Particle with a track, the track object's
   * accessor is used. If a track is not present but a vertex is the vertex's
   * accessor is used.
   */
  struct Chi2PerDoF : public Function {
    template <typename Data>
    auto operator()( Data&& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto pp  = Sel::Utils::deref_if_ptr( d ).proto();
        auto trk = ( pp ) ? pp->track() : nullptr;
        auto ev  = Sel::Utils::deref_if_ptr( d ).endVertex();
        return ( trk ) ? ( Functors::Optional{Sel::get::chi2PerDoF( Sel::Utils::deref_if_ptr( trk ) )} )
                       : ( ( ev ) ? Functors::Optional{Sel::get::chi2PerDoF( Sel::Utils::deref_if_ptr( ev ) )}
                                  : std::nullopt );
      } else {
        return Sel::get::chi2PerDoF( Sel::Utils::deref_if_ptr( d ) );
      }
    }
  };

  /** @brief chi^2, as defined by the chi2 accessor.
   *
   * If the input is a legacy LHCb::Particle with a track, the track object's
   * accessor is used. If a track is not present but a vertex is the vertex's
   * accessor is used.
   */
  struct Chi2 : public Function {
    template <typename Data>
    auto operator()( Data&& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto pp  = Sel::Utils::deref_if_ptr( d ).proto();
        auto trk = ( pp ) ? pp->track() : nullptr;
        auto ev  = Sel::Utils::deref_if_ptr( d ).endVertex();
        return ( trk ) ? ( Functors::Optional{Sel::Utils::deref_if_ptr( trk ).chi2()} )
                       : ( ( ev ) ? Functors::Optional{Sel::Utils::deref_if_ptr( ev ).chi2()} : std::nullopt );
      } else {
        return Sel::Utils::deref_if_ptr( d ).chi2();
      }
    }
  };

  /** @brief q/p, as defined by the qOverP accessor.
   */
  struct QoverP : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( detail::is_legacy_track<decltype( d )> ) {
        return Sel::Utils::deref_if_ptr( d ).firstState().qOverP();
      } else {
        return Sel::Utils::deref_if_ptr( d ).qOverP();
      }
    }
  };

  /** @brief Ghost probability, as defined by the ghostProbability accessor.
   */
  struct GhostProbability : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).ghostProbability();
    }
  };

  template <detail::Pid pid>
  struct ComDLL : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return detail::combDLL<pid>( d );
    }
  };

  /** @brief PIDmu, as defined by the CombDLLmu variable
   */
  using PIDmu = ComDLL<detail::Pid::muon>;

  /** @brief PIDp, as defined by the CombDLLp variable
   */
  using PIDp = ComDLL<detail::Pid::proton>;

  /** @brief PIDe, as defined by the CombDLLe variable
   */
  using PIDe = ComDLL<detail::Pid::electron>;

  /** @brief PIDk, as defined by the CombDLLk variable
   */
  using PIDk = ComDLL<detail::Pid::kaon>;

  /** @brief PIDpi, as defined by the CombDLLpi variable
   */
  using PIDpi = ComDLL<detail::Pid::pion>;

  /** @brief ProbNN templated definition
   */
  template <detail::Pid id>
  struct ProbNN : public Function {
    template <typename T>
    auto operator()( const T& d ) const {
      if constexpr ( detail::has_probNN_v<id, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>> ) {
        return Sel::Utils::deref_if_ptr( d ).template probNN<id>();
      } else if constexpr ( detail::has_proto_v<std::decay_t<std::remove_pointer_t<std::decay_t<T>>>> ) {
        auto        pp = Sel::Utils::deref_if_ptr( d ).proto();
        return pp ? operator()( *pp ) : std::nullopt;
      } else if constexpr ( detail::is_proto_particle<T> ) {
        auto gpid = Sel::Utils::deref_if_ptr( d ).globalChargedPID();
        return gpid ? detail::probNN<id>( *gpid ) : std::nullopt;
      } else {
        throw GaudiException{"The type T neither has a `proto()` member function nor a `probNN<id>`() member function "
                             "-- sorry, not supported",
                             "Functors::Track::ProbNN::operator()", StatusCode::FAILURE};
      }
    }
  };

  /** @brief The explicit definition for the probNN quantities
   * the interpreter doesn't like that. See
   * https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/2471#note_4863872
   *
   *    constexpr auto PROBNN_D     = ProbNN<detail::Pid::deuteron>{};
   *    constexpr auto PROBNN_E     = ProbNN<detail::Pid::electron>{};
   *    constexpr auto PROBNN_GHOST = ProbNN<detail::Pid::ghost>{};
   *    constexpr auto PROBNN_K     = ProbNN<detail::Pid::kaon>{};
   *    constexpr auto PROBNN_MU    = ProbNN<detail::Pid::muon>{};
   *    constexpr auto PROBNN_PI    = ProbNN<detail::Pid::pion>{};
   *    constexpr auto PROBNN_P     = ProbNN<detail::Pid::proton>{};
   */
  /** @brief The explicit definition for the probNN quantities
   * Warning: these are types, so you need a {} to get an instance.
   */
  using PROBNN_D_t     = ProbNN<detail::Pid::deuteron>;
  using PROBNN_E_t     = ProbNN<detail::Pid::electron>;
  using PROBNN_GHOST_t = ProbNN<detail::Pid::ghost>;
  using PROBNN_K_t     = ProbNN<detail::Pid::kaon>;
  using PROBNN_MU_t    = ProbNN<detail::Pid::muon>;
  using PROBNN_PI_t    = ProbNN<detail::Pid::pion>;
  using PROBNN_P_t     = ProbNN<detail::Pid::proton>;

  /** @brief nHits, as defined by the nHits accessor.
   */
  struct nHits : public Function {
    static constexpr auto name() { return "nHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nHits();
    }
  };

  /** @brief number of VP hits
   */
  struct nVPHits : public Function {
    static constexpr auto name() { return "nVPHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nVPHits();
    }
  };

  /** @brief number of UT hits
   */
  struct nUTHits : public Function {
    static constexpr auto name() { return "nUTHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nUTHits();
    }
  };

  /** @brief number of FT hits
   */
  struct nFTHits : public Function {
    static constexpr auto name() { return "nFTHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nFTHits();
    }
  };

  /** @brief Track history
   */
  struct History : public Function {
    static constexpr auto name() { return "History"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).history();
    }
  };

  /** @brief Track flag
   */
  struct Flag : public Function {
    static constexpr auto name() { return "Flag"; }
    template <typename Data>
    LHCb::Event::Enum::Track::Flag operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).flag();
    }
  };

  template <LHCb::Event::Enum::Track::Flag f>
  struct HasTrackFlag : public Predicate {
    static constexpr auto name() { return "HasTrackFlag"; }
    template <typename Data>
    bool operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).checkFlag( f );
    }
  };

  /** @brief Track type
   */
  struct Type : public Function {
    static constexpr auto name() { return "Type"; }
    template <typename Data>
    LHCb::Event::Enum::Track::Type operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).type();
    }
  };

  template <LHCb::Event::Enum::Track::Type t>
  struct IsTrackType : public Predicate {
    static constexpr auto name() { return "IsTrackType"; }
    template <typename Data>
    bool operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).type() == t;
    }
  };

  /** @brief Track hasT, the input of this functor must be the track type
   */
  struct HasT : public Predicate {
    static constexpr auto name() { return "HasT"; }
    constexpr auto        operator()( LHCb::Event::Enum::Track::Type t ) const { return hasT( t ); }
  };

  /** @brief Track hasUT, the input of this functor must be the track type
   */
  struct HasUT : public Predicate {
    static constexpr auto name() { return "HasUT"; }
    constexpr auto        operator()( LHCb::Event::Enum::Track::Type t ) const { return hasUT( t ); }
  };

  /** @brief Track hasVelo, the input of this functor must be the track type
   */
  struct HasVelo : public Predicate {
    static constexpr auto name() { return "HasVelo"; }
    constexpr auto        operator()( LHCb::Event::Enum::Track::Type t ) const { return hasVelo( t ); }
  };

  /** @brief Number of expected Velo clusters from VELO 3D pattern recognition
   */
  struct nPRVelo3DExpect : public Function {
    static constexpr auto name() { return "nPRVelo3DExpect"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return (int)Sel::Utils::deref_if_ptr( d ).info( LHCb::Event::Enum::Track::AdditionalInfo::nPRVelo3DExpect, -1. );
    }
  };

  /**
   * @brief MC_Reconstructed, return the reconstructed category
   * for MC Particle. The input of this functor must be reconstructed
   * object, such as LHCb::ProtoParticle and LHCb::Particle.
   *
   * For possible values of IMCReconstructed::RecCategory, see IMCReconstructed.h
   *
   */
  struct MC_Reconstructed : public Function {
    static constexpr auto name() { return "MC_Reconstructed"; }
    template <typename Data>
    IMCReconstructed::RecCategory operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        const auto* pp = Sel::Utils::deref_if_ptr( d ).proto();
        return get_reconstructed_category( pp );
      } else {
        const auto* rec = &Sel::Utils::deref_if_ptr( d );
        return get_reconstructed_category( rec );
      };
    }

  private:
    template <typename T>
    IMCReconstructed::RecCategory get_reconstructed_category( const T* rec ) const {
      if ( !rec ) return IMCReconstructed::RecCategory::NotReconstructed;

      if ( !rec->charge() ) { /// Neutral particle
        return IMCReconstructed::RecCategory::Neutral;
      }
      const auto* track = rec->track();
      if ( track && !track->checkFlag( LHCb::Track::Flags::Clone ) ) {
        LHCb::Event::Enum::Track::Type t{track->type()};
        switch ( t ) {
        case LHCb::Event::Enum::Track::Type::Long:
          return IMCReconstructed::RecCategory::ChargedLong;
        case LHCb::Event::Enum::Track::Type::Downstream:
          return IMCReconstructed::RecCategory::ChargedDownstream;
        case LHCb::Event::Enum::Track::Type::Upstream:
          return IMCReconstructed::RecCategory::ChargedUpstream;
        case LHCb::Event::Enum::Track::Type::Unknown:
          return IMCReconstructed::RecCategory::CatUnknown;
        case LHCb::Event::Enum::Track::Type::Velo:
          return IMCReconstructed::RecCategory::ChargedVelo;
        case LHCb::Event::Enum::Track::Type::Ttrack:
          return IMCReconstructed::RecCategory::ChargedTtrack;
        case LHCb::Event::Enum::Track::Type::Muon:
          return IMCReconstructed::RecCategory::ChargedMuon;
        default: /* empty on purpose */;
        }
      }
      return IMCReconstructed::RecCategory::NotReconstructed;
    }
  };

  /** @brief Wrapper around Particle/ChargedBasic/Track to access brem corrected information
   */
  struct Bremsstrahlung : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return LHCb::Event::Bremsstrahlung::BremsstrahlungWrapper<Data>( d );
    }
  };

} // namespace Functors::Track

/*****************************************************************************\
* (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 *
 * This header only exists to capture all includes which are necessary for JIT
 * compilation via the FunctorFactory.
 *
 * Do NOT include this in any cpp files!!
 *
 * If you do edit this header, please make sure to check if `JIT_LINK_LIBS`
 * in Phys/FunctorCore/CMakeLists.txt needs to be updated too.
 */
#include <any>
// Functors
#include "Functors/Adapters.h"
#include "Functors/Combination.h"
#include "Functors/Common.h"
#include "Functors/Composite.h"
#include "Functors/Decay.h"
#include "Functors/Detector.h"
#include "Functors/Example.h"
#include "Functors/Filter.h"
#include "Functors/Function.h"
#include "Functors/Functional.h"
#include "Functors/LHCbMath.h"
#include "Functors/MVA.h"
#include "Functors/NeutralLike.h"
#include "Functors/PID.h"
#include "Functors/Particle.h"
#include "Functors/Simulation.h"
#include "Functors/TES.h"
#include "Functors/TrackLike.h"
// RecEvent
#include "Event/CaloClusters_v2.h"
#include "Event/RecVertex.h"
#include "Event/RecVertex_v2.h"
// PhysEvent
#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "Event/Vertex.h"
// DigiEvent
#include "Event/CaloDigits_v2.h"
// MCEvent
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
// TrackEvent
#include "Event/Track_v1.h"
#include "Event/Track_v2.h"
#include "Event/Track_v3.h"
// TrackKernel
#include "TrackKernel/TrackCompactVertex.h"
// SelTools
#include "SelTools/MatrixNet.h"
#include "SelTools/SigmaNet.h"
#include "SelTools/TMVA.h"
// SelKernel
#include "SelKernel/ParticleCombination.h"
#include "SelKernel/VertexRelation.h"
// PrKernel
#include "PrKernel/PrSelection.h"
// RelTables
#include "Event/TableView.h"
#include "Relations/Relation2D.h"

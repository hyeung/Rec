/***************************************************************************** \
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Functors/Function.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/System.h"
#include "Relations/RelationWeighted1D.h"
#include "SelKernel/VertexRelation.h"

namespace Functors::Common {

  /**
   * @brief Functor to invoke call operator of input
   *
   * This functor is meant to e.g. access individual matrix values. Thus, we
   * only support integer values for now to not make this functor too complex.
   */
  template <int... indices>
  struct Call final : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return std::invoke( Sel::Utils::deref_if_ptr( d ), indices... );
    }
  };

  namespace details {
    template <typename T>
    decltype( auto ) wrap_if_ref( T&& in ) {
      if constexpr ( std::is_lvalue_reference_v<T> ) {
        return std::ref( std::forward<T>( in ) );
      } else {
        return std::forward<T>( in );
      }
    }
  } // namespace details
  /**
   * @brief Functor to forward input
   *
   * Used in composition, to forward the top_level input to underlying functor
   *
   * take care of lifetime issues. We want to forward if it is a reference but copy if it is a rvalue.
   * That's because forwarding a rvalue would likely yield to dangling references.
   * See e.g. the add_fwd functor in TestFunctors.cpp. The result of the PlusN{5} functor is forwarded to AddInputs,
   * but the forwarded result would go out of scope before AddInputs would be called.
   *
   * To achieve the wanted semantics we use reference wrapper and tuple. Note
   * that a tuple automatically decays the reference wrapper ->
   * make_typle(reference_wrapper<T0> ref, T1 val) -> tuple<T0&, T1>(ref, val).
   *
   * That's super useful because the downstream functor codebase already has
   * many type checks that aren't written to handle e.g.
   * reference_wrapper<LHCb::Particle>
   */
  template <int I = -1>
  struct ForwardArgs;

  template <>
  struct ForwardArgs<> final : public Function {
    template <typename... Ts>
    auto operator()( Ts&&... d ) const {
      return std::make_tuple( details::wrap_if_ref( std::forward<Ts>( d ) )... );
    }
  };

  // even in the single argument case we use make tuple to decay the reference
  // wrapper a different option would be to have 2 overloads, (T0&&, ...)
  // returing by value and (T0&, ...) returning T&
  template <>
  struct ForwardArgs<0> final : public Function {
    template <typename T0, typename... Ts>
    auto operator()( T0&& d, Ts... ) const {
      return std::make_tuple( details::wrap_if_ref( std::forward<T0>( d ) ) );
    }
  };

  template <>
  struct ForwardArgs<1> final : Function {
    template <typename T0, typename T1, typename... Ts>
    auto operator()( T0, T1&& d, Ts... ) const {
      return std::make_tuple( details::wrap_if_ref( std::forward<T1>( d ) ) );
    }
  };

  template <>
  struct ForwardArgs<2> final : Function {
    template <typename T0, typename T1, typename T2, typename... Ts>
    auto operator()( T0, T1, T2&& d, Ts... ) const {
      return std::make_tuple( details::wrap_if_ref( std::forward<T2>( d ) ) );
    }
  };

  /**
   * @brief Functor to return address of object
   */
  struct AddressOf final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return std::addressof( d );
    }
  };

  /**
   * @brief Functor to trafo vecs into linalg vectors
   * FIXME make it such that we don't need this hack
   */
  struct ToLinAlg final : Function {
    template <typename Data>
    // warning we can't perfectly forward lvalues here.
    // using decltype(auto) would lead to dangling references
    Data operator()( Data&& d ) const {
      return std::forward<Data>( d );
    }

    template <typename A, typename B>
    auto operator()( ROOT::Math::PositionVector3D<A, B> d ) const {
      return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{d.x(), d.y(), d.z()};
    }
  };

  /**
   * @brief Functor to return X(input).
   */
  struct X_Coordinate final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return X( Sel::Utils::deref_if_ptr( d ) );
    }
  };
  /**
   * @brief Functor to return Y(input).
   */
  struct Y_Coordinate final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Y( Sel::Utils::deref_if_ptr( d ) );
    }
  };

  /**
   * @brief Functor to return Z(input).
   */
  struct Z_Coordinate final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Z( Sel::Utils::deref_if_ptr( d ) );
    }
  };

  /**
   * @brief Functor to return E(input).
   */
  struct E_Coordinate final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return E( Sel::Utils::deref_if_ptr( d ) );
    }
  };

  /**
   * @brief Functor to return input.phi().
   */
  struct Phi_Coordinate final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).phi();
    }
  };

  /**
   * @brief Functor to return input.eta().
   */
  struct Eta_Coordinate final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).eta();
    }
  };

  /**
   * @brief Functor to return input.rho().
   */
  struct Rho_Coordinate final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).rho();
    }
  };

  /**
   * @brief Functor to return input.mag().
   */
  struct Magnitude final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).mag();
    }
  };

  /**
   * @brief Functor to return input/input.mag().
   */
  struct UnitVector final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      auto const& vec = Sel::Utils::deref_if_ptr( d );
      return vec / vec.mag();
    }
  };

  /**
   * @brief Functor to return dot product of 2 inputs
   *
   * assumes dot(input1, input2) is defined
   */
  struct Dot final : Function {
    template <typename Data>
    auto operator()( Data const& d1, Data const& d2 ) const {
      return dot( Sel::Utils::deref_if_ptr( d1 ), Sel::Utils::deref_if_ptr( d2 ) );
    }
  };

  /**
   * @brief Functor to return normalized dot product of 2 inputs
   */
  struct NormedDot final : Function {
    template <typename Data>
    auto operator()( Data const& d1, Data const& d2 ) const {
      using std::sqrt;
      return dot( Sel::Utils::deref_if_ptr( d1 ), Sel::Utils::deref_if_ptr( d2 ) ) / sqrt( d1.mag2() * d2.mag2() );
    }

    //  FIXME another hack because TrackCompactVertex 3-mom returns vec3...
    template <typename A, typename B>
    auto operator()( LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> const& d1,
                     ROOT::Math::DisplacementVector3D<A, B> const&             r2 ) const {
      using std::sqrt;
      decltype( d1 ) d2{r2.x(), r2.y(), r2.z()};
      return dot( Sel::Utils::deref_if_ptr( d1 ), Sel::Utils::deref_if_ptr( d2 ) ) / sqrt( d1.mag2() * d2.mag2() );
    }
  };

  /**
   * @brief Functor for adjusting the angle if greater than PI
   */
  struct AdjustAngle final : Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using std::abs, std::fmod, std::copysign;
      auto angle = LHCb::Utils::as_arithmetic( d );
      while ( angle > 2 * M_PI ) angle -= 2 * M_PI;
      while ( angle < -2 * M_PI ) angle += 2 * M_PI;
      return abs( angle ) > M_PI ? fmod( angle, M_PI ) - copysign( M_PI, angle ) : angle;
    }
  };

  /**
   * @brief Functor to return best pv.
   */
  struct BestPV final : Function {
    // decltype(auto) because PV could be heavy and we only want to forward the
    // ref, right?
    template <typename VContainer, typename TrackLike>
    decltype( auto ) operator()( VContainer const& vertices, TrackLike const& tr ) const {
      const auto& particle = Sel::Utils::deref_if_ptr( tr );
      return Sel::getBestPV( particle, vertices );
    }

    template <typename Particle>
    decltype( auto ) operator()( Particle const& p ) const {
      return Sel::Utils::deref_if_ptr( p ).pv().target();
    }
  };

  /**
   * @brief Functor to return pv associated to object.
   */
  struct OwnPV final : Function {
    static constexpr auto name() { return "OwnPV"; }
    // decltype(auto) because PV could be heavy and we only want to forward the
    // ref, right?
    template <typename Particle>
    decltype( auto ) operator()( Particle const& p ) const {
      return Sel::Utils::deref_if_ptr( p ).pv().target();
    }
  };

  /**
   * @brief Functor to return endvertex.
   */
  struct EndVertex final : Function {
    template <typename HasEndVertex>
    decltype( auto ) operator()( HasEndVertex* p ) const {
      return ( *this )( *p );
    }

    template <typename HasEndVertex>
    auto operator()( HasEndVertex const& p ) const -> decltype( p.endVertex() ) {
      return p.endVertex();
    }

    LHCb::MCVertex const* operator()( LHCb::MCParticle const& p ) const { return p.goodEndVertex(); }
  };

  /**
   * @brief Functor to return endvertex.
   */
  struct Position final : Function {

    template <typename HasAPosition>
    auto operator()( HasAPosition const& p ) const {
      return Sel::Utils::deref_if_ptr( p ).position();
    }
    // // hack around inconsistent interfaces, e.g endVertex returning a 3d point or
    // // vertex object and calling position on a 3d point doesn't work...
    template <typename T>
    auto operator()( LHCb::LinAlg::Vec<T, 3> const& p ) const {
      return p;
    }
    template <typename C, typename T>
    auto operator()( ROOT::Math::PositionVector3D<C, T> const& p ) const {
      return p;
    }

    auto operator()( LHCb::MCVertex const* p ) const {
      if ( p != nullptr ) { return p->position(); }
      constexpr auto nan = std::numeric_limits<float>::quiet_NaN();
      return Gaudi::XYZPoint( nan, nan, nan );
    }
    template <typename T, std::size_t N>
    auto operator()( std::array<T const*, N> const& items ) const {
      using float_v = typename SIMDWrapper::type_map<LHCb::Event::details::instructionSet_for_<N>>::type::float_v;
      return std::apply(
          [this]( const auto&... i ) {
            constexpr auto nan       = std::numeric_limits<float>::quiet_NaN();
            const auto     invalid   = LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{nan, nan, nan};
            auto const     into_vec3 = []( auto const& pos ) {
              return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{pos.x(), pos.y(), pos.z()};
            };
            using LHCb::LinAlg::gather;
            return gather<float_v>( std::array{( i ? into_vec3( ( *this )( *i ) ) : invalid )...} );
          },
          items );
    }
  };

  struct ImpactParameter : public Function {
    static constexpr auto name() { return "ImpactParameter"; }

    template <typename Position_t, typename TrackChunk>
    auto operator()( Position_t const& vertex_pos, TrackChunk const& track_chunk ) const {
      using Sel::Utils::impactParameterSquared;
      return sqrt( impactParameterSquared( vertex_pos, track_chunk ) );
    }
  };

  struct ImpactParameterChi2 : public Function {
    static constexpr auto name() { return "ImpactParameterChi2"; }

    template <typename VertexType, typename TrackChunk>
    auto operator()( VertexType const& vertex, TrackChunk const& track_chunk ) const {
      using Sel::Utils::impactParameterChi2;
      return impactParameterChi2( track_chunk, vertex );
    }
  };

  struct ImpactParameterChi2ToVertex : public Function {
    static constexpr auto name() { return "ImpactParameterChi2ToVertex"; }

    template <typename VContainer, typename Particle>
    auto operator()( VContainer const& vertices, Particle const& particlep ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.
      using Sel::Utils::impactParameterChi2;
      const auto& particle = Sel::Utils::deref_if_ptr( particlep );
      return impactParameterChi2( particle, Sel::getBestPV( particle, vertices ) );
    }

    template <typename Particle>
    auto operator()( Particle const& particlep ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.
      using Sel::Utils::impactParameterChi2;
      const auto& particle = Sel::Utils::deref_if_ptr( particlep );
      return particle.pv() ? impactParameterChi2( particle, particle.pv().target() ) : -1;
    }
  };

  struct ImpactParameterChi2ToOwnPV : public Function {
    static constexpr auto name() { return "ImpactParameterChi2ToOwnPV"; }
    template <typename Particle>
    auto operator()( Particle const& particlep ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.
      using Sel::Utils::impactParameterChi2;
      const auto& particle = Sel::Utils::deref_if_ptr( particlep );
      return particle.pv() ? impactParameterChi2( particle, *particle.pv() ) : -1;
    }
  };

  template <typename... TESDepTypes>
  struct TES final : public Function {

    // grammar.py turns list into vector so we just accept this for now
    // constructor efficiency isn't that important to us anyhow
    TES( std::vector<std::string> tes_locs ) {
      if ( tes_locs.size() != m_tes_locs.size() ) {
        throw GaudiException{"TES Functor constructor expects " + std::to_string( m_tes_locs.size() ) +
                                 " locations, but only got " + std::to_string( tes_locs.size() ),
                             name(), StatusCode::FAILURE};
      }
      for ( std::size_t i{0}; i < m_tes_locs.size(); ++i ) { m_tes_locs[i] = std::move( tes_locs[i] ); }
    }

    // need this as per default this copy would be deleted because a copy of a
    // DataObjectReadHandle is deleted because AnyDataWrapper has a deleted copy.
    TES( TES<TESDepTypes...> const& other ) : m_tes_locs( other.m_tes_locs ) {}

    /** Set up the DataHandles, attributing the data dependencies to the given
     *  algorithm.
     */
    void bind( TopLevelInfo& top_level ) {
      // Delegate to a helper that sets up each DataHandle in the tuple.
      bind_helper( top_level.algorithm(), std::index_sequence_for<TESDepTypes...>{} );
    }

    /**
     * Retrieve the data dependencies from the TES and bake them into the
     * "prepared" functor that we return
     */
    auto prepare() const {
      // Get a tuple of references to the data objects on the TES
      auto tuple_of_deps = prepare_helper( std::index_sequence_for<TESDepTypes...>{} );

      // Bake this into a new lambda that we return
      return [tuple_of_deps]( auto const&... ) { return tuple_of_deps; };
    }

    [[nodiscard]] std::string name() const {
      std::stringstream s;
      using GaudiUtils::operator<<;
      s << m_tes_locs;
      return "TES" + s.str();
    }

  private:
    std::array<std::string, sizeof...( TESDepTypes )>               m_tes_locs;
    std::tuple<std::optional<DataObjectReadHandle<TESDepTypes>>...> m_handles;

    /** Set up each member of m_handles by calling .emplace() with the TES
     *  location and algorithm pointer.
     */
    template <typename Algorithm, std::size_t... Is>
    void bind_helper( Algorithm* alg, std::index_sequence<Is...> /*unused*/ ) {
      static_assert( std::is_base_of_v<IDataHandleHolder, Algorithm>,
                     "You must include the full declaration of the owning algorithm type!" );

      if ( alg->msgLevel( MSG::DEBUG ) ) { alg->debug() << "Init of DataHandles of Functor: " << name() << endmsg; }
      ( init_data_handle( std::get<Is>( m_handles ).emplace( m_tes_locs[Is], alg ), alg ), ... );
    }

    /**
     * @brief  Initialize a TES DataHandle and check that the owning algorithm
     * was configured correctly and already holds our input in ExtraInputs
     *
     * For more info on the logic please see the detailed explanation of how
     * functors obtain their data dependencies in the doc of the FunctorFactory.
     *
     * @param handle This handle will be initialized
     * @param alg Algorithm/Tool which owns this functor
     */
    template <typename T, typename Algorithm>
    void init_data_handle( DataObjectReadHandle<T>& handle, Algorithm* alg ) {
      if ( alg->msgLevel( MSG::DEBUG ) ) {
        alg->debug() << "  +  " << handle.objKey()
                     << " (will call init(): " << ( alg->FSMState() == Gaudi::StateMachine::INITIALIZED ) << ")"
                     << endmsg;
      }
      if ( alg->extraInputDeps().count( handle.objKey() ) == 0 ) {
        throw GaudiException{"Usage of DataHandle[\"" + handle.objKey() +
                                 "\"] in TES Functor requires that owning algorithm " + alg->name() +
                                 " contains this TES location inside the ExtraInputs property. This is likely a "
                                 "Configuration/PyConf bug!",
                             name(), StatusCode::FAILURE};
      }

      // DataObjectReadHandle has a protected `init()` so we need to call it
      // through it's base class. This is the same thing Gaudi::Algorithm does in
      // sysInitialize(). We do it here because this DataHandle is created inside
      // start(), at which point the step of initializing the handles of an
      // algorithm has already happened.
      // !! Exception !! if we are getting this functor from the cache then we
      // are already creating it in intialize(), and we need to skip the init()
      // call as it's also done in the sysInitialize() of the algorithm and it is
      // apparently forbidden to call init() twice on a DataHandle which is
      // checked via an assert in DataObjectHandleBase->init(). So we only run
      // init() here if the algorithm is already in an INITIALIZEDD state which
      // means this construction is happening inside start()
      if ( alg->FSMState() == Gaudi::StateMachine::INITIALIZED ) { static_cast<Gaudi::DataHandle*>( &handle )->init(); }
    }

    /** Make a tuple of references to the result of dereferencing each
     *  DataHandle in m_handles.
     */
    template <std::size_t... Is>
    auto prepare_helper( std::index_sequence<Is...> /*unused*/ ) const {
      return std::tie( deref( std::get<Is>( m_handles ) )... );
    }

    /** Check an optional data handle is initialised and doesn't return
     *  nullptr, then return a reference to the actual data.
     */
    template <typename T>
    auto const& deref( std::optional<T> const& optional_handle ) const {
      if ( !optional_handle ) {
        throw GaudiException{"TES Functor called without being bound to an algorithm", name(), StatusCode::FAILURE};
      }
      auto data_ptr = optional_handle->get();
      if ( !data_ptr ) { throw GaudiException{"Functor got nullptr from its DataHandle", name(), StatusCode::FAILURE}; }
      return *data_ptr;
    }
  };
  /**
   * @brief Evaluates the abs of a quantity
   */
  struct Abs final : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using std::abs;
      return abs( d );
    }
  };

  /**
   * @brief Evaluates the sqrt of a quantity
   */
  struct Sqrt final : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using std::sqrt;
      return sqrt( d );
    }
  };

  /**
   * @brief return the set of relations associated to a relation table
   * */
  struct Relations final : public Function {
    template <typename Table, typename From>
    auto operator()( Table const& table, From const& from ) const {
      const auto& range = table.relations( &Sel::Utils::deref_if_ptr( from ) );
      return !range.empty() ? Functors::Optional{std::move( range )} : std::nullopt;
    }
  };

  /**
   * @brief return the element on the TO side of a relation
   * */
  struct To final : public Function {
    template <typename Data>
    auto operator()( Data const& relation ) const {
      return relation.to();
    }
  };

  /**
   * @brief return the element on the WEIGHT side of a relation
   * */
  struct Weight final : public Function {
    template <typename Data>
    auto operator()( Data const& relation ) const {
      return relation.weight();
    }
  };

  /**
   * @brief return true of the floating pointing numbers are close to each other
   * within some tolerance
   */
  struct RequireClose final : Predicate {
    RequireClose( float abs_th = 1e-34, float rel_th = 1e-7 ) : m_abs( abs_th ), m_rel( rel_th / 2 ) {}

    template <typename Data>
    bool operator()( Data v1_d, Data v2_d ) const {
      auto const v1 = LHCb::Utils::as_arithmetic( v1_d );
      auto const v2 = LHCb::Utils::as_arithmetic( v2_d );
      using std::abs;
      using std::max;
      return abs( v1 - v2 ) < max( m_abs, m_rel * ( abs( v1 ) + abs( v2 ) ) );
    }

  private:
    float m_abs, m_rel;
  };
} // namespace Functors::Common

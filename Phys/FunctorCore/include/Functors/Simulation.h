/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Functors/Adapters.h"
#include "Functors/Core.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/MCTaggingHelper.h"
#include "Kernel/ParticleProperty.h"
#include "MCInterfaces/IMCReconstructible.h"

#include <functional>
#include <type_traits>
#include <utility>

namespace Functors::detail {
  inline bool check_mask( int value, int mask ) { return ( value & mask ) == mask; };

} // namespace Functors::detail
namespace Functors::Simulation {

  /**
   * @brief Check against one bitmask
   */
  struct CheckMask : public Function {
    static constexpr auto name() { return "CheckMask"; }

    CheckMask( int mask ) : m_mask( mask ) {}

    int operator()( int const value ) const {
      using Functors::detail::check_mask;
      return check_mask( value, m_mask );
    }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert( detail::always_false_v<Data...>,
                     "The functor Functors::Simulation::CheckMask can only apply to int object" );
    }

  private:
    int m_mask;
  };

  /** @brief Particle ID of LHCb::Particle or LHCb::MCParticle.
   * Can be used together with F.MAP_INPUT and MC association relations table to obtain the TRUEID*/
  struct Particle_Id : public Function {
    /* "name" to improve error messages. */
    constexpr auto name() const { return "Particle_Id"; }

    template <typename Particle>
    auto operator()( Particle const& p ) const {
      using LHCb::Event::pid;
      return pid( p );
    }
  };

  /** @brief General Category class that implements the main "operator()" function
   * that simply returns the input data of "int" type, asserting that it is different
   * to the pre-defined invalid value (e.g for basic particle).
   * The background category is actually determined, with
   * algorithm "MCTruthAndBkgCatAlg" (living in "Phys" package).
   *
   * @see Functors::Simulation::Category
   */
  struct Category : public Function {
    /* "name" to improve error messages. */
    static constexpr auto name() { return "Category"; }

    auto operator()( int const i ) const { return i; }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert( detail::always_false_v<Data...>,
                     "The functor Functors::Simulation::CheckMask can only apply to int object" );
    }
  };

  namespace MC {
    /**
     * @brief Property, this functor will return the "int" type MCProperty for
     * an MC Particle, which contains the bitwise information.
     */

    struct Property : public Function {

      constexpr auto name() const { return "MC::Property"; }

      auto operator()( const LHCb::MCProperty& mc_track_info, const LHCb::MCParticle& mc_particle ) const {
        Functors::Optional<int> result{std::nullopt};
        if ( mc_particle.particleID().threeCharge() != 0 ) { result = mc_track_info.property( &mc_particle ); }
        return result;
      }

      template <typename T1, typename T2>
      auto operator()( const T1& p1, const T2& p2 ) const -> decltype( ( *this )( *p1, *p2 ) ) {
        return ( *this )( *p1, *p2 );
      }

      template <typename T1, typename T2>
      auto operator()( const T1& p1, const T2& p2 ) const -> decltype( ( *this )( p1, *p2 ) ) {
        return ( *this )( p1, *p2 );
      }

      template <typename T1, typename T2>
      auto operator()( const T1& p1, const T2& p2 ) const -> decltype( ( *this )( *p1, p2 ) ) {
        return ( *this )( *p1, p2 );
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false_v<Data...>,
            "The functor Functors::Simulation::MC::Property can only apply to (LHCb::MCProperty, LHCb::MCParticle)" );
      }
    };

    /**
     * @brief ChargeReconstructible, return the reconstructible category
     * for charge MC Particle.
     *
     * The charge reconstructible categories:
     *
     *    -1  = No MC classification possible
     *     0  = Outside detector acceptance
     *     1  = In acceptance but not reconstructible
     *     2  = Reconstructible as a Long charged track
     *     3  = Reconstructible as a Downstream charged track
     *     4  = Reconstructible as an Upstream charged track
     *     5  = Reconstructible as a T charged track
     *     6  = Reconstructible as a VELO charged track
     *
     */
    struct ChargeReconstructible : public Function {
      static constexpr auto name() { return "MC::ChargeReconstructible"; }
      auto                  operator()( int const property ) const {

        using Functors::detail::check_mask;
        auto rec = IMCReconstructible::RecCategory::NoClassification;

        if ( property ) {
          /// Acceptance
          auto inAcc = ( check_mask( property, MCTrackInfo::flagMasks::maskAccT ) ||
                         check_mask( property, MCTrackInfo::flagMasks::maskAccUT ) ||
                         check_mask( property, MCTrackInfo::flagMasks::maskAccVelo ) );

          /// Category
          if ( inAcc ) {
            if ( check_mask( property, MCTrackInfo::flagMasks::maskHasVeloAndT ) )
              rec = IMCReconstructible::RecCategory::ChargedLong;
            else if ( check_mask( property, MCTrackInfo::flagMasks::maskHasVelo ) &&
                      check_mask( property, MCTrackInfo::flagMasks::maskHasUT ) )
              rec = IMCReconstructible::RecCategory::ChargedUpstream;
            else if ( check_mask( property, MCTrackInfo::flagMasks::maskHasT ) &&
                      check_mask( property, MCTrackInfo::flagMasks::maskHasUT ) )
              rec = IMCReconstructible::RecCategory::ChargedDownstream;
            else if ( check_mask( property, MCTrackInfo::flagMasks::maskHasVelo ) )
              rec = IMCReconstructible::RecCategory::ChargedVelo;
            else if ( check_mask( property, MCTrackInfo::flagMasks::maskHasT ) )
              rec = IMCReconstructible::RecCategory::ChargedTtrack;
          } else
            rec = IMCReconstructible::RecCategory::OutsideAcceptance;
        }

        return rec;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MC::ChargeReconstructible can only apply to int object" );
      }
    };

    /**
     * @brief functor accessing the parent MCParticle
     * @note The grandparent or ancestors can be accessed by chainning more Parent functors.
     */
    struct Mother : public Function {

      constexpr auto name() const { return "MC::Mother"; }

      auto operator()( const LHCb::MCParticle& mcp ) const {
        auto mother = mcp.mother();
        return mother ? Functors::Optional{mother} : std::nullopt;
      }

      auto operator()( const LHCb::MCVertex& mcv ) const {
        auto mother = mcv.mother();
        return mother ? Functors::Optional{mother} : std::nullopt;
      }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false_v<Data...>,
            "The functor Functors::Simulation::MC::Mother can only apply to LHCb::MCParticle or LHCb::MCVertex" );
      }
    };

    /**
     * @brief functor accessing the origin vertex of a MCParticle
     * @note this functor does currently the same as Track::ReferencePoint as the referencePoint
     * of a MCParticle is its originVertex
     */
    struct OriginVertex : public Function {

      constexpr auto name() const { return "MC::OriginVertex"; }

      auto operator()( const LHCb::MCParticle& mcp ) const { return mcp.originVertex(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MC::OriginVertex can only apply to LHCb::MCParticle" );
      }
    };

    /**
     * @brief functor accessing the primary vertex of a MCParticle
     */
    struct PrimaryVertex : public Function {

      constexpr auto name() const { return "MC::PrimaryVertex"; }

      auto operator()( const LHCb::MCParticle& mcp ) const { return mcp.primaryVertex(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MC::PrimaryVertex can only apply to LHCb::MCParticle" );
      }
    };

    /**
     * @brief functor accessing the true tagging information of a MCParticle
     */
    struct OriginFlag : public Function {
      constexpr auto name() const { return "MC::OriginFlag"; }

      auto operator()( const LHCb::MCParticle* tMC, const LHCb::MCParticle* bMC ) const {
        return LHCb::FlavourTagging::originType( *bMC, *tMC );
      }
    };

    /**
     * @brief This functor return the first longlived ancestor of a MCParticle
     */
    struct FirstLongLivedAncestor : public Function {
      constexpr auto name() const { return "MC::FirstLongLivedAncestor"; }

      // 10^-15 seconds is between the lifetimes of the pi0 (considered prompt) and the tau (nonprompt).
      FirstLongLivedAncestor( const double minLifetime = 1e-7 * Gaudi::Units::ns ) : m_minLifetime( minLifetime ) {}

      void bind( TopLevelInfo& top_level ) {
        m_ppSvc.emplace( top_level.algorithm(), top_level.generate_property_name(), "LHCb::ParticlePropertySvc" );
      }

      auto operator()( const LHCb::MCParticle& mcp ) const {
        if ( !m_ppSvc ) {
          throw GaudiException{"Can not initialize the LHCb::ParticlePropertySvc.",
                               "Functors::Simulation::FirstLongLivedAncestor", StatusCode::FAILURE};
        }

        const LHCb::MCParticle* parent = mcp.mother();
        while ( parent ) {
          auto pProp = m_ppSvc.value()->find( parent->particleID() );
          if ( pProp && pProp->lifetime() > m_minLifetime ) break;
          parent = parent->mother();
        }
        return parent ? Functors::Optional{parent} : std::nullopt;
      }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false_v<Data...>,
            "The functor Functors::Simulation::MC::FirstLongLivedAncestor can only apply to LHCb::MCParticle" );
      }

    private:
      const double                                             m_minLifetime;
      std::optional<ServiceHandle<LHCb::IParticlePropertySvc>> m_ppSvc;
    };

    /**
     * @brief functor accessing the lifetime of a MCParticle
     */
    struct LifeTime : public Function {
      constexpr auto name() const { return "MC::LifeTime"; }

      auto operator()( const LHCb::MCParticle& mcp ) const {
        Functors::Optional<double> result{std::nullopt};
        const auto                 end_vertex    = mcp.goodEndVertex();
        const auto                 origin_vertex = mcp.originVertex();
        if ( !end_vertex || !origin_vertex ) return result;

        const auto trueDist = end_vertex->position() - origin_vertex->position();
        const auto trueP    = mcp.momentum();
        result = ( trueP.M() * trueDist.Dot( trueP.Vect() ) / trueP.Vect().mag2() ) / Gaudi::Units::c_light;
        return result;
      }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MC::LifeTime can only apply to LHCb::MCParticle" );
      }
    };

  } // namespace MC

  namespace MCHeader {
    /**
     * @brief get all primary vertices from the MCHeader
     */
    struct AllPVs : public Function {

      constexpr auto name() const { return "MCHeader::AllPVs"; }

      auto operator()( const LHCb::MCHeader& mch ) const { return mch.primaryVertices(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCHeader::AllPVs can only apply to LHCb::MCHeader" );
      }
    };

    /**
     * @brief get the event time of the MCHeader
     */
    struct EvtTime : public Function {

      constexpr auto name() const { return "MCHeader::EvtTime"; }

      auto operator()( const LHCb::MCHeader& mch ) const { return mch.evtTime(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCHeader::EvtTime can only apply to LHCb::MCHeader" );
      }
    };

    /**
     * @brief get the event number of the MCHeader
     */
    struct EvtNumber : public Function {

      constexpr auto name() const { return "MCHeader::EvtNumber"; }

      auto operator()( const LHCb::MCHeader& mch ) const { return mch.evtNumber(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCHeader::EvtNumber can only apply to LHCb::MCHeader" );
      }
    };

    /**
     * @brief get the run number of the MCHeader
     */
    struct RunNumber : public Function {

      constexpr auto name() const { return "MCHeader::RunNumber"; }

      auto operator()( const LHCb::MCHeader& mch ) const { return mch.runNumber(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCHeader::RunNumber can only apply to LHCb::MCHeader" );
      }
    };
  } // namespace MCHeader

  namespace MCVertex {
    using LHCb::MCVertex;

    /**
     * @brief get the time of a MCVertex.
     */
    struct Time : public Function {

      constexpr auto name() const { return "MCVertex::Time"; }

      auto operator()( const LHCb::MCVertex& mcv ) const { return mcv.time(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCVertex::Time can only apply to LHCb::MCVertex" );
      }
    };

    /**
     * @brief get the type of a MCVertex.
     */
    struct Type : public Function {

      constexpr auto name() const { return "MCVertex::Type"; }

      auto operator()( const LHCb::MCVertex& mcv ) const { return mcv.type(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCVertex::Type can only apply to LHCb::MCVertex" );
      }
    };

    /**
     * @brief returns true if the MCVertex is a primary vertex.
     */
    struct IsPrimary : public Predicate {

      constexpr auto name() const { return "MCVertex::IsPrimary"; }

      auto operator()( const LHCb::MCVertex& mcv ) const { return mcv.isPrimary(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCVertex::IsPrimary can only apply to LHCb::MCVertex" );
      }
    };

    /**
     * @brief returns true if the MCVertex is a decay vertex.
     */
    struct IsDecay : public Predicate {

      constexpr auto name() const { return "MCVertex::IsDecay"; }

      auto operator()( const LHCb::MCVertex& mcv ) const { return mcv.isDecay(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCVertex::IsDecay can only apply to LHCb::MCVertex" );
      }
    };

    /**
     * @brief functor accessing the decay products of a MCVertex
     */
    struct Products : public Function {

      constexpr auto name() const { return "MCVertex::Products"; }

      auto operator()( const LHCb::MCVertex& mcv ) const { return mcv.products(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false_v<Data...>,
                       "The functor Functors::Simulation::MCVertex::Products can only apply to LHCb::MCVertex" );
      }
    };

  } // namespace MCVertex

} // namespace Functors::Simulation

/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/Calo/CaloCellID.h"
#include "Event/ProtoParticle.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "Kernel/RichParticleIDType.h"

namespace Functors::PID {

  // RICH
  namespace details {
    template <Rich::ParticleIDType pidtype, bool scaled = false>
    struct RichDLL : public Function {
      auto operator()( LHCb::Particle const& p ) const {
        auto const* pid = p.proto() ? p.proto()->richPID() : nullptr;
        if constexpr ( scaled ) {
          return pid ? Functors::Optional{pid->scaledDLLForCombDLL( pidtype )} : std::nullopt;
        } else {
          return pid ? Functors::Optional{pid->particleDeltaLL( pidtype )} : std::nullopt;
        }
      }

      auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };

    template <auto fn, auto... args>
    struct RichPIDPredicate : Predicate {
      bool operator()( LHCb::Particle const& p ) const {
        auto const* pid = p.proto() ? p.proto()->richPID() : nullptr;
        return pid ? std::invoke( fn, *pid, args... ) : false;
      }
      bool operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };
  } // namespace details

  using RichDLLe  = details::RichDLL<Rich::ParticleIDType::Electron>;
  using RichDLLmu = details::RichDLL<Rich::ParticleIDType::Muon>;
  using RichDLLp  = details::RichDLL<Rich::ParticleIDType::Proton>;
  using RichDLLk  = details::RichDLL<Rich::ParticleIDType::Kaon>;
  using RichDLLpi = details::RichDLL<Rich::ParticleIDType::Pion>;
  using RichDLLd  = details::RichDLL<Rich::ParticleIDType::Deuteron>;
  using RichDLLbt = details::RichDLL<Rich::ParticleIDType::BelowThreshold>;

  using RichScaledDLLe  = details::RichDLL<Rich::ParticleIDType::Electron, true>;
  using RichScaledDLLmu = details::RichDLL<Rich::ParticleIDType::Muon, true>;

  using Rich1GasUsed = details::RichPIDPredicate<&LHCb::RichPID::usedRich1Gas>;
  using Rich2GasUsed = details::RichPIDPredicate<&LHCb::RichPID::usedRich2Gas>;

  using RichThresholdEl = details::RichPIDPredicate<&LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Electron>;
  using RichThresholdKa = details::RichPIDPredicate<&LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Kaon>;
  using RichThresholdMu = details::RichPIDPredicate<&LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Muon>;
  using RichThresholdPi = details::RichPIDPredicate<&LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Pion>;
  using RichThresholdPr = details::RichPIDPredicate<&LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Proton>;
  using RichThresholdDe = details::RichPIDPredicate<&LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Deuteron>;

  // MUON
  //

  /** @brief IsMuon, as defined by the accessor of the same name.
   */
  struct IsMuon : public Predicate {
    bool operator()( LHCb::ProtoParticle const& pp ) const {
      return Functors::and_then( pp.muonPID(), *this ).value_or( false );
    }

    bool operator()( LHCb::Particle const& p ) const {
      return Functors::and_then( p.proto(), *this ).value_or( false );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).IsMuon() ) {
      return Sel::Utils::deref_if_ptr( d ).IsMuon();
    }
  };

  /** @brief InMuon to access the Muon Inacceptance information.
   */
  struct InAcceptance : public Predicate {
    auto operator()( LHCb::Particle const& p ) const {
      return Functors::and_then( p.proto(), &LHCb::ProtoParticle::muonPID, &LHCb::MuonPID::InAcceptance )
          .value_or( false );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    // template <typename Data>
    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( d.InAcceptance() ) {
      return d.InAcceptance();
    }
  };

  /** @brief IsMuonTight, as defined by the accessor of the same name.
   */
  struct IsMuonTight : public Predicate {
    bool operator()( LHCb::ProtoParticle const& pp ) const {
      return Functors::and_then( pp.muonPID(), &LHCb::MuonPID::IsMuonTight ).value_or( false );
    }

    bool operator()( LHCb::Particle const& p ) const {
      return Functors::and_then( p.proto(), *this ).value_or( false );
    }

    auto operator()( LHCb::Particle const* p ) const { return Functors::and_then( p, *this ).value_or( false ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).IsMuonTight() ) {
      return Sel::Utils::deref_if_ptr( d ).IsMuonTight();
    }
  };

  /** @brief Chi2Corr, as defined by the accessor of the same name. Chi2 taking into account correlations using only
   * hits from the Muon detector.
   */
  struct MuonChi2Corr : public Function {
    auto operator()( LHCb::Particle const& p ) const {
      return Functors::and_then( p.proto(), &LHCb::ProtoParticle::muonPID, &LHCb::MuonPID::chi2Corr );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).Chi2Corr() ) {
      return Sel::Utils::deref_if_ptr( d ).Chi2Corr();
    }
  };

  /** @brief MuonLLMu, as defined by the accessor LLMu. Muon likelihood using only Muon detector information
   */
  struct MuonLLMu : public Function {
    auto operator()( LHCb::Particle const& p ) const {
      return Functors::and_then( p.proto(), &LHCb::ProtoParticle::muonPID, &LHCb::MuonPID::MuonLLMu );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).LLMu() ) {
      return Sel::Utils::deref_if_ptr( d ).LLMu();
    }
  };

  /** @brief MuonLLBg, as defined by the accessor LLBg. Background likelihood using only Muon detector information
   */
  struct MuonLLBg : public Function {
    auto operator()( LHCb::Particle const& p ) const {
      return Functors::and_then( p.proto(), &LHCb::ProtoParticle::muonPID, &LHCb::MuonPID::MuonLLBg );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).LLBg() ) {
      return Sel::Utils::deref_if_ptr( d ).LLBg();
    }
  };

  /** @brief MuonCatBoost, as defined by the accessor CatBoost. MVA output using only Muon detector and track
   * information
   */
  struct MuonCatBoost : public Function {
    auto operator()( LHCb::Particle const& p ) const {
      return Functors::and_then( p.proto(), &LHCb::ProtoParticle::muonPID, &LHCb::MuonPID::muonMVA2 );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).CatBoost() ) {
      return Sel::Utils::deref_if_ptr( d ).CatBoost();
    }
  };

  // CALO
  //
  using namespace LHCb::Event::Calo::v1;

  namespace details {

    template <auto obj, auto fn, auto... args>
    struct CaloPredicate : Predicate {
      bool operator()( LHCb::ProtoParticle const& p ) const {
        auto const* pid = std::invoke( obj, p );
        return pid ? std::invoke( fn, *pid, args... ) : false;
      }
      auto operator()( LHCb::ProtoParticle const* p ) const { return p ? ( *this )( *p ) : false; }
      auto operator()( LHCb::Particle const& p ) const { return ( *this )( p.proto() ); }
      auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };

    template <auto obj, auto acc, auto fn, auto... args>
    struct CaloFunction : Function {
      auto operator()( LHCb::ProtoParticle const& p ) const {
        auto const* pid = std::invoke( obj, p );
        return pid && std::invoke( acc, *pid ) ? Functors::Optional{std::invoke( fn, *pid, args... )} : std::nullopt;
      }
      auto operator()( LHCb::ProtoParticle const* p ) const { return p ? ( *this )( *p ) : std::nullopt; }
      auto operator()( LHCb::Particle const& p ) const { return ( *this )( p.proto() ); }
      auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };

    template <auto obj, auto fn, auto... args>
    struct CellIDFunc : Function {
      LHCb::Detector::Calo::CellID operator()( LHCb::ProtoParticle const& p ) const {
        auto const* pid = std::invoke( obj, p );
        return pid ? std::invoke( fn, *pid, args... ) : LHCb::Detector::Calo::CellID{};
      }
      auto operator()( LHCb::ProtoParticle const* p ) const {
        return p ? ( *this )( *p ) : LHCb::Detector::Calo::CellID{};
      }
      auto operator()( LHCb::Particle const& p ) const { return ( *this )( p.proto() ); }
      auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };

    template <auto fn, auto... args>
    using EcalFunc = CaloFunction<&LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal, fn, args...>;

    template <auto fn, auto... args>
    using HcalFunc = CaloFunction<&LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InHcal, fn, args...>;

    template <auto fn, auto... args>
    using BremFunc = CaloFunction<&LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem, fn, args...>;

  } // namespace details

  using InEcal = details::CaloPredicate<&LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal>;
  using InHcal = details::CaloPredicate<&LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InHcal>;

  using InBrem  = details::CaloPredicate<&LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem>;
  using HasBrem = details::CaloPredicate<&LHCb::ProtoParticle::bremInfo, &BremInfo::HasBrem>;

  using EcalPIDe          = details::EcalFunc<&CaloChargedPID::EcalPIDe>;
  using EcalPIDmu         = details::EcalFunc<&CaloChargedPID::EcalPIDmu>;
  using ElectronShowerEoP = details::EcalFunc<&CaloChargedPID::ElectronShowerEoP>;
  using ElectronShowerDLL = details::EcalFunc<&CaloChargedPID::ElectronShowerDLL>;
  using ElectronMatch     = details::EcalFunc<&CaloChargedPID::ElectronMatch>;
  using ElectronEnergy    = details::EcalFunc<&CaloChargedPID::ElectronEnergy>;
  using ElectronID        = details::CellIDFunc<&LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::ElectronID>;
  using ClusterID         = details::CellIDFunc<&LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::ClusterID>;

  using HcalPIDe  = details::HcalFunc<&CaloChargedPID::HcalPIDe>;
  using HcalPIDmu = details::HcalFunc<&CaloChargedPID::HcalPIDmu>;
  using HcalEoP   = details::HcalFunc<&CaloChargedPID::HcalEoP>;

  using BremEnergy           = details::BremFunc<&BremInfo::BremEnergy>;
  using BremPIDe             = details::BremFunc<&BremInfo::BremPIDe>;
  using BremBendCorr         = details::BremFunc<&BremInfo::BremBendingCorrection>;
  using BremHypoMatch        = details::BremFunc<&BremInfo::BremHypoMatch>;
  using BremHypoEnergy       = details::BremFunc<&BremInfo::BremHypoEnergy>;
  using BremHypoDeltaX       = details::BremFunc<&BremInfo::BremHypoDeltaX>;
  using BremTrackBasedEnergy = details::BremFunc<&BremInfo::BremTrackBasedEnergy>;
  using BremHypoID           = details::CellIDFunc<&LHCb::ProtoParticle::bremInfo, &BremInfo::BremHypoID>;

  struct ClusterMatch : public Function {
    Functors::Optional<float> operator()( LHCb::ProtoParticle const& p ) const {
      if ( p.track() ) {
        auto pid = p.caloChargedPID();
        return pid ? Functors::Optional{pid->ClusterMatch()} : invalid_value;
      } else {
        auto pid = p.neutralPID();
        return pid ? Functors::Optional{pid->CaloTrMatch()} : std::nullopt;
      }
    }
    auto operator()( LHCb::ProtoParticle const* p ) const { return p ? ( *this )( *p ) : std::nullopt; }
    auto operator()( LHCb::Particle const& p ) const { return ( *this )( p.proto() ); }
    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
  };

  namespace CaloCellID {
    struct All : Function {
      auto operator()( LHCb::Detector::Calo::CellID id ) const { return id.all(); }
    };

    struct Area : Function {
      auto operator()( LHCb::Detector::Calo::CellID id ) const { return id.area(); }
    };

    struct Row : Function {
      auto operator()( LHCb::Detector::Calo::CellID id ) const { return id.row(); }
    };

    struct Column : Function {
      auto operator()( LHCb::Detector::Calo::CellID id ) const { return id.col(); }
    };

  } // namespace CaloCellID

} // namespace Functors::PID

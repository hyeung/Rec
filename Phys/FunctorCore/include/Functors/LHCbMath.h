/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Functors/Adapters.h"
#include "Functors/Core.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbMath/ParticleParams.h"
#include "LHCbMath/ValueWithError.h"

#include <functional>
#include <type_traits>
#include <utility>

/*
 * Functors that access information of Gaudi::Math
 */
namespace Functors::LHCbMath {

  /**
   * @brief scalarMomentum, access the scalar momentum by input.scalarMomentum()
   */
  struct scalarMomentum : public Function {

    constexpr auto name() const { return "scalarMomentum"; }

    template <typename T>
    auto operator()( const T& input ) const -> decltype( input.scalarMomentum() ) {
      return input.scalarMomentum();
    }

    template <typename T>
    auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
      return p == nullptr ? Functors::Optional{( *this )( *p )} : std::nullopt;
    }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert( detail::always_false<Data...>::value,
                     "The fucntor Functors::LHCbMath::scalarMomentum can only apply to object that has member function "
                     "scalarMomentum()" );
    }
  };

  /**
   * @brief mass, access the scalar momentum by input.invariantMass()
   */
  struct invariantMass : public Function {

    constexpr auto name() const { return "invariantMass"; }

    template <typename T>
    auto operator()( const T& input ) const -> decltype( input.invariantMass() ) {
      return input.invariantMass();
    }

    template <typename T>
    auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
      return p == nullptr ? Functors::Optional{( *this )( *p )} : std::nullopt;
    }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert( detail::always_false<Data...>::value, "The fucntor Functors::LHCbMath::invariantMass can only "
                                                           "apply to object that has member function invariantMass()" );
    }
  };

  namespace ValueWithError {
    /**
     * @brief Value, access the value of Gaudi::Math::ValueWithError
     */
    struct Value : public Function {

      constexpr auto name() const { return "ValueWithError::Value"; }

      auto operator()( const Gaudi::Math::ValueWithError& input ) const { return input.value(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p == nullptr ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ValueWithError::Value can only apply to Gaudi::Math::ValueWithError" );
      }
    };

    /**
     * @brief Error, access the value of Gaudi::Math::ValueWithError
     */
    struct Error : public Function {

      constexpr auto name() const { return "ValueWithError::Error"; }

      auto operator()( const Gaudi::Math::ValueWithError& input ) const { return input.error(); }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ValueWithError::Error can only apply to Gaudi::Math::ValueWithError" );
      }
    };

  } // namespace ValueWithError

  namespace ParticleParams {
    /**
     * @brief flightDistance, access the decay length of Gaudi::Math::ParticleParams
     */
    struct flightDistance : public Function {

      constexpr auto name() const { return "ParticleParams::flightDistance"; }

      auto operator()( const Gaudi::Math::ParticleParams& input ) const { return input.flightDistance(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p == nullptr ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false<Data...>::value,
                       "The fucntor Functors::LHCbMath::ParticleParams::flightDistance can only apply to "
                       "Gaudi::Math::ParticleParams" );
      }
    };

    /**
     * @brief ctau, access the c*tau of Gaudi::Math::ParticleParams
     */
    struct ctau : public Function {

      constexpr auto name() const { return "ParticleParams::ctau"; }

      auto operator()( const Gaudi::Math::ParticleParams& input ) const { return input.ctau(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p == nullptr ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ParticleParams::ctau can only apply to Gaudi::Math::ParticleParams" );
      }
    };

    /**
     * @brief lenPosCov, access the "Matrix" with correlation errors between position and decay length from
     * Gaudi::Math::ParticleParams
     */
    struct lenPosCov : public Function {

      constexpr auto name() const { return "ParticleParams::lenPosCov"; }

      auto operator()( const Gaudi::Math::ParticleParams& input ) const { return input.lenPosCov(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p == nullptr ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ParticleParams::lenPosCov can only apply to Gaudi::Math::ParticleParams" );
      }
    };

    /**
     * @brief lenMomCov, access the "Matrix" with correlation errors between momentum and decay length from
     * Gaudi::Math::ParticleParams
     */
    struct lenMomCov : public Function {

      constexpr auto name() const { return "ParticleParams::lenMomCov"; }

      auto operator()( const Gaudi::Math::ParticleParams& input ) const { return input.lenMomCov(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p == nullptr ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ParticleParams::lenMomCov can only apply to Gaudi::Math::ParticleParams" );
      }
    };
  } // namespace ParticleParams

} // namespace Functors::LHCbMath

/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "SelTools/MVAUtils.h"

#include <string>

namespace Functors {
  namespace detail {
    inline bool testbit( bool b, [[maybe_unused]] int i ) {
      assert( i == 0 );
      return b;
    }

    template <typename MVAImpl, typename... Inputs>
    struct MVA final : public Function {
      MVA( Sel::MVA_config_dict const& config, std::pair<std::string, Inputs>... inputs )
          : m_impl{config}
          , m_inputs{std::move( std::get<1>( inputs ) )...}
          , m_input_names{std::move( std::get<0>( inputs ) )...} {
        // Check for duplicate entries in m_input_names
        auto input_names = m_input_names;
        std::sort( input_names.begin(), input_names.end() );
        if ( std::adjacent_find( input_names.begin(), input_names.end() ) != input_names.end() ) {
          throw GaudiException{"MVA functor given multiple definitions of an input variable.", "Functors::MVA",
                               StatusCode::FAILURE};
        }
      }

      void bind( TopLevelInfo& top_level ) {
        // Forward the call to the functors we own in m_inputs
        detail::bind( m_inputs, top_level );

        // Set up the classifier
        m_impl.bind( top_level.algorithm() );

        // Ask the classifier what variables it's expecting and in what order
        auto const& variables = m_impl.variables();

        // Check the m_input_names matches the variables that the classifier is
        // expecting, and fill a list of indices that we can use when actually
        // evaluating to put the values we get from m_inputs into the right
        // order. Choose the convention that m_indices[i] is the position in
        // the classifier inputs that we should put the result of evaluating
        // std::get<i>( m_inputs ).
        if ( variables.size() != m_input_names.size() ) {
          throw GaudiException{"MVA functor given " + std::to_string( m_input_names.size() ) +
                                   " functors, but the given classifier expects " + std::to_string( variables.size() ),
                               "Functors::MVA", StatusCode::FAILURE};
        }
        for ( auto fun_index = 0ul; fun_index < m_input_names.size(); ++fun_index ) {
          auto const& input_name = m_input_names[fun_index];
          // What's the index of input_name in variables?
          auto iter = std::find( variables.begin(), variables.end(), input_name );
          if ( iter == variables.end() ) {
            throw GaudiException{"MVA functor knows how to calculate " + input_name +
                                     ", but the given classifier does not expect that",
                                 "Functors::MVA", StatusCode::FAILURE};
          }
          m_indices[fun_index] = std::distance( variables.begin(), iter );
        }
      }

      /** Prepare the functor for use. */
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
        return prepare( evtCtx, top_level, std::index_sequence_for<Inputs...>{} );
      }

      /** Flag that this functor would like to be invoked with an extra zeroth
       *  parameter that indicates which elements of the other arguments are
       *  valid.
       */
      static constexpr bool requires_explicit_mask = true;

    private:
      template <std::size_t... I>
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level, std::index_sequence<I...> ) const {
        // Prepare all of the functors and capture the resulting tuple
        return [this, fs = std::tuple{detail::prepare( std::get<I>( m_inputs ), evtCtx, top_level )...}](
                   auto const& mask, auto const&... input ) {
          // Let the implementation specify if inputs should be float, double
          // etc. For now, until we have SIMD-friendly MVA implementations,
          // this will just be a scalar numeric type.
          using MVA_input_t = typename MVAImpl::input_type;
          static_assert( std::is_arithmetic_v<MVA_input_t> );
          // Figure out what the functors are going to yield...
          // using functor_ret_ts = std::tuple<decltype( std::get<I>( fs )( input... ) )...>;
          // Don't strictly demand the types are the same, we could stomach a mix of float/double/...
          using functor_ret_t =
              std::common_type_t<decltype( std::invoke( std::get<I>( fs ), mask_arg, mask, input... ) )...>;
          // If this is a plain arithmetic type, this should be easy...
          if constexpr ( std::is_arithmetic_v<functor_ret_t> ) {
            // MVA expects float/double/.., functors yield that, just calculate
            // the inputs and evaluate it
            std::array<MVA_input_t, sizeof...( Inputs )> values{};
            // Want to put the result of evaluating std::get<I>( m_inputs ) into values[m_indices[I]]
            ( ( values[m_indices[I]] = std::invoke( std::get<I>( fs ), mask_arg, mask, input... ) ), ... );
            // Evaluate the classifier
            return m_impl( values );
          } else {
            // Presumably this is because functor_ret_t is a SIMDWrapper type
            // (though maybe we should check that explicitly)
            std::array<std::array<typename LHCb::type_map<functor_ret_t>::scalar_t, functor_ret_t::size()>,
                       sizeof...( Inputs )>
                values{};
            // Evaluate the functors SIMD-wise
            ( static_cast<functor_ret_t>( std::invoke( std::get<I>( fs ), mask_arg, mask, input... ) )
                  .store( values[m_indices[I]] ),
              ... );
            // Evaluate the classifier element-wise
            // TODO get MVA implementations that accept vector types
            std::array<float, functor_ret_t::size()> mva_vals{};
            for ( auto i = 0ul; i < functor_ret_t::size(); ++i ) {
              // Avoid evaluating the MVA for entries that are already masked out
              using detail::testbit;
              if ( !testbit( mask, i ) ) {
                mva_vals[i] = std::numeric_limits<float>::lowest();
                continue;
              }
              std::array<MVA_input_t, sizeof...( Inputs )> values2{};
              for ( auto j = 0ul; j < values2.size(); ++j ) { values2[j] = values[j][i]; }
              mva_vals[i] = m_impl( values2 );
            }
            return functor_ret_t{mva_vals.data()};
          }
        };
      }

      MVAImpl                                      m_impl;
      std::tuple<Inputs...>                        m_inputs;
      std::array<std::size_t, sizeof...( Inputs )> m_indices;
      std::array<std::string, sizeof...( Inputs )> m_input_names;
    };
  } // namespace detail

  /** Helper for specifying {name, functor} pairs. */
  template <typename F>
  auto MVAInput( std::string s, F f ) {
    return std::pair{std::move( s ), std::move( f )};
  }

  /** Helper for producing a detail::MVA instance without explicitly spelling
   *  out the functor types or using decltype. It should be transparent that
   *  this is actually a function, while in most cases functor objects are
   *  instantiated directly.
   */
  template <typename MVAImpl, typename... Inputs>
  auto MVA( Sel::MVA_config_dict const& config, std::pair<std::string, Inputs>&&... inputs ) {
    return detail::MVA<MVAImpl, Inputs...>{config, std::forward<std::pair<std::string, Inputs>>( inputs )...};
  }
} // namespace Functors

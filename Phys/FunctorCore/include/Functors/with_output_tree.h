/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/with_functor_maps.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/SIMDWrapper.h"

#include <TFile.h>
#include <TTree.h>

#include <any>
/** @file  with_output_tree.h
 *  @brief with_output_tree mixin saves functor return values to a ROOT TTree
 */

namespace Functors::detail::with_output_tree {
  /** @class  variadic_numeric_value_wrapper
   *  @brief  std::variant wrapper that can be initialised from std::type_info
   *  @tparam RTypes... Parameter pack of valid types for the contained variant
   *
   *  Thin wrapper around a std::variant that makes sure it is initialised
   *  according to the given std::type_info instance, or throws
   *  std::runtime_error if that is not contained among the parameter pack
   *  RTypes...
   */
  template <typename... RTypes>
  struct variadic_numeric_value_wrapper {
    /** @typedef numeric_value
     *  @brief   Wrapped std::variant type
     */
    using numeric_value = std::variant<RTypes...>;

    /** Value-initialise the wrapped std::variant with a value of the type
     *  represented by the given std::type_info.
     */
    variadic_numeric_value_wrapper( std::type_info const& type ) {
      // If one of the types matches, we're done
      if ( ( help<RTypes>( type ) || ... ) ) return;
      // Otherwise, throw
      throw std::runtime_error(
          "Functors::detail::with_output_tree::variadic_numeric_value_wrapper did not recognise return type: " +
          System::typeinfoName( type ) );
    }

    /** Return the wrapped std::variant
     */
    numeric_value& get() { return m_value; }

  private:
    template <typename T>
    bool help( std::type_info const& type ) {
      if ( type != typeid( T ) ) return false;
      // type represents T, initialise our variant with T.
      m_value = T{};
      return true;
    }
    numeric_value m_value;
  };

  /** @typedef numeric_value_wrapper
   *
   *  Instantiation of variadic_numeric_value_wrapper for all types we are
   *  interested in having ROOT save to the output TTree.
   */
  using numeric_value_wrapper =
      variadic_numeric_value_wrapper<bool, float, double, short, unsigned short, int, unsigned int, long, unsigned long,
                                     long long, unsigned long long, SIMDWrapper::scalar::int_v,
                                     SIMDWrapper::scalar::float_v, SIMDWrapper::scalar::mask_v>;

  /** @typedef numeric_value
   *  @brief   std::variant<Ts...> type corresponding to numeric_value_wrapper
   */
  using numeric_value = numeric_value_wrapper::numeric_value;

  template <typename, typename U = int>
  struct has_branch_prefix : std::false_type {};

  template <typename T>
  struct has_branch_prefix<T, decltype( (void)T::BranchPrefix, 0 )> : std::true_type {};

  /** @brief Check if the given type has a static member called BranchPrefix
   */
  template <typename T>
  inline constexpr bool has_branch_prefix_v = has_branch_prefix<T>::value;

  template <typename T, typename = std::void_t<>>
  struct functor_tag {
    using type = T;
  };

  template <typename T>
  struct functor_tag<T, std::void_t<typename T::FunctorTagType>> {
    using type = typename T::FunctorTagType;
  };

  /** @brief Return T::FunctorTagType if it exists, otherwise T.
   */
  template <typename T>
  using functor_tag_t = typename functor_tag<T>::type;

  /** Shorthand folding together with_functor_maps_deduplicated and
   *  functor_tag_t into the base class of with_output_tree.
   */
  template <typename base_t, typename... Tags>
  using mixin_base = with_functor_maps_deduplicated<base_t, functor_tag_t<Tags>...>;

  /** @class  PreparedFiller
   *  @tparam TagType
   *
   *  This is a small helper class that holds a prepared functor and a
   *  std::variant<Ts...>& where the result of evaluating the functor should be
   *  stored. The variant is assumed to already be initialised with a value of
   *  the correct type.
   */
  template <typename TagType>
  struct PreparedFiller {
    using functor_type  = Functors::Functor<typename functor_tag_t<TagType>::Signature>;
    using prepared_type = typename functor_type::prepared_type;

    PreparedFiller( prepared_type f, numeric_value& value ) : m_f{std::move( f )}, m_value{value} {}

    /** Evaluate the functor and store the result in the storage we hold a
     *  reference to.
     */
    template <typename... Args>
    void operator()( Args&&... args ) {
      // Get the functor value as std::any
      auto fval = std::invoke( m_f, std::forward<Args>( args )... );
      // m_value should already hold the same return type as that hidden by
      // the std::any functor return type, thanks to the magic of rtype(), so
      // we should be able to pass this type directly to std::any_cast.
      std::visit( [&]( auto& x ) { m_value = std::any_cast<std::decay_t<decltype( x )>>( fval ); }, m_value );
    }

  private:
    prepared_type  m_f;
    numeric_value& m_value;
  };
} // namespace Functors::detail::with_output_tree

/** @brief  Mixin adding tuple-dumping functionality to an algorithm
 *  @tparam base_t  Base class to augment with tuple-dumping features
 *  @tparam Tags... List of tag types used to configure and access the functors
 *
 *  This mixin can be used to augment an algorithm with dump-to-ROOT-TTree
 *  functionality. The mixin adds functor properties to the algorithm with
 *  names and signatures configured by the given tag types (Tags...), and
 *  provides an interface (this->prepareBranchFillers()) for the algorithm that
 *  uses/derives from this mixin to call the functors and add rows to the
 *  output TTree.
 */
template <typename base_t, typename... Tags>
struct with_output_tree : public Functors::detail::with_output_tree::mixin_base<base_t, Tags...> {
  /** Shorthand for below
   */
  using mixin_base = Functors::detail::with_output_tree::mixin_base<base_t, Tags...>;

  using numeric_value = Functors::detail::with_output_tree::numeric_value;

  template <typename T>
  using functor_tag_t = typename Functors::detail::with_output_tree::functor_tag_t<T>;

  /** Expose the base class constructor.
   */
  using mixin_base::mixin_base;

  StatusCode initialize() override {
    // Open the ROOT file and create the TTree
    m_root_file.reset( TFile::Open( m_root_file_name.value().c_str(), "recreate" ) );
    m_root_file->cd();
    // m_root_tree gets managed by m_root_file, this isn't a dangling pointer
    m_root_tree = new TTree( m_root_tree_name.value().c_str(), "" );
    return mixin_base::initialize();
  }

  StatusCode start() override {
    // Set up our vectors of branch-filling helpers that go with those functors
    // we can not call this in initialize() because the current implementation
    // relies on calling functor->rtype() thus we need to wait unilt after the
    // FunctorFactory's start() call.
    ( initialize<Tags>(), ... );
    return mixin_base::start();
  }

  StatusCode finalize() override {
    m_root_file->Write();
    m_root_file->Close();
    return mixin_base::finalize();
  }

  /** @class PreparedFillers
   *
   *  This is a collection of callables that is passed to the algorithm using
   *  the mixin. Each callable invokes a [prepared] functor and stores its
   *  result.
   */
  struct PreparedFillers {
    template <typename... Args>
    PreparedFillers( TTree* root_tree, Args&&... prepared_fillers )
        : m_root_tree{root_tree}, m_prepared_branch_fillers{std::forward<Args>( prepared_fillers )...} {}

    /** @brief  Get the list of callables corresponding to the given tag type
     *  @tparam Tag Tag type indicating which collection of functors is
     *              requested. This encodes the specification of the functor
     *              signature and the algorithm property name that was used to
     *              provide the functors in the job options.
     */
    template <typename Tag>
    auto& get() {
      return std::get<LHCb::index_of_v<Tag, TagTypesTuple>>( m_prepared_branch_fillers );
    }

    /** Save the current values to a new row in the output TTree
     */
    void fill() { m_root_tree->Fill(); }

  private:
    TTree*                                                                               m_root_tree{nullptr};
    std::tuple<std::vector<Functors::detail::with_output_tree::PreparedFiller<Tags>>...> m_prepared_branch_fillers;
  };

  /** Prepare the functors we manage and [conceptually] take a lock on the I/O
   *  machinery.
   *
   *  @todo Actually implement the locking part, for now we just assume we're
   *        running single-threaded. Should be able to do this quite easily
   *        with RAII.
   */
  PreparedFillers prepareBranchFillers( /* evt_context */ ) const {
    // Delegate to a per-tag-type helper
    return {m_root_tree, prepareBranchFillers<Tags>( /* evt_context */ )...};
  }

private:
  // TODO this should be refactored!
  std::unique_ptr<TFile> m_root_file;
  mutable TTree*         m_root_tree{nullptr};

  using TagTypesTuple = std::tuple<Tags...>;

  // Storage for the branch-filling helpers. This is an array of vectors that
  // matches the tuple of maps held by the with_functor_maps mixin.
  mutable std::array<std::vector<Functors::detail::with_output_tree::numeric_value_wrapper>, sizeof...( Tags )>
      m_branch_helpers_store;

  // TODO make the property name configurable
  Gaudi::Property<std::string> m_root_file_name{this, "DumpFileName", "with_output_tree.root"};
  Gaudi::Property<std::string> m_root_tree_name{this, "DumpTreeName", "Tree"};

  // Set up a specific element of m_branch_helpers_tuple
  template <typename TagType>
  void initialize() {
    // Get the helper vector corresponding to this tag type
    auto& branch_helpers = m_branch_helpers_store[LHCb::index_of_v<TagType, TagTypesTuple>];
    // Get the map of functors corresponding to this tag
    auto const& functors = this->template getFunctorMap<functor_tag_t<TagType>>();
    // Not sure if this is sometimes needed?
    branch_helpers.clear();
    // Make sure we will not reallocate, important as that would change
    // addresss that ROOT knows about.
    branch_helpers.reserve( functors.size() );
    // Get the branch prefix, if there is one
    std::string branch_prefix;
    if constexpr ( Functors::detail::with_output_tree::has_branch_prefix_v<TagType> ) {
      branch_prefix = TagType::BranchPrefix;
    }
    // Set up the variants that ROOT knows the addresses of
    for ( auto const& [branch_name, functor] : functors ) {
      if ( not functor ) continue;
      // Get the return type of the functor, this will be wrapped up in
      // std::any before we actually see it
      auto const& rtype = functor.rtype();
      // Initialise the variant that we'll point ROOT to with this type
      auto& numeric_value = branch_helpers.emplace_back( rtype );
      // Set up the branch
      std::visit( [this, name = branch_prefix +
                                branch_name]( auto& value ) { this->m_root_tree->Branch( name.c_str(), &value ); },
                  numeric_value.get() );
    }
  }

  // Prepare the functors corresponding to this tag type and zip them up into a
  // new vector with references to the variants ROOT knows the addresses of.
  template <typename TagType>
  auto prepareBranchFillers( /* evt_context */ ) const {
    // Get the vector of variants that ROOT knows the addresses of
    auto& branch_helpers = m_branch_helpers_store[LHCb::index_of_v<TagType, TagTypesTuple>];

    // Get the map of functors corresponding to this tag
    auto const& functors = this->template getFunctorMap<functor_tag_t<TagType>>();

    // Prepare the functors and bundle each prepared functor up with a
    // reference to the variant that will hold its result.
    std::vector<Functors::detail::with_output_tree::PreparedFiller<TagType>> prepared_fillers;
    prepared_fillers.reserve( functors.size() );
    std::size_t index{0};
    for ( auto const& [branch_name, functor] : functors ) {
      prepared_fillers.emplace_back( functor.prepare( /* evt_context */ ), branch_helpers[index++].get() );
    }
    return prepared_fillers;
  }
};

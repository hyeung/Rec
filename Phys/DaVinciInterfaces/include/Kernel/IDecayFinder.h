/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef KERNEL_IDECAYFINDER_H
#define KERNEL_IDECAYFINDER_H 1

#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "GaudiKernel/IAlgTool.h"
#include <string>
#include <vector>

namespace Decay {
  class Tree;
} // namespace Decay

struct GAUDI_API IDecayFinder : extend_interfaces<IAlgTool> {
  // interface machinery
  DeclareInterfaceID( IDecayFinder, 4, 0 );

  template <class PARTICLE>
  using vec_ptr     = std::vector<const PARTICLE*>;
  using vec_ptrv1   = vec_ptr<LHCb::Particle>;
  using vec_ptrv1mc = vec_ptr<LHCb::MCParticle>;
  using vec_dt_t    = std::vector<Decay::Tree>;

  /// find decays for Particles with decay descriptor
  virtual StatusCode findDecay( const std::string& decay_descriptor, const vec_ptrv1& input_parts,
                                vec_ptrv1& out_parts ) const = 0;

  /// find decays for MCParticles with decay descriptor
  // Note: The arrows do NOT mean anything currently for MCParticles
  virtual StatusCode findDecay( const std::string& decay_descriptor, const vec_ptrv1mc& input_parts,
                                vec_ptrv1mc& out_parts ) const = 0;

  /// find decays for Particles with a vector of Tree possibilities
  virtual StatusCode findDecay( const vec_dt_t& vec_dt_possibilities, const vec_ptrv1& input_parts,
                                vec_ptrv1& out_parts ) const = 0;

  /// find decays for MCParticles with a vector of Tree possibilities
  // Note: The arrows do NOT mean anything currently for MCParticles
  virtual StatusCode findDecay( const vec_dt_t& vec_dt_possibilities, const vec_ptrv1mc& input_parts,
                                vec_ptrv1mc& out_parts ) const = 0;

  // TODO: add here v2 variant type of composite and charged basic

  /// get parsed descriptor (for testing purposes)
  virtual std::vector<std::string> getParsedDescriptors( const std::vector<std::string>& decay_descriptor ) const = 0;

  /// get possibilities of decay descriptors from a "complex" decay descriptor (for testing purposes)
  virtual std::vector<std::string> getDescriptorPossibilities( const std::string& decay_descriptor ) const = 0;

  /// get possibility of decay "Tree" objects from a "complex" decay descriptor
  virtual vec_dt_t getTreePossibilities( const std::string& decay_descriptor ) const = 0;
};

#endif // KERNEL_IDECAYFINDER_H

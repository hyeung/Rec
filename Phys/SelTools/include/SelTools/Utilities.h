/*****************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "LHCbMath/MatVec.h"
#include "SelTools/State4.h"
#include <Event/FunctorDefaults.h>
#include <SelKernel/Utilities.h>

namespace Sel {
  template <typename Particle>
  static auto getClosestToBeamState( Particle const& p ) {
    if constexpr ( Sel::Utils::canBeExtrapolatedDownstream_v<Particle> ) {
      using LHCb::Event::trackState;
      return trackState( p );
    } else {
      // Make a state vector from a particle (position + 4-momentum)
      return stateVectorFromComposite( p );
    }
  }

  template <typename pos_t, typename mom_t, typename momCov_t, typename posCov_t, typename momPosCov_t>
  auto stateVectorComputations( pos_t const& pos, mom_t const& mom, momCov_t const& momCov, posCov_t const& posCov,
                                momPosCov_t const& momPosCov ) {
    auto const invpz = 1.f / Z( mom );
    using float_v    = std::decay_t<decltype( invpz )>;

    State4<float_v> s{};
    s.x()  = X( pos );
    s.y()  = Y( pos );
    s.z()  = Z( pos );
    s.tx() = X( mom ) * invpz;
    s.ty() = Y( mom ) * invpz;
    // For the computation of the Jacobian it is important to note the following.
    //  x' = x + (z' - z) * tx
    // --> dx/dz = - tx ;
    //
    // Notation:
    //      J_{4,6} = ( Jxpos      Jxmom )
    //                ( Jtxpos     Jtxmom )
    // Jtxpos = 0 and Jxmom=0, so we rather do not introduce them. Instead, we'll compute Jxpos and Jxtxmom
    //
    // to test this, the full calculation is included below
    auto Jxpos    = LHCb::LinAlg::initialize_with_zeros<LHCb::LinAlg::Mat<float_v, 2, 3>>();
    Jxpos( 0, 0 ) = 1.f;
    Jxpos( 1, 1 ) = 1.f;
    Jxpos( 0, 2 ) = -s.tx();
    Jxpos( 1, 2 ) = -s.ty();

    auto Jtxmom    = LHCb::LinAlg::initialize_with_zeros<LHCb::LinAlg::Mat<float_v, 2, 3>>();
    Jtxmom( 0, 0 ) = invpz;
    Jtxmom( 1, 1 ) = invpz;
    Jtxmom( 0, 2 ) = -s.tx() * invpz;
    Jtxmom( 1, 2 ) = -s.ty() * invpz;

    s.covXX() = similarity( Jxpos, posCov );
    s.covTT() = similarity( Jtxmom, momCov );
    s.covXT() = Jxpos * momPosCov.transpose() * Jtxmom.transpose();

    return s;
  }

  /** @fn    stateVectorFromComposite
   *  @brief Convert a particle (3-momentum + position) to 4-state (at some z).
   *
   *  Adapted from ParticleVertexFitter.cpp / Wouter Huslbergen, templated for
   *  new event model types and explicit vectorisation.
   */
  template <typename particle_t>
  auto stateVectorFromComposite( particle_t const& p ) {
    using LHCb::Event::posCovMatrix;
    using LHCb::Event::referencePoint;
    using LHCb::Event::threeMomCovMatrix;
    using LHCb::Event::threeMomentum;
    using LHCb::Event::threeMomPosCovMatrix;

    auto const pos       = referencePoint( p );
    auto const mom       = threeMomentum( p );
    auto const momCov    = threeMomCovMatrix( p );
    auto const posCov    = posCovMatrix( p );
    auto const momPosCov = threeMomPosCovMatrix( p );
    return stateVectorComputations( pos, mom, momCov, posCov, momPosCov );
  }

  template <typename particle_t>
  auto stateVectorFromNeutral( particle_t const& p ) {
    auto const invpz = 1.f / p.pz();
    return StateVector5{p.position().x(), p.position().y(), p.position().z(),
                        p.px() * invpz,   p.py() * invpz,   1.f / p.p()};
  }
} // namespace Sel

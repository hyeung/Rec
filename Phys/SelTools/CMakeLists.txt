###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/SelTools
-------------
#]=======================================================================]

gaudi_add_header_only_library(SelToolsLib
    LINK
        Gaudi::GaudiKernel
        LHCb::EventBase
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        Rec::SelKernelLib
        BLAS::BLAS  # needed because ROOT::TMVA does not link correctly
        ROOT::TMVA
)

/*****************************************************************************\
* (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "DecayTreeFitter/Fitter.h"
#include "DetDesc/DetectorElement.h"
#include "Event/Particle.h"
#include "Kernel/DecayTree.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IParticlePropertySvc.h"
#include "LHCbAlgs/Transformer.h"
//#include "Kernel/IPrintDecay.h"
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"
#include "Relations/ObjectTypeTraits.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"
#include "SelKernel/VertexRelation.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include "TrackKernel/PrimaryVertexUtils.h"
#include <cassert>

namespace {

  using PVs = LHCb::PrimaryVertices;

  enum RunMode_t { NoPV, BestPV, AllPVs };
  template <RunMode_t>
  struct AlgorithmRunMode {
    template <typename T1, typename T2>
    using Table_t = LHCb::Relation1D<T1, T2>;
  };
  template <>
  struct AlgorithmRunMode<RunMode_t::AllPVs> {
    template <typename T1, typename T2>
    using Table_t = LHCb::RelationWeighted1D<T1, T2, double>;
  };

  template <RunMode_t RunMode>
  StatusCode
  FillFitResult( const DecayTreeFitter::Fitter& fitter, LHCb::Particles& output_particles,
                 typename AlgorithmRunMode<RunMode>::template Table_t<LHCb::Particle, LHCb::Particle>& output_relations,
                 typename AlgorithmRunMode<RunMode>::template Table_t<LHCb::Particle, Gaudi::Math::ParticleParams>&
                                                                                            output_particle_params,
                 typename AlgorithmRunMode<RunMode>::template Table_t<LHCb::Particle, int>& output_nIter ) {
    // Get head particle
    const auto head_particle = fitter.particle();

    // Copy the whole tree
    LHCb::DecayTree decay_tree( *head_particle );
    const auto      clone_map = decay_tree.cloneMap();

    // Update the tree
    const auto sc = fitter.UpdateDecayTree( decay_tree );
    if ( sc.isFailure() ) return sc;

    // Fill output
    if constexpr ( RunMode == AllPVs ) {
      const auto chi2 = fitter.chiSquare();
      for ( const auto& [original_particle, cloned_particle] : clone_map ) {
        output_particles.insert( cloned_particle );
        output_relations.relate( original_particle, cloned_particle, chi2 ).ignore();
        output_particle_params.relate( original_particle, *fitter.fitParams( original_particle ), chi2 ).ignore();
      };

      // Fill final output
      decay_tree.release(); // Very important! otherwise the desctructor of DecayTree will delete the output particle
      output_nIter.relate( head_particle, fitter.nIter(), chi2 ).ignore();
    } else {
      for ( const auto& [original_particle, cloned_particle] : clone_map ) {
        output_particles.insert( cloned_particle );
        output_relations.relate( original_particle, cloned_particle ).ignore();
        output_particle_params.relate( original_particle, *fitter.fitParams( original_particle ) ).ignore();
      };

      // Fill final output
      decay_tree.release(); // Very important! otherwise the desctructor of DecayTree will delete the output particle
      output_nIter.relate( head_particle, fitter.nIter() ).ignore();
    }

    return StatusCode::SUCCESS;
  }

  /**
   *  Signature for gaudi transformer
   */
  template <RunMode_t RunMode, typename... Inputs_t>
  struct GaudiSignature {};

  template <RunMode_t RunMode>
  struct GaudiSignature<RunMode, LHCb::Particle::Range> {
    // Relation
    template <typename T1, typename T2>
    using Table_t = typename AlgorithmRunMode<RunMode>::template Table_t<T1, T2>;

    // Output types
    using FittedParticles_t = Table_t<LHCb::Particle, LHCb::Particle>;
    using ParticleParams_t  = Table_t<LHCb::Particle, Gaudi::Math::ParticleParams>;
    using NIter_t           = Table_t<LHCb::Particle, int>;

    // Input types
    using Input_Particles_t = LHCb::Particle::Range;

    /**
     *  Common
     */
    using Output_t = std::tuple<LHCb::Particles, FittedParticles_t, ParticleParams_t, NIter_t>;
    using MultiTransformer =
        LHCb::Algorithm::MultiTransformer<Output_t( Input_Particles_t const&, DetectorElement const& ),
                                          LHCb::DetDesc::usesConditions<DetectorElement>>;
  };

  template <RunMode_t RunMode>
  struct GaudiSignature<RunMode, LHCb::Particle::Range, PVs> {
    // Relation
    template <typename T1, typename T2>
    using Table_t = typename AlgorithmRunMode<RunMode>::template Table_t<T1, T2>;

    // Output types
    using FittedParticles_t = Table_t<LHCb::Particle, LHCb::Particle>;
    using ParticleParams_t  = Table_t<LHCb::Particle, Gaudi::Math::ParticleParams>;
    using NIter_t           = Table_t<LHCb::Particle, int>;

    // Input types
    using Input_Particles_t = LHCb::Particle::Range;
    using Input_Vertices_t  = PVs;

    /**
     *  Common
     */
    using Output_t = std::tuple<LHCb::Particles, FittedParticles_t, ParticleParams_t, NIter_t>;
    using MultiTransformer =
        LHCb::Algorithm::MultiTransformer<Output_t( Input_Particles_t const&, Input_Vertices_t const&,
                                                    DetectorElement const& ),
                                          LHCb::DetDesc::usesConditions<DetectorElement>>;
  };

} // namespace

// ==========================================================================
// Templated class definition
// ==========================================================================
template <RunMode_t RunMode, typename... Inputs_t>
class DecayTreeFitterAlg : public GaudiSignature<RunMode, Inputs_t...>::MultiTransformer {
public:
  DecayTreeFitterAlg( const std::string& name, ISvcLocator* pSvc );

  typename GaudiSignature<RunMode, Inputs_t...>::Output_t operator()( Inputs_t const&...,
                                                                      DetectorElement const& ) const override;

private:
  // property service
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{this, "ParticleProperty",
                                                                  "LHCb::ParticlePropertySvc"};
  // tools
  ToolHandle<ITrackStateProvider> m_stateprovider{this, "StateProvider", "TrackStateProvider"};

  // properties
  Gaudi::Property<std::vector<std::string>> m_massConstraints{this, "MassConstraints"};
  Gaudi::Property<bool>                     m_useTrackTraj{this, "useTrackTraj", true};
  Gaudi::Property<bool>                     m_forceFitAll{this, "forceFitAll", true};
  Gaudi::Property<bool>                     m_usePVConstraint{this, "usePVConstraint", false};

  // counters
  mutable Gaudi::Accumulators::Counter<>                 m_eventCount{this, "Events"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_candidateCount{this, "Input Particles"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_fittedCount{this, "Fitted Particles"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_failedCount{this, "Failed fits"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_savedCount{this, "saved Particles"};
};

// ==========================================================================
// V1 particles NoPV
// ==========================================================================
template <>
DecayTreeFitterAlg<NoPV, LHCb::Particle::Range>::DecayTreeFitterAlg( const std::string& name, ISvcLocator* pSvc )
    : MultiTransformer( name, pSvc,
                        {KeyValue{"Input", ""}, KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}},
                        {KeyValue{"Output", ""}, KeyValue{"OutputRelations", ""}, KeyValue{"ParticleParams", ""},
                         KeyValue{"NIter", ""}} ) {}
template <>
typename GaudiSignature<NoPV, LHCb::Particle::Range>::Output_t DecayTreeFitterAlg<NoPV, LHCb::Particle::Range>::
                                                               operator()( const LHCb::Particle::Range& parts, DetectorElement const& geometry ) const {
  // output
  using FittedParticles_t = GaudiSignature<NoPV, LHCb::Particle::Range>::FittedParticles_t;
  using ParticleParams_t = GaudiSignature<NoPV, LHCb::Particle::Range>::ParticleParams_t;
  using NIter_t = GaudiSignature<NoPV, LHCb::Particle::Range>::NIter_t;
  LHCb::Particles   parts_out;
  FittedParticles_t p2p_out;
  ParticleParams_t  params_out;
  NIter_t           niter_out;

  for ( auto* p : parts ) {
    if ( !m_stateprovider.retrieve().isSuccess() ) {
      warning() << "Can not retrieve StateProvider correctly." << endmsg;
    }

    const auto* pv = p->pv().target();
    if ( m_usePVConstraint && !pv ) warning() << "Particle has no PV. Will omit PV constraint." << endmsg;

    auto fitter =
        m_usePVConstraint && pv
            ? DecayTreeFitter::Fitter( *p, *pv, m_stateprovider.get(), m_forceFitAll.value(), m_useTrackTraj.value() )
            : DecayTreeFitter::Fitter( *p, m_stateprovider.get(), m_forceFitAll.value(), m_useTrackTraj.value() );
    for ( const auto& C : m_massConstraints ) {
      const LHCb::ParticleProperty* p_prop = m_particlePropertySvc->find( C );
      fitter.setMassConstraint( p_prop->particleID() );
    }
    fitter.fit( geometry );

    if ( FillFitResult<BestPV>( fitter, parts_out, p2p_out, params_out, niter_out ).isFailure() ) {
      throw GaudiException( "Faild to fill the output in", name(), StatusCode::FAILURE );
    }

    if ( fitter.status() == DecayTreeFitter::Fitter::Success ) {
      ++m_fittedCount;
    } else {
      ++m_failedCount;
      warning() << "Failed fit" << endmsg;
    }
  }
  if ( !parts.empty() ) {
    ++m_eventCount;
    m_candidateCount += parts.size();
    m_savedCount += parts_out.size();
  }
  return std::make_tuple( std::move( parts_out ), p2p_out, params_out, niter_out );
}

// ==========================================================================
// V1 particles + v2 PV (BestPV)
// ==========================================================================
template <>
DecayTreeFitterAlg<BestPV, LHCb::Particle::Range, PVs>::DecayTreeFitterAlg( const std::string& name, ISvcLocator* pSvc )
    : MultiTransformer( name, pSvc,
                        {KeyValue{"Input", ""}, KeyValue{"InputPVs", ""},
                         KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}},
                        {KeyValue{"Output", ""}, KeyValue{"OutputRelations", ""}, KeyValue{"ParticleParams", ""},
                         KeyValue{"NIter", ""}} ) {}

template <>
typename GaudiSignature<BestPV, LHCb::Particle::Range, PVs>::Output_t
DecayTreeFitterAlg<BestPV, LHCb::Particle::Range, PVs>::
operator()( const LHCb::Particle::Range& parts, const PVs& vertices, DetectorElement const& geometry ) const {

  // output
  using FittedParticles_t = GaudiSignature<BestPV, LHCb::Particle::Range>::FittedParticles_t;
  using ParticleParams_t  = GaudiSignature<BestPV, LHCb::Particle::Range>::ParticleParams_t;
  using NIter_t           = GaudiSignature<BestPV, LHCb::Particle::Range>::NIter_t;
  LHCb::Particles   parts_out;
  FittedParticles_t p2p_out;
  ParticleParams_t  params_out;
  NIter_t           niter_out;

  for ( const auto& p : parts ) {
    if ( !m_stateprovider.retrieve().isSuccess() ) {
      warning() << "Can not retrive StateProvider correctly." << endmsg;
    }

    const auto* pv = LHCb::bestPV( vertices, *p );
    if ( !pv ) warning() << "LHCb::bestPV returned no vertex. Will omit pv constraint." << endmsg;

    auto fitter =
        pv ? DecayTreeFitter::Fitter( *p, *pv, m_stateprovider.get(), m_forceFitAll.value(), m_useTrackTraj.value() )
           : DecayTreeFitter::Fitter( *p, m_stateprovider.get(), m_forceFitAll.value(), m_useTrackTraj.value() );
    for ( const auto& C : m_massConstraints ) {
      const LHCb::ParticleProperty* p_prop = m_particlePropertySvc->find( C );
      fitter.setMassConstraint( p_prop->particleID() );
    }
    fitter.fit( geometry );

    if ( FillFitResult<BestPV>( fitter, parts_out, p2p_out, params_out, niter_out ).isFailure() ) {
      throw GaudiException( "Faild to fill the output in", name(), StatusCode::FAILURE );
    }

    if ( fitter.status() == DecayTreeFitter::Fitter::Success ) {
      m_fittedCount++;
    } else {
      m_failedCount++;
      warning() << "Failed fit" << endmsg;
    }
  }

  if ( !parts.empty() ) {
    ++m_eventCount;
    m_candidateCount += parts.size();
    m_savedCount += parts_out.size();
  }
  return std::make_tuple( std::move( parts_out ), p2p_out, params_out, niter_out );
}

// ==========================================================================
// V1 particles + v2 PV (AllPVs)
// ==========================================================================
template <>
DecayTreeFitterAlg<AllPVs, LHCb::Particle::Range, PVs>::DecayTreeFitterAlg( const std::string& name, ISvcLocator* pSvc )
    : MultiTransformer( name, pSvc,
                        {KeyValue{"Input", ""}, KeyValue{"InputPVs", ""},
                         KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}},
                        {KeyValue{"Output", ""}, KeyValue{"OutputRelations", ""}, KeyValue{"ParticleParams", ""},
                         KeyValue{"NIter", ""}} ) {}

template <>
typename GaudiSignature<AllPVs, LHCb::Particle::Range, PVs>::Output_t
DecayTreeFitterAlg<AllPVs, LHCb::Particle::Range, PVs>::
operator()( const LHCb::Particle::Range& parts, const PVs& vertices, DetectorElement const& geometry ) const {

  // output
  using FittedParticles_t = GaudiSignature<AllPVs, LHCb::Particle::Range>::FittedParticles_t;
  using ParticleParams_t  = GaudiSignature<AllPVs, LHCb::Particle::Range>::ParticleParams_t;
  using NIter_t           = GaudiSignature<AllPVs, LHCb::Particle::Range>::NIter_t;
  LHCb::Particles   parts_out;
  FittedParticles_t p2p_out;
  ParticleParams_t  params_out;
  NIter_t           niter_out;

  for ( const auto& p : parts ) {

    for ( const auto& v : vertices ) {
      if ( !m_stateprovider.retrieve().isSuccess() ) {
        warning() << "Can not retrive StateProvider correctly." << endmsg;
      }
      auto fitter =
          DecayTreeFitter::Fitter( *p, *v, m_stateprovider.get(), m_forceFitAll.value(), m_useTrackTraj.value() );
      for ( const auto& C : m_massConstraints ) {
        const LHCb::ParticleProperty* p_prop = m_particlePropertySvc->find( C );
        fitter.setMassConstraint( p_prop->particleID() );
      }
      fitter.fit( geometry );

      if ( FillFitResult<AllPVs>( fitter, parts_out, p2p_out, params_out, niter_out ).isFailure() ) {
        throw GaudiException( "Faild to fill the output in", name(), StatusCode::FAILURE );
      }

      if ( fitter.status() == DecayTreeFitter::Fitter::Success ) {
        m_fittedCount++;
      } else {
        m_failedCount++;
        warning() << "Failed fit" << endmsg;
      }
    }
  }

  if ( !parts.empty() ) {
    ++m_eventCount;
    m_candidateCount += parts.size();
    m_savedCount += parts_out.size();
  }
  return std::make_tuple( std::move( parts_out ), p2p_out, params_out, niter_out );
}

// ============================================================================

using DecayTreeFitterAlg_v1Particle        = DecayTreeFitterAlg<NoPV, LHCb::Particle::Range>;
using DecayTreeFitterAlg_v1Particle_BestPV = DecayTreeFitterAlg<BestPV, LHCb::Particle::Range, PVs>;
using DecayTreeFitterAlg_v1Particle_AllPVs = DecayTreeFitterAlg<AllPVs, LHCb::Particle::Range, PVs>;
DECLARE_COMPONENT_WITH_ID( DecayTreeFitterAlg_v1Particle, "DecayTreeFitterAlg_v1Particle" )
DECLARE_COMPONENT_WITH_ID( DecayTreeFitterAlg_v1Particle_BestPV, "DecayTreeFitterAlg_v1Particle_BestPV" )
DECLARE_COMPONENT_WITH_ID( DecayTreeFitterAlg_v1Particle_AllPVs, "DecayTreeFitterAlg_v1Particle_AllPVs" )

// ============================================================================
// The END
// ============================================================================

###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/ParticleConverters
-----------------------
#]=======================================================================]

gaudi_add_module(ParticleConverters
    SOURCES
        src/KeyedToVectorParticle.cpp
        src/Particle_to_Particle_v2.cpp
    LINK
        Gaudi::GaudiAlgLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::DaVinciInterfacesLib
        Rec::FunctorCoreLib

)

gaudi_add_tests(QMTest)

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef DAVINCIKERNEL_GETPARTICLESFORDECAY_H
#  define DAVINCIKERNEL_GETPARTICLESFORDECAY_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#  include <string>
#  include <vector>
// ============================================================================
// PartProp
// ============================================================================
#  include "Kernel/Decay.h"
// ============================================================================
namespace DaVinci {
  // ==========================================================================
  namespace Utils {
    // ========================================================================
    /** simple function for filtering (by PID) the particles, which
     *  matched the PID
     *
     *  @code
     *
     *   const LHCb::Particle::ConstVector input = ... ;
     *   const Decays::Decay& decay = ... ;
     *
     *   LHCb::Particle::ConstVector result ;
     *
     *   // use the function:
     *
     *   DaVinci::Utils::getParticles
     *            ( input . begin () ,
     *              input . end   () ,
     *              decay ( 1 )      , // get the first component
     *              std::back_inserter ( result ) ) ;
     *
     *  @endcode
     *
     *  It is also applicable to Monte-Carlo data:
     *
     *  @code
     *
     *   const LHCb::MCParticle::ConstVector input = ... ;
     *   const Decays::Decay& decay = ... ;
     *
     *   LHCb::MCParticle::ConstVector result ;
     *
     *   // use the function:
     *
     *   DaVinci::Utils::getParticles
     *            ( input . begin () ,
     *              input . end   () ,
     *              decay ( 1 )      , // get the first component
     *              std::back_inserter ( result ) ) ;
     *
     *  @endcode
     *
     *  @param begin the begin iterator for the input sequence
     *  @param end   the end   iterator for the input sequence
     *  @param item  the decay component
     *  @param result the output iterator
     *  @return the updated output iterator
     *
     *  @author  Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2008-03-31
     */
    template <class INPUT, class OUTPUT>
    inline OUTPUT getParticles( INPUT begin, INPUT end, const Decays::Decay::Item& item, OUTPUT result ) {
      for ( ; begin != end; ++begin ) {
        if ( !( *begin ) ) { continue; } // CONTINUE
        const LHCb::ParticleID& pid = ( *begin )->particleID();
        if ( pid != item.pid() ) { continue; } // CONTINUE
        *result = *begin;
        ++result;
      }
      return result;
    }
    // ========================================================================
  } // namespace Utils
  // ==========================================================================
} // end of namespace DaVinci
// ============================================================================
// The END
// ============================================================================
#endif // DAVINCIKERNEL_GETPARTICLESDECAY_H
// ============================================================================

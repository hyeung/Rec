/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_PARTICLEPREDICATES_H
#define KERNEL_PARTICLEPREDICATES_H 1

// Include files
#include "Event/Particle.h"

/** @namespace DaVinci Kernel/ParticlePredicates.h
 *
 *
 *  @author Juan Palacios
 *  @date   2011-01-10
 */
namespace DaVinci {

  /** @namespace Utils Kernel/ParticlePredicates.h
   *
   *
   *  @author Juan Palacios
   *  @date   2011-01-10
   */
  namespace Utils {

    struct IParticlePredicate {
      virtual ~IParticlePredicate()                              = default;
      virtual bool operator()( const LHCb::Particle* obj ) const = 0;
    };

    ///  Functor to check if a Particle is in the TES.
    struct ParticleInTES : IParticlePredicate {
      inline bool operator()( const LHCb::Particle* obj ) const override { return ( obj && obj->parent() ); }
    };

    struct ParticleTrue : IParticlePredicate {
      inline bool operator()( const LHCb::Particle* ) const override { return true; }
    };

    struct ParticleFalse : IParticlePredicate {
      inline bool operator()( const LHCb::Particle* ) const override { return false; }
    };

  } // namespace Utils

} // namespace DaVinci

#endif // KERNEL_PARTICLEPREDICATES_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// DaVinciInterfaces
// ============================================================================
#include "Kernel/IParticleCombiner.h"
// ============================================================================
// local
// ============================================================================
#include "ParticleClassificator.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @class SmartParticleCombiner
   *
   *  Implementation file for class
   *
   *  @see  IParticleCombiner
   *  @date 2010-11-13
   *  @author Vanya Belyaev  Ivan.Belyaev@cern.ch
   *
   *  This file is a part of
   *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
   *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   */
  // ==========================================================================
  class SmartParticleCombiner : public extends<ParticleClassificator, IParticleCombiner> {
  public:
    // ========================================================================
    /** combine set of particles into vertex
     *  @see IParticleCombiner
     *  @see IParticleCombiner::combiner
     *  @param daughters the vector of daughter particles    (input)
     *  @param mother    the "mother" particle               (output)
     *  @param vertex    the decay vertex of mother particle (output)
     *  @return status code
     */
    StatusCode combine( LHCb::Particle::ConstVector const& daughters, LHCb::Particle& mother, LHCb::Vertex& vertex,
                        IGeometryInfo const& ) const override;
    // ========================================================================
    /// standard constructor
    SmartParticleCombiner( const std::string& type, const std::string& name, const IInterface* parent )
        : base_class( type, name, parent )
        //
        , m_adderName( "MomentumCombiner/Adder" )
        , m_fitterName( "Loki::VertexFitter/VertexFitter" )
        //
        , m_adder( 0 )
        , m_fitter( 0 ) {
      // ======================================================================
      declareProperty( "Adder", m_adderName, "The type/name of MomentumCombiner/ParticleAdder" );
      // ======================================================================
      declareProperty( "VertexFitter", m_fitterName, "The type/name of Vertex fitter " );
      // ======================================================================
    }
    // ========================================================================
  private:
    // ========================================================================
    // get the adder
    inline const IParticleCombiner* adder() const {
      if ( 0 != m_adder ) { return m_adder; }
      m_adder = tool<IParticleCombiner>( m_adderName, this );
      return m_adder;
    }
    // get the fitter
    inline const IParticleCombiner* fitter() const {
      if ( 0 != m_fitter ) { return m_fitter; }
      m_fitter = tool<IParticleCombiner>( m_fitterName, this );
      return m_fitter;
    }
    // ========================================================================
  private:
    // ========================================================================
    std::string                      m_adderName;  // the name of adder
    std::string                      m_fitterName; // the name of fitter
    mutable const IParticleCombiner* m_adder;      // the adder
    mutable const IParticleCombiner* m_fitter;     // the fitter
    // ========================================================================
  };
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
/*  combine set of particles into vertex
 *  @see IParticleCombiner
 *  @see IParticleCombiner::combiner
 *  @param daughters the vector of daughter particles    (input)
 *  @param mother    the "mother" particle               (output)
 *  @param vertex    the decay vertex of mother particle (output)
 *  @return status code
 */
StatusCode LoKi::SmartParticleCombiner::combine( LHCb::Particle::ConstVector const& daughters, LHCb::Particle& mother,
                                                 LHCb::Vertex& vertex, IGeometryInfo const& geometry ) const {
  const IParticleCombiner* actor = goodForVertex( daughters ) ? fitter() : adder();
  //
  return actor->combine( daughters, mother, vertex, geometry );
}
// ============================================================================
/// the factory needed for instantiation
DECLARE_COMPONENT( LoKi::SmartParticleCombiner )
// ============================================================================

// ============================================================================
// The END
// ============================================================================

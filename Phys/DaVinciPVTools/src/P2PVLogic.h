/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/IDistanceCalculator.h"

/**
 *  @author Juan PALACIOS
 *  @date   2008-10-20
 */
struct _p2PVWithIP {
  static double weight( const LHCb::Particle* particle, const LHCb::VertexBase* pv, const IDistanceCalculator* distCalc,
                        IGeometryInfo const& geometry ) {
    double           fom( 0. );
    const StatusCode sc = distCalc->distance( particle, pv, fom, geometry );
    return ( ( sc.isSuccess() ) && fom > std::numeric_limits<double>::epsilon() ) ? 1. / fom : 0.;
  }
};

/**
 *  @author Juan PALACIOS
 *  @date   2008-10-20
 */
struct _p2PVWithIPChi2 {
  static double weight( const LHCb::Particle* particle, const LHCb::VertexBase* pv, const IDistanceCalculator* distCalc,
                        IGeometryInfo const& geometry ) {
    double           fom( 0. );
    double           chi2( 0. );
    const StatusCode sc = distCalc->distance( particle, pv, fom, chi2, geometry );
    return ( ( sc.isSuccess() ) && chi2 > std::numeric_limits<double>::epsilon() ) ? 1. / chi2 : 0;
  }
};

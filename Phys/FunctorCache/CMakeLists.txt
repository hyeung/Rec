###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/FunctorCache
-----------------
#]=======================================================================]

option(THOR_BUILD_TEST_FUNCTOR_CACHE "Build functor cache for THOR functors" ON)

if(THOR_BUILD_TEST_FUNCTOR_CACHE)

    include(LoKiFunctorsCache)

    # need to make sure the FunctorFactory is built
    set(cache_deps FunctorCore)

    # inter-project dependencies for super project build (include current project)
    set(projects Gaudi LHCb Lbcom Rec ${PROJECT_NAME})
    list(REMOVE_DUPLICATES projects)
    foreach(project IN LISTS projects)
        foreach(merge_target IN ITEMS MergeComponents MergeRootmaps MergeConfdb MergeConfDB2)
            if (project STREQUAL PROJECT_NAME AND merge_target STREQUAL "MergeComponents")
                # a functor cache is already part of ${PROJECT_NAME}_MergeComponents target
                # so FunctorDatahandleTest cannot depend on it
                continue()
            endif()
            if (TARGET ${project}_${merge_target} )
                list(APPEND cache_deps ${project}_${merge_target})
            endif()
        endforeach()
    endforeach()

    # Also make sure that FunctorFactory is available FIXME Since the new
    # FunctorFactory in Rec!2699, I'm not sure if the directory is still needed
    # but the comment never said "why" it was in the first place so it's hard
    # to judge. (Same comment for SelAlgorithms...)
    if(NOT EXISTS ${PROJECT_SOURCE_DIR}/Phys/FunctorCore/CMakeLists.txt)
        message(FATAL_ERROR "Functor test cache can be build only if Phys/FunctorCore is present in the current project too")
    endif()

    # if(EXISTS) protection allows build in satellite (lb-dev) projects
    # Make sure the InstantiateFunctors__ algorithms are available
    if(EXISTS ${PROJECT_SOURCE_DIR}/Phys/SelAlgorithms/CMakeLists.txt)
        list(APPEND cache_deps SelAlgorithms)
    endif()

    # Disable LoKi-specific hacks in LoKiFunctorsCachePostActionOpts.py
    # For now there is no need for a ThOr-specific alternative.
    set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS)

    loki_functors_cache(FunctorDatahandleTest
            options/SilenceErrors.py
            options/SuppressLogMessages.py
            options/ThOr_create_cache_opts.py
            ${PROJECT_SOURCE_DIR}/Phys/FunctorCore/tests/options/functor_datahandle_test.py
        FACTORIES FunctorFactory
        LINK_LIBRARIES Rec::FunctorCoreLib
        DEPENDS ${cache_deps}
        SPLIT 2
    )

endif(THOR_BUILD_TEST_FUNCTOR_CACHE)

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERTMVA_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERTMVA_H 1

#include <cstddef>

#include "ITaggingClassifier.h"

class TaggingClassifierTMVA : public ITaggingClassifier {

public:
  double getClassifierValue( const std::vector<double>& featureValues ) override {
    return GetMvaValue( featureValues );
  }

  /**
   * @brief      Returns the classifier value
   *
   * @param[in]  featureValues  The feature values
   *
   * @return     The classifier value
   */
  virtual double GetMvaValue( const std::vector<double>& featureValues ) const = 0;

  // returns classifier status
  bool IsStatusClean() const { return fStatusIsClean; }

protected:
  bool fStatusIsClean = true;
};

#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERTMVA_H

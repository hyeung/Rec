/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <vector>

#include "../SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1.h"

/* @brief a BDT implementation, returning the sum of all tree weights given
 * a feature vector
 */
double SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1::tree_3( const std::vector<double>& features ) const {
  double sum = 0;

  // tree 900
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000116277;
    } else {
      sum += -0.000116277;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000116277;
    } else {
      sum += 0.000116277;
    }
  }
  // tree 901
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -0.000104274;
    } else {
      sum += 0.000104274;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000104274;
    } else {
      sum += -0.000104274;
    }
  }
  // tree 902
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000121615;
    } else {
      sum += 0.000121615;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000121615;
    } else {
      sum += -0.000121615;
    }
  }
  // tree 903
  if ( features[0] < 3.03054 ) {
    if ( features[8] < 2.12266 ) {
      sum += -0.00011025;
    } else {
      sum += 0.00011025;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.00011025;
    } else {
      sum += 0.00011025;
    }
  }
  // tree 904
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -0.000104158;
    } else {
      sum += 0.000104158;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000104158;
    } else {
      sum += -0.000104158;
    }
  }
  // tree 905
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000120954;
    } else {
      sum += -0.000120954;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000120954;
    } else {
      sum += -0.000120954;
    }
  }
  // tree 906
  if ( features[8] < 2.38156 ) {
    if ( features[5] < 1.0889 ) {
      sum += -8.98414e-05;
    } else {
      sum += 8.98414e-05;
    }
  } else {
    if ( features[3] < 1.12549 ) {
      sum += 8.98414e-05;
    } else {
      sum += -8.98414e-05;
    }
  }
  // tree 907
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 0.000140732;
    } else {
      sum += -0.000140732;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000140732;
    } else {
      sum += 0.000140732;
    }
  }
  // tree 908
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000104267;
    } else {
      sum += -0.000104267;
    }
  } else {
    if ( features[7] < 4.81007 ) {
      sum += 0.000104267;
    } else {
      sum += -0.000104267;
    }
  }
  // tree 909
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000103188;
    } else {
      sum += -0.000103188;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 0.000103188;
    } else {
      sum += -0.000103188;
    }
  }
  // tree 910
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000103806;
    } else {
      sum += 0.000103806;
    }
  } else {
    if ( features[7] < 4.81007 ) {
      sum += 0.000103806;
    } else {
      sum += -0.000103806;
    }
  }
  // tree 911
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000184337;
    } else {
      sum += -0.000184337;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000184337;
    } else {
      sum += 0.000184337;
    }
  }
  // tree 912
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000103509;
    } else {
      sum += 0.000103509;
    }
  } else {
    if ( features[7] < 5.04875 ) {
      sum += 0.000103509;
    } else {
      sum += -0.000103509;
    }
  }
  // tree 913
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000103118;
    } else {
      sum += 0.000103118;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000103118;
    } else {
      sum += -0.000103118;
    }
  }
  // tree 914
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -0.000103954;
    } else {
      sum += 0.000103954;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000103954;
    } else {
      sum += -0.000103954;
    }
  }
  // tree 915
  if ( features[0] < 3.03054 ) {
    if ( features[6] < 2.41557 ) {
      sum += -0.000110298;
    } else {
      sum += 0.000110298;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000110298;
    } else {
      sum += 0.000110298;
    }
  }
  // tree 916
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -0.000103498;
    } else {
      sum += 0.000103498;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000103498;
    } else {
      sum += -0.000103498;
    }
  }
  // tree 917
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 0.000103396;
    } else {
      sum += -0.000103396;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000103396;
    } else {
      sum += -0.000103396;
    }
  }
  // tree 918
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 0.000102839;
    } else {
      sum += -0.000102839;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000102839;
    } else {
      sum += -0.000102839;
    }
  }
  // tree 919
  if ( features[8] < 2.38156 ) {
    if ( features[0] < 2.06819 ) {
      sum += 9.94595e-05;
    } else {
      sum += -9.94595e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.94595e-05;
    } else {
      sum += -9.94595e-05;
    }
  }
  // tree 920
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000102938;
    } else {
      sum += 0.000102938;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000102938;
    } else {
      sum += -0.000102938;
    }
  }
  // tree 921
  if ( features[5] < 0.329645 ) {
    if ( features[8] < 2.38187 ) {
      sum += -0.000139408;
    } else {
      sum += 0.000139408;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000139408;
    } else {
      sum += 0.000139408;
    }
  }
  // tree 922
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000115301;
    } else {
      sum += -0.000115301;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000115301;
    } else {
      sum += 0.000115301;
    }
  }
  // tree 923
  if ( features[3] < 1.04065 ) {
    if ( features[8] < 2.67103 ) {
      sum += -0.000159432;
    } else {
      sum += 0.000159432;
    }
  } else {
    if ( features[6] < 1.65196 ) {
      sum += 0.000159432;
    } else {
      sum += -0.000159432;
    }
  }
  // tree 924
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000102937;
    } else {
      sum += -0.000102937;
    }
  } else {
    if ( features[7] < 4.81007 ) {
      sum += 0.000102937;
    } else {
      sum += -0.000102937;
    }
  }
  // tree 925
  if ( features[8] < 2.38156 ) {
    if ( features[8] < 2.13252 ) {
      sum += -8.49408e-05;
    } else {
      sum += 8.49408e-05;
    }
  } else {
    if ( features[7] < 5.04875 ) {
      sum += 8.49408e-05;
    } else {
      sum += -8.49408e-05;
    }
  }
  // tree 926
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -0.000102575;
    } else {
      sum += 0.000102575;
    }
  } else {
    if ( features[7] < 4.81007 ) {
      sum += 0.000102575;
    } else {
      sum += -0.000102575;
    }
  }
  // tree 927
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000112362;
    } else {
      sum += -0.000112362;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.000112362;
    } else {
      sum += -0.000112362;
    }
  }
  // tree 928
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000102521;
    } else {
      sum += -0.000102521;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000102521;
    } else {
      sum += -0.000102521;
    }
  }
  // tree 929
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -0.000102379;
    } else {
      sum += 0.000102379;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000102379;
    } else {
      sum += -0.000102379;
    }
  }
  // tree 930
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000102394;
    } else {
      sum += 0.000102394;
    }
  } else {
    if ( features[7] < 5.04875 ) {
      sum += 0.000102394;
    } else {
      sum += -0.000102394;
    }
  }
  // tree 931
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000114173;
    } else {
      sum += -0.000114173;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000114173;
    } else {
      sum += 0.000114173;
    }
  }
  // tree 932
  if ( features[8] < 2.38156 ) {
    if ( features[5] < 1.0889 ) {
      sum += -8.84933e-05;
    } else {
      sum += 8.84933e-05;
    }
  } else {
    if ( features[1] < -0.185621 ) {
      sum += -8.84933e-05;
    } else {
      sum += 8.84933e-05;
    }
  }
  // tree 933
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000173891;
    } else {
      sum += -0.000173891;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000173891;
    } else {
      sum += 0.000173891;
    }
  }
  // tree 934
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -0.000101922;
    } else {
      sum += 0.000101922;
    }
  } else {
    sum += 0.000101922;
  }
  // tree 935
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -0.000101454;
    } else {
      sum += 0.000101454;
    }
  } else {
    sum += 0.000101454;
  }
  // tree 936
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -0.000101816;
    } else {
      sum += 0.000101816;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 0.000101816;
    } else {
      sum += -0.000101816;
    }
  }
  // tree 937
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 0.000101376;
    } else {
      sum += -0.000101376;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000101376;
    } else {
      sum += -0.000101376;
    }
  }
  // tree 938
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000113726;
    } else {
      sum += -0.000113726;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000113726;
    } else {
      sum += 0.000113726;
    }
  }
  // tree 939
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000101369;
    } else {
      sum += 0.000101369;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 0.000101369;
    } else {
      sum += -0.000101369;
    }
  }
  // tree 940
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 0.000113104;
    } else {
      sum += -0.000113104;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000113104;
    } else {
      sum += 0.000113104;
    }
  }
  // tree 941
  if ( features[4] < -3.0468 ) {
    sum += -9.31371e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -9.31371e-05;
    } else {
      sum += 9.31371e-05;
    }
  }
  // tree 942
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000102103;
    } else {
      sum += 0.000102103;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000102103;
    } else {
      sum += -0.000102103;
    }
  }
  // tree 943
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -0.000101666;
    } else {
      sum += 0.000101666;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000101666;
    } else {
      sum += -0.000101666;
    }
  }
  // tree 944
  if ( features[0] < 3.03054 ) {
    if ( features[6] < 2.41557 ) {
      sum += -0.000109573;
    } else {
      sum += 0.000109573;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000109573;
    } else {
      sum += 0.000109573;
    }
  }
  // tree 945
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000101459;
    } else {
      sum += -0.000101459;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000101459;
    } else {
      sum += -0.000101459;
    }
  }
  // tree 946
  if ( features[0] < 3.03054 ) {
    if ( features[6] < 2.41557 ) {
      sum += -0.000101395;
    } else {
      sum += 0.000101395;
    }
  } else {
    sum += 0.000101395;
  }
  // tree 947
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -0.000101241;
    } else {
      sum += 0.000101241;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000101241;
    } else {
      sum += -0.000101241;
    }
  }
  // tree 948
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000101191;
    } else {
      sum += 0.000101191;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 0.000101191;
    } else {
      sum += -0.000101191;
    }
  }
  // tree 949
  if ( features[2] < -0.974311 ) {
    sum += -7.13353e-05;
  } else {
    if ( features[6] < 2.32779 ) {
      sum += -7.13353e-05;
    } else {
      sum += 7.13353e-05;
    }
  }
  // tree 950
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000114878;
    } else {
      sum += 0.000114878;
    }
  } else {
    sum += 0.000114878;
  }
  // tree 951
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 0.000149365;
    } else {
      sum += -0.000149365;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000149365;
    } else {
      sum += 0.000149365;
    }
  }
  // tree 952
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000101172;
    } else {
      sum += -0.000101172;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000101172;
    } else {
      sum += -0.000101172;
    }
  }
  // tree 953
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -0.000101342;
    } else {
      sum += 0.000101342;
    }
  } else {
    if ( features[7] < 5.04875 ) {
      sum += 0.000101342;
    } else {
      sum += -0.000101342;
    }
  }
  // tree 954
  if ( features[4] < -3.0468 ) {
    sum += -9.30997e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -9.30997e-05;
    } else {
      sum += 9.30997e-05;
    }
  }
  // tree 955
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 0.000100735;
    } else {
      sum += -0.000100735;
    }
  } else {
    if ( features[0] < 1.64276 ) {
      sum += -0.000100735;
    } else {
      sum += 0.000100735;
    }
  }
  // tree 956
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000182711;
    } else {
      sum += -0.000182711;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000182711;
    } else {
      sum += 0.000182711;
    }
  }
  // tree 957
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000100317;
    } else {
      sum += -0.000100317;
    }
  } else {
    sum += 0.000100317;
  }
  // tree 958
  if ( features[3] < 1.04065 ) {
    if ( features[1] < -0.0677344 ) {
      sum += -0.000129795;
    } else {
      sum += 0.000129795;
    }
  } else {
    if ( features[1] < -0.259795 ) {
      sum += 0.000129795;
    } else {
      sum += -0.000129795;
    }
  }
  // tree 959
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 0.000120118;
    } else {
      sum += -0.000120118;
    }
  } else {
    if ( features[5] < 0.893056 ) {
      sum += -0.000120118;
    } else {
      sum += 0.000120118;
    }
  }
  // tree 960
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000181646;
    } else {
      sum += -0.000181646;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000181646;
    } else {
      sum += 0.000181646;
    }
  }
  // tree 961
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000100677;
    } else {
      sum += 0.000100677;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000100677;
    } else {
      sum += -0.000100677;
    }
  }
  // tree 962
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -0.000146657;
    } else {
      sum += 0.000146657;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -0.000146657;
    } else {
      sum += 0.000146657;
    }
  }
  // tree 963
  if ( features[4] < -3.0468 ) {
    sum += -6.62374e-05;
  } else {
    if ( features[6] < 2.15225 ) {
      sum += -6.62374e-05;
    } else {
      sum += 6.62374e-05;
    }
  }
  // tree 964
  if ( features[8] < 2.38156 ) {
    if ( features[6] < 1.62551 ) {
      sum += 9.54456e-05;
    } else {
      sum += -9.54456e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 9.54456e-05;
    } else {
      sum += -9.54456e-05;
    }
  }
  // tree 965
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 0.000150535;
    } else {
      sum += -0.000150535;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000150535;
    } else {
      sum += 0.000150535;
    }
  }
  // tree 966
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000100612;
    } else {
      sum += 0.000100612;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 0.000100612;
    } else {
      sum += -0.000100612;
    }
  }
  // tree 967
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 0.000142103;
    } else {
      sum += -0.000142103;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000142103;
    } else {
      sum += 0.000142103;
    }
  }
  // tree 968
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000100424;
    } else {
      sum += -0.000100424;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 0.000100424;
    } else {
      sum += -0.000100424;
    }
  }
  // tree 969
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000120311;
    } else {
      sum += -0.000120311;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000120311;
    } else {
      sum += -0.000120311;
    }
  }
  // tree 970
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000105447;
    } else {
      sum += -0.000105447;
    }
  } else {
    sum += 0.000105447;
  }
  // tree 971
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000100595;
    } else {
      sum += 0.000100595;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000100595;
    } else {
      sum += -0.000100595;
    }
  }
  // tree 972
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000142245;
    } else {
      sum += -0.000142245;
    }
  } else {
    if ( features[6] < 2.67895 ) {
      sum += -0.000142245;
    } else {
      sum += 0.000142245;
    }
  }
  // tree 973
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000100842;
    } else {
      sum += -0.000100842;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000100842;
    } else {
      sum += -0.000100842;
    }
  }
  // tree 974
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -0.000100494;
    } else {
      sum += 0.000100494;
    }
  } else {
    if ( features[7] < 5.04875 ) {
      sum += 0.000100494;
    } else {
      sum += -0.000100494;
    }
  }
  // tree 975
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -0.000112762;
    } else {
      sum += 0.000112762;
    }
  } else {
    sum += 0.000112762;
  }
  // tree 976
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000100074;
    } else {
      sum += 0.000100074;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 0.000100074;
    } else {
      sum += -0.000100074;
    }
  }
  // tree 977
  if ( features[3] < 1.04065 ) {
    if ( features[1] < -0.0677344 ) {
      sum += -0.000128655;
    } else {
      sum += 0.000128655;
    }
  } else {
    if ( features[1] < -0.259795 ) {
      sum += 0.000128655;
    } else {
      sum += -0.000128655;
    }
  }
  // tree 978
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000100501;
    } else {
      sum += -0.000100501;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000100501;
    } else {
      sum += -0.000100501;
    }
  }
  // tree 979
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.98049e-05;
    } else {
      sum += 9.98049e-05;
    }
  } else {
    sum += 9.98049e-05;
  }
  // tree 980
  if ( features[0] < 3.03054 ) {
    if ( features[8] < 2.12266 ) {
      sum += -0.000105276;
    } else {
      sum += 0.000105276;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.000105276;
    } else {
      sum += -0.000105276;
    }
  }
  // tree 981
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 0.000100413;
    } else {
      sum += -0.000100413;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 0.000100413;
    } else {
      sum += -0.000100413;
    }
  }
  // tree 982
  if ( features[0] < 3.03054 ) {
    if ( features[6] < 2.41557 ) {
      sum += -9.99709e-05;
    } else {
      sum += 9.99709e-05;
    }
  } else {
    sum += 9.99709e-05;
  }
  // tree 983
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000116516;
    } else {
      sum += 0.000116516;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -0.000116516;
    } else {
      sum += 0.000116516;
    }
  }
  // tree 984
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000179844;
    } else {
      sum += -0.000179844;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000179844;
    } else {
      sum += 0.000179844;
    }
  }
  // tree 985
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000171512;
    } else {
      sum += -0.000171512;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000171512;
    } else {
      sum += 0.000171512;
    }
  }
  // tree 986
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000178608;
    } else {
      sum += -0.000178608;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000178608;
    } else {
      sum += 0.000178608;
    }
  }
  // tree 987
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -0.000100433;
    } else {
      sum += 0.000100433;
    }
  } else {
    if ( features[3] < 1.12549 ) {
      sum += 0.000100433;
    } else {
      sum += -0.000100433;
    }
  }
  // tree 988
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000104905;
    } else {
      sum += -0.000104905;
    }
  } else {
    sum += 0.000104905;
  }
  // tree 989
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000119128;
    } else {
      sum += 0.000119128;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000119128;
    } else {
      sum += -0.000119128;
    }
  }
  // tree 990
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 0.000100096;
    } else {
      sum += -0.000100096;
    }
  } else {
    if ( features[3] < 1.12549 ) {
      sum += 0.000100096;
    } else {
      sum += -0.000100096;
    }
  }
  // tree 991
  if ( features[2] < -0.974311 ) {
    sum += -8.321e-05;
  } else {
    if ( features[8] < 2.38156 ) {
      sum += -8.321e-05;
    } else {
      sum += 8.321e-05;
    }
  }
  // tree 992
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000116411;
    } else {
      sum += -0.000116411;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -0.000116411;
    } else {
      sum += 0.000116411;
    }
  }
  // tree 993
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.97379e-05;
    } else {
      sum += -9.97379e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.97379e-05;
    } else {
      sum += -9.97379e-05;
    }
  }
  // tree 994
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.98873e-05;
    } else {
      sum += 9.98873e-05;
    }
  } else {
    sum += 9.98873e-05;
  }
  // tree 995
  if ( features[1] < 0.309319 ) {
    if ( features[5] < 0.329645 ) {
      sum += 0.000103724;
    } else {
      sum += -0.000103724;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000103724;
    } else {
      sum += -0.000103724;
    }
  }
  // tree 996
  if ( features[4] < -3.0468 ) {
    sum += -9.18332e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -9.18332e-05;
    } else {
      sum += 9.18332e-05;
    }
  }
  // tree 997
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -0.000117542;
    } else {
      sum += 0.000117542;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000117542;
    } else {
      sum += -0.000117542;
    }
  }
  // tree 998
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000115802;
    } else {
      sum += -0.000115802;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -0.000115802;
    } else {
      sum += 0.000115802;
    }
  }
  // tree 999
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000177817;
    } else {
      sum += -0.000177817;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000177817;
    } else {
      sum += 0.000177817;
    }
  }
  // tree 1000
  if ( features[4] < -3.0468 ) {
    sum += -6.27682e-05;
  } else {
    if ( features[1] < -0.0875134 ) {
      sum += -6.27682e-05;
    } else {
      sum += 6.27682e-05;
    }
  }
  // tree 1001
  if ( features[1] < -0.067765 ) {
    if ( features[6] < 1.63591 ) {
      sum += 0.000156603;
    } else {
      sum += -0.000156603;
    }
  } else {
    if ( features[3] < 0.96687 ) {
      sum += 0.000156603;
    } else {
      sum += -0.000156603;
    }
  }
  // tree 1002
  if ( features[4] < -3.0468 ) {
    sum += -9.14783e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -9.14783e-05;
    } else {
      sum += 9.14783e-05;
    }
  }
  // tree 1003
  if ( features[3] < 1.04065 ) {
    if ( features[8] < 2.67103 ) {
      sum += -0.000144631;
    } else {
      sum += 0.000144631;
    }
  } else {
    if ( features[5] < 0.730972 ) {
      sum += 0.000144631;
    } else {
      sum += -0.000144631;
    }
  }
  // tree 1004
  if ( features[7] < 3.73601 ) {
    sum += -7.39812e-05;
  } else {
    if ( features[3] < 1.04065 ) {
      sum += 7.39812e-05;
    } else {
      sum += -7.39812e-05;
    }
  }
  // tree 1005
  if ( features[4] < -3.0468 ) {
    sum += -6.58237e-05;
  } else {
    if ( features[6] < 2.15225 ) {
      sum += -6.58237e-05;
    } else {
      sum += 6.58237e-05;
    }
  }
  // tree 1006
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.99761e-05;
    } else {
      sum += -9.99761e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.99761e-05;
    } else {
      sum += -9.99761e-05;
    }
  }
  // tree 1007
  if ( features[4] < -3.0468 ) {
    sum += -7.58e-05;
  } else {
    if ( features[5] < 1.13208 ) {
      sum += 7.58e-05;
    } else {
      sum += -7.58e-05;
    }
  }
  // tree 1008
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.97295e-05;
    } else {
      sum += 9.97295e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.97295e-05;
    } else {
      sum += -9.97295e-05;
    }
  }
  // tree 1009
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.96085e-05;
    } else {
      sum += 9.96085e-05;
    }
  } else {
    sum += 9.96085e-05;
  }
  // tree 1010
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.93567e-05;
    } else {
      sum += -9.93567e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.93567e-05;
    } else {
      sum += -9.93567e-05;
    }
  }
  // tree 1011
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 0.000118184;
    } else {
      sum += -0.000118184;
    }
  } else {
    if ( features[5] < 0.893056 ) {
      sum += -0.000118184;
    } else {
      sum += 0.000118184;
    }
  }
  // tree 1012
  if ( features[7] < 3.73601 ) {
    sum += -7.33542e-05;
  } else {
    if ( features[3] < 1.04065 ) {
      sum += 7.33542e-05;
    } else {
      sum += -7.33542e-05;
    }
  }
  // tree 1013
  if ( features[0] < 3.03054 ) {
    if ( features[2] < 0.956816 ) {
      sum += -9.48885e-05;
    } else {
      sum += 9.48885e-05;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 9.48885e-05;
    } else {
      sum += -9.48885e-05;
    }
  }
  // tree 1014
  if ( features[4] < -3.0468 ) {
    sum += -9.11474e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -9.11474e-05;
    } else {
      sum += 9.11474e-05;
    }
  }
  // tree 1015
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.91776e-05;
    } else {
      sum += 9.91776e-05;
    }
  } else {
    sum += 9.91776e-05;
  }
  // tree 1016
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.91791e-05;
    } else {
      sum += -9.91791e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.91791e-05;
    } else {
      sum += -9.91791e-05;
    }
  }
  // tree 1017
  if ( features[4] < -3.0468 ) {
    sum += -9.04542e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -9.04542e-05;
    } else {
      sum += 9.04542e-05;
    }
  }
  // tree 1018
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.9297e-05;
    } else {
      sum += 9.9297e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.9297e-05;
    } else {
      sum += -9.9297e-05;
    }
  }
  // tree 1019
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000112844;
    } else {
      sum += -0.000112844;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000112844;
    } else {
      sum += 0.000112844;
    }
  }
  // tree 1020
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.87183e-05;
    } else {
      sum += 9.87183e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.87183e-05;
    } else {
      sum += -9.87183e-05;
    }
  }
  // tree 1021
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.88166e-05;
    } else {
      sum += 9.88166e-05;
    }
  } else {
    sum += 9.88166e-05;
  }
  // tree 1022
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.85546e-05;
    } else {
      sum += -9.85546e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 9.85546e-05;
    } else {
      sum += -9.85546e-05;
    }
  }
  // tree 1023
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000112175;
    } else {
      sum += -0.000112175;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000112175;
    } else {
      sum += 0.000112175;
    }
  }
  // tree 1024
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.83471e-05;
    } else {
      sum += 9.83471e-05;
    }
  } else {
    sum += 9.83471e-05;
  }
  // tree 1025
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000118331;
    } else {
      sum += 0.000118331;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000118331;
    } else {
      sum += -0.000118331;
    }
  }
  // tree 1026
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 0.000109721;
    } else {
      sum += -0.000109721;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000109721;
    } else {
      sum += 0.000109721;
    }
  }
  // tree 1027
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.84465e-05;
    } else {
      sum += 9.84465e-05;
    }
  } else {
    if ( features[7] < 5.04875 ) {
      sum += 9.84465e-05;
    } else {
      sum += -9.84465e-05;
    }
  }
  // tree 1028
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000177344;
    } else {
      sum += -0.000177344;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000177344;
    } else {
      sum += 0.000177344;
    }
  }
  // tree 1029
  if ( features[3] < 1.04065 ) {
    if ( features[7] < 4.92941 ) {
      sum += 0.000113691;
    } else {
      sum += -0.000113691;
    }
  } else {
    if ( features[1] < -0.259795 ) {
      sum += 0.000113691;
    } else {
      sum += -0.000113691;
    }
  }
  // tree 1030
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.82579e-05;
    } else {
      sum += -9.82579e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.82579e-05;
    } else {
      sum += -9.82579e-05;
    }
  }
  // tree 1031
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -0.000144598;
    } else {
      sum += 0.000144598;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -0.000144598;
    } else {
      sum += 0.000144598;
    }
  }
  // tree 1032
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000117427;
    } else {
      sum += 0.000117427;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000117427;
    } else {
      sum += -0.000117427;
    }
  }
  // tree 1033
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.83901e-05;
    } else {
      sum += 9.83901e-05;
    }
  } else {
    sum += 9.83901e-05;
  }
  // tree 1034
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.78906e-05;
    } else {
      sum += 9.78906e-05;
    }
  } else {
    sum += 9.78906e-05;
  }
  // tree 1035
  if ( features[6] < 2.32779 ) {
    if ( features[5] < 0.245244 ) {
      sum += 0.000129455;
    } else {
      sum += -0.000129455;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -0.000129455;
    } else {
      sum += 0.000129455;
    }
  }
  // tree 1036
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.76522e-05;
    } else {
      sum += 9.76522e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 9.76522e-05;
    } else {
      sum += -9.76522e-05;
    }
  }
  // tree 1037
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.76737e-05;
    } else {
      sum += 9.76737e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.76737e-05;
    } else {
      sum += -9.76737e-05;
    }
  }
  // tree 1038
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 9.78588e-05;
    } else {
      sum += -9.78588e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.78588e-05;
    } else {
      sum += -9.78588e-05;
    }
  }
  // tree 1039
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.78862e-05;
    } else {
      sum += -9.78862e-05;
    }
  } else {
    sum += 9.78862e-05;
  }
  // tree 1040
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 9.749e-05;
    } else {
      sum += -9.749e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.749e-05;
    } else {
      sum += -9.749e-05;
    }
  }
  // tree 1041
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000176641;
    } else {
      sum += -0.000176641;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000176641;
    } else {
      sum += 0.000176641;
    }
  }
  // tree 1042
  if ( features[1] < -0.067765 ) {
    if ( features[6] < 1.63591 ) {
      sum += 0.0001554;
    } else {
      sum += -0.0001554;
    }
  } else {
    if ( features[3] < 0.96687 ) {
      sum += 0.0001554;
    } else {
      sum += -0.0001554;
    }
  }
  // tree 1043
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.77766e-05;
    } else {
      sum += 9.77766e-05;
    }
  } else {
    sum += 9.77766e-05;
  }
  // tree 1044
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.00011803;
    } else {
      sum += -0.00011803;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.00011803;
    } else {
      sum += -0.00011803;
    }
  }
  // tree 1045
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.78553e-05;
    } else {
      sum += -9.78553e-05;
    }
  } else {
    sum += 9.78553e-05;
  }
  // tree 1046
  if ( features[2] < -0.974311 ) {
    sum += -8.1681e-05;
  } else {
    if ( features[8] < 2.38156 ) {
      sum += -8.1681e-05;
    } else {
      sum += 8.1681e-05;
    }
  }
  // tree 1047
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.72862e-05;
    } else {
      sum += 9.72862e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.72862e-05;
    } else {
      sum += -9.72862e-05;
    }
  }
  // tree 1048
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.71286e-05;
    } else {
      sum += -9.71286e-05;
    }
  } else {
    sum += 9.71286e-05;
  }
  // tree 1049
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.74e-05;
    } else {
      sum += 9.74e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.74e-05;
    } else {
      sum += -9.74e-05;
    }
  }
  // tree 1050
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.70275e-05;
    } else {
      sum += 9.70275e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.70275e-05;
    } else {
      sum += -9.70275e-05;
    }
  }
  // tree 1051
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 0.000106593;
    } else {
      sum += -0.000106593;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.000106593;
    } else {
      sum += -0.000106593;
    }
  }
  // tree 1052
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000111858;
    } else {
      sum += -0.000111858;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000111858;
    } else {
      sum += 0.000111858;
    }
  }
  // tree 1053
  if ( features[5] < 0.329645 ) {
    if ( features[3] < 0.421425 ) {
      sum += 0.000112245;
    } else {
      sum += -0.000112245;
    }
  } else {
    if ( features[5] < 0.893056 ) {
      sum += -0.000112245;
    } else {
      sum += 0.000112245;
    }
  }
  // tree 1054
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.73882e-05;
    } else {
      sum += -9.73882e-05;
    }
  } else {
    sum += 9.73882e-05;
  }
  // tree 1055
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -9.70024e-05;
    } else {
      sum += 9.70024e-05;
    }
  } else {
    sum += 9.70024e-05;
  }
  // tree 1056
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000117845;
    } else {
      sum += -0.000117845;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000117845;
    } else {
      sum += -0.000117845;
    }
  }
  // tree 1057
  if ( features[8] < 2.38156 ) {
    if ( features[0] < 2.06819 ) {
      sum += 9.36537e-05;
    } else {
      sum += -9.36537e-05;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 9.36537e-05;
    } else {
      sum += -9.36537e-05;
    }
  }
  // tree 1058
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.66687e-05;
    } else {
      sum += 9.66687e-05;
    }
  } else {
    sum += 9.66687e-05;
  }
  // tree 1059
  if ( features[1] < 0.309319 ) {
    if ( features[8] < 3.21289 ) {
      sum += -0.000100469;
    } else {
      sum += 0.000100469;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -0.000100469;
    } else {
      sum += 0.000100469;
    }
  }
  // tree 1060
  if ( features[4] < -3.0468 ) {
    sum += -8.94266e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -8.94266e-05;
    } else {
      sum += 8.94266e-05;
    }
  }
  // tree 1061
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.00010911;
    } else {
      sum += -0.00010911;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.00010911;
    } else {
      sum += -0.00010911;
    }
  }
  // tree 1062
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.61946e-05;
    } else {
      sum += 9.61946e-05;
    }
  } else {
    sum += 9.61946e-05;
  }
  // tree 1063
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.71166e-05;
    } else {
      sum += 9.71166e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.71166e-05;
    } else {
      sum += -9.71166e-05;
    }
  }
  // tree 1064
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.66087e-05;
    } else {
      sum += -9.66087e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.66087e-05;
    } else {
      sum += -9.66087e-05;
    }
  }
  // tree 1065
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000111;
    } else {
      sum += 0.000111;
    }
  } else {
    sum += 0.000111;
  }
  // tree 1066
  if ( features[4] < -3.0468 ) {
    sum += -8.5925e-05;
  } else {
    if ( features[7] < 3.73601 ) {
      sum += -8.5925e-05;
    } else {
      sum += 8.5925e-05;
    }
  }
  // tree 1067
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000108463;
    } else {
      sum += -0.000108463;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.000108463;
    } else {
      sum += -0.000108463;
    }
  }
  // tree 1068
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 0.00010022;
    } else {
      sum += -0.00010022;
    }
  } else {
    sum += 0.00010022;
  }
  // tree 1069
  if ( features[8] < 2.38156 ) {
    sum += -7.95077e-05;
  } else {
    sum += 7.95077e-05;
  }
  // tree 1070
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.00011735;
    } else {
      sum += -0.00011735;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.00011735;
    } else {
      sum += -0.00011735;
    }
  }
  // tree 1071
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.62036e-05;
    } else {
      sum += 9.62036e-05;
    }
  } else {
    sum += 9.62036e-05;
  }
  // tree 1072
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -9.5903e-05;
    } else {
      sum += 9.5903e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 9.5903e-05;
    } else {
      sum += -9.5903e-05;
    }
  }
  // tree 1073
  if ( features[1] < -0.067765 ) {
    if ( features[0] < 2.68767 ) {
      sum += -0.000102105;
    } else {
      sum += 0.000102105;
    }
  } else {
    if ( features[6] < 2.15257 ) {
      sum += -0.000102105;
    } else {
      sum += 0.000102105;
    }
  }
  // tree 1074
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.61761e-05;
    } else {
      sum += 9.61761e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.61761e-05;
    } else {
      sum += -9.61761e-05;
    }
  }
  // tree 1075
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 0.000143095;
    } else {
      sum += -0.000143095;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000143095;
    } else {
      sum += 0.000143095;
    }
  }
  // tree 1076
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000110301;
    } else {
      sum += -0.000110301;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000110301;
    } else {
      sum += 0.000110301;
    }
  }
  // tree 1077
  if ( features[1] < -0.067765 ) {
    if ( features[3] < 0.87759 ) {
      sum += -0.000146193;
    } else {
      sum += 0.000146193;
    }
  } else {
    if ( features[3] < 0.96687 ) {
      sum += 0.000146193;
    } else {
      sum += -0.000146193;
    }
  }
  // tree 1078
  if ( features[8] < 2.38156 ) {
    if ( features[0] < 2.06819 ) {
      sum += 9.27954e-05;
    } else {
      sum += -9.27954e-05;
    }
  } else {
    sum += 9.27954e-05;
  }
  // tree 1079
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000109665;
    } else {
      sum += -0.000109665;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000109665;
    } else {
      sum += 0.000109665;
    }
  }
  // tree 1080
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.64042e-05;
    } else {
      sum += -9.64042e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.64042e-05;
    } else {
      sum += -9.64042e-05;
    }
  }
  // tree 1081
  if ( features[4] < -3.0468 ) {
    sum += -8.91282e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -8.91282e-05;
    } else {
      sum += 8.91282e-05;
    }
  }
  // tree 1082
  if ( features[1] < -0.067765 ) {
    if ( features[3] < 0.87759 ) {
      sum += -0.000145434;
    } else {
      sum += 0.000145434;
    }
  } else {
    if ( features[3] < 0.96687 ) {
      sum += 0.000145434;
    } else {
      sum += -0.000145434;
    }
  }
  // tree 1083
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.601e-05;
    } else {
      sum += -9.601e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.601e-05;
    } else {
      sum += -9.601e-05;
    }
  }
  // tree 1084
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.00017515;
    } else {
      sum += -0.00017515;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.00017515;
    } else {
      sum += 0.00017515;
    }
  }
  // tree 1085
  if ( features[1] < -0.067765 ) {
    if ( features[3] < 0.87759 ) {
      sum += -0.000133709;
    } else {
      sum += 0.000133709;
    }
  } else {
    if ( features[6] < 2.15257 ) {
      sum += -0.000133709;
    } else {
      sum += 0.000133709;
    }
  }
  // tree 1086
  if ( features[5] < 0.329645 ) {
    sum += 0.000128013;
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000128013;
    } else {
      sum += 0.000128013;
    }
  }
  // tree 1087
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.57573e-05;
    } else {
      sum += 9.57573e-05;
    }
  } else {
    if ( features[0] < 1.64276 ) {
      sum += -9.57573e-05;
    } else {
      sum += 9.57573e-05;
    }
  }
  // tree 1088
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000109408;
    } else {
      sum += -0.000109408;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000109408;
    } else {
      sum += 0.000109408;
    }
  }
  // tree 1089
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.5762e-05;
    } else {
      sum += -9.5762e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.5762e-05;
    } else {
      sum += -9.5762e-05;
    }
  }
  // tree 1090
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000108739;
    } else {
      sum += -0.000108739;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000108739;
    } else {
      sum += 0.000108739;
    }
  }
  // tree 1091
  if ( features[4] < -3.0468 ) {
    sum += -8.88556e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -8.88556e-05;
    } else {
      sum += 8.88556e-05;
    }
  }
  // tree 1092
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.55258e-05;
    } else {
      sum += 9.55258e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.55258e-05;
    } else {
      sum += -9.55258e-05;
    }
  }
  // tree 1093
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.5499e-05;
    } else {
      sum += 9.5499e-05;
    }
  } else {
    if ( features[0] < 1.64276 ) {
      sum += -9.5499e-05;
    } else {
      sum += 9.5499e-05;
    }
  }
  // tree 1094
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 9.55065e-05;
    } else {
      sum += -9.55065e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.55065e-05;
    } else {
      sum += -9.55065e-05;
    }
  }
  // tree 1095
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.51973e-05;
    } else {
      sum += 9.51973e-05;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 9.51973e-05;
    } else {
      sum += -9.51973e-05;
    }
  }
  // tree 1096
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.00011338;
    } else {
      sum += 0.00011338;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -0.00011338;
    } else {
      sum += 0.00011338;
    }
  }
  // tree 1097
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 9.93693e-05;
    } else {
      sum += -9.93693e-05;
    }
  } else {
    sum += 9.93693e-05;
  }
  // tree 1098
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.51443e-05;
    } else {
      sum += 9.51443e-05;
    }
  } else {
    sum += 9.51443e-05;
  }
  // tree 1099
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.51777e-05;
    } else {
      sum += 9.51777e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.51777e-05;
    } else {
      sum += -9.51777e-05;
    }
  }
  // tree 1100
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.47003e-05;
    } else {
      sum += 9.47003e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.47003e-05;
    } else {
      sum += -9.47003e-05;
    }
  }
  // tree 1101
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000113722;
    } else {
      sum += -0.000113722;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -0.000113722;
    } else {
      sum += 0.000113722;
    }
  }
  // tree 1102
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000173992;
    } else {
      sum += -0.000173992;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000173992;
    } else {
      sum += 0.000173992;
    }
  }
  // tree 1103
  if ( features[5] < 0.329645 ) {
    if ( features[3] < 0.421425 ) {
      sum += 0.000138695;
    } else {
      sum += -0.000138695;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000138695;
    } else {
      sum += 0.000138695;
    }
  }
  // tree 1104
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.66168 ) {
      sum += -9.48675e-05;
    } else {
      sum += 9.48675e-05;
    }
  } else {
    sum += 9.48675e-05;
  }
  // tree 1105
  if ( features[4] < -3.0468 ) {
    sum += -6.198e-05;
  } else {
    if ( features[1] < -0.0875134 ) {
      sum += -6.198e-05;
    } else {
      sum += 6.198e-05;
    }
  }
  // tree 1106
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.52671e-05;
    } else {
      sum += -9.52671e-05;
    }
  } else {
    sum += 9.52671e-05;
  }
  // tree 1107
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.42529e-05;
    } else {
      sum += 9.42529e-05;
    }
  } else {
    sum += 9.42529e-05;
  }
  // tree 1108
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.45627e-05;
    } else {
      sum += 9.45627e-05;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 9.45627e-05;
    } else {
      sum += -9.45627e-05;
    }
  }
  // tree 1109
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.44633e-05;
    } else {
      sum += -9.44633e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.44633e-05;
    } else {
      sum += -9.44633e-05;
    }
  }
  // tree 1110
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 9.50781e-05;
    } else {
      sum += -9.50781e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.50781e-05;
    } else {
      sum += -9.50781e-05;
    }
  }
  // tree 1111
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000113444;
    } else {
      sum += -0.000113444;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -0.000113444;
    } else {
      sum += 0.000113444;
    }
  }
  // tree 1112
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 0.000113127;
    } else {
      sum += -0.000113127;
    }
  } else {
    if ( features[1] < -0.53912 ) {
      sum += 0.000113127;
    } else {
      sum += -0.000113127;
    }
  }
  // tree 1113
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.46967e-05;
    } else {
      sum += 9.46967e-05;
    }
  } else {
    sum += 9.46967e-05;
  }
  // tree 1114
  if ( features[1] < -0.067765 ) {
    if ( features[1] < -0.779359 ) {
      sum += 0.000145498;
    } else {
      sum += -0.000145498;
    }
  } else {
    if ( features[3] < 0.96687 ) {
      sum += 0.000145498;
    } else {
      sum += -0.000145498;
    }
  }
  // tree 1115
  if ( features[0] < 3.03054 ) {
    if ( features[4] < -1.10944 ) {
      sum += 9.58253e-05;
    } else {
      sum += -9.58253e-05;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -9.58253e-05;
    } else {
      sum += 9.58253e-05;
    }
  }
  // tree 1116
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000106238;
    } else {
      sum += -0.000106238;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.000106238;
    } else {
      sum += -0.000106238;
    }
  }
  // tree 1117
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.43968e-05;
    } else {
      sum += 9.43968e-05;
    }
  } else {
    if ( features[0] < 1.64276 ) {
      sum += -9.43968e-05;
    } else {
      sum += 9.43968e-05;
    }
  }
  // tree 1118
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.40463e-05;
    } else {
      sum += -9.40463e-05;
    }
  } else {
    sum += 9.40463e-05;
  }
  // tree 1119
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.44808e-05;
    } else {
      sum += 9.44808e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.44808e-05;
    } else {
      sum += -9.44808e-05;
    }
  }
  // tree 1120
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.38149e-05;
    } else {
      sum += -9.38149e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 9.38149e-05;
    } else {
      sum += -9.38149e-05;
    }
  }
  // tree 1121
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.3698e-05;
    } else {
      sum += 9.3698e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -9.3698e-05;
    } else {
      sum += 9.3698e-05;
    }
  }
  // tree 1122
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -9.35387e-05;
    } else {
      sum += 9.35387e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.35387e-05;
    } else {
      sum += -9.35387e-05;
    }
  }
  // tree 1123
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.00010777;
    } else {
      sum += -0.00010777;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.00010777;
    } else {
      sum += 0.00010777;
    }
  }
  // tree 1124
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.39115e-05;
    } else {
      sum += 9.39115e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.39115e-05;
    } else {
      sum += -9.39115e-05;
    }
  }
  // tree 1125
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 9.35729e-05;
    } else {
      sum += -9.35729e-05;
    }
  } else {
    sum += 9.35729e-05;
  }
  // tree 1126
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -9.36657e-05;
    } else {
      sum += 9.36657e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.36657e-05;
    } else {
      sum += -9.36657e-05;
    }
  }
  // tree 1127
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000105108;
    } else {
      sum += -0.000105108;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.000105108;
    } else {
      sum += -0.000105108;
    }
  }
  // tree 1128
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.33625e-05;
    } else {
      sum += 9.33625e-05;
    }
  } else {
    sum += 9.33625e-05;
  }
  // tree 1129
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.34536e-05;
    } else {
      sum += 9.34536e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.34536e-05;
    } else {
      sum += -9.34536e-05;
    }
  }
  // tree 1130
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 9.3276e-05;
    } else {
      sum += -9.3276e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -9.3276e-05;
    } else {
      sum += 9.3276e-05;
    }
  }
  // tree 1131
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.31229e-05;
    } else {
      sum += 9.31229e-05;
    }
  } else {
    if ( features[0] < 1.64276 ) {
      sum += -9.31229e-05;
    } else {
      sum += 9.31229e-05;
    }
  }
  // tree 1132
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -0.000114741;
    } else {
      sum += 0.000114741;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000114741;
    } else {
      sum += -0.000114741;
    }
  }
  // tree 1133
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000110373;
    } else {
      sum += -0.000110373;
    }
  } else {
    sum += 0.000110373;
  }
  // tree 1134
  if ( features[4] < -3.0468 ) {
    sum += -8.7625e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -8.7625e-05;
    } else {
      sum += 8.7625e-05;
    }
  }
  // tree 1135
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000145124;
    } else {
      sum += -0.000145124;
    }
  } else {
    if ( features[5] < 0.893056 ) {
      sum += -0.000145124;
    } else {
      sum += 0.000145124;
    }
  }
  // tree 1136
  if ( features[8] < 2.38156 ) {
    sum += -7.85978e-05;
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 7.85978e-05;
    } else {
      sum += -7.85978e-05;
    }
  }
  // tree 1137
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000104692;
    } else {
      sum += -0.000104692;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.000104692;
    } else {
      sum += -0.000104692;
    }
  }
  // tree 1138
  if ( features[4] < -3.0468 ) {
    sum += -8.74036e-05;
  } else {
    if ( features[8] < 2.18859 ) {
      sum += -8.74036e-05;
    } else {
      sum += 8.74036e-05;
    }
  }
  // tree 1139
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.32874e-05;
    } else {
      sum += -9.32874e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.32874e-05;
    } else {
      sum += -9.32874e-05;
    }
  }
  // tree 1140
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.3036e-05;
    } else {
      sum += 9.3036e-05;
    }
  } else {
    if ( features[0] < 1.64276 ) {
      sum += -9.3036e-05;
    } else {
      sum += 9.3036e-05;
    }
  }
  // tree 1141
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.27805e-05;
    } else {
      sum += 9.27805e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 9.27805e-05;
    } else {
      sum += -9.27805e-05;
    }
  }
  // tree 1142
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.27574e-05;
    } else {
      sum += 9.27574e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.27574e-05;
    } else {
      sum += -9.27574e-05;
    }
  }
  // tree 1143
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 0.000140446;
    } else {
      sum += -0.000140446;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000140446;
    } else {
      sum += 0.000140446;
    }
  }
  // tree 1144
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.28393e-05;
    } else {
      sum += -9.28393e-05;
    }
  } else {
    sum += 9.28393e-05;
  }
  // tree 1145
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 0.000107328;
    } else {
      sum += -0.000107328;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000107328;
    } else {
      sum += 0.000107328;
    }
  }
  // tree 1146
  if ( features[7] < 3.73601 ) {
    sum += -8.38225e-05;
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -8.38225e-05;
    } else {
      sum += 8.38225e-05;
    }
  }
  // tree 1147
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000172282;
    } else {
      sum += -0.000172282;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000172282;
    } else {
      sum += 0.000172282;
    }
  }
  // tree 1148
  if ( features[1] < -0.067765 ) {
    if ( features[4] < -0.463829 ) {
      sum += -0.000117604;
    } else {
      sum += 0.000117604;
    }
  } else {
    if ( features[6] < 2.15257 ) {
      sum += -0.000117604;
    } else {
      sum += 0.000117604;
    }
  }
  // tree 1149
  if ( features[1] < -0.067765 ) {
    if ( features[1] < -0.779359 ) {
      sum += 0.000144535;
    } else {
      sum += -0.000144535;
    }
  } else {
    if ( features[3] < 0.96687 ) {
      sum += 0.000144535;
    } else {
      sum += -0.000144535;
    }
  }
  // tree 1150
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.2755e-05;
    } else {
      sum += 9.2755e-05;
    }
  } else {
    sum += 9.2755e-05;
  }
  // tree 1151
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.22562e-05;
    } else {
      sum += 9.22562e-05;
    }
  } else {
    sum += 9.22562e-05;
  }
  // tree 1152
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.24376e-05;
    } else {
      sum += 9.24376e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.24376e-05;
    } else {
      sum += -9.24376e-05;
    }
  }
  // tree 1153
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000109453;
    } else {
      sum += -0.000109453;
    }
  } else {
    sum += 0.000109453;
  }
  // tree 1154
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 0.000115157;
    } else {
      sum += -0.000115157;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000115157;
    } else {
      sum += -0.000115157;
    }
  }
  // tree 1155
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.66168 ) {
      sum += -9.25113e-05;
    } else {
      sum += 9.25113e-05;
    }
  } else {
    sum += 9.25113e-05;
  }
  // tree 1156
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.26243e-05;
    } else {
      sum += -9.26243e-05;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 9.26243e-05;
    } else {
      sum += -9.26243e-05;
    }
  }
  // tree 1157
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 0.000106657;
    } else {
      sum += -0.000106657;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000106657;
    } else {
      sum += 0.000106657;
    }
  }
  // tree 1158
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.2151e-05;
    } else {
      sum += 9.2151e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -9.2151e-05;
    } else {
      sum += 9.2151e-05;
    }
  }
  // tree 1159
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000116073;
    } else {
      sum += 0.000116073;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000116073;
    } else {
      sum += -0.000116073;
    }
  }
  // tree 1160
  if ( features[3] < 1.04065 ) {
    if ( features[6] < 2.24 ) {
      sum += -0.000133521;
    } else {
      sum += 0.000133521;
    }
  } else {
    if ( features[6] < 1.65196 ) {
      sum += 0.000133521;
    } else {
      sum += -0.000133521;
    }
  }
  // tree 1161
  if ( features[8] < 2.38156 ) {
    if ( features[0] < 3.03054 ) {
      sum += -8.92985e-05;
    } else {
      sum += 8.92985e-05;
    }
  } else {
    sum += 8.92985e-05;
  }
  // tree 1162
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 0.000138959;
    } else {
      sum += -0.000138959;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000138959;
    } else {
      sum += 0.000138959;
    }
  }
  // tree 1163
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.26843e-05;
    } else {
      sum += -9.26843e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.26843e-05;
    } else {
      sum += -9.26843e-05;
    }
  }
  // tree 1164
  if ( features[8] < 2.38156 ) {
    sum += -7.72862e-05;
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -7.72862e-05;
    } else {
      sum += 7.72862e-05;
    }
  }
  // tree 1165
  if ( features[5] < 0.329645 ) {
    sum += 0.000102291;
  } else {
    if ( features[5] < 0.893056 ) {
      sum += -0.000102291;
    } else {
      sum += 0.000102291;
    }
  }
  // tree 1166
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.22575e-05;
    } else {
      sum += -9.22575e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.22575e-05;
    } else {
      sum += -9.22575e-05;
    }
  }
  // tree 1167
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 0.000106237;
    } else {
      sum += -0.000106237;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000106237;
    } else {
      sum += 0.000106237;
    }
  }
  // tree 1168
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.2181e-05;
    } else {
      sum += -9.2181e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.2181e-05;
    } else {
      sum += -9.2181e-05;
    }
  }
  // tree 1169
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.19621e-05;
    } else {
      sum += -9.19621e-05;
    }
  } else {
    if ( features[0] < 1.64276 ) {
      sum += -9.19621e-05;
    } else {
      sum += 9.19621e-05;
    }
  }
  // tree 1170
  if ( features[8] < 2.38156 ) {
    if ( features[0] < 3.03054 ) {
      sum += -8.90828e-05;
    } else {
      sum += 8.90828e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 8.90828e-05;
    } else {
      sum += -8.90828e-05;
    }
  }
  // tree 1171
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.20132e-05;
    } else {
      sum += 9.20132e-05;
    }
  } else {
    sum += 9.20132e-05;
  }
  // tree 1172
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.16532e-05;
    } else {
      sum += 9.16532e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.16532e-05;
    } else {
      sum += -9.16532e-05;
    }
  }
  // tree 1173
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 0.000105916;
    } else {
      sum += -0.000105916;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000105916;
    } else {
      sum += 0.000105916;
    }
  }
  // tree 1174
  if ( features[1] < -0.067765 ) {
    if ( features[1] < -0.779359 ) {
      sum += 0.000143065;
    } else {
      sum += -0.000143065;
    }
  } else {
    if ( features[3] < 0.96687 ) {
      sum += 0.000143065;
    } else {
      sum += -0.000143065;
    }
  }
  // tree 1175
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.21609e-05;
    } else {
      sum += -9.21609e-05;
    }
  } else {
    sum += 9.21609e-05;
  }
  // tree 1176
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.18029e-05;
    } else {
      sum += 9.18029e-05;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 9.18029e-05;
    } else {
      sum += -9.18029e-05;
    }
  }
  // tree 1177
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000111877;
    } else {
      sum += 0.000111877;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -0.000111877;
    } else {
      sum += 0.000111877;
    }
  }
  // tree 1178
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.15141e-05;
    } else {
      sum += 9.15141e-05;
    }
  } else {
    sum += 9.15141e-05;
  }
  // tree 1179
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.1165e-05;
    } else {
      sum += -9.1165e-05;
    }
  } else {
    sum += 9.1165e-05;
  }
  // tree 1180
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.66168 ) {
      sum += -9.08787e-05;
    } else {
      sum += 9.08787e-05;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 9.08787e-05;
    } else {
      sum += -9.08787e-05;
    }
  }
  // tree 1181
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.04452e-05;
    } else {
      sum += 9.04452e-05;
    }
  } else {
    sum += 9.04452e-05;
  }
  // tree 1182
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.824502 ) {
      sum += -9.18168e-05;
    } else {
      sum += 9.18168e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.18168e-05;
    } else {
      sum += -9.18168e-05;
    }
  }
  // tree 1183
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.15281e-05;
    } else {
      sum += -9.15281e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.15281e-05;
    } else {
      sum += -9.15281e-05;
    }
  }
  // tree 1184
  if ( features[6] < 2.32779 ) {
    if ( features[3] < 0.442764 ) {
      sum += 0.000130903;
    } else {
      sum += -0.000130903;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -0.000130903;
    } else {
      sum += 0.000130903;
    }
  }
  // tree 1185
  if ( features[2] < -0.974311 ) {
    sum += -6.90958e-05;
  } else {
    if ( features[6] < 2.32779 ) {
      sum += -6.90958e-05;
    } else {
      sum += 6.90958e-05;
    }
  }
  // tree 1186
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.12264e-05;
    } else {
      sum += 9.12264e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 9.12264e-05;
    } else {
      sum += -9.12264e-05;
    }
  }
  // tree 1187
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -0.000113485;
    } else {
      sum += 0.000113485;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000113485;
    } else {
      sum += -0.000113485;
    }
  }
  // tree 1188
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.08849e-05;
    } else {
      sum += 9.08849e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -9.08849e-05;
    } else {
      sum += 9.08849e-05;
    }
  }
  // tree 1189
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.539082 ) {
      sum += 9.04151e-05;
    } else {
      sum += -9.04151e-05;
    }
  } else {
    if ( features[5] < 1.13177 ) {
      sum += 9.04151e-05;
    } else {
      sum += -9.04151e-05;
    }
  }
  // tree 1190
  if ( features[8] < 2.38156 ) {
    if ( features[0] < 3.03054 ) {
      sum += -8.84705e-05;
    } else {
      sum += 8.84705e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 8.84705e-05;
    } else {
      sum += -8.84705e-05;
    }
  }
  // tree 1191
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 9.11601e-05;
    } else {
      sum += -9.11601e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.11601e-05;
    } else {
      sum += -9.11601e-05;
    }
  }
  // tree 1192
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -9.06999e-05;
    } else {
      sum += 9.06999e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 9.06999e-05;
    } else {
      sum += -9.06999e-05;
    }
  }
  // tree 1193
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.07931e-05;
    } else {
      sum += 9.07931e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -9.07931e-05;
    } else {
      sum += 9.07931e-05;
    }
  }
  // tree 1194
  if ( features[0] < 3.03054 ) {
    if ( features[2] < 0.956816 ) {
      sum += -9.61677e-05;
    } else {
      sum += 9.61677e-05;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -9.61677e-05;
    } else {
      sum += 9.61677e-05;
    }
  }
  // tree 1195
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.245271 ) {
      sum += 0.000105727;
    } else {
      sum += -0.000105727;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -0.000105727;
    } else {
      sum += 0.000105727;
    }
  }
  // tree 1196
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -9.07399e-05;
    } else {
      sum += 9.07399e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -9.07399e-05;
    } else {
      sum += 9.07399e-05;
    }
  }
  // tree 1197
  if ( features[0] < 3.03054 ) {
    if ( features[6] < 2.41557 ) {
      sum += -0.000101384;
    } else {
      sum += 0.000101384;
    }
  } else {
    if ( features[5] < 0.754375 ) {
      sum += 0.000101384;
    } else {
      sum += -0.000101384;
    }
  }
  // tree 1198
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.66168 ) {
      sum += -9.03394e-05;
    } else {
      sum += 9.03394e-05;
    }
  } else {
    sum += 9.03394e-05;
  }
  // tree 1199
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.36419 ) {
      sum += -0.000114294;
    } else {
      sum += 0.000114294;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 0.000114294;
    } else {
      sum += -0.000114294;
    }
  }
  return sum;
}

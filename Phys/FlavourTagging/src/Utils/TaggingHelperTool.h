/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "DetDesc/IGeometryInfo.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/ICheckOverlap.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IRelatedPVFinder.h"
#include "Kernel/IVertexFit.h"
#include "SelKernel/Utilities.h"
#include "TaggingHelper.h"

class TaggingHelperTool : public extends<GaudiTool, IAlgTool> {

public:
  /// Standard constructor
  using extends::extends;

  std::optional<std::pair<double, double>> calcIPWithErr( const LHCb::Particle& part, const LHCb::VertexBase& vtx,
                                                          const IGeometryInfo& geometry ) const;
  std::optional<std::pair<double, double>> calcMinIPWithErr( const LHCb::Particle& part,
                                                             LHCb::span<LHCb::VertexBase const* const>,
                                                             const IGeometryInfo& geometry ) const;
  std::optional<std::pair<double, double>> calcIPWithChi2( const LHCb::Particle& part, const LHCb::VertexBase& vtx,
                                                           const IGeometryInfo& geometry ) const;
  std::optional<std::pair<double, double>> calcMinIPWithChi2( const LHCb::Particle& part,
                                                              LHCb::span<LHCb::VertexBase const* const>,
                                                              const IGeometryInfo& geometry ) const;
  std::optional<std::pair<double, double>> calcMinDocaWithErr( const LHCb::Particle& particle,
                                                               const LHCb::Particle& referenceParticleA,
                                                               const LHCb::Particle& referenceParticleB,
                                                               const IGeometryInfo&  geometry ) const;

  template <typename PVContainer>
  double calcMinIPChi2( const LHCb::Particle& part, const PVContainer& vertices, const IGeometryInfo& geometry ) const {
    return std::accumulate( vertices.begin(), vertices.end(), std::numeric_limits<double>::max(),
                            [&]( double min, const auto& vertex ) {
                              auto tempIP = calcIPWithChi2( part, Sel::Utils::deref_if_ptr( vertex ), geometry );
                              return ( tempIP && tempIP->second < min ) ? tempIP->second : min;
                            } );
  }

  bool hasOverlap( const LHCb::Particle& taggingCand, const LHCb::Particle& bCand ) const;

  std::optional<LHCb::Vertex> fitVertex( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                         const IGeometryInfo& geometry ) const;

  bool passesCommonPreSelection( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                 LHCb::span<LHCb::VertexBase const* const> pileups,
                                 const IGeometryInfo&                      geometry ) const;

private:
  Gaudi::Property<double> m_minDistPhi{this, "MinDistPhi", 0.005,
                                       "Tagging particle requirement: Minimum phi distance to B candidate."};
  Gaudi::Property<double> m_minIpSigTagPileUpVertices{
      this, "MinIpSigTagPileUpVertices", 3.0,
      "Tagging particle requirement: Minimim IP significance wrt to all pileup vertices."};
  Gaudi::Property<double> m_maxSharedHitFraction{this, "MaxSharedHitFraction", 0.3,
                                                 "Maximum allowed fraction of hits shared between tracks."};

  ToolHandle<IDistanceCalculator> m_distCalcTool{this, "DistanceCalculatorTool", "LoKi::DistanceCalculator"};
  ToolHandle<ICheckOverlap>       m_overlapTool{this, "OverlapTool", "CheckOverlap"};
  ToolHandle<IRelatedPVFinder>    m_relatedPVTool{
      this, "RelatedPVFinderTool", "GenericParticle2PVRelator__p2PVWithIPChi2_OnlineDistanceCalculatorName_"};
  ToolHandle<IPVReFitter>          m_pvRefitterTool{this, "PVRefitterTool", "LoKi::PVReFitter"};
  ToolHandle<IVertexFit>           m_vertexFitTool{this, "VertexFitterTool", "LoKi::VertexFitter"};
  ToolHandle<IParticleDescendants> m_descendantsTool{this, "ParticleDescentantsTool", "ParticleDescendants"};
};

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "../Classification/OSElectron/OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0.h"
#include "Core/FloatComparison.h"
#include "DetDesc/DetectorElement.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "Event/Tagger.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IParticleDescendants.h"
#include "LHCbAlgs/Transformer.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class Run2OSElectronTagger
    : public LHCb::Algorithm::Transformer<LHCb::FlavourTags( const LHCb::Particle::Range&, const LHCb::Particle::Range&,
                                                             const LHCb::PrimaryVertices&, const DetectorElement& ),
                                          LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  Run2OSElectronTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingElectrons", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particle::Range& bCandidates, const LHCb::Particle::Range& taggingElectrons,
                                const LHCb::PrimaryVertices& primaryVertices, const DetectorElement& ) const override;

private:
  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle&        bCand,
                                              const LHCb::Particle::Range& taggingElectrons,
                                              const LHCb::PrimaryVertices& primaryVertices,
                                              const IGeometryInfo&         geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::OSElectronLatest; }

  // cut values for OSElectron selection
  Gaudi::Property<double> m_minDistPhi{this, "MinDistPhi", 0.016728744332624838,
                                       "Tagging particle requirement: Minimum phi distance to B daughters"};
  Gaudi::Property<double> m_minIpSigTagBestPV{this, "MinIpSigTagBestPV", 0.0419896167433678,
                                              "Tagging particle requirement: Minimum IP significance wrt to best PV"};
  Gaudi::Property<double> m_minIpSigTagPileUpVertices{
      this, "MinIpSigPileUp", 9.334968382765219,
      "Tagging particle requirement: Minimum IP significance wrt to all pile-up PV"};

  Gaudi::Property<double> m_maxEOverP{this, "MaxEOverP", 2, "Tagging particle requirement: Maximum electron e/p."};
  Gaudi::Property<double> m_minEOverP{this, "MinEOverP", 0.85, "Tagging particle requirement: Minimum electron e/p."};
  Gaudi::Property<double> m_maxVeloCharge{this, "MaxVeloCharge", 1.4,
                                          "Tagging particle requirement: Maximum velo charge."};
  Gaudi::Property<double> m_minVeloCharge{this, "MinVeloCharge", 0.,
                                          "Tagging particle requirement: Minimum velo charge."};

  Gaudi::Property<double> m_maxProbNNmu{this, "MaxProbNNmu", 0.15808731889714595,
                                        "Tagging particle requirement: Maximum ProbNNmu."};
  Gaudi::Property<double> m_maxProbNNpi{this, "MaxProbNNpi", 0.9833355298219992,
                                        "Tagging particle requirement: Maximum ProbNNpi."};
  Gaudi::Property<double> m_maxProbNNp{this, "MaxProbNNp", 0.27070346277844964,
                                       "Tagging particle requirement: Maximum ProbNNp."};
  Gaudi::Property<double> m_maxProbNNk{this, "MaxProbNNk", 0.6949348710000995,
                                       "Tagging particle requirement: Maximum ProbNNk."};
  Gaudi::Property<double> m_minProbNNe{this, "MinProbNNe", 0.24262849376048545,
                                       "Tagging particle requirement: Minimum ProbNNe."};
  // values for calibration purposes
  // Gaudi::Property<double> m_averageEta{this, "AverageEta", 0.3949};
  // Gaudi::Property<double> m_minPosDecision{this, "MinPositiveTaggingDecision", 0.5,
  //                                          "Minimum value of the Eta for a tagging decision of +1"};
  // Gaudi::Property<double> m_maxNegDecision{this, "MaxNegativeTaggingDecision", 0.5,
  //                                          "Maximum value of the Eta for a tagging decision of -1"};

  mutable Gaudi::Accumulators::SummingCounter<>  m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<>  m_ElectronCount{this, "#taggingElectrons"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_FTCount{this, "#goodFlavourTags"};

  ToolHandle<TaggingHelperTool>    m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};
  ToolHandle<IParticleDescendants> m_particleDescendantTool{this, "ParticleDescendantTool", "ParticleDescendants"};

  std::unique_ptr<ITaggingClassifier> m_classifier = std::make_unique<OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0>();
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( Run2OSElectronTagger )

LHCb::FlavourTags Run2OSElectronTagger::operator()( const LHCb::Particle::Range& bCandidates,
                                                    const LHCb::Particle::Range& taggingElectrons,
                                                    const LHCb::PrimaryVertices& primaryVertices,
                                                    const DetectorElement&       lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();

  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_ElectronCount += taggingElectrons.size();

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingElectrons.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      auto flavourTag = std::make_unique<LHCb::FlavourTag>();
      flavourTag->addTagger( LHCb::Tagger{}.setType( taggerType() ) ).setTaggedB( bCand );
      flavourTags.insert( flavourTag.release() );
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {

    auto tagger = performTagging( *bCand, taggingElectrons, primaryVertices, geometry );
    m_FTCount += tagger.has_value();

    auto flavourTag = std::make_unique<LHCb::FlavourTag>();
    flavourTag->setTaggedB( bCand );
    // if no appropriate tagging candidate found, fill with "empty" FlavourTag object
    flavourTag->addTagger( tagger ? *tagger : LHCb::Tagger{}.setType( taggerType() ) );
    flavourTags.insert( flavourTag.release() );
  }

  return flavourTags;
}

std::optional<LHCb::Tagger> Run2OSElectronTagger::performTagging( const LHCb::Particle&        bCand,
                                                                  const LHCb::Particle::Range& taggingElectrons,
                                                                  const LHCb::PrimaryVertices& primaryVertices,
                                                                  const IGeometryInfo&         geometry ) const {
  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best fits the B candidate
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( bestPV == nullptr ) return std::nullopt;

  /*std::optional<const LHCb::VertexBase> refittedPV = m_taggingHelperTool->refitPVWithoutB(*bestPV, bCand);
  if(! refittedPV)
    return std::nullopt;
  */
  // PV refitting isn't possible in Moore (right now) since the PV doesn't save the particles it was made of
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, bestPV );

  auto b_daughters = m_particleDescendantTool->descendants( &bCand );
  b_daughters.push_back( &bCand );

  const LHCb::Particle*              bestTagCand = nullptr;
  double                             bestBDT     = -99.0;
  std::vector<const LHCb::Particle*> preselectionElectrons;

  // Preselection to choose Electrons which passed selections in BTaggingTool.cpp
  for ( const auto* tagCand : taggingElectrons ) {
    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;
    preselectionElectrons.push_back( tagCand );
  }

  for ( const auto* tagCand : preselectionElectrons ) {
    Gaudi::LorentzVector tag_mom = tagCand->momentum();

    const LHCb::ProtoParticle* tagProto = tagCand->proto();

    const double tagEOverP = tagProto->info( LHCb::ProtoParticle::CaloEoverP, -1000.0 );
    if ( tagEOverP < m_minEOverP ) continue;
    if ( tagEOverP > m_maxEOverP ) continue;

    const bool   isMuon      = tagProto->muonPID() ? tagProto->muonPID()->IsMuon() : false;
    auto const   gpid        = tagProto->globalChargedPID();
    const double tagProbNNk  = gpid ? gpid->ProbNNk() : -1000.0;
    const double tagProbNNpi = gpid ? gpid->ProbNNpi() : -1000.0;
    const double tagProbNNp  = gpid ? gpid->ProbNNp() : -1000.0;
    const double tagProbNNmu = gpid ? gpid->ProbNNmu() : -1000.0;
    const double tagProbNNe  = gpid ? gpid->ProbNNe() : -1000.0;

    if ( isMuon ) continue;
    if ( tagProbNNk > m_maxProbNNk ) continue;
    if ( tagProbNNpi > m_maxProbNNpi ) continue;
    if ( tagProbNNp > m_maxProbNNp ) continue;
    if ( tagProbNNmu > m_maxProbNNmu ) continue;
    if ( tagProbNNe < m_minProbNNe ) continue;

    // const double tagVeloCharge = tagProto->info( LHCb::ProtoParticle::VeloCharge, -998 );
    const double tagInAccHcal = tagProto->info( LHCb::ProtoParticle::InAccHcal, -998 );
    // if (tagVeloCharge < m_minVeloCharge) continue;
    // if (tagVeloCharge > m_maxVeloCharge) continue;
    if ( !LHCb::essentiallyEqual( tagInAccHcal, 1. ) ) continue;

    // Calculate minimum phi distance
    std::vector<double> bDaughtersPhi;
    std::transform(
        b_daughters.begin(), b_daughters.end(), std::back_inserter( bDaughtersPhi ),
        [&tag_mom]( const LHCb::Particle* p ) { return std::abs( TaggingHelper::dPhi( tag_mom, p->momentum() ) ); } );
    const auto minPhi = std::min_element( bDaughtersPhi.begin(), bDaughtersPhi.end() );
    if ( *minPhi < m_minDistPhi ) continue;

    // IP significance cut wrt bestPV
    std::optional<std::pair<double, double>> refittedPVIP =
        m_taggingHelperTool->calcIPWithChi2( *tagCand, refittedPV.value(), geometry );
    if ( !refittedPVIP ) continue;
    const double ipChi2 = refittedPVIP->second;
    const double ipSig  = std::sqrt( refittedPVIP->second );
    const double ipErr  = refittedPVIP->first / ipSig;
    const double absIp  = std::abs( refittedPVIP->first );
    if ( ipSig < m_minIpSigTagBestPV ) continue;
    if ( LHCb::essentiallyZero( ipErr ) ) continue;

    // Minimum IP significance wrt to pile up PVs
    auto minIp = m_taggingHelperTool->calcMinIPWithChi2( *tagCand, pileups, geometry );
    if ( !minIp ) continue;
    const double minIpSig = std::sqrt( minIp->second );
    if ( minIpSig < m_minIpSigTagPileUpVertices ) continue;

    const double tagProbNNghost = gpid ? gpid->ProbNNghost() : -1000.0;

    // auto bestVertex = m_DVAlgorithm->bestVertex( tagCand, m_DVAlgorithm->geometry() );
    // std::optional<std::pair<double, double>> bestVertexIp =
    //     m_taggingHelperTool->calcIPWithChi2( *tagCand, *bestVertex, geometry );
    // if ( !bestVertexIp ) continue;
    // const double bestVertexIpChi2 = bestVertexIp->second;

    const double         tagMass     = tagCand->measuredMass();
    Gaudi::LorentzVector tmp_tag_mom = tagCand->momentum();
    tmp_tag_mom.SetE( std::sqrt( tagMass * tagMass + tmp_tag_mom.P2() ) );
    const double deltaQ = ( b_mom + tmp_tag_mom ).M() - b_mom.M() - tagMass;

    const double deltaEta = std::fabs( b_mom.Eta() - tag_mom.Eta() );

    const double deltaPhi = TaggingHelper::dPhi( tag_mom, b_mom );
    const double deltaR   = std::sqrt( deltaEta * deltaEta + deltaPhi * deltaPhi );

    std::vector<double> inputVals( 12 );
    inputVals[0]  = tag_mom.Pt();
    inputVals[1]  = minIpSig;
    inputVals[2]  = preselectionElectrons.size(); // trackCounts
    inputVals[3]  = b_mom.Pt();
    inputVals[4]  = tagEOverP;
    inputVals[5]  = absIp;
    inputVals[6]  = ipErr;
    inputVals[7]  = ipChi2;
    inputVals[8]  = tagProbNNghost;
    inputVals[9]  = deltaQ;
    inputVals[10] = deltaEta;
    inputVals[11] = deltaR;

    const double mvaValue = m_classifier->getClassifierValue( inputVals );

    if ( mvaValue < bestBDT ) continue;

    bestTagCand = tagCand;
    bestBDT     = mvaValue;
  }

  if ( bestBDT < 0 || bestBDT > 1 ) return std::nullopt;

  auto   tagdecision = bestTagCand->charge() > 0 ? LHCb::Tagger::TagResult::bbar : LHCb::Tagger::TagResult::b;
  double omega       = 1 - bestBDT;
  if ( bestBDT < 0.5 ) {
    tagdecision = reversed( tagdecision );
    omega       = bestBDT;
  }

  return LHCb::Tagger{}
      .setDecision( tagdecision )
      .setOmega( omega )
      .setType( taggerType() )
      .addToTaggerParts( bestTagCand )
      .setCharge( bestTagCand->charge() )
      .setMvaValue( bestBDT );
}

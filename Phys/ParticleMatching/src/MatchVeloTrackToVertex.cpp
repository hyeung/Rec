/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from LHCb
#include "Event/Particle.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"

namespace {
  using output_t        = std::tuple<LHCb::Particles>;
  using filter_output_t = std::tuple<bool, LHCb::Particles>;
} // namespace

// Trivial algorithm that matches a particle's vertex to a Velo track.
// Done by filtering on the IP of the Velo track w.r.t. that vertex.
// Can be used to see if charged particles decaying outside the Velo left a track in the Velo.
class MatchVeloTrackToVertex : public LHCb::Algorithm::MultiTransformerFilter<output_t(
                                   const LHCb::Particles&, const LHCb::Event::v1::Tracks& )> {
public:
  /// Standard constructor
  MatchVeloTrackToVertex( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformerFilter{name,
                               pSvcLocator,
                               {KeyValue{"SeedParticles", ""}, KeyValue{"VeloTracks", "Rec/Track/Velo"}},
                               {KeyValue{"OutputParticles", ""}}} {}

  filter_output_t operator()( const LHCb::Particles&, const LHCb::Event::v1::Tracks& ) const override;

private:
  Gaudi::Property<float> m_match_ip{this, "VeloMatchIP", 5.f * Gaudi::Units::mm};
  Gaudi::Property<float> m_eta_tolerance{this, "EtaTolerance", 5.f}; // max Delta Eta between VELO track and seed

  mutable Gaudi::Accumulators::StatCounter<>     m_nVeloIn{this, "Input VELO tracks"};
  mutable Gaudi::Accumulators::StatCounter<>     m_nCombIn{this, "Input vertices"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_matched{this, "Matched"};

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_end_velo_state_warning{
      this, "Provided VELO track does not have an end velo state"};
  // we may need this if we want to get a good estimate of the Velo track PV IP(CHI2) (check PrFilterIPSoA)
  // PublicToolHandle<ITrackExtrapolator> m_extrapolator{this, "TrackExtrapolator", "TrackMasterExtrapolator"};
  // PublicToolHandle<ITrackFitter> m_fitter{this, "TrackFitter", "FastVeloFitLHCbIDs"};
};
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MatchVeloTrackToVertex )

filter_output_t MatchVeloTrackToVertex::operator()( const LHCb::Particles&         seeds,
                                                    const LHCb::Event::v1::Tracks& velo_tracks ) const {
  LHCb::Particles filtered_seeds;
  m_nVeloIn += velo_tracks.size();
  m_nCombIn += seeds.size();

  for ( const auto seed : seeds ) {
    bool               matched = false;
    const LHCb::State* end_velo_state;

    for ( const auto& velo_track : velo_tracks ) {
      // skip backward tracks
      if ( velo_track->isVeloBackward() ) continue;

      if ( !velo_track->hasStateAt( LHCb::State::Location::EndVelo ) ) {
        ++m_no_end_velo_state_warning;
        end_velo_state = &velo_track->firstState();
      } else {
        end_velo_state = velo_track->stateAt( LHCb::State::Location::EndVelo );
      }
      // get the state at the end of the VELO

      // check the rough IP of the VELO w.r.t. the seed's decay vertex
      const Gaudi::XYZPoint& seed_vertex_pos = seed->endVertex()->position();
      const double           ip              = Gaudi::Math::impactParameter(
          seed_vertex_pos, Gaudi::Math::Line{end_velo_state->position(), end_velo_state->slopes()} );

      const float track_eta = end_velo_state->slopes().eta();
      const float seed_eta  = seed->momentum().eta();

      if ( ip < m_match_ip && fabs( track_eta - seed_eta ) < m_eta_tolerance ) {
        matched = true;
        filtered_seeds.insert( seed );
        break; // we don't need to know if there are multiple Velo tracks that match
      }
    }
    m_matched += matched;
  }

  return {filtered_seeds.size() > 0, std::move( filtered_seeds )};
}
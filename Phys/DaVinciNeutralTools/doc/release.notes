
!-----------------------------------------------------------------------------
! Package     : Phys/DaVinciNeutralTools
! Responsible : CALO Group
! Purpose     : DaVinci Neutral Tools
!-----------------------------------------------------------------------------

! 2018-29-03 - O.Deschamps
 - RestoreCaloRecoChain.cpp : 'refitDecayTree' option relies on default DVA particleCombiner instead of DecayTreeRefit to avoid issues when V0s are in
volved

! 2018-15-02 - O.Deschamps
 - minor fix in RestoreCaloRecoChain.cpp : fix issue with refitTree when the calo particle is a fragment of an intermediate resonance (e.g. D+->eta'(pipigamma)pi+)

! 2017-11-03 - O.Deschamps
 - minor change in RestoreCaloRecoChain.cpp : no longer update ProtoP::info(CaloNeutralSpd) and update ProtoP::info(ClusterSpread) only when rawBank are available

! 2017-10-08 - O.Deschamps
 - add RestoreCaloRecoChain.{cpp,h} algorithm

! 2017-04-07 - O.Deschamps
 - add StoreCaloRecoChain.{cpp,h} algorithm

!==================== DaVinciNeutralTools v1r1 2015-10-05 ====================

! 2015-08-12 - Gerhard Raven
 - remove #include of obsolete Gaudi headers

!========================= DaVinciNeutralTools v1r0 2014-09-30 =========================

! 2014-09-24 - O. Deschamps for E. Tournefier  
 - BremAdder : change default CL cut on brem photons (CL>0.1)

! 2014-08-05 - Chris Jones
 - First version. Split off from DaVinciTools tools relating to neutrals to
   this dedicated package.

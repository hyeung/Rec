/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CombKernel/ThOrCombiner.h"

#include "Event/Particle_v2.h"

using ThOrCombiner__2ChargedBasics = ThOr::CombinerBest<LHCb::Event::ChargedBasics, LHCb::Event::ChargedBasics>;
using ThOrCombiner__3ChargedBasics =
    ThOr::CombinerBest<LHCb::Event::ChargedBasics, LHCb::Event::ChargedBasics, LHCb::Event::ChargedBasics>;
using ThOrCombiner__4ChargedBasics = ThOr::CombinerBest<LHCb::Event::ChargedBasics, LHCb::Event::ChargedBasics,
                                                        LHCb::Event::ChargedBasics, LHCb::Event::ChargedBasics>;

DECLARE_COMPONENT_WITH_ID( ThOrCombiner__2ChargedBasics, "ThOrCombiner__2ChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombiner__3ChargedBasics, "ThOrCombiner__3ChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombiner__4ChargedBasics, "ThOrCombiner__4ChargedBasics" )

/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <string>

namespace {
  using counter_t       = Gaudi::Accumulators::SummingCounter<unsigned int>;
  using inputs_t        = const Gaudi::Functional::vector_of_const_<LHCb::Particle::Range>&;
  using output_t        = std::tuple<LHCb::Particle::Selection>;
  using filter_output_t = std::tuple<bool, LHCb::Particle::Selection>;
  using base_t          = LHCb::Algorithm::MergingMultiTransformerFilter<output_t( inputs_t )>;
} // namespace

namespace LHCb {

  struct ParticleContainerMergerT final : base_t {
    ParticleContainerMergerT( const std::string& name, ISvcLocator* pSvcLocator )
        : base_t( name, pSvcLocator, {"InputContainers", {}}, {KeyValue{"OutputContainer", ""}} ) {}

    filter_output_t operator()( inputs_t lists ) const override {
      LHCb::Particle::Selection out_particles;
      bool                      out_decision   = false;
      auto                      ninput_buffer  = m_ninput_particles.buffer();
      auto                      noutput_buffer = m_noutput_particles.buffer();
      for ( const auto& list : lists ) {
        ninput_buffer += list.size();
        for ( const auto& particle : list ) {
          // make sure the particle is not yet there!
          if ( std::find( out_particles.begin(), out_particles.end(), particle ) == out_particles.end() ) {
            out_particles.insert( particle );
            noutput_buffer++;
          }
        }
      }
      if ( out_particles.size() > 0 ) { out_decision = true; }
      m_npassed += out_decision;
      return {out_decision, std::move( out_particles )};
    }

  private:
    mutable Gaudi::Accumulators::BinomialCounter<> m_npassed{this, "# passed"};
    mutable counter_t                              m_ninput_particles{this, "# input particles"};
    mutable counter_t                              m_noutput_particles{this, "# output particles"};
  };

  DECLARE_COMPONENT_WITH_ID( ParticleContainerMergerT, "ParticleContainerMerger" )
} // namespace LHCb

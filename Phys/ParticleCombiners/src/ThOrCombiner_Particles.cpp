/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CombKernel/ThOrCombiner.h"

#include "Event/Particle_v2.h"

template <std::size_t N>
using ThOrCombiner__NParticle =
    boost::mp11::mp_apply<ThOr::CombinerBest,
                          boost::mp11::mp_repeat_c<boost::mp11::mp_list<LHCb::Event::Particles>, N>>;

DECLARE_COMPONENT_WITH_ID( ThOrCombiner__NParticle<2>, "ThOrCombiner__2Particle" )
DECLARE_COMPONENT_WITH_ID( ThOrCombiner__NParticle<3>, "ThOrCombiner__3Particle" )
DECLARE_COMPONENT_WITH_ID( ThOrCombiner__NParticle<4>, "ThOrCombiner__4Particle" )

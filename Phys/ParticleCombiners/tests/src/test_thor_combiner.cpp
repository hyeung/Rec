/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utest_thor_combiner
#include <boost/mp11/algorithm.hpp>
#include <boost/mp11/bind.hpp>
#include <boost/test/unit_test.hpp>
#include <ostream>
#include <sstream>
#include <type_traits>

#include "CombKernel/ThOrCombiner.h"
#include "Event/GenerateSOATracks.h"
#include "Event/Particle_v2.h"
#include "Event/State.h"
#include "Gaudi/Property.h"

using simd_t  = SIMDWrapper::type_map_t<SIMDWrapper::InstructionSet::Scalar>;
using float_v = typename simd_t::float_v;
using int_v   = typename simd_t::int_v;

using MuonFlags = LHCb::Event::flags_v<SIMDWrapper::scalar::types, LHCb::Event::v2::Muon::StatusMasks>;

namespace ChargedBasicsTag = LHCb::Event::ChargedBasicsTag;

static const LHCb::UniqueIDGenerator unique_id_gen;

// CompositeLike
template <class T>
struct is_compositelike : std::false_type {};

template <typename... Ts>
struct is_compositelike<LHCb::Event::ZipProxy<Ts...>>
    : Sel::Utils::details::contains<std::decay_t<LHCb::Event::Composites>, std::decay_t<typename Ts::container_t>...> {
};

template <SIMDWrapper::InstructionSet Arch, class... T>
struct is_compositelike<LHCb::Event::Zip<Arch, T...>>
    : std::conditional_t<
          Sel::Utils::details::contains<std::decay_t<LHCb::Event::Composites>, std::decay_t<T>...>::value,
          std::true_type, std::false_type> {};

template <typename T>
constexpr bool is_compositelike_v = is_compositelike<T>::value;

// Definition of a set of proxies
// TODO: use std::enable_if_t<are_proxy_v<Proxy ...>, std::tuple<Proxy ...>>
// once we switch to a stable proxy type
template <class... Proxy>
using proxy_pack = std::tuple<Proxy...>;

template <class... Proxy>
proxy_pack<Proxy...> make_proxy_pack( Proxy&&... proxy ) {
  return {std::forward<Proxy>( proxy )...};
}

template <class... Proxy, class P>
proxy_pack<Proxy..., P> extend_proxy_pack( proxy_pack<Proxy...> const& proxies, P&& proxy ) {
  return std::tuple_cat( proxies, std::make_tuple( std::forward<P>( proxy ) ) );
}

// Number of unique IDs for a proxy/container/zip (count its own unique ID and
// those of the descendants)
template <class T>
std::size_t num_unique_ids( [[maybe_unused]] T const& arg ) {

  if constexpr ( !is_compositelike_v<T> ) {
    return 1;
  } else {
    // TODO: somehow implement and use is_proxy?
    if constexpr ( LHCb::Event::is_zip_v<T> ) {
      return arg.template get<LHCb::Event::Composites>().scalar()[0].numDescendants();
    } else
      return arg.numDescendants();
  }
}

// Fill a vector with the unique IDs of a particle, including its own ID
template <class Proxy>
void unroll_unique_ids( std::vector<LHCb::UniqueIDGenerator::ID<int_v>>& ids, Proxy const& proxy ) {
  if constexpr ( !is_compositelike_v<Proxy> )
    ids.emplace_back( proxy.unique_id() );
  else
    for ( auto i = 0u; i < proxy.numDescendants(); ++i ) ids.emplace_back( proxy.descendant_unique_id( i ) );
}

// Create a vector with the unique IDs of a particle, including its own ID
template <class... P>
auto all_unique_ids( P const&... p ) {
  std::vector<LHCb::UniqueIDGenerator::ID<int_v>> ids;
  ids.reserve( ( num_unique_ids( p ) + ... ) );
  ( unroll_unique_ids( ids, p ), ... );
  return ids;
}

template <class... P, std::size_t... I>
auto all_unique_ids( proxy_pack<P...> const& proxies, std::index_sequence<I...> ) {
  return all_unique_ids( std::get<I>( proxies )... );
}

template <class... P>
auto all_unique_ids( proxy_pack<P...> const& proxies ) {
  return all_unique_ids( proxies, std::make_index_sequence<sizeof...( P )>() );
}

// Check whether two particles have an overlap of unique IDs
template <class Ref, class... P, std::size_t... I>
bool check_overlap_impl( Ref const& ref, proxy_pack<P...> const& proxies, std::index_sequence<I...> ) {
  return ( !none( ThOr::detail::Combiner::have_overlap( ref, std::get<I>( proxies ) ) ) || ... );
}

template <class Ref, class... P>
auto check_overlap( Ref const& ref, proxy_pack<P...> const& proxies ) {
  return check_overlap_impl( ref, proxies, std::make_index_sequence<sizeof...( P )>() );
}

// Create the array of indices (use some integers since this number is does not
// need to be robust/consistent for this test)
template <std::size_t... I>
auto child_indices( std::index_sequence<I...> ) {
  std::array<int_v, sizeof...( I )> result{int_v{int{I}}...};
  return result;
}

// Create the array of zip identifiers
template <class... P>
auto zip_identifiers( proxy_pack<P...> const& proxies ) {
  return std::apply( []( const auto&... p ) { return std::array{int_v{int( p.zipIdentifier() )}...}; }, proxies );
}

/*
  The following lines contains some basic types to iterate over zip containers
  and avoid processing duplicates. For example, if a combination is done using
  objects from the same zip container, it avoids processing the same combination
  twice e.g. (a[i], a[i + 1]) and (a[i + 1], a[i]). However, it allows to process
  pairs of elements of the same index twice to check that
  ThOr::detail::Combiner::have_overlap works.
 */

template <class Zip, class Accessors>
class basic_accessor;

template <class Zip, class Accessors>
class dependent_accessor;

template <class Zip, class Accessors>
using accessor = std::variant<basic_accessor<Zip, Accessors>, dependent_accessor<Zip, Accessors>>;

// Simple accessor to elements of a zip container
template <class Zip, class Accessors>
class basic_accessor {
public:
  basic_accessor() = default;
  basic_accessor( Accessors& a, Zip& z ) : m_accessors{&a}, m_zip{&z} {}
  auto current() const { return ( *m_zip )[m_index]; }
  auto index() const { return m_index; }
  void start() { m_index = 0; }
  bool at_end() const { return m_index < m_zip->size(); }
  void next() { ++m_index; }

private:
  Accessors*  m_accessors = nullptr;
  Zip*        m_zip       = nullptr;
  std::size_t m_index     = 0;
};

// Accessor to elements of a zip container where the initial index is determined
// by another accessor.
template <class Zip, class Accessors>
class dependent_accessor {
public:
  dependent_accessor() = default;
  dependent_accessor( std::size_t r, Accessors& a, Zip& z ) : m_reference{r}, m_accessors{&a}, m_zip{&z} {}
  auto current() const { return ( *m_zip )[m_index]; }
  auto index() const { return m_index; }
  void start() {
    // start at "index()" (instead of "index() + 1") to check that ThOr::detail::Combiner::have_overlap
    // works. The results are the same.
    m_index = std::visit( []( auto const& v ) { return v.index(); },
                          std::get<accessor<Zip, Accessors>>( ( *m_accessors )[m_reference] ) );
  }
  bool at_end() const { return m_index < m_zip->size(); }
  void next() { ++m_index; }

private:
  std::size_t m_reference = 0;
  Accessors*  m_accessors = nullptr;
  Zip*        m_zip       = nullptr;
  std::size_t m_index     = 0;
};

// Get the element at the given inded in the argument list
template <std::size_t I>
struct element_at_t {
  template <class T0, class... T>
  constexpr auto& operator()( T0&, T&... t ) const {
    static_assert( sizeof...( T ) > 0 );
    return element_at_t<I - 1>{}( t... );
  }
};

template <>
struct element_at_t<0> {
  template <class T0, class... T>
  constexpr auto& operator()( T0& t0, T&... ) const {
    return t0;
  }
};

template <std::size_t I, class... T>
constexpr auto& element_at( T&... t ) {
  return element_at_t<I>{}( t... );
}

// If Enable=true, extends the given tuple by adding the provided
// type T
template <bool Enable, class Tuple, class T>
struct extend_tuple_if;

template <class... P, class T>
struct extend_tuple_if<true, std::tuple<P...>, T> {
  using type = std::tuple<P..., T>;
};

template <class... P, class T>
struct extend_tuple_if<false, std::tuple<P...>, T> {
  using type = std::tuple<P...>;
};

template <bool Enable, class Tuple, class T>
using extend_tuple_if_t = typename extend_tuple_if<Enable, Tuple, T>::type;

// Proxy for the accessors to different zip containers
template <class Accessors, class... Zip>
using accessor_variant =
    boost::mp11::mp_assign<std::variant<>, boost::mp11::mp_unique<std::tuple<accessor<Zip, Accessors>...>>>;

// Functor to find the closest duplicate reference to that at the
// given index. The search is done from zero to the given index.
// Returns the same index if none is found
template <std::size_t I, std::size_t Stop>
struct find_previous_same_zip_t {
  template <class ZipRef, class... Zip>
  auto operator()( ZipRef const& ref, Zip const&... zip ) const {
    auto next = find_previous_same_zip_t<I + 1, Stop>{}( ref, zip... );
    if ( next == Stop && (void*)&ref == (void*)&element_at<I>( zip... ) )
      return I;
    else
      return next;
  }
};

template <std::size_t I>
struct find_previous_same_zip_t<I, I> {
  template <class ZipRef, class... Zip>
  auto operator()( ZipRef const&, Zip const&... ) const {
    return I;
  }
};

template <std::size_t I, class... Zip>
auto find_previous_same_zip( Zip const&... zip ) {
  return find_previous_same_zip_t<0, I>{}( element_at<I>( zip... ), zip... );
}

// Forward declaration
template <class... Zip>
class combination_accessors;

// Create an accessor to the zip at the given index
template <std::size_t I, class... Zip>
auto make_accessor( combination_accessors<Zip...>& comb, Zip&... zip ) {
  using zip_type                                 = boost::mp11::mp_at<std::tuple<Zip...>, boost::mp11::mp_size_t<I>>;
  using combination_type                         = combination_accessors<Zip...>;
  auto                                 reference = find_previous_same_zip<I>( zip... );
  accessor<zip_type, combination_type> result;
  if ( reference == I )
    result = basic_accessor<zip_type, combination_type>( comb, element_at<I>( zip... ) );
  else
    result = dependent_accessor<zip_type, combination_type>( reference, comb, element_at<I>( zip... ) );
  return result;
}

// Represent a tuple of accessors to zip containers, taking into account
// duplicated containers.
template <class... Zip>
class combination_accessors
    : public std::array<accessor_variant<combination_accessors<Zip...>, Zip...>, sizeof...( Zip )> {
public:
  using base_class_t = std::array<accessor_variant<combination_accessors<Zip...>, Zip...>, sizeof...( Zip )>;
  combination_accessors( Zip&... zip )
      : combination_accessors( std::make_index_sequence<sizeof...( Zip )>(), zip... ) {}

private:
  template <std::size_t... I>
  combination_accessors( std::index_sequence<I...>, Zip&... zip )
      : base_class_t{make_accessor<I>( *this, zip... )...} {}
};

template <class... Zip>
auto make_combination_accessors( Zip&... zip ) {
  return combination_accessors<Zip...>( zip... );
}

// Fill a LHCb::Event::Composites object from a set of zip containers
template <std::size_t I, std::size_t Stop>
struct make_composites_t {

  template <class... P, class... Z>
  void operator()( LHCb::Event::Composites& result, proxy_pack<P...> const& proxies,
                   combination_accessors<Z...>&& accessors ) const {

    using accessor_type =
        accessor<boost::mp11::mp_at<std::tuple<Z...>, boost::mp11::mp_size_t<I>>, combination_accessors<Z...>>;

    auto start = [&]() {
      return std::visit( []( auto& v ) { return v.start(); }, std::get<accessor_type>( accessors[I] ) );
    };
    auto at_end = [&]() {
      return std::visit( []( auto const& v ) { return v.at_end(); }, std::get<accessor_type>( accessors[I] ) );
    };
    auto next = [&]() {
      return std::visit( []( auto& v ) { return v.next(); }, std::get<accessor_type>( accessors[I] ) );
    };

    for ( start(); at_end(); next() ) {

      auto const new_proxy =
          std::visit( []( auto const& v ) { return v.current(); }, std::get<accessor_type>( accessors[I] ) );

      // avoid processing additional loops if we already detect an overlap
      if constexpr ( sizeof...( P ) > 0 ) {
        if ( check_overlap( new_proxy, proxies ) ) continue;
      }

      auto const all_proxies = extend_proxy_pack( proxies, std::move( new_proxy ) );

      make_composites_t<I + 1, Stop>{}( result, all_proxies, std::forward<decltype( accessors )>( accessors ) );
    }
  }
};

template <std::size_t I>
struct make_composites_t<I, I> {

  template <class... P, class... Z>
  void operator()( LHCb::Event::Composites& result, proxy_pack<P...> const& proxies,
                   combination_accessors<Z...>&& ) const {

    result.emplace_back<SIMDWrapper::InstructionSet::Scalar>(
        LHCb::LinAlg::Vec<float_v, 3>{},                             // position
        LHCb::LinAlg::Vec<float_v, 4>{},                             // 4-momenta
        int_v{0},                                                    // PID
        float_v{1.f},                                                // chi2
        int_v{1},                                                    // ndof
        LHCb::LinAlg::MatSym<float_v, 3>{},                          // covariance (position)
        LHCb::LinAlg::MatSym<float_v, 4>{},                          // covariance (4-momenta)
        LHCb::LinAlg::Mat<float_v, 4, 3>{},                          // covariance (position-momenta)
        child_indices( std::make_index_sequence<sizeof...( P )>() ), // child indices
        zip_identifiers( proxies ),                                  // child zip identifiers
        all_unique_ids( proxies )                                    // descendant unique IDs
    );
  }
};

// Create composites from zip containers representing particle objects
template <class... Z>
std::enable_if_t<( sizeof...( Z ) > 1 ), LHCb::Event::Composites>
make_composites( LHCb::UniqueIDGenerator const& unique_id_gen, Z const&... z ) {
  LHCb::Event::Composites result( unique_id_gen );
  make_composites_t<0, sizeof...( Z )>{}( result, proxy_pack<>{}, make_combination_accessors( z... ) );
  return result;
}

void test_case( const char* name, unsigned value, unsigned reference ) {
  std::cout << name << ": " << value << " (value), " << reference << " (reference)" << std::endl;
  BOOST_CHECK( value == reference );
}

// Create a set of particles
auto generate_particles( std::size_t n_elements, LHCb::UniqueIDGenerator const& unique_id_gen ) {

  // create some particles with random values
  auto zn = Zipping::generateZipIdentifier();

  auto tracks =
      std::make_unique<LHCb::Event::v3::Tracks>( LHCb::Event::v3::generate_tracks( n_elements, unique_id_gen, 1, zn ) );
  auto muon_pids = std::make_unique<LHCb::Event::v2::Muon::PIDs>( zn );
  muon_pids->reserve( n_elements );
  LHCb::Event::ChargedBasics charged_basics{tracks.get(), muon_pids.get()};
  charged_basics.reserve( n_elements );

  for ( auto i = 0u; i < n_elements; ++i ) {
    auto part = charged_basics.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    part.field<ChargedBasicsTag::RichPIDCode>().set( 0 );
    auto mu_pid = muon_pids->emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    part.field<ChargedBasicsTag::MuonPID>().set( mu_pid.offset() );
    mu_pid.field<LHCb::Event::v2::Muon::Tag::Status>().set( MuonFlags( 0 ) );
    mu_pid.field<LHCb::Event::v2::Muon::Tag::Chi2Corr>().set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::Mass>().set( 0 );
    part.field<ChargedBasicsTag::ParticleID>().set( 0 );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::p ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::e ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::pi ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::K ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::mu ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::d ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::Track>().set( i );
  }

  return std::make_tuple( std::move( charged_basics ), std::move( muon_pids ), std::move( tracks ) );
}

BOOST_AUTO_TEST_CASE( test_thor_combiner ) {

  // Must be a relatively small number, otherwise we might end-up with overflow
  // values. If we decide to change this value, there is a complex test case
  // below whose reference needs to be updated.
  std::size_t const n_elements = 10;

  auto gpart     = generate_particles( n_elements, unique_id_gen );
  auto particles = std::get<0>( gpart ).scalar();

  // alternative set of particles to check cross-combinations
  auto gpart_alt     = generate_particles( n_elements, unique_id_gen );
  auto particles_alt = std::get<0>( gpart_alt ).scalar();

  // Calculate N! / ((N - k)! * k!)
  auto combinations = []( unsigned N, unsigned k ) -> unsigned {
    assert( N >= k );

    if ( N == k ) return 1;

    unsigned num = N, den = k;
    for ( auto i = 1u; i < k; ++i ) {
      num *= ( N - i );
      den *= i;
    }

    return num / den;
  };

  /* Combine composites and particles together

     The combinations must always yield a number of particles equal to
     n! / ((n - k)! * k!) where n is the number of particles in the container
     and k the number that are combined. In the following "C" denotes a
     composite and "p" a particle. C(2p) corresponds to a composite made from
     two particles and C(C(2p), p) to a composite made from another composite
     and a particle.
  */

  // [p, p]
  auto C2p = make_composites( unique_id_gen, particles, particles );
  std::cout << "descendants 2: " << C2p.scalar()[0].numDescendants() << std::endl;
  test_case( "[p, p]", C2p.size(), combinations( particles.size(), 2 ) );
  auto zC2p = LHCb::Event::make_zip<SIMDWrapper::InstructionSet::Scalar>( C2p );

  // [p, p, p]
  auto C3p = make_composites( unique_id_gen, particles, particles, particles );
  std::cout << "descendants 3: " << C3p.scalar()[0].numDescendants() << std::endl;
  test_case( "[p, p, p]", C3p.size(), combinations( particles.size(), 3 ) );
  auto zC3p = LHCb::Event::make_zip<SIMDWrapper::InstructionSet::Scalar>( C3p );

  // [C(2p), p]
  auto C2p_p = make_composites( unique_id_gen, zC2p, particles );
  std::cout << "descendants 2-1: " << C2p_p.scalar()[0].numDescendants() << std::endl;
  test_case( "[C(2p), p]", C2p_p.size(), zC2p.size() * ( particles.size() - 2 ) );

  // [C(3p), p]
  auto C3p_p = make_composites( unique_id_gen, zC3p, particles );
  std::cout << "descendants 3-1: " << C3p_p.scalar()[0].numDescendants() << std::endl;
  test_case( "[C(3p), p]", C3p_p.size(), zC3p.size() * ( particles.size() - 3 ) );

  // [C(2p), C(2p)]
  auto C2p_C2p = make_composites( unique_id_gen, zC2p, zC2p );
  test_case( "[C(2p), C(2p)]", C2p_C2p.size(), 3 * combinations( particles.size(), 4 ) );

  // [C(2p), p, p]
  auto C2p_p_p = make_composites( unique_id_gen, zC2p, particles, particles );
  test_case( "[C(2p), p, p]", C2p_p_p.size(), 2 * C2p_C2p.size() );

  // [C(2p), C(2p), p]
  auto C2p_C2p_p = make_composites( unique_id_gen, zC2p, zC2p, particles );
  test_case( "[C(2p), C(2p), p]", C2p_C2p_p.size(), C2p_C2p.size() * ( particles.size() - 4 ) );
  auto zC2p_C2p_p = LHCb::Event::make_zip<SIMDWrapper::InstructionSet::Scalar>( C2p_C2p_p );

  /* TODO:
     Is there any formula to determine the hardcoded numbers? So far they
     have been determined by hand using combinations of "n_elements".
  */

  // [C(C(2p), C(2p), p), C(3p), p]
  auto C2p_C2p_p__C3p__p = make_composites( unique_id_gen, zC2p_C2p_p, zC3p, particles );
  test_case( "[C(C(2p), C(2p), p), C(3p), p]", C2p_C2p_p__C3p__p.size(), 75600 ); // reference

  // [p, p] (different containers)
  auto C2p_alt = make_composites( unique_id_gen, particles, particles_alt );
  std::cout << "descendants 2 (different containers): " << C2p_alt.scalar()[0].numDescendants() << std::endl;
  test_case( "[p, p] (different containers)", C2p_alt.size(), particles.size() * particles_alt.size() );
}

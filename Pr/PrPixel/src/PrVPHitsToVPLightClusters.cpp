/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <array>
#include <tuple>
#include <vector>

// Gaudi
#include "LHCbAlgs/Transformer.h"

// LHCb
#include "Detector/VP/VPChannelID.h"
#include "Event/PrHits.h"
#include "Event/VPLightCluster.h"
#include "Kernel/STLExtensions.h"
#include "PrKernel/VeloPixelInfo.h"

namespace LHCb::Pr::Velo {
  class PrVPHitsToVPLightClusters
      : public LHCb::Algorithm::MultiTransformer<std::tuple<
            std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>( const VP::Hits& )> {
  public:
    PrVPHitsToVPLightClusters( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator, {KeyValue{"HitsLocation", "Raw/VP/Hits"}},
                            {KeyValue{"ClusterLocation", VPClusterLocation::Light},
                             KeyValue{"ClusterOffsets", "Raw/VP/LightClustersOffsets"}} ) {}

    std::tuple<std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
    operator()( const VP::Hits& hits ) const override {
      auto result = std::tuple<std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>{};

      auto& [pool, offsets] = result;
      pool.reserve( hits.size() );

      for ( const auto hit : hits.scalar() ) {
        Detector::VPChannelID cid{static_cast<unsigned>( hit.template get<VP::VPHitsTag::ChannelId>().cast() )};
        float                 gx = hit.template get<VP::VPHitsTag::pos>().x().cast();
        float                 gy = hit.template get<VP::VPHitsTag::pos>().y().cast();
        float                 gz = hit.template get<VP::VPHitsTag::pos>().z().cast();
        pool.emplace_back( 1, 1, gx, gy, gz, cid );
        ++offsets[cid.module()];
      }

      std::partial_sum( offsets.begin(), offsets.end(), offsets.begin() );

      // sorting in phi for even modules
      auto cmp_phi_for_odd_modules = []( const VPLightCluster& a, const VPLightCluster& b ) {
        return ( a.y() < 0.f && b.y() > 0.f ) ||
               // same y side even and odd modules, check y1/x1 < y2/x2
               ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
      };

      // sorting in phi for odd modules
      auto cmp_phi_for_even_modules = []( const VPLightCluster& a, const VPLightCluster& b ) {
        return ( a.y() > 0.f && b.y() < 0.f ) ||
               // same y side even and odd modules, check y1/x1 < y2/x2
               ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
      };

      auto sort_module = [pool = std::ref( pool ), offsets = std::ref( offsets )]( auto id, auto cmp ) {
        std::sort( pool.get().begin() + offsets.get()[id], pool.get().begin() + offsets.get()[id + 1], cmp );
      };

      for ( size_t moduleID = 0; moduleID < VeloInfo::Numbers::NModules; ++moduleID ) {
        if ( moduleID % 2 == 1 ) {
          sort_module( moduleID, cmp_phi_for_odd_modules );
        } else {
          sort_module( moduleID, cmp_phi_for_even_modules );
        }
      }

      m_nbClustersCounter += pool.size();
      return result;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbClustersCounter{this, "Nb of Produced Clusters"};
  };
} // namespace LHCb::Pr::Velo

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::PrVPHitsToVPLightClusters, "PrVPHitsToVPLightClusters" )

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRKERNEL_PRHIT_H
#define PRKERNEL_PRHIT_H 1

// Include files
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"

/** @class PrHit PrHit.h PrKernel/PrHit.h
 *  Hits to be used in the pattern in the T/TT stations
 *
 *  @author Olivier Callot
 *  @date   2012-03-13
 *  @author Thomas Nikodem
 *  @date   2016-04-11
 */

// struct to capsul modifiable information
struct ModPrHit final {
  ModPrHit() = default;
  ModPrHit( float c, size_t i ) : coord{c}, fullDex{i} {}
  float  coord   = 0.f;
  size_t fullDex = 0;
  bool   isValid() const {
    return coord > -std::numeric_limits<float>::max();
  }; //---LoH: crucial for the logic of the HybridSeeding that it is a large negative
  void setInvalid() {
    coord = -std::numeric_limits<float>::max();
  }; //---LoH: crucial for the logic of the HybridSeeding that it is a large negative
  friend bool operator==( const ModPrHit& lhs, const ModPrHit& rhs ) {
    return lhs.fullDex == rhs.fullDex;
  } // FIXME: this is _not_ equality, but (at best) equivalence... one can have ( a==b &&  a<b ) evaluate to true --
    // which is confusing!
  friend bool operator<( const ModPrHit& lhs, const ModPrHit& rhs ) { return lhs.coord < rhs.coord; }
};
using ModPrHits            = std::vector<ModPrHit, LHCb::Allocators::EventLocal<ModPrHit>>;
using ModPrHitIter         = ModPrHits::iterator;
using ModPrHitConstIter    = ModPrHits::const_iterator;
using ModPrHitConstRevIter = ModPrHits::const_reverse_iterator;

#endif // PRKERNEL_PRHIT_H

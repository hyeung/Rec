/*******************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#include "Phoenix/Store.h"

#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloDigit.h"
#include "Event/CaloDigits_v2.h"
#include "Event/ODIN.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiKernel/Point3DTypes.h"

#include <string>

namespace {
  using CaloDigit    = LHCb::Event::Calo::v2::Digit;
  using CaloV2Digits = LHCb::Event::Calo::v2::Digits;
  using json         = nlohmann::json;

  struct DigitDet {
    CaloDigit const&     digit;
    DeCalorimeter const& det;
    std::string const&   color;
  };
  struct DigitsDet {
    CaloV2Digits const&  digits;
    DeCalorimeter const& det;
    std::string const&   color;
  };
} // namespace

namespace nlohmann {

  /// converter of XYZPoint to json
  void to_json( json& j, Gaudi::XYZPoint const& point ) { j = {point.x(), point.y()}; }

  /// converter of DigitDet to json
  void to_json( json& j, DigitDet const& input ) {
    j = {{"pos", input.det.cellCenter( input.digit.cellID() )},
         {"energy", input.digit.energy()},
         {"cellID", input.digit.cellID().all()},
         {"cellSize", input.det.cellSize( input.digit.cellID() )},
         {"color", input.color}};
  }

  /// converter of DigitsDet to json
  void to_json( json& j, DigitsDet const& input ) {
    j = json::array();
    for ( const auto& d : input.digits ) { j.push_back( DigitDet{d, input.det, input.color} ); }
  }
} // namespace nlohmann

/**
 * Get ECal, HCal hits data from the Event Model and dump them into a .json file with the particular file form.
 * To be used in the LHCb web event display or the phoenix framework.
 *
 * See detailed documentation for the project's purpose in:
 * LHCb/Vis/Phoenix/
 *
 * @author Andreas PAPPAS
 * @date 08-09-2021
 */
namespace LHCb::Phoenix {
  class DumpPlanarCaloCellsToJson final
      : public Algorithm::Consumer<void( CaloV2Digits const&, CaloV2Digits const&, ODIN const&, DeCalorimeter const&,
                                         DeCalorimeter const& ),
                                   LHCb::DetDesc::usesConditions<DeCalorimeter>> {
  public:
    DumpPlanarCaloCellsToJson( const std::string& name, ISvcLocator* pSvcLocator );
    void operator()( CaloV2Digits const&, CaloV2Digits const&, ODIN const&, DeCalorimeter const&,
                     DeCalorimeter const& ) const override;

  private:
    mutable Store                             m_storePl{this, "Calo_store", "Phoenix:Calo"};
    Gaudi::Property<std::vector<std::string>> colors{
        this,
        "Colors",
        {"#8FF0A4", "#FFBE6F"},
        "Colors to be used for the different calo deposits as a vector of strings with format '#rrggbb'"};
  };
} // namespace LHCb::Phoenix

LHCb::Phoenix::DumpPlanarCaloCellsToJson::DumpPlanarCaloCellsToJson( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"CaloEDigits", CaloDigitLocation::Ecal}, KeyValue{"CaloHDigits", CaloDigitLocation::Hcal},
                 KeyValue{"ODIN", LHCb::ODINLocation::Default}, KeyValue{"DetectorEcal", DeCalorimeterLocation::Ecal},
                 KeyValue{"DetectorHcal", DeCalorimeterLocation::Hcal}} ) {}

void LHCb::Phoenix::DumpPlanarCaloCellsToJson::operator()( CaloV2Digits const& calo_Edig, CaloV2Digits const& calo_Hdig,
                                                           ODIN const& odin, DeCalorimeter const& det_E,
                                                           DeCalorimeter const& det_H ) const {
  m_storePl.storeEventData(
      {{"gps time", odin.gpsTime()},
       {"run number", odin.runNumber()},
       {"event number", odin.eventNumber()},
       {"bunch crossing type", odin.bunchCrossingType()},
       {"Content",
        {{"PlanarCaloCells",
          {{"HCal",
            {{"plane", {0., 0., 1, det_H.toGlobal( Gaudi::XYZPoint{0, 0, 0} ).z()}}, // this one gives the
                                                                                     // z point of the
                                                                                     // plane of the
                                                                                     // calorimeter
             {"cells", DigitsDet{calo_Hdig, det_H, colors[1]}}}},
           {"ECal",
            {{"plane", {0., 0., 1, det_E.toGlobal( Gaudi::XYZPoint{0, 0, 0} ).z()}},
             {"cells", DigitsDet{calo_Edig, det_E, colors[0]}}}}}}}}} );
}

DECLARE_COMPONENT( LHCb::Phoenix::DumpPlanarCaloCellsToJson )

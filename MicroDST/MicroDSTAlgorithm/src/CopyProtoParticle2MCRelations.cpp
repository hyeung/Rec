/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "ICloneMCParticle.h"
#include "Kernel/Particle2MCParticle.h"
#include "Kernel/STLExtensions.h"
#include "MicroDSTCommon.h"
#include "boost/algorithm/string.hpp"
#include <GaudiAlg/GaudiAlgorithm.h>
#include <functional>

typedef LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double> PP2MCPTable;

//=============================================================================

/** @namespace MicroDST RelTableFunctors.h MicroDST/RelTableFunctors.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2009-04-17
 */
namespace {

  template <class TABLE>
  class TableCloner {
    typedef std::function<typename TABLE::From( typename TABLE::From )> From;
    typedef std::function<typename TABLE::To( typename TABLE::To )>     To;

    From m_fromCloner;
    To   m_toCloner;

  public:
    TableCloner( From fromCloner, To toCloner )
        : m_fromCloner( std::move( fromCloner ) ), m_toCloner( std::move( toCloner ) ) {}
    typename TABLE::Entry operator()( const typename TABLE::Entry& entry ) const {
      static_assert( TABLE::weighted );
      return {m_fromCloner( entry.from() ), m_toCloner( entry.to() ), entry.weight()};
    }
    std::unique_ptr<TABLE> operator()( const TABLE* table ) const {
      auto cloneTable = std::make_unique<TABLE>();
      cloneTable->setVersion( table->version() );
      for ( const auto& rel : table->relations() ) {
        auto entryClone = ( *this )( rel );
        if ( entryClone.from() && entryClone.to() ) cloneTable->add( std::move( entryClone ) ).ignore();
      } // loop on all relations
      return cloneTable;
    }
  };
} // namespace

namespace MicroDST {
  /** @class RelationsClonerAlg RelationsClonerAlg.h MicroDST/RelationsClonerAlg.h
   *
   *  Templated algorithm to clone a relations table between Froms and Tos. The Froms
   *  are assumed to be cloner already. The Tos are cloned, unless the ClonerType
   *  property is set to "NONE", in which case the cloner table points to the original
   *  Tos.
   *
   *  @author Juan PALACIOS juan.palacios@nikhef.nl
   *  @date   2009-04-14
   */

  class CopyProtoParticle2MCRelations : public MicroDSTCommon<GaudiAlgorithm> {

  public:
    //===========================================================================
    /// Standard constructor
    CopyProtoParticle2MCRelations( const std::string& name, ISvcLocator* pSvcLocator )
        : MicroDSTCommon( name, pSvcLocator ) {
      declareProperty( "ClonerType", m_clonerType );
      declareProperty( "UseOriginalFrom", m_useOriginalFrom, "Take 'from' object from original location" );
    }

    //===========================================================================

    StatusCode initialize() override {
      StatusCode sc = MicroDSTCommon::initialize();
      if ( sc.isFailure() ) return sc;
      if ( !m_inputTESLocations.empty() && !m_inputTESLocation.empty() ) {
        return Error( "You have set both InputLocation AND InputLocations properties" );
      }
      if ( m_inputTESLocations.empty() ) { setInputTESLocation( m_inputTESLocation ); }
      if ( inputTESLocations().empty() ) { setInputTESLocation( "NO DEFAULT LOCATION" ); }
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "inputTESLocation() is " << inputTESLocation() << endmsg;
      if ( m_clonerType.empty() || m_clonerType == "NONE" ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "ClonerType set to NONE. No cloning of To performed." << endmsg;
      } else {
        m_cloner = tool<ICloneMCParticle>( m_clonerType, this );
        if ( !m_cloner ) return Error( "Failed to find cloner " + m_clonerType );
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Found cloner " << m_clonerType << endmsg;
      }
      return sc;
    }

    //=========================================================================

    StatusCode execute() override {
      setFilterPassed( true );
      for ( const auto& loc : inputTESLocations() ) { copyTableFromLocation( loc ); }
      return StatusCode::SUCCESS;
    }

    //=========================================================================

  private:
    void copyTableFromLocation( const std::string& inputLocation ) const {

      const std::string outputLocation = outputTESLocation( inputLocation );

      if ( exist<PP2MCPTable>( outputLocation ) ) {
        Warning( "Object " + outputLocation + " already exists. Not cloning.", StatusCode::SUCCESS, 0 ).ignore();
        return;
      }

      const PP2MCPTable* table = getIfExists<PP2MCPTable>( inputLocation );
      if ( table && !table->relations().empty() ) {

        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Going to clone " << table->relations().size() << " relations from " << inputLocation << " into "
                    << outputLocation << endmsg;
        }

        // Note makes a new object !!
        auto cloneTable = m_tableCloner( table );

        // Print
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Cloned Table :- " << endmsg;
          for ( const auto& rel : cloneTable->relations() ) {
            verbose() << "  From : " << *rel.from() << " To : " << *rel.to() << endmsg;
          }
        }

        // Save if not empty
        if ( !cloneTable->relations().empty() ) { put( cloneTable.release(), outputLocation ); }

      } else {
        if ( msgLevel( MSG::VERBOSE ) ) {
          Warning( "Found no table at " + inputLocation, StatusCode::FAILURE, 0 ).ignore();
        }
      }
    }

    //===========================================================================

  private:
    const LHCb::ProtoParticle* cloneFrom( const LHCb::ProtoParticle* from ) {
      if ( m_useOriginalFrom ) { return from; }
      if ( !from ) {
        error() << "From is nullptr. Cannot clone!" << endmsg;
        return nullptr;
      }
      if ( !from->parent() ) {
        Warning( "From is not in TES. Cannot clone!", StatusCode::FAILURE, 0 ).ignore();
        return nullptr;
      }
      return getStoredClone<LHCb::ProtoParticle>( from );
    }

    const LHCb::MCParticle* cloneTo( const LHCb::MCParticle* to ) {
      if ( !m_cloner ) { return to; }
      if ( !to ) {
        error() << "To is nullptr. Cannot clone!" << endmsg;
        return nullptr;
      }

      if ( !to->parent() ) {
        Warning( "To is not in TES. Cannot clone!", StatusCode::FAILURE, 0 ).ignore();
        return nullptr;
      }

      const LHCb::MCParticle* storedTo = getStoredClone<LHCb::MCParticle>( to );
      return ( storedTo ? storedTo : ( *m_cloner )( to ) );
    }

    //===========================================================================
    const std::string& inputTESLocation() const {
      return m_inputTESLocations.empty() ? m_inputTESLocation.value() : m_inputTESLocations[0];
    }

    const std::vector<std::string>& inputTESLocations() const { return m_inputTESLocations; }

    void setInputTESLocation( std::string newLocation ) {
      m_inputTESLocations.clear();
      m_inputTESLocations.value().push_back( std::move( newLocation ) );
    }

  private:
    ICloneMCParticle*        m_cloner          = nullptr;
    std::string              m_clonerType      = "MCParticleCloner";
    bool                     m_useOriginalFrom = false;
    TableCloner<PP2MCPTable> m_tableCloner = {LHCb::cxx::bind_front( &CopyProtoParticle2MCRelations::cloneFrom, this ),
                                              LHCb::cxx::bind_front( &CopyProtoParticle2MCRelations::cloneTo, this )};
    Gaudi::Property<std::string>              m_inputTESLocation{this, "InputLocation", ""};
    Gaudi::Property<std::vector<std::string>> m_inputTESLocations{this, "InputLocations", {}};
  };

  DECLARE_COMPONENT_WITH_ID( CopyProtoParticle2MCRelations, "CopyProtoParticle2MCRelations" )

} // namespace MicroDST

//=============================================================================
